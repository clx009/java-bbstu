INSERT INTO bp_minidao(id, age, birthday, content, dep_id, email, mobile_phone, office_phone, salary, sex, user_name)
VALUES(
	:bpMinidao.id, 
	:bpMinidao.age, 
	:bpMinidao.birthday, 
	:bpMinidao.content, 
	:bpMinidao.depId, 
	:bpMinidao.email, 
	:bpMinidao.mobilePhone, 
	:bpMinidao.officePhone, 
	:bpMinidao.salary, 
	:bpMinidao.sex, 
	:bpMinidao.userName
)