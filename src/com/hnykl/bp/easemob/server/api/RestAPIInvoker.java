package com.hnykl.bp.easemob.server.api;

import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.HeaderWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.QueryWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;

import java.io.File;

public interface RestAPIInvoker {
	ResponseWrapper sendRequest(String method, String url, HeaderWrapper header, BodyWrapper body, QueryWrapper query);
	ResponseWrapper uploadFile(String url, HeaderWrapper header, File file);
    ResponseWrapper downloadFile(String url, HeaderWrapper header, QueryWrapper query);
}
