package com.hnykl.bp.easemob.server.api;

public interface AuthTokenAPI{	
	Object getAuthToken(String clientId, String clientSecret);
}
