package com.hnykl.bp.easemob.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.hnykl.bp.easemob.server.api.IMUserAPI;
import com.hnykl.bp.easemob.server.api.SendMessageAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.CmdMessageBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;

public class MessageService {
	private static final Logger log = LoggerFactory
			.getLogger(MessageService.class);

	static EasemobRestAPIFactory factory = ClientContext.getInstance()
			.init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();

	static IMUserAPI userAPI = (IMUserAPI) factory
			.newInstance(EasemobRestAPIFactory.USER_CLASS);
	static SendMessageAPI sendMessageAPI = (SendMessageAPI) factory
			.newInstance(EasemobRestAPIFactory.SEND_MESSAGE_CLASS);

	public static void sendMessageToAll(String from, long limit, String cursor,
			Map<String, String> ext) {
		ResponseWrapper imUsersBatch = (ResponseWrapper) userAPI
				.getIMUsersBatch(20L, null);
		Object responseBody = imUsersBatch.getResponseBody();
		JSONObject jsonObject = JSONObject.fromObject(responseBody.toString());
		try {
			cursor = jsonObject.getString("cursor");
		} catch (Exception e) {
			cursor = null;
		}
		Object object = jsonObject.getString("entities");
		JSONArray jsonArray = JSONArray.fromObject(object.toString());
		Iterator<?> it = jsonArray.iterator();
		List<String> users = new ArrayList<String>();
		while (it.hasNext()) {
			Object next = it.next();
			JSONObject jo = JSONObject.fromObject(next.toString());
			String username = (String) jo.getString("username"); 
			users.add(username);
		}
		String[] userArray = new String[users.size()];
		for(int i=0;i<users.size();i++){
			userArray[i] = users.get(i);
		}
		BodyWrapper cmdMessageBody = new CmdMessageBody("users", userArray,
				null, ext, "system message");

		ResponseWrapper sendMessage = (ResponseWrapper) sendMessageAPI
				.sendMessage(cmdMessageBody);
		log.debug("批量发送消息给所有人结果：" + sendMessage.getResponseBody().toString());
		if (cursor != null && !"".equals(cursor)) {// 递归，如果有游标则继续查找用户并发送消息
			sendMessageToAll(from, 20L, cursor, ext);
		}
	}

}
