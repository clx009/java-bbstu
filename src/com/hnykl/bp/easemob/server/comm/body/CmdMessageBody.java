package com.hnykl.bp.easemob.server.comm.body;

import com.hnykl.bp.easemob.server.comm.constant.MsgType;
import com.fasterxml.jackson.databind.node.ContainerNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class CmdMessageBody extends MessageBody {
	private String action;

	public CmdMessageBody(String targetType, String[] targets, String from, Map<String, String> ext, String action) {
		super(targetType, targets, from, ext);
		this.setAction(action);
	}

    public ContainerNode<?> getBody() {
        if(!isInit()){
        	ObjectNode msgBody = JsonNodeFactory.instance.objectNode();
        	msgBody.put("type", MsgType.CMD);
        	msgBody.put("action", action); 
            this.getMsgBody().put("msg", msgBody);
            this.setInit(true);
        }

        return this.getMsgBody();
    }

    public Boolean validate() {
		return super.validate() && StringUtils.isNotBlank(action);
	} 

	public String getAction() {
		return action;
	} 

	public void setAction(String action) {
		this.action = action;
	}
}
