package com.hnykl.bp.web.family.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hnykl.bp.web.position.entity.CommonAddressEntity;

public class FamilyUserDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;// 用户id
	private String memberId;
	private String userType;// 用户类型
	private String memeberType;// 成员类型
	private String nickname;// 用户昵称
	private String longitude;// 经度
	private String latitude;// 纬度
	private String headPortraitUrl;//头像路径
	private Date lastUploadTime;
	private String timeZone;
	private String addrLatitude;
	private String addrLongitude;
	private String address;
	private String positionUploadTime;
	public String getPositionUploadTime() {
		return positionUploadTime;
	}

	public void setPositionUploadTime(String positionUploadTime) {
		this.positionUploadTime = positionUploadTime;
	}

	private List<CommonAddressEntity>  commonAddressEntityList = new ArrayList<CommonAddressEntity>();

	public List<CommonAddressEntity> getCommonAddressEntityList() {
		return commonAddressEntityList;
	}

	public void setCommonAddressEntityList(
			List<CommonAddressEntity> commonAddressEntityList) {
		this.commonAddressEntityList = commonAddressEntityList;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddrLatitude() {
		return addrLatitude;
	}

	public void setAddrLatitude(String addrLatitude) {
		this.addrLatitude = addrLatitude;
	}

	public String getAddrLongitude() {
		return addrLongitude;
	}

	public void setAddrLongitude(String addrLongitude) {
		this.addrLongitude = addrLongitude;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getMemeberType() {
		return memeberType;
	}

	public void setMemeberType(String memeberType) {
		this.memeberType = memeberType;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getHeadPortraitUrl() {
		return headPortraitUrl;
	}

	public void setHeadPortraitUrl(String headPortraitUrl) {
		this.headPortraitUrl = headPortraitUrl;
	}

	public Date getLastUploadTime() {
		return lastUploadTime;
	}

	public void setLastUploadTime(Date lastUploadTime) {
		this.lastUploadTime = lastUploadTime;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
