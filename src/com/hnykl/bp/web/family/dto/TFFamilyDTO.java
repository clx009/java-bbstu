package com.hnykl.bp.web.family.dto; 
import java.sql.Timestamp;

public class TFFamilyDTO implements java.io.Serializable { 
	private String id;
	private static final long serialVersionUID = 1L;
	/**name*/
	private java.lang.String name;
	/**code*/
	private java.lang.String code;
	/**creatorId*/
	private java.lang.String creatorId;
	/**createTime*/
	private Timestamp createTime;
	/**status*/
	private java.lang.String status; 
	/**环信对接群组id**/
	private String groupId;
	private String address;
	private String latitude;
	private String longitude;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getCode() {
		return code;
	}

	public void setCode(java.lang.String code) {
		this.code = code;
	}

	public java.lang.String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(java.lang.String creatorId) {
		this.creatorId = creatorId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public java.lang.String getStatus() {
		return status;
	}

	public void setStatus(java.lang.String status) {
		this.status = status;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
