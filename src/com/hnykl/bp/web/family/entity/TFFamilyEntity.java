package com.hnykl.bp.web.family.entity; 
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity; 
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table; 

import com.hnykl.bp.base.core.common.entity.IdEntity;

/**   
 * @Title: Entity
 * @Description: t_f_family
 * @author onlineGenerator
 * @date 2016-05-24 18:10:19
 * @version V1.0   
 *
 */ 
@Entity
@Table(name = "t_f_family")
@PrimaryKeyJoinColumn(name = "id")
public class TFFamilyEntity extends IdEntity implements java.io.Serializable { 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**name*/
	private java.lang.String name;
	/**code*/
	private java.lang.String code;
	/**creatorId*/
	private java.lang.String creatorId;
	/**createTime*/
	private Timestamp createTime;
	/**status*/
	private java.lang.String status; 
	/**环信对接群组id**/
	private String groupId;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  name
	 */
	@Column(name ="name")
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  name
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  code
	 */
	@Column(name ="code")
	public java.lang.String getCode(){
		return this.code;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  code
	 */
	public void setCode(java.lang.String code){
		this.code = code;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  creatorId
	 */
	@Column(name ="creator_id")
	public java.lang.String getCreatorId(){
		return this.creatorId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  creatorId
	 */
	public void setCreatorId(java.lang.String creatorId){
		this.creatorId = creatorId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  createTime
	 */
	@Column(name ="create_time")
	public Timestamp getCreateTime(){
		return this.createTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  createTime
	 */
	public void setCreateTime(Timestamp createTime){
		this.createTime = createTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  status
	 */
	@Column(name ="status")
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  status
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}

	/**
	 * 
	 * */
	@Column(name="group_id")
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
}
