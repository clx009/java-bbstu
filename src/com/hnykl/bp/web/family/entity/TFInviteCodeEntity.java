package com.hnykl.bp.web.family.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.hnykl.bp.base.core.common.entity.IdEntity;

@Entity
@Table(name = "t_f_invite_code")
@PrimaryKeyJoinColumn(name = "id")
public class TFInviteCodeEntity extends IdEntity{
	private String familyId;
	private Timestamp createTime;
	private String inviterId;
	private String inviteType;
	private String inviteCode;
	
	@Column(name="family_id")
	public String getFamilyId() {
		return familyId;
	}
	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}
	
	@Column(name ="create_time")
	public Timestamp getCreateTime() {
		return createTime;
	}
	
	
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	@Column(name="inviter_id")
	public String getInviterId() {
		return inviterId;
	}
	public void setInviterId(String inviterId) {
		this.inviterId = inviterId;
	}
	
	@Column(name="invite_type")
	public String getInviteType() {
		return inviteType;
	}
	public void setInviteType(String inviteType) {
		this.inviteType = inviteType;
	}
	
	@Column(name="invite_code")
	public String getInviteCode() {
		return inviteCode;
	}
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}
	
	
}
