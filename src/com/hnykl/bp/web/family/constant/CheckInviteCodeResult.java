package com.hnykl.bp.web.family.constant;

public enum CheckInviteCodeResult {
	
	SUCCESS(0,"验证成功"),NOT_EXSITS(1,"邀请码不存在"),TIME_OUT(2,"邀请码过时了"),FAMILY_NOT_EXSITS(3,"家人圈已不存在");
	
	int code;
	
	String message;
	
	CheckInviteCodeResult(int code,String message){
		this.code = code;
		this.message = message;
	}
	
	public String toString(){
		return code+" : "+message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
