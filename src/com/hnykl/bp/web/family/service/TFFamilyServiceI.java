package com.hnykl.bp.web.family.service;

import java.util.List;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.family.dto.FamilyUserDTO;
import com.hnykl.bp.web.family.dto.TFFamilyDTO;
import com.hnykl.bp.web.family.entity.TFFamilyEntity;

public interface TFFamilyServiceI extends CommonService { 

	/**
	 * 默认按钮-sql增强-新增操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doAddSql(TFFamilyEntity t);

	/**
	 * 默认按钮-sql增强-更新操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doUpdateSql(TFFamilyEntity t);

	/**
	 * 默认按钮-sql增强-删除操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doDelSql(TFFamilyEntity t);

	public void createDefaultFamily(TFFamilyEntity entity);

	public List<FamilyUserDTO> findFamilyMembers(String familyId);

	public List<TFFamilyEntity> findFamily(String userId);
}
