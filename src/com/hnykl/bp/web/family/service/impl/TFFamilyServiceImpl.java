package com.hnykl.bp.web.family.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.bean.BeanUtils;
import com.hnykl.bp.web.family.dto.FamilyUserDTO;
import com.hnykl.bp.web.family.dto.TFFamilyDTO;
import com.hnykl.bp.web.family.entity.TFFamilyEntity;
import com.hnykl.bp.web.family.entity.TFMemberEntity;
import com.hnykl.bp.web.family.service.TFFamilyServiceI;
import com.hnykl.bp.web.family.service.TFMemberServiceI;

@Service("tFFamilyService")
@Transactional
public class TFFamilyServiceImpl extends CommonServiceImpl implements
		TFFamilyServiceI {
	
	@Autowired
	TFMemberServiceI tFMemberService;

	public <T> void delete(T entity) {
		super.delete(entity);
		// 执行删除操作配置的sql增强
		this.doDelSql((TFFamilyEntity) entity);
	}

	public <T> Serializable save(T entity) {
		Serializable t = super.save(entity);
		// 执行新增操作配置的sql增强
		this.doAddSql((TFFamilyEntity) entity);
		return t;
	}

	public <T> void saveOrUpdate(T entity) {
		super.saveOrUpdate(entity);
		// 执行更新操作配置的sql增强
		this.doUpdateSql((TFFamilyEntity) entity);
	}

	/**
	 * 默认按钮-sql增强-新增操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doAddSql(TFFamilyEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-更新操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doUpdateSql(TFFamilyEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-删除操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doDelSql(TFFamilyEntity t) {
		return true;
	}

	/**
	 * 替换sql中的变量
	 * 
	 * @param sql
	 * @return
	 */
	public String replaceVal(String sql, TFFamilyEntity t) {
		sql = sql.replace("#{id}", String.valueOf(t.getId()));
		sql = sql.replace("#{name}", String.valueOf(t.getName()));
		sql = sql.replace("#{code}", String.valueOf(t.getCode()));
		sql = sql.replace("#{creator_id}", String.valueOf(t.getCreatorId()));
		sql = sql.replace("#{create_time}", String.valueOf(t.getCreateTime()));
		sql = sql.replace("#{status}", String.valueOf(t.getStatus()));
		sql = sql.replace("#{UUID}", UUID.randomUUID().toString());
		return sql;
	}

	@Override
	public void createDefaultFamily(TFFamilyEntity entity) {
		// TODO Auto-generated method stub
		this.save(entity);
		TFMemberEntity member = new TFMemberEntity();
		member.setType("1");
		member.setUserId(entity.getCreatorId());
		member.setFamilyId(entity.getId());
		tFMemberService.save(member);

	}

	@Override
	public List<FamilyUserDTO> findFamilyMembers(String familyId) {
		// TODO Auto-generated method stub
		
		String fileService = BaseConfigMgr.getWebVisitUrl();
		
		String queryFamilyMembers = " select m.user_id userId,m.id memberId,bu.TYPE userType,m.type memeberType,concat('"+fileService+"',u.head_portrait_url) headPortraitUrl,u.nickname,u.last_longitude longitude,u.last_latitude latitude,u.last_position_time_zone timeZone,u.position_upload_time positionUploadTime  from t_f_family_user m,t_s_user u,t_s_base_user bu "
				+ "  where m.user_id=u.id and u.id = bu.id and m.family_id = :familyId ";
		
		Query familyMembersQuery = getSession().createSQLQuery(
				queryFamilyMembers).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		familyMembersQuery.setParameter("familyId", familyId);
		List<Map> familys = familyMembersQuery.list();
		
		Map dataMap = new HashMap();
		FamilyUserDTO  familyUserDTO = null;
		List<FamilyUserDTO> dataList = new ArrayList<FamilyUserDTO>();
		for(int i=0;familys != null && i<familys.size();i++){
			familyUserDTO = new FamilyUserDTO();
			dataMap = familys.get(i);
			BeanUtils.populate(familyUserDTO, dataMap);
			dataList.add(familyUserDTO);
		}
		return dataList;
	}

	@Override
	public List<TFFamilyEntity> findFamily(String userId) {
		String query = " select f.* from t_f_family f ,(select tffu.*  from t_f_family_user tffu  where tffu.user_id = :userId) u  where u.family_id = f.id ";
		SQLQuery queryObject = getSession().createSQLQuery(query);
		queryObject.setParameter("userId", userId); 
		queryObject.addEntity(TFFamilyEntity.class);
		List<TFFamilyEntity> familys = queryObject.list(); 
		return familys;
	}
}