package com.hnykl.bp.web.family.service.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.family.entity.TFMemberEntity;
import com.hnykl.bp.web.family.service.TFMemberServiceI;

@Service("tFMemberService")
@Transactional
public class TFMemberServiceImpl extends CommonServiceImpl implements
		TFMemberServiceI {

	@Override
	public boolean checkMemberExsits(String familyId, String userId) {
		String query = "from TFMemberEntity m where m.familyId=:familyId and m.userId = :userId ";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("familyId", familyId);
		queryObject.setParameter("userId", userId);
		@SuppressWarnings("unchecked")
		List<TFMemberEntity> lst = queryObject.list();
		if (lst.isEmpty())
			return true;
		else
			return false;
	}

	@Override
	public List<Map> findAdultMember(String familyId) {
		String query = "select u.id,u.username from t_s_base_user u,t_f_family_user fu where u.id = fu.user_id and family_id=:familyId and fu.type<>'2'";
		Query queryObject = getSession().createSQLQuery(query).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		queryObject.setParameter("familyId", familyId);  
		@SuppressWarnings("unchecked")
		List<Map> lst = queryObject.list(); 
		return lst; 
	}
	public List<Map> findAdultMemberByUserId(String userId) {
		String query = "select u.id userId,u.username from t_s_base_user u,t_f_family_user fu where u.id = fu.user_id and u.id=:userId ";
		Query queryObject = getSession().createSQLQuery(query).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		queryObject.setParameter("userId", userId);  
		@SuppressWarnings("unchecked")
		List<Map> lst = queryObject.list(); 
		return lst; 
	}
}
