package com.hnykl.bp.web.family.service.impl;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.family.constant.CheckInviteCodeResult;
import com.hnykl.bp.web.family.entity.TFInviteCodeEntity;
import com.hnykl.bp.web.family.service.TFInviteCodeServiceI;
import com.hnykl.bp.web.system.pojo.base.TSPhoneValidateCode;

@Service("tFInviteCodeService")
@Transactional
public class TFInviteCodeServiceImpl extends CommonServiceImpl implements TFInviteCodeServiceI{

	@Override
	public CheckInviteCodeResult checkInviteCode(TFInviteCodeEntity entity) {
		
		// TODO Auto-generated method stub
		String query = "from TFInviteCodeEntity v where v.inviteCode=:inviteCode ";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("inviteCode", entity.getInviteCode());  
		
		List<TFInviteCodeEntity> lst = queryObject.list();

		if (lst.isEmpty()) {
			return CheckInviteCodeResult.NOT_EXSITS;
		} else {
			TFInviteCodeEntity obj = lst.get(0); 
			Timestamp createTime=obj.getCreateTime();
			long btTime = System.currentTimeMillis() - createTime.getTime();
			long maxtime = BaseConfigMgr.getUserInviteCodeMaxtime()*60*1000;
			if (btTime > maxtime) {
				return CheckInviteCodeResult.TIME_OUT;
			} else {
				return CheckInviteCodeResult.SUCCESS;
			}
		} 
	}

}
