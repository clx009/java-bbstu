package com.hnykl.bp.web.family.service;

import java.util.List;
import java.util.Map;

import com.hnykl.bp.base.core.common.service.CommonService;

public interface TFMemberServiceI extends CommonService{

	boolean checkMemberExsits(String familyId, String userId);

	List<Map> findAdultMember(String familyId);
	public List<Map> findAdultMemberByUserId(String userId) ;
}
