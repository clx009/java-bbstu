package com.hnykl.bp.web.family.service;

import java.io.Serializable;
 

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.family.constant.CheckInviteCodeResult;
import com.hnykl.bp.web.family.entity.TFInviteCodeEntity;

public interface TFInviteCodeServiceI extends CommonService {
	public <T> void delete(T entity);

	public <T> Serializable save(T entity);

	public <T> void saveOrUpdate(T entity);
	
	
	public CheckInviteCodeResult checkInviteCode(TFInviteCodeEntity entity);
}
