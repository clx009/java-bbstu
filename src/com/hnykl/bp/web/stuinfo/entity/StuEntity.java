package com.hnykl.bp.web.stuinfo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.hnykl.bp.web.system.pojo.base.TSBaseUser;

/**   
 * @Title: Entity
 * @Description: t_s_user
 * @author onlineGenerator
 * @date 2016-10-08 10:40:46
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_user", schema = "")
@SuppressWarnings("serial")
public class StuEntity  extends TSBaseUser  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private String signatureFile;// 签名文件 
	private String mobilePhone;// 手机
	private String officePhone;// 办公电话
	private String email;// 邮箱
	private String timeZone;//时区设置
	private String nickname;//昵称
	private String headPortraitUrl;//头像路径
	private String messageAlert;//消息提醒设置
	private String firstName;
	private String lastName;
	private String lastLongitude;
	private String lastLatitude;
	private Date createTime;
	private Date modifyTime;
	private Date positionUploadTime;  
	private String lastPositionTimeZone;
	private String sex;
	private String grade;
	private String season;
	private String birthday;
	private String ID;

	
	@Column(name = "signatureFile", length = 100)
	public String getSignatureFile() {
		return this.signatureFile;
	}

	public void setSignatureFile(String signatureFile) {
		this.signatureFile = signatureFile;
	}
	@Column(name = "birthday", length = 100)
	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	@Column(name = "mobilePhone", length = 30)
	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@Column(name = "officePhone", length = 20)
	public String getOfficePhone() {
		return this.officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	@Column(name = "email", length = 50)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "time_zone", length = 10)
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	@Column(name = "sex", length = 10)
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	@Column(name = "grade", length = 10)
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}
	@Column(name = "season", length = 100)
	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	@Column(name = "nickname", length = 20)
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Column(name = "head_portrait_url", length = 100)
	public String getHeadPortraitUrl() {
		return headPortraitUrl;
	}

	public void setHeadPortraitUrl(String headPortraitUrl) {
		this.headPortraitUrl = headPortraitUrl;
	}
	
	@Column(name = "message_alert", length = 1)
	public String getMessageAlert() {
		return messageAlert;
	}

	public void setMessageAlert(String messageAlert) {
		this.messageAlert = messageAlert;
	}

	@Column(name = "first_name", length = 1)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name = "last_name", length = 1)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name = "last_longitude", length = 50)
	public String getLastLongitude() {
		return lastLongitude;
	}

	public void setLastLongitude(String lastLongitude) {
		this.lastLongitude = lastLongitude;
	}

	@Column(name = "last_latitude", length = 50)
	public String getLastLatitude() {
		return lastLatitude;
	}

	public void setLastLatitude(String lastLatitude) {
		this.lastLatitude = lastLatitude;
	}
	
	
	@Column(name ="create_time")
	public Date getCreateTime() {
		return createTime;
	}
	 
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@Column(name = "modify_time")
	public Date getModifyTime() {
		return modifyTime;
	}
	 
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	
	@Column(name ="position_upload_time")
	public Date getPositionUploadTime() {
		return positionUploadTime;
	}

	public void setPositionUploadTime(Date positionUploadTime) {
		this.positionUploadTime = positionUploadTime;
	}
	@Column(name="last_position_time_zone")
	public String getLastPositionTimeZone() {
		return lastPositionTimeZone;
	}

	public void setLastPositionTimeZone(String lastPositionTimeZone) {
		this.lastPositionTimeZone = lastPositionTimeZone;
	}
}