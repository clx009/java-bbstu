package com.hnykl.bp.web.stuinfo.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.stuinfo.service.StudentServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.stuinfo.entity.StuEntity;
import java.util.UUID;
import java.io.Serializable;

@Service("studentService")
@Transactional
public class StudentServiceImpl extends CommonServiceImpl implements StudentServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((StuEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((StuEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((StuEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(StuEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(StuEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(StuEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,StuEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{email}",String.valueOf(t.getEmail()));
 		sql  = sql.replace("#{mobilephone}",String.valueOf(t.getMobilePhone()));
 		sql  = sql.replace("#{officephone}",String.valueOf(t.getOfficePhone()));
 		sql  = sql.replace("#{signaturefile}",String.valueOf(t.getSignatureFile()));
 		sql  = sql.replace("#{time_zone}",String.valueOf(t.getTimeZone()));
 		sql  = sql.replace("#{nickname}",String.valueOf(t.getNickname()));
 		sql  = sql.replace("#{head_portrait_url}",String.valueOf(t.getHeadPortraitUrl()));
 		sql  = sql.replace("#{message_alert}",String.valueOf(t.getMessageAlert()));
 		sql  = sql.replace("#{first_name}",String.valueOf(t.getFirstName()));
 		sql  = sql.replace("#{last_name}",String.valueOf(t.getLastName()));
 		sql  = sql.replace("#{last_longitude}",String.valueOf(t.getLastLongitude()));
 		sql  = sql.replace("#{last_latitude}",String.valueOf(t.getLastLatitude()));
 		sql  = sql.replace("#{position_upload_time}",String.valueOf(t.getPositionUploadTime()));
 		sql  = sql.replace("#{create_time}",String.valueOf(t.getCreateTime()));
 		sql  = sql.replace("#{modify_time}",String.valueOf(t.getModifyTime()));
 		sql  = sql.replace("#{last_position_time_zone}",String.valueOf(t.getLastPositionTimeZone()));
 		sql  = sql.replace("#{sex}",String.valueOf(t.getSex()));
 		sql  = sql.replace("#{grade}",String.valueOf(t.getGrade()));
 		sql  = sql.replace("#{season}",String.valueOf(t.getSeason()));
 		sql  = sql.replace("#{birthday}",String.valueOf(t.getBirthday()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
}