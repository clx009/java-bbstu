package com.hnykl.bp.web.exam.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.exam.entity.ExamItemEntity;
import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.school.entity.SchoolEntity;

import java.io.Serializable;
import java.util.List;

public interface ExamItemServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(ExamItemEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(ExamItemEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(ExamItemEntity t);

	public List<ExamItemEntity> getItemSelectItems(String q);
	
	public List<ExamItemEntity> findAll();
}
