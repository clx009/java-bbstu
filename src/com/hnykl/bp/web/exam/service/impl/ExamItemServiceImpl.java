package com.hnykl.bp.web.exam.service.impl;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.character.entity.TSCharactertestEntity;
import com.hnykl.bp.web.exam.service.ExamItemServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.exam.entity.ExamItemEntity; 

import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("examItemService")
@Transactional
public class ExamItemServiceImpl extends CommonServiceImpl implements ExamItemServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((ExamItemEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((ExamItemEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((ExamItemEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(ExamItemEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(ExamItemEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(ExamItemEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,ExamItemEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{chinese_name}",String.valueOf(t.getChineseName()));
 		sql  = sql.replace("#{english_name}",String.valueOf(t.getEnglishName()));
 		sql  = sql.replace("#{comments}",String.valueOf(t.getComments()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}

	@Override
	public List<ExamItemEntity> getItemSelectItems(String q) {
		String query = "select * from t_exam_item  where 1=1";
		if (q != null && !"".equals(query))
			query += " and ( chinesename like '%" + q + "%' or englishname like  '%" + q + "%' )";
	//	Query hq = this.getSession().createQuery(query);
		SQLQuery queryObject = getSession().createSQLQuery(query);
		queryObject.addEntity(ExamItemEntity.class);
		List<ExamItemEntity> list = queryObject.list();
		return list;
	}

	@Override
	public List<ExamItemEntity> findAll() {
		Query query = getSession().createQuery("from ExamItemEntity ");
		return query.list();
	}
}