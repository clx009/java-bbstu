package com.hnykl.bp.web.exam.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.exam.entity.ExamItemEntity;
import com.hnykl.bp.web.exam.entity.ExamResultEntity;
import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.exam.entity.timeTableEntity;
import com.hnykl.bp.web.exam.service.SubjectServiceI;
import com.hnykl.bp.web.exam.service.TimeTableServiveI;

@Service("timeTableService")
@Transactional
public class TimeTableServiceImpl extends CommonServiceImpl implements
        TimeTableServiveI {

	public <T> void delete(T entity) {
		super.delete(entity);
		// 执行删除操作配置的sql增强
		this.doDelSql((timeTableEntity) entity);
	}

	public <T> Serializable save(T entity) {
		Serializable t = super.save(entity);
		// 执行新增操作配置的sql增强
		this.doAddSql((timeTableEntity) entity);
		return t;
	}

	public <T> void saveOrUpdate(T entity) {
		super.saveOrUpdate(entity);
		// 执行更新操作配置的sql增强
		this.doUpdateSql((timeTableEntity) entity);
	}

	/**
	 * 默认按钮-sql增强-新增操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doAddSql(timeTableEntity entity) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-更新操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doUpdateSql(timeTableEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-删除操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doDelSql(timeTableEntity t) {
		return true;
	}

	/**
	 * 替换sql中的变量
	 * 
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,timeTableEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{user_id}",String.valueOf(t.getUserId()));
 		sql  = sql.replace("#{school_year}",String.valueOf(t.getSchoolYear()));
 		sql  = sql.replace("#{term}",String.valueOf(t.getTerm()));
 		sql  = sql.replace("#{starttime}",String.valueOf(t.getStarttime()));
 		sql  = sql.replace("#{endtime}",String.valueOf(t.getEndtime()));
 		sql  = sql.replace("#{course_id}",String.valueOf(t.getCourseId()));
 		sql  = sql.replace("#{teacher_id}",String.valueOf(t.getTeacherId()));
 		sql  = sql.replace("#{place}",String.valueOf(t.getPlace()));
 		sql  = sql.replace("#{status}",String.valueOf(t.getStatus()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
	
}