package com.hnykl.bp.web.exam.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.exam.dto.ExamResultDTO;
import com.hnykl.bp.web.exam.dto.StudyRecordDTO;
import com.hnykl.bp.web.exam.dto.StutyRecordDetailDTO;
import com.hnykl.bp.web.exam.entity.ExamResultEntity;
import com.hnykl.bp.web.exam.service.ExamResultServiceI;

@Service("examResultService")
@Transactional
public class ExamResultServiceImpl extends CommonServiceImpl implements ExamResultServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((ExamResultEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((ExamResultEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((ExamResultEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(ExamResultEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(ExamResultEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(ExamResultEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,ExamResultEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{user_id}",String.valueOf(t.getUserId()));
 		sql  = sql.replace("#{school_year}",String.valueOf(t.getSchoolYear()));
 		sql  = sql.replace("#{term}",String.valueOf(t.getTerm()));
 		sql  = sql.replace("#{course_id}",String.valueOf(t.getCourseId()));
 		sql  = sql.replace("#{score}",String.valueOf(t.getScore()));
 		sql  = sql.replace("#{teacher_id}",String.valueOf(t.getTeacherId()));
 		sql  = sql.replace("#{create_time}",String.valueOf(t.getCreateTime()));
 		sql  = sql.replace("#{creator_id}",String.valueOf(t.getCreatorId()));
 		sql  = sql.replace("#{comments}",String.valueOf(t.getComments()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
 	
 	
 	
 	public List<ExamResultDTO> queryExamResult(ExamResultEntity examResultEntity) {
 		StringBuffer sbStr = new StringBuffer();
 		sbStr.append(" select tes.id subjectId,ter.id,school_year schoolYear,ter.term,tes.code,tes.english_name englishName,tes.chinese_name chineseName,tes.comments,'' scale,       ");
 		sbStr.append(" ter.score,tst.`name` teachName,'' credit                                                   ");
 		sbStr.append("  from t_exam_result ter,t_exam_subject tes,t_sch_teacher tst         ");
 		sbStr.append(" where                                                                ");
 		sbStr.append(" ter.course_id = tes.id and  ter.teacher_id = tst.id                  ");
 		sbStr.append(" and ter.user_id = :userId                                                ");
 		sbStr.append(" and ter.school_year = :schoolYear                                             ");
 		sbStr.append(" and term = :term                                                        ");
		SQLQuery sQLQuery = this.getSession().createSQLQuery(sbStr.toString());
		sQLQuery.setParameter("userId", examResultEntity.getUserId());
		sQLQuery.setParameter("schoolYear", examResultEntity.getSchoolYear());
		sQLQuery.setParameter("term", examResultEntity.getTerm());
		sQLQuery.addEntity(ExamResultDTO.class);
		List<ExamResultDTO> list = sQLQuery.list();
		return list;
	}
 	
 	public List<ExamResultEntity> queryExamDate(String id) {
 		StringBuffer sbStr = new StringBuffer();
 		sbStr.append(" select *");
 		sbStr.append("  from t_exam_result ter         ");
 		sbStr.append(" where                                                                ");
 		sbStr.append(" ter.user_id = :userId order by ter.school_year, ter.term                                   ");
		SQLQuery sQLQuery = this.getSession().createSQLQuery(sbStr.toString());
		sQLQuery.setParameter("userId", id);
		sQLQuery.addEntity(ExamResultEntity.class);
		List<ExamResultEntity> list = sQLQuery.list();
		return list;
	}
 	
	public List<ExamResultDTO> analyseExamResult(ExamResultEntity examResultEntity,String[] subjectIds) {
 		StringBuffer sbStr = new StringBuffer();
 		//sbStr.append(" select * from (                                                          ");
 		sbStr.append(" select tes.id subjectId,ter.id,school_year schoolYear,ter.term as term,tes.code,tes.english_name englishName,tes.chinese_name chineseName,tes.comments,ter.score,tst.`name` teachName,'' scale,'' credit        ");
 		sbStr.append(" from t_exam_result ter,t_exam_subject tes,t_sch_teacher tst                                     ");
 		sbStr.append(" where                                                                    ");
 		sbStr.append(" ter.course_id = tes.id and  ter.teacher_id = tst.id                                                   ");
 		sbStr.append(" and ter.user_id = :userId                     ");
 		if(subjectIds != null){
 			sbStr.append(" and tes.id in("+StringUtils.array2Str(subjectIds)+")                                                           ");
 		}
 		String[] schoolYear = (examResultEntity.getSchoolYear() != null)?examResultEntity.getSchoolYear().split(","):null;
 		for(int i=0; schoolYear != null && i <schoolYear.length;i++ ){
 			if( i== 0)sbStr.append(" and (( ter.school_year = :schoolYear_"+i+" and ter.term = :term_"+i+"  )       ");
 			else if( i== schoolYear.length -1)sbStr.append(" or ( ter.school_year = :schoolYear_"+i+" and ter.term = :term_"+i+"  ))      ");
 			else  sbStr.append("  or ( ter.school_year = :schoolYear_"+i+" and ter.term = :term_"+i+"  )      ");
 		}
 		sbStr.append(" order by ter.school_year desc,ter.term desc                              ");
 		//sbStr.append(" )t limit 0,4                                                             ");
		
 		SQLQuery sQLQuery = this.getSession().createSQLQuery(sbStr.toString());
		sQLQuery.setParameter("userId", examResultEntity.getUserId());
		for(int i=0; schoolYear != null && i <schoolYear.length;i++){
			sQLQuery.setParameter("schoolYear_"+i, examResultEntity.getSchoolYear().split(",")[i]);
			sQLQuery.setParameter("term_"+i, examResultEntity.getTerm().split(",")[i]);
		}
		sQLQuery.addEntity(ExamResultDTO.class);
		List<ExamResultDTO> list = sQLQuery.list();
		return list;
	}
 	
//	public List<Map> analyseExamResult4GPA(ExamResultEntity examResultEntity,String[] subjectIds) {
// 		StringBuffer sbStr = new StringBuffer();
// 		sbStr.append(" select * from (select school_year schoolYear,term,ter.user_id userId,     ");
// 		sbStr.append(" ROUND((sum(case when ter.score>=93 and ter.score<=100 then 4     ");
// 		sbStr.append(" when  ter.score>=90 and ter.score<=92 then 3.7        ");
// 		sbStr.append(" when  ter.score>=87 and ter.score<=89 then 3.3        ");
// 		sbStr.append(" when  ter.score>=83 and ter.score<=86 then 3.0        ");
// 		sbStr.append(" when  ter.score>=80 and ter.score<=82 then 2.7        ");
// 		sbStr.append(" when  ter.score>=77 and ter.score<=79 then 2.3        ");
// 		sbStr.append(" when  ter.score>=73 and ter.score<=76 then 2.0        ");
// 		sbStr.append(" when  ter.score>=70 and ter.score<=72 then 1.7        ");
// 		sbStr.append(" when  ter.score>=67 and ter.score<=69 then 1.3        ");
// 		sbStr.append(" when  ter.score>=67 and ter.score<=69 then 1.3        ");
// 		sbStr.append(" when  ter.score>=65 and ter.score<=66 then 1.0        ");
// 		sbStr.append(" when  ter.score<65  then 0.0        ");
// 		sbStr.append(" end)*tes.credit)/sum(tes.credit),2) scale                ");
// 		sbStr.append(" from t_exam_result ter,t_exam_subject tes,t_sch_teacher tst                                     ");
// 		sbStr.append(" where                                                                    ");
// 		sbStr.append(" ter.course_id = tes.id and  ter.teacher_id = tst.id                                                   ");
// 		sbStr.append(" and ter.user_id = :userId                     ");
// 		if(subjectIds != null){
// 			sbStr.append(" and tes.id in("+StringUtils.array2Str(subjectIds)+")                                                           ");
// 		}
//		sbStr.append(" and ( (ter.school_year = :schoolYearStart  and   ter.term >= :termStart)  ");
//		sbStr.append(" or (ter.school_year = :schoolYearEnd  and   ter.term <= :termEnd)  ");
//		sbStr.append(" or (ter.school_year >:schoolYearStart ");
//		sbStr.append(" and ter.school_year <:schoolYearEnd) ) group by school_year,term,ter.user_id)t  ");
//		sbStr.append(" order by t.schoolYear desc,t.term desc                              ");
//		Query sQLQuery = this.getSession().createSQLQuery(sbStr.toString()).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
//		sQLQuery.setParameter("userId", examResultEntity.getUserId());
//		sQLQuery.setParameter("schoolYearStart", examResultEntity.getSchoolYear().split(",")[0]);
//		sQLQuery.setParameter("schoolYearEnd", examResultEntity.getSchoolYear().split(",")[1]);
//		sQLQuery.setParameter("termStart", examResultEntity.getTerm().split(",")[0]);
//		sQLQuery.setParameter("termEnd", examResultEntity.getTerm().split(",")[1]);
//		List<Map> list = sQLQuery.list();
//		return list;
//	}
//	
	public List<Map> analyseExamResult4GPA(ExamResultEntity examResultEntity,String[] subjectIds) {
 		StringBuffer sbStr = new StringBuffer();
 		sbStr.append(" select * from (select school_year schoolYear,term,ter.user_id userId,     ");
 		sbStr.append(" ROUND((sum(case when ter.scorelevel = 'A+' then 4.33    ");
 		sbStr.append(" when  ter.scorelevel = 'A' then 4.0        ");
 		sbStr.append(" when ter.scorelevel = 'A-' then 3.67        ");
 		sbStr.append(" when ter.scorelevel = 'B+' then 3.33        ");
 		sbStr.append(" when ter.scorelevel = 'B' then 3.0        ");
 		sbStr.append(" when ter.scorelevel = 'B-' then 2.67        ");
 		sbStr.append(" when ter.scorelevel = 'C+' then 2.33        ");
 		sbStr.append(" when ter.scorelevel = 'C' then 2.0        ");
 		sbStr.append(" when ter.scorelevel = 'C-' then 1.67        ");
 		sbStr.append(" when ter.scorelevel = 'D+' then 1.33        ");
 		sbStr.append(" when ter.scorelevel = 'D' then 1.0        ");
 		sbStr.append(" when ter.scorelevel = 'D-'  then 0.67        ");
		sbStr.append(" when ter.scorelevel = 'F'  then 0.0        ");
 		sbStr.append(" end)*tes.credit)/sum(tes.credit),2) scale                ");
 		sbStr.append(" from t_exam_result ter,t_exam_subject tes,t_sch_teacher tst                                     ");
 		sbStr.append(" where                                                                    ");
 		sbStr.append(" ter.course_id = tes.id and  ter.teacher_id = tst.id                                                   ");
 		sbStr.append(" and ter.user_id = :userId                     ");
 		if(subjectIds != null){
 			sbStr.append(" and tes.id in("+StringUtils.array2Str(subjectIds)+")                                                           ");
 		}
		sbStr.append(" and ( (ter.school_year = :schoolYearStart  and   ter.term >= :termStart)  ");
		sbStr.append(" or (ter.school_year = :schoolYearEnd  and   ter.term <= :termEnd)  ");
		sbStr.append(" or (ter.school_year >:schoolYearStart ");
		sbStr.append(" and ter.school_year <:schoolYearEnd) ) group by school_year,term,ter.user_id)t  ");
		sbStr.append(" order by t.schoolYear desc,t.term desc                              ");
		Query sQLQuery = this.getSession().createSQLQuery(sbStr.toString()).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		sQLQuery.setParameter("userId", examResultEntity.getUserId());
		sQLQuery.setParameter("schoolYearStart", examResultEntity.getSchoolYear().split(",")[0]);
		sQLQuery.setParameter("schoolYearEnd", examResultEntity.getSchoolYear().split(",")[1]);
		sQLQuery.setParameter("termStart", examResultEntity.getTerm().split(",")[0]);
		sQLQuery.setParameter("termEnd", examResultEntity.getTerm().split(",")[1]);
		List<Map> list = sQLQuery.list();
		return list;
	}

	@Override
	public List<StudyRecordDTO> findStudyRecordList(String userId) {
		StringBuffer sb = new StringBuffer(); 
		sb.append(" SELECT es.id as 'id', ");
		sb.append(" ei.chinese_name as 'itemName', "); 
		sb.append(" es.id as 'subjectId', "); 
		sb.append(" es.chinese_name as 'subjectName', "); 
		sb.append(" er.level as 'diffLevel', "); 
		sb.append(" FORMAT(avg(er.score),2) 'score' "); 
		sb.append(" FROM "); 
		sb.append(" t_exam_result er "); 
		sb.append(" JOIN t_exam_subject es ON (er.course_id = es.id) "); 
		sb.append(" JOIN t_exam_item ei ON (es.item_id = ei.id) "); 
		sb.append(" where er.user_id = :userId "); 
		sb.append(" group by es.id "); 
		SQLQuery sQLQuery = this.getSession().createSQLQuery(sb.toString());
		sQLQuery.setParameter("userId",userId);
		sQLQuery.addEntity(StudyRecordDTO.class);
		List<StudyRecordDTO> list = sQLQuery.list();
		return list;
	}

	@Override
	public List<StutyRecordDetailDTO> findStudyRecordDetailList(String userId,
			String subjectId) {
		StringBuffer sb = new StringBuffer(); 
		sb.append(" SELECT er.id as 'id', ");
		sb.append(" e.`name` as 'examName', ");
		sb.append(" er.score as 'score' ");
		sb.append(" FROM ");
		sb.append(" t_exam_result er ");
		sb.append(" JOIN t_exam e ON (er.exam_id = e.id) ");
		sb.append(" where er.user_id = :userId and er.course_id = :subjectId ");
		SQLQuery sQLQuery = this.getSession().createSQLQuery(sb.toString());
		sQLQuery.setParameter("userId",userId);
		sQLQuery.setParameter("subjectId",subjectId);
		sQLQuery.addEntity(StutyRecordDetailDTO.class);
		List<StutyRecordDetailDTO> list = sQLQuery.list();
		return list;
	}
 	
}