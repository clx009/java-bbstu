package com.hnykl.bp.web.exam.service.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.exam.service.ExamServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.exam.entity.ExamEntity;
import com.hnykl.bp.web.exam.entity.ExamItemEntity;

import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("examService")
@Transactional
public class ExamServiceImpl extends CommonServiceImpl implements ExamServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((ExamEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((ExamEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((ExamEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(ExamEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(ExamEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(ExamEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,ExamEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{name}",String.valueOf(t.getName()));
// 		sql  = sql.replace("#{school_year}",String.valueOf(t.getSchoolYear()));
// 		sql  = sql.replace("#{term}",String.valueOf(t.getTerm()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}

	@Override
	public List<ExamEntity> getExamSelectItems(String q) {
		String query = " from ExamEntity s where 1=1";
		if (q != null && !"".equals(query))
			query += " and s.name like '%" + q + "%'";
		Query hq = this.getSession().createQuery(query);
		@SuppressWarnings("unchecked")
		List<ExamEntity> list = hq.list();
		return list;
	}
}