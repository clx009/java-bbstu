package com.hnykl.bp.web.exam.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.exam.entity.ExamItemEntity;
import com.hnykl.bp.web.exam.entity.ExamResultEntity;
import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.exam.service.SubjectServiceI;

@Service("subjectService")
@Transactional
public class SubjectServiceImpl extends CommonServiceImpl implements
		SubjectServiceI {

	public <T> void delete(T entity) {
		super.delete(entity);
		// 执行删除操作配置的sql增强
		this.doDelSql((SubjectEntity) entity);
	}

	public <T> Serializable save(T entity) {
		Serializable t = super.save(entity);
		// 执行新增操作配置的sql增强
		this.doAddSql((SubjectEntity) entity);
		return t;
	}

	public <T> void saveOrUpdate(T entity) {
		super.saveOrUpdate(entity);
		// 执行更新操作配置的sql增强
		this.doUpdateSql((SubjectEntity) entity);
	}

	/**
	 * 默认按钮-sql增强-新增操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doAddSql(SubjectEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-更新操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doUpdateSql(SubjectEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-删除操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doDelSql(SubjectEntity t) {
		return true;
	}

	/**
	 * 替换sql中的变量
	 * 
	 * @param sql
	 * @return
	 */
	public String replaceVal(String sql, SubjectEntity t) {
		sql = sql.replace("#{id}", String.valueOf(t.getId()));
		sql = sql
				.replace("#{chinese_name}", String.valueOf(t.getChineseName()));
		sql = sql
				.replace("#{english_name}", String.valueOf(t.getEnglishName()));
		sql = sql.replace("#{comments}", String.valueOf(t.getComments()));
		sql = sql.replace("#{UUID}", UUID.randomUUID().toString());
		return sql;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectEntity> subjectCombobox(String q) {
		String query = " from SubjectEntity s where 1=1";
		if (q != null && !"".equals(query))
			query += " and ( s.chineseName like '%" + q + "%' or s.englishName like  '%" + q + "%' )";
		Query hq = this.getSession().createQuery(query);
		@SuppressWarnings("unchecked")
		List<SubjectEntity> list = hq.list();
		return list; 
	}
	
	public List<SubjectEntity> findAll() {
		Query query = getSession().createQuery("from SubjectEntity ");
		return query.list();
	}

	@Override
	public List<SubjectEntity> findById(String Id) {
		SQLQuery sQLQuery = getSession().createSQLQuery("select * from t_exam_subject where item_id =:itemId");
		sQLQuery.setParameter("itemId", Id);
		sQLQuery.addEntity(SubjectEntity.class);
		return sQLQuery.list();
	}	
}