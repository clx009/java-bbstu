package com.hnykl.bp.web.exam.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.exam.dto.ExamResultDTO;
import com.hnykl.bp.web.exam.dto.StudyRecordDTO;
import com.hnykl.bp.web.exam.dto.StutyRecordDetailDTO;
import com.hnykl.bp.web.exam.entity.ExamResultEntity;

public interface ExamResultServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(ExamResultEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(ExamResultEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(ExamResultEntity t);
 	/**
 	 * 查询成绩结果
 	 * @param 
 	 * @return
 	 */
 	public List<ExamResultDTO> queryExamResult(ExamResultEntity examResultEntity);
 	/**
 	 * 查询有成绩日期
 	 * @param 
 	 * @return
 	 */
 	public List<ExamResultEntity> queryExamDate(String id);
 	/**
 	 * 统计分析
 	 * @param examResultEntity
 	 * @param subjectEntity
 	 * @return
 	 */
 	public List<ExamResultDTO> analyseExamResult(ExamResultEntity examResultEntity,String [] subjectIds);
 	public List<Map> analyseExamResult4GPA(ExamResultEntity examResultEntity,String[] subjectIds);

	public List<StudyRecordDTO> findStudyRecordList(String userId);

	public List<StutyRecordDetailDTO> findStudyRecordDetailList(String userId,
			String subjectId);
}
