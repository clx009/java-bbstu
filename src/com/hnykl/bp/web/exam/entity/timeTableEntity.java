package com.hnykl.bp.web.exam.entity;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

/**   
 * @Title: Entity
 * @Description: 成绩维护
 * @author onlineGenerator
 * @date 2016-06-27 10:32:35
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_exam_timetable", schema = "")
@SuppressWarnings("serial")
public class timeTableEntity implements java.io.Serializable {
	/**主键*/
	private java.lang.String id;
	/**userId*/
	private java.lang.String userId;
	/**学年*/
	private java.lang.String schoolYear;
	/**学期,外键关联到数据字典*/
	private java.lang.String term;
	private java.lang.String starttime;
	private java.lang.String endtime;
	/**课程id*/
	private java.lang.String courseId;
	/**teacherId*/
	private java.lang.String teacherId;
	private String place;
	
	private String  status;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  userId
	 */
	@Column(name ="USER_ID",nullable=false)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  userId
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学年
	 */
	@Column(name ="SCHOOL_YEAR",nullable=false)
	public java.lang.String getSchoolYear(){
		return this.schoolYear;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学年
	 */
	public void setSchoolYear(java.lang.String schoolYear){
		this.schoolYear = schoolYear;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学期,外键关联到数据字典
	 */
	@Column(name ="TERM",nullable=false)
	public java.lang.String getTerm(){
		return this.term;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学期,外键关联到数据字典
	 */
	public void setTerm(java.lang.String term){
		this.term = term;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  课程id
	 */
	@Column(name ="COURSE_ID",nullable=false)
	public java.lang.String getCourseId(){
		return this.courseId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  课程id
	 */
	public void setCourseId(java.lang.String courseId){
		this.courseId = courseId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  teacherId
	 */
	@Column(name ="TEACHER_ID",nullable=false)
	public java.lang.String getTeacherId(){
		return this.teacherId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  teacherId
	 */
	public void setTeacherId(java.lang.String teacherId){
		this.teacherId = teacherId;
	}

	@Column(name ="STARTTIME",nullable=false)
	public java.lang.String getStarttime() {
		return starttime;
	}

	public void setStarttime(java.lang.String starttime) {
		this.starttime = starttime;
	}

	@Column(name ="ENDTIME",nullable=false)
	public java.lang.String getEndtime() {
		return endtime;
	}

	public void setEndtime(java.lang.String endtime) {
		this.endtime = endtime;
	}

	@Column(name ="PLACE",nullable=false)
	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}
	
	@Column(name ="STATUS",nullable=false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	


}
