package com.hnykl.bp.web.exam.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: t_exam_item
 * @author onlineGenerator
 * @date 2016-10-08 14:33:51
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_exam_item", schema = "")
@SuppressWarnings("serial")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"}) 
public class ExamItemEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**中文名称*/
	private java.lang.String chineseName;
	/**英文名称*/
	private java.lang.String englishName;
	/**备注*/
	private java.lang.String comments;
	
	private List<SubjectEntity> subjectEntityList;
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  中文名称
	 */
	@Column(name ="CHINESE_NAME",nullable=false)
	public java.lang.String getChineseName(){
		return this.chineseName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  中文名称
	 */
	public void setChineseName(java.lang.String chineseName){
		this.chineseName = chineseName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  英文名称
	 */
	@Column(name ="ENGLISH_NAME",nullable=false)
	public java.lang.String getEnglishName(){
		return this.englishName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  英文名称
	 */
	public void setEnglishName(java.lang.String englishName){
		this.englishName = englishName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="COMMENTS",nullable=false)
	public java.lang.String getComments(){
		return this.comments;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setComments(java.lang.String comments){
		this.comments = comments;
	}

	@Transient
	public List<SubjectEntity> getSubjectEntityList() {
		return subjectEntityList;
	}

	public void setSubjectEntityList(List<SubjectEntity> subjectEntityList) {
		this.subjectEntityList = subjectEntityList;
	}
	
	
}
