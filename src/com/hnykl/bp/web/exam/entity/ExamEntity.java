package com.hnykl.bp.web.exam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: t_exam
 * @author onlineGenerator
 * @date 2016-10-08 14:34:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_exam", schema = "")
@SuppressWarnings("serial")
public class ExamEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**考试名称*/
	private java.lang.String name;
//	/**学年*/
//	private java.lang.String schoolYear;
//	/**学期*/
//	private java.lang.String term;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  考试名称
	 */
	@Column(name ="NAME",nullable=false)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  考试名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
//	/**
//	 *方法: 取得java.lang.String
//	 *@return: java.lang.String  学年
//	 */
//	@Column(name ="SCHOOL_YEAR",nullable=false)
//	public java.lang.String getSchoolYear(){
//		return this.schoolYear;
//	}
//
//	/**
//	 *方法: 设置java.lang.String
//	 *@param: java.lang.String  学年
//	 */
//	public void setSchoolYear(java.lang.String schoolYear){
//		this.schoolYear = schoolYear;
//	}
//	/**
//	 *方法: 取得java.lang.String
//	 *@return: java.lang.String  学期
//	 */
//	@Column(name ="TERM",nullable=false)
//	public java.lang.String getTerm(){
//		return this.term;
//	}
//
//	/**
//	 *方法: 设置java.lang.String
//	 *@param: java.lang.String  学期
//	 */
//	public void setTerm(java.lang.String term){
//		this.term = term;
//	}
}
