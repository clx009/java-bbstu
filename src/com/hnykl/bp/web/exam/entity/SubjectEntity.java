package com.hnykl.bp.web.exam.entity;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;  

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**   
 * @Title: Entity
 * @Description: 考试科目
 * @author onlineGenerator
 * @date 2016-06-27 15:03:21
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_exam_subject", schema = "")
@SuppressWarnings("serial")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"}) 
public class SubjectEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**ExamItem*/
	private ExamItemEntity examItem;
	
	/**name*/
	private java.lang.String chineseName;
	
	private java.lang.String englishName;
	
	//private java.lang.String diffLevel;
	/**comments*/
	private java.lang.String comments;
	private String credit;
	
	public SubjectEntity(){}
	public SubjectEntity(String subjectId){this.id = subjectId;}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	 
	
	@Column(name="chinese_name")
	public java.lang.String getChineseName() {
		return chineseName;
	}

	public void setChineseName(java.lang.String chineseName) {
		this.chineseName = chineseName;
	}

	@Column(name="english_name")
	public java.lang.String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(java.lang.String englishName) {
		this.englishName = englishName;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  comments
	 */
	@Column(name ="COMMENTS",nullable=false)
	public java.lang.String getComments(){
		return this.comments;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  comments
	 */
	public void setComments(java.lang.String comments){
		this.comments = comments;
	}
	@Column(name ="credit")
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="item_id") 
	@JsonIgnore
	public ExamItemEntity getExamItem() {
		return examItem;
	}
	public void setExamItem(ExamItemEntity examItem) {
		this.examItem = examItem;
	}
//	@Column(name ="diff_level")
//	public java.lang.String getDiffLevel() {
//		return diffLevel;
//	}
//	public void setDiffLevel(java.lang.String diffLevel) {
//		this.diffLevel = diffLevel;
//	}
//	
	
}
