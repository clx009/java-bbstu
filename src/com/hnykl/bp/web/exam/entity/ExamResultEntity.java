package com.hnykl.bp.web.exam.entity;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

/**   
 * @Title: Entity
 * @Description: 成绩维护
 * @author onlineGenerator
 * @date 2016-06-27 10:32:35
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_exam_result", schema = "")
@SuppressWarnings("serial")
public class ExamResultEntity implements java.io.Serializable {
	/**主键*/
	private java.lang.String id;
	/**userId*/
	private java.lang.String userId;
	/**学年*/
	private java.lang.String schoolYear;
	/**学期,外键关联到数据字典*/
	private java.lang.String term;
	
	/**课程id*/
	private java.lang.String courseId;
	/**分数*/
	private java.lang.String score;
	/**分数*/
	private java.lang.String scorelevel;
	/**teacherId*/
	private java.lang.String teacherId;
	/**创建时间*/
	private java.lang.String createTime;
	/**创建人id，关联到t_s_user.id*/
	private java.lang.String creatorId;
	/**备注*/
	private java.lang.String comments;
	private String diffLevel;
	
	/**考试**/
	private ExamEntity exam;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  userId
	 */
	@Column(name ="USER_ID",nullable=false)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  userId
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学年
	 */
	@Column(name ="SCHOOL_YEAR",nullable=false)
	public java.lang.String getSchoolYear(){
		return this.schoolYear;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学年
	 */
	public void setSchoolYear(java.lang.String schoolYear){
		this.schoolYear = schoolYear;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学期,外键关联到数据字典
	 */
	@Column(name ="TERM",nullable=false)
	public java.lang.String getTerm(){
		return this.term;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学期,外键关联到数据字典
	 */
	public void setTerm(java.lang.String term){
		this.term = term;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  课程id
	 */
	@Column(name ="COURSE_ID",nullable=false)
	public java.lang.String getCourseId(){
		return this.courseId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  课程id
	 */
	public void setCourseId(java.lang.String courseId){
		this.courseId = courseId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  分数
	 */
	@Column(name ="SCORE",nullable=false)
	public java.lang.String getScore(){
		return this.score;
	}
	@Column(name ="SCORELEVEL",nullable=false)
	public java.lang.String getScorelevel() {
		return scorelevel;
	}

	public void setScorelevel(java.lang.String scorelevel) {
		this.scorelevel = scorelevel;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  分数
	 */
	public void setScore(java.lang.String score){
		this.score = score;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  teacherId
	 */
	@Column(name ="TEACHER_ID",nullable=false)
	public java.lang.String getTeacherId(){
		return this.teacherId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  teacherId
	 */
	public void setTeacherId(java.lang.String teacherId){
		this.teacherId = teacherId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建时间
	 */
	@Column(name ="CREATE_TIME",nullable=false)
	public java.lang.String getCreateTime(){
		return this.createTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建时间
	 */
	public void setCreateTime(java.lang.String createTime){
		this.createTime = createTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建人id，关联到t_s_user.id
	 */
	@Column(name ="CREATOR_ID",nullable=false)
	public java.lang.String getCreatorId(){
		return this.creatorId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建人id，关联到t_s_user.id
	 */
	public void setCreatorId(java.lang.String creatorId){
		this.creatorId = creatorId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name ="COMMENTS",nullable=false)
	public java.lang.String getComments(){
		return this.comments;
	}
	
	
	@Column(name ="LEVEL",nullable=false)
	public String getDiffLevel() {
		return diffLevel;
	}

	public void setDiffLevel(String diffLevel) {
		this.diffLevel = diffLevel;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setComments(java.lang.String comments){
		this.comments = comments;
	}
	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="exam_id")
	public ExamEntity getExam() {
		return exam;
	}

	public void setExam(ExamEntity exam) {
		this.exam = exam;
	}
}
