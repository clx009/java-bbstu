package com.hnykl.bp.web.exam.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class StutyRecordDetailDTO {
	
	private String id;

	private String examName;
	private String score;

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
