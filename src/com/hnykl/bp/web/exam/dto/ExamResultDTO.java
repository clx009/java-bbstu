package com.hnykl.bp.web.exam.dto;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
public class ExamResultDTO{

	
	private String id;

	private String subjectId;
	

	private String code;
	
	private String englishName;
	
	private String chineseName;
	
	private String comments;
	
	private String score;
	
	private String teachName;
	
	private String schoolYear;
	
	private String term;
	
	private String scale; 

	private String credit;  
	
	public  ExamResultDTO(String subjectId,String score){
		this.subjectId = subjectId;
		this.score = score;
	}
	
	public String getSubjectId() {
		return subjectId;
	}


	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	
	
	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public ExamResultDTO(){
		
	}

	public ExamResultDTO(String id,String code,String englishName,String chineseName
			,String comments,String score,String teachName){
		this.id = id;
		this.code = code;
		this.englishName = englishName;
		this.chineseName = chineseName;
		this.comments = comments;
		this.score = score;
		this.teachName = teachName;
	}
	public ExamResultDTO(String schoolYear,String term,String chineseName
			,String score){
		this.chineseName = chineseName;
		this.score = score;
		this.schoolYear = schoolYear;
		this.term = term;
	}
	

	public String getSchoolYear() {
		return schoolYear;
	}

	public void setSchoolYear(String schoolYear) {
		this.schoolYear = schoolYear;
	}
	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getTeachName() {
		return teachName;
	}

	public void setTeachName(String teachName) {
		this.teachName = teachName;
	}
	
	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}
	
}
