package com.hnykl.bp.web.character.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.character.entity.TSCharactertestEntity;
import com.hnykl.bp.web.family.entity.TFFamilyEntity;

import java.io.Serializable;
import java.util.List;

public interface TSCharactertestServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TSCharactertestEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TSCharactertestEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TSCharactertestEntity t);
 	
	public List<TSCharactertestEntity> findCharacters(String userId);
}
