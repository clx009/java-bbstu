package com.hnykl.bp.web.rt.service.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.rt.service.RealTimeServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.rt.entity.RealTimeEntity;
import com.hnykl.bp.web.system.pojo.base.TSUser;

import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("realTimeService")
@Transactional
public class RealTimeServiceImpl extends CommonServiceImpl implements RealTimeServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((RealTimeEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((RealTimeEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((RealTimeEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(RealTimeEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(RealTimeEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(RealTimeEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,RealTimeEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{table_name}",String.valueOf(t.getTableName()));
 		sql  = sql.replace("#{business_id}",String.valueOf(t.getBusinessId()));
 		sql  = sql.replace("#{type}",String.valueOf(t.getType()));
 		sql  = sql.replace("#{title}",String.valueOf(t.getTitle()));
 		sql  = sql.replace("#{content}",String.valueOf(t.getContent()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}

	@Override
	public List<RealTimeEntity> findByUserId(String userId) {
		if(StringUtils.isEmpty(userId))
			return null;
		String[] userIds = userId.split(",");
		String query = "from RealTimeEntity rt where rt.userId in (:userId)";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameterList("userId", userIds);
		@SuppressWarnings("unchecked")
		List<RealTimeEntity> list = queryObject.list();
		return list;
	}
}