package com.hnykl.bp.web.ca.entity;
 
import java.util.Date; 
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.system.pojo.base.TSAttachment;
import com.hnykl.bp.web.system.pojo.base.TSUser; 

/**   
 * @Title: Entity
 * @Description: t_ca_apply
 * @author onlineGenerator
 * @date 2016-09-24 17:11:08
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_ca_apply", schema = "")
@SuppressWarnings("serial")
public class TCaApplyEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id; 
	/**类型: 1-家庭作业辅导,2-课程辅导,3-伴学导师*/
	private java.lang.String type; 
	/**处理状态:0-待处理,1-已处理*/
	private java.lang.String status;
	/**申请时间*/
	private Date createTime;
	/**描述*/
	private java.lang.String comments;
	
	/**处理结果*/
	private java.lang.String results;
	
	/**
	 * 经度
	 */
	private String longitude;
	
	/**
	 * 纬度
	 */
	private String latitude;
	
	/**
	 * 地址描述
	 */
	private String addressDesc;
	
	/**
	 * 通话开妈时间
	 */
	private Date startTime;
	
	/**
	 * 通话结束 时间
	 */
	private Date endTime;
	
	
	/**
	 * 通话时间
	 */
	private String timeSpan;
	
	private TSUser applicant;
	
	private SubjectEntity subject;
	
	private List<TSAttachment> attachments;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	} 
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  类型: 1-家庭作业辅导,2-课程辅导,3-伴学导师
	 */
	@Column(name ="TYPE",nullable=false)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  类型: 1-家庭作业辅导,2-课程辅导,3-伴学导师
	 */
	public void setType(java.lang.String type){
		this.type = type;
	} 
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  处理状态:0-待处理,1-已处理
	 */
	@Column(name ="STATUS",nullable=false)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  处理状态:0-待处理,1-已处理
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  申请时间
	 */
	@Column(name ="CREATE_TIME",nullable=false)
	public Date getCreateTime(){
		return this.createTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  申请时间
	 */
	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  描述
	 */
	@Column(name ="COMMENTS")
	public java.lang.String getComments(){
		return this.comments;
	}

	
	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  描述
	 */
	public void setComments(java.lang.String comments){
		this.comments = comments;
	}
	
	public java.lang.String getResults() {
		return results;
	}
	
	
	@Column(name ="lontitude")
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name ="latitude")
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	@Column(name ="addressdesc")
	public String getAddressDesc() {
		return addressDesc;
	}

	public void setAddressDesc(String addressDesc) {
		this.addressDesc = addressDesc;
	}
	
	@Column(name ="starttime")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Column(name ="endtime")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	@Column(name ="timespan")
	public String getTimeSpan() {
		return timeSpan;
	}

	public void setTimeSpan(String timeSpan) {
		this.timeSpan = timeSpan;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  描述
	 */
	@Column(name ="RESULTS",nullable=false)
	public void setResults(java.lang.String results) {
		this.results = results;
	}

	@ManyToOne(fetch = FetchType.LAZY,cascade={CascadeType.REFRESH})
	@JoinColumn(name="user_id")
	public TSUser getApplicant() {
		return applicant;
	}

	public void setApplicant(TSUser applicant) {
		this.applicant = applicant;
	}

	@ManyToOne(fetch = FetchType.LAZY,cascade={CascadeType.REFRESH})
	@JoinColumn(name="subject_id")
	public SubjectEntity getSubject() {
		return subject;
	}

	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}

	@Transient
	public List<TSAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<TSAttachment> attachments) {
		this.attachments = attachments;
	}
	
	
	
}
