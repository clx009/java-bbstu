package com.hnykl.bp.web.ca.service.impl;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.ca.service.TCaApplyServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.ca.entity.TCaApplyEntity;
import com.hnykl.bp.web.rt.entity.RealTimeEntity;
import com.hnykl.bp.web.rt.service.RealTimeServiceI;
import com.hnykl.bp.web.system.pojo.base.TSAttachment;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.UserService;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("tCaApplyService")
@Transactional
public class TCaApplyServiceImpl extends CommonServiceImpl implements TCaApplyServiceI {
	@Autowired
	private UserService userService;
	@Autowired
	RealTimeServiceI realTimeService;
	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((TCaApplyEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((TCaApplyEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((TCaApplyEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TCaApplyEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TCaApplyEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TCaApplyEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,TCaApplyEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{type}",String.valueOf(t.getType()));
 		sql  = sql.replace("#{status}",String.valueOf(t.getStatus()));
 		sql  = sql.replace("#{create_time}",String.valueOf(t.getCreateTime()));
 		sql  = sql.replace("#{comments}",String.valueOf(t.getComments()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TCaApplyEntity> findTCaApplyListByUserIds(String[] studentIds,String type) {
		String hql = "from TCaApplyEntity where applicant.id in ("
				+ StringUtils.array2Str(studentIds)
				+ ") and type='"+type+"' order by createTime desc ";
		Query queryObject = getSession().createQuery(hql);
		return queryObject.list(); 
	}
	
	/**
	 * 查询附件列表
	 */
	public List<TSAttachment> findAttachments(String applyId){
		String hql = "from TSAttachment where businessKey ='"
				+ applyId
				+ "'  order by createdate desc ";
		Query queryObject = getSession().createQuery(hql);
		return queryObject.list(); 
	}

	@Override
	public String saveTCaApply(TCaApplyEntity entity) {
		String id = (String) this.save(entity);
		
		TSUser user = (TSUser)userService.getEntity(TSUser.class,entity.getApplicant().getId());
		RealTimeEntity realTimeEntity = new RealTimeEntity();
		realTimeEntity.setBusinessId(entity.getId());
		realTimeEntity.setTableName("VisitEntity"); 
		realTimeEntity.setType("2");
		realTimeEntity.setCreateTime(new Date());
		realTimeEntity.setUserId(entity.getApplicant().getId());
		if("1".equals(entity.getType())){
			realTimeEntity.setTitle("家庭作业辅导");
			realTimeEntity.setContent(user.getNickname()+" 申请家庭作业辅导："+entity.getComments());
		}
		if("2".equals(entity.getType())){
			realTimeEntity.setTitle("课程辅导");
			realTimeEntity.setContent(user.getNickname()+" 申请了课程辅导："+entity.getComments());
		}
		if("3".equals(entity.getType())){
			realTimeEntity.setTitle("伴学导师");
			realTimeEntity.setContent(user.getNickname()+" 申请了伴学导师："+entity.getComments());
		}
		if("4".equals(entity.getType())){
			realTimeEntity.setType("4");
			realTimeEntity.setTitle("热线电话");
			realTimeEntity.setContent(user.getNickname()+" 拨打了热线电话："+entity.getComments());
		}
		
		realTimeService.save(realTimeEntity);
		return id;
	}
	
	
}