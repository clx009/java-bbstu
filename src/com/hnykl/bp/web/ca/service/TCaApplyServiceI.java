package com.hnykl.bp.web.ca.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.ca.entity.TCaApplyEntity;
import com.hnykl.bp.web.system.pojo.base.TSAttachment;

import java.io.Serializable;
import java.util.List;

public interface TCaApplyServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TCaApplyEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TCaApplyEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TCaApplyEntity t);

	public List<TCaApplyEntity> findTCaApplyListByUserIds(String[] studentIds, String type);
	
	public List<TSAttachment> findAttachments(String applyId);

	public String saveTCaApply(TCaApplyEntity entity);
	
}
