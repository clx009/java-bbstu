package com.hnykl.bp.web.system.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class StudentComboboxDTO {
	
	private String id;
	private String username;
	private String realname;
	
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	
	

}
