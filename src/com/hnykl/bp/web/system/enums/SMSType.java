package com.hnykl.bp.web.system.enums;

public enum SMSType {
	
	REGISTER("注册短信验证"),FORGET_PASSWORD("忘记密码短信验证"),THIRD_PART_REGISTER("第三方注册短信验证"),CHANGE_PHONE_NUMBER("修改手机号短信验证"),CHANGE_EMAIL("修改邮箱短信验证"); 
	
	private String content;
	
	SMSType(String content){ 
		this.content = content;
	} 

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String toString(){
		return  content;
	} 
	
	public static void main(String[] args) {
		SMSType.valueOf("01");
	}

}
