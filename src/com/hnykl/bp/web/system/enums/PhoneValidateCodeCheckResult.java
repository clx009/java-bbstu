package com.hnykl.bp.web.system.enums;

public enum PhoneValidateCodeCheckResult {
	 
	SUCCESS("0","验证成功"),OVERDUE("2","验证码已过期"),NOT_EXSITS("3","验证码不存在");
	
	private String code;
	private String message;
	
	PhoneValidateCodeCheckResult(String code,String message){
		this.code = code;
		this.message = message;
	} 
	
	
	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}

 
	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String toString(){
		return code+" : "+message;
	}
	
	
}
