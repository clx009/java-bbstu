package com.hnykl.bp.web.system.listener;

import javax.servlet.ServletConfig;

import com.hnykl.bp.base.core.util.ResourceUtil;

import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.ckfinder.connector.configuration.Configuration;

/**
 * @Title: listener
 * @Description: ckfinder监听器
 * @author Alexander
 * @date 2013-09-19 23:01:20
 * @version V1.0
 * 
 */
public class CkfinderConfiguration extends Configuration {

	String path = "";

	public CkfinderConfiguration(ServletConfig servletConfig) {
		super(servletConfig);
		path = servletConfig.getServletContext().getContextPath();
	}

	
	public void init() throws Exception {
		super.init();
		String files = BaseConfigMgr.getCkUserfiles(); ;
		if (files.contains("http://"))
			this.baseURL = files;
		else
			this.baseURL = path + "/" + files + "/";
		this.baseDir = BaseConfigMgr.getCkBaseDir();
	}

}
