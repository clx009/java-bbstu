package com.hnykl.bp.web.system.pojo.base;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity; 
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.hnykl.bp.base.core.common.entity.IdEntity;  
/**
 * 手机验证码表实体类
 * 
 * */
@Entity
@Table(name = "t_s_phone_validatecode")
@PrimaryKeyJoinColumn(name = "id")
public class TSPhoneValidateCode extends IdEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; 
	
	private String validateCode;
	private String validateCodeId;
	private String type;
	private String userName;
	private String userId;
	private Timestamp createTime;
	private Date modifyTime;
	private String mobilephone;
	private String isEnable;  
	
	@Column(name = "validate_code", length = 10)
	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}
 
	
	@Column(name = "validate_code_id", length = 100)
	public String getValidateCodeId() {
		return validateCodeId;
	}
	 
	public void setValidateCodeId(String validateCodeId) {
		this.validateCodeId = validateCodeId;
	}
	
	@Column(name = "type", length = 2) 
	public String getType() {
		return type;
	}
	 
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name = "user_name", length = 100)
	public String getUserName() {
		return userName;
	}
	 
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name = "user_id", length = 32)
	public String getUserId() {
		return userId;
	}
	 
	public void setUserId(String userId) {
		this.userId = userId;
	}
	 
	@Column(name ="create_time")
	public Timestamp getCreateTime() {
		return createTime;
	}
	 
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	@Column(name = "modify_time")
	public Date getModifyTime() {
		return modifyTime;
	}
	 
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	
	@Column(name = "mobilephone", length = 20)
	public String getMobilephone() {
		return mobilephone;
	}
	 
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}
	
	@Column(name = "is_enable", length = 1)
	public String getIsEnable() {
		return isEnable;
	}
	
	public void setIsEnable(String isEnable) {
		this.isEnable = isEnable;
	}  
	
}
