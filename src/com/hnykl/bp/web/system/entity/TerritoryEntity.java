package com.hnykl.bp.web.system.entity;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

/**   
 * @Title: Entity
 * @Description: t_s_territory
 * @author onlineGenerator
 * @date 2016-06-24 15:07:08
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_territory", schema = "")
@SuppressWarnings("serial")
public class TerritoryEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**territorycode*/
	private java.lang.String territorycode;
	/**territorylevel*/
	private java.lang.String territorylevel;
	/**territoryname*/
	private java.lang.String territoryname;
	/**territoryPinyin*/
	private java.lang.String territoryPinyin;
	/**territorysort*/
	private java.lang.String territorysort;
	/**xWgs84*/
	private java.lang.String xWgs84;
	/**yWgs84*/
	private java.lang.String yWgs84;
	/**territoryparentid*/
	private java.lang.String territoryparentid;
	/**��������*/
	private java.lang.String postcode;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  territorycode
	 */
	@Column(name ="TERRITORYCODE",nullable=false)
	public java.lang.String getTerritorycode(){
		return this.territorycode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  territorycode
	 */
	public void setTerritorycode(java.lang.String territorycode){
		this.territorycode = territorycode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  territorylevel
	 */
	@Column(name ="TERRITORYLEVEL",nullable=false,length=5)
	public java.lang.String getTerritorylevel(){
		return this.territorylevel;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  territorylevel
	 */
	public void setTerritorylevel(java.lang.String territorylevel){
		this.territorylevel = territorylevel;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  territoryname
	 */
	@Column(name ="TERRITORYNAME",nullable=false)
	public java.lang.String getTerritoryname(){
		return this.territoryname;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  territoryname
	 */
	public void setTerritoryname(java.lang.String territoryname){
		this.territoryname = territoryname;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  territoryPinyin
	 */
	@Column(name ="TERRITORY_PINYIN",nullable=false)
	public java.lang.String getTerritoryPinyin(){
		return this.territoryPinyin;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  territoryPinyin
	 */
	public void setTerritoryPinyin(java.lang.String territoryPinyin){
		this.territoryPinyin = territoryPinyin;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  territorysort
	 */
	@Column(name ="TERRITORYSORT",nullable=false)
	public java.lang.String getTerritorysort(){
		return this.territorysort;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  territorysort
	 */
	public void setTerritorysort(java.lang.String territorysort){
		this.territorysort = territorysort;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  xWgs84
	 */
	@Column(name ="X_WGS84",nullable=false)
	public java.lang.String getXWgs84(){
		return this.xWgs84;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  xWgs84
	 */
	public void setXWgs84(java.lang.String xWgs84){
		this.xWgs84 = xWgs84;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  yWgs84
	 */
	@Column(name ="Y_WGS84",nullable=false)
	public java.lang.String getYWgs84(){
		return this.yWgs84;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  yWgs84
	 */
	public void setYWgs84(java.lang.String yWgs84){
		this.yWgs84 = yWgs84;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  territoryparentid
	 */
	@Column(name ="TERRITORYPARENTID",nullable=false)
	public java.lang.String getTerritoryparentid(){
		return this.territoryparentid;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  territoryparentid
	 */
	public void setTerritoryparentid(java.lang.String territoryparentid){
		this.territoryparentid = territoryparentid;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  ��������
	 */
	@Column(name ="POSTCODE",nullable=false)
	public java.lang.String getPostcode(){
		return this.postcode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  ��������
	 */
	public void setPostcode(java.lang.String postcode){
		this.postcode = postcode;
	}
}
