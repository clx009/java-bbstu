package com.hnykl.bp.web.system.entity; 
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

/**   
 * @Title: Entity
 * @Description: t_s_message
 * @author onlineGenerator
 * @date 2016-06-14 17:39:31
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_message", schema = "")
@SuppressWarnings("serial")
public class MessageEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**标题*/
	private java.lang.String title;
	/**内容*/
	private java.lang.String content;
	/**创建时间*/
	private Timestamp createTime;
	/**创建人id，对应到t_s_user.id*/
	private java.lang.String creatorId;
	/**类型：0-系统消息*/
	private java.lang.String type;
	
	
	public MessageEntity(){
		
	}
	
	public MessageEntity(String title,String content,Timestamp createTime,String creatorId,String type){
		this.title = title;
		this.content = content;
		this.createTime = createTime;
		this.creatorId = creatorId;
		this.type = type;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  标题
	 */
	@Column(name ="TITLE",nullable=false)
	public java.lang.String getTitle(){
		return this.title;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  标题
	 */
	public void setTitle(java.lang.String title){
		this.title = title;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  内容
	 */
	@Column(name ="CONTENT",nullable=false)
	public java.lang.String getContent(){
		return this.content;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  内容
	 */
	public void setContent(java.lang.String content){
		this.content = content;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  createTime
	 */
	@Column(name ="create_time")
	public Timestamp getCreateTime(){
		return this.createTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  createTime
	 */
	public void setCreateTime(Timestamp createTime){
		this.createTime = createTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建人id，对应到t_s_user.id
	 */
	@Column(name ="CREATOR_ID",nullable=false)
	public java.lang.String getCreatorId(){
		return this.creatorId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建人id，对应到t_s_user.id
	 */
	public void setCreatorId(java.lang.String creatorId){
		this.creatorId = creatorId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  类型：0-系统消息
	 */
	@Column(name ="TYPE",nullable=false)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  类型：0-系统消息
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
}
