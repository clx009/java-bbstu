package com.hnykl.bp.web.system.entity; 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

/**   
 * @Title: Entity
 * @Description: t_s_message
 * @author onlineGenerator
 * @date 2016-06-14 17:39:31
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_message_user", schema = "")
@SuppressWarnings("serial")
public class MessageUserEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**标题*/
	private java.lang.String messageId;
	/**内容*/
	private java.lang.String fromUserId;
	/**创建时间*/
	private java.lang.String toUserId;
	
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	
	@Column(name ="message_id")
	public java.lang.String getMessageId() {
		return messageId;
	}

	public void setMessageId(java.lang.String messageId) {
		this.messageId = messageId;
	}
	
	@Column(name ="from_user_id")
	public java.lang.String getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(java.lang.String fromUserId) {
		this.fromUserId = fromUserId;
	}

	@Column(name ="to_user_id")
	public java.lang.String getToUserId() {
		return toUserId;
	}

	public void setToUserId(java.lang.String toUserId) {
		this.toUserId = toUserId;
	}

	
	
}
