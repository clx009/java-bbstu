package com.hnykl.bp.web.system.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.system.enums.PhoneValidateCodeCheckResult;
import com.hnykl.bp.web.system.pojo.base.TSPhoneValidateCode;

public interface PhoneValidateCodeService extends CommonService { 
	
	/**
	 * 验证短信验证码
	 * */
	public PhoneValidateCodeCheckResult checkPhoneValidateCode(TSPhoneValidateCode tSPhoneValidateCode);

}
