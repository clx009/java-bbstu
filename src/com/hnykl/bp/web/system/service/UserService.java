package com.hnykl.bp.web.system.service;

import java.util.List;

import com.hnykl.bp.base.core.common.service.CommonService;

import com.hnykl.bp.web.system.pojo.base.TSUser;
/**
 * 
 * @author  
 *
 */
public interface UserService extends CommonService{  
	
	/**
	 *修改个人资料 
	 * */
	public void changeProfile(TSUser user);
	
	/** 
	 * 第三方登录
	 * */
	public TSUser checkUserExsits(String openId,String thirdpartyLoginType);
	
	public TSUser checkUserExits(TSUser user); 
	public String getUserRole(TSUser user);
	public void pwdInit(TSUser user, String newPwd);
	/**
	 * 判断这个角色是不是还有用户使用
	 *@Author 
	 *@date   2013-11-12
	 *@param id
	 *@return
	 */
	public int getUsersOfThisRole(String id);

	public void changeUserName(TSUser user, String newPhoneNumber);
	public void updateLastPosition(String userId,String lastLongitude,String lastLatitude,String timeZone,String createTime);
	public void pwdInitByUserId(TSUser user,String newPwd);
	public void updateOpenId(String userId,String openId,String thirdpartyLoginType);

	public TSUser findUserInfo(String userId);
	
	public List<TSUser> findStudentListByUser(String userId,String type);
}
