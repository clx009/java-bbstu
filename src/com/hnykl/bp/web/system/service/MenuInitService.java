package com.hnykl.bp.web.system.service;

import com.hnykl.bp.base.core.common.service.CommonService;

/**
 * 
 * @author  
 *
 */
public interface MenuInitService extends CommonService{
	
	public void initMenu();
}
