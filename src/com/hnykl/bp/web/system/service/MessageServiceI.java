package com.hnykl.bp.web.system.service;

import com.hnykl.bp.base.core.common.service.CommonService; 
import com.hnykl.bp.web.system.entity.MessageEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface MessageServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(MessageEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(MessageEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(MessageEntity t);
 	/**
 	 * 通过用户id查询消息
 	 * @param userId
 	 * @return
 	 */
 	public List<Map>  queryMsgByUserId(String userId);
}
