package com.hnykl.bp.web.system.service;

import java.util.List;

import com.hnykl.bp.web.system.pojo.base.TSAttachment;

import com.hnykl.bp.base.core.common.service.CommonService;

/**
 * 
 * @author  
 *
 */
public interface DeclareService extends CommonService{
	
	public List<TSAttachment> getAttachmentsByCode(String businessKey,String description);
	
}
