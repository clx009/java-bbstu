package com.hnykl.bp.web.system.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.system.entity.MessageEntity;
import com.hnykl.bp.web.system.service.MessageServiceI;

@Service("messageService")
@Transactional
public class MessageServiceImpl extends CommonServiceImpl implements MessageServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((MessageEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((MessageEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((MessageEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(MessageEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(MessageEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(MessageEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,MessageEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{title}",String.valueOf(t.getTitle()));
 		sql  = sql.replace("#{content}",String.valueOf(t.getContent()));
 		sql  = sql.replace("#{create_time}",String.valueOf(t.getCreateTime()));
 		sql  = sql.replace("#{creator_id}",String.valueOf(t.getCreatorId()));
 		sql  = sql.replace("#{type}",String.valueOf(t.getType()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
 	
	public List<Map>  queryMsgByUserId(String userId){
		String webVisitUrl = BaseConfigMgr.getWebVisitUrl();
		StringBuffer sbStr = new StringBuffer();
		sbStr.append(" select * from (select tsm.id,tsm.title,'' nickname,'' headPortraitUrl,tsm.content,tsm.create_time,'' url,'' latitude,'' longitude,tsm.type,'' timeZone,'' description from t_s_message tsm   where tsm.type='0'                                                                ");
		sbStr.append(" UNION select tsm.id,tsm.title,'' nickname,'' headPortraitUrl,tsm.content,tsm.create_time,'' url,'' latitude,'' longitude,tsm.type,'' timeZone,'' description from t_s_message tsm,t_s_message_user tsmu   where tsm.type='1' and tsm.id=tsmu.message_id and  tsmu.to_user_id=:userId                                                               ");
		sbStr.append(" UNION                                                                                                                                                   ");
		sbStr.append(" select tpp.id,CONCAT(tsu.nickname,'给您报平安') title,tsu.nickname,concat('"+webVisitUrl+"/',tsu.head_portrait_url) headPortraitUrl,'' content,tpp.create_time,concat('"+webVisitUrl+"/',tpp.position_screenshot_url) url,tpp.latitude,tpp.longitude,'2' type,tpp.time_zone timeZone,tpp.description from t_p_position tpp,          ");
		sbStr.append(" t_s_message_user tsmu,t_s_base_user tsbu,t_s_user tsu where                                                                                                          ");
		sbStr.append(" tpp.id = tsmu.message_id and tsbu.id=tsu.id                                                                                                                               ");
		sbStr.append(" and tsbu.id = tsmu.from_user_id                                                                                                                         ");
		sbStr.append(" and tpp.position_screenshot_url <> ''   and  tsmu.to_user_id =:userId )t order by t.create_time desc                                                                                   ");
		Query query = getSession().createSQLQuery(sbStr.toString()).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		query.setParameter("userId",userId);
		List<Map> dataList =   query.list();
		return dataList;
 	}
 	
}