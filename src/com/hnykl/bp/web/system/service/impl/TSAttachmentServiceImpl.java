package com.hnykl.bp.web.system.service.impl; 
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional; 
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl; 
import com.hnykl.bp.web.system.service.TSAttachmentServiceI;

@Service("tSAttachmentService")
@Transactional
public class TSAttachmentServiceImpl extends CommonServiceImpl implements TSAttachmentServiceI {
 	
}