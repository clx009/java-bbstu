package com.hnykl.bp.web.system.service.impl;

import java.sql.Timestamp; 
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.system.enums.PhoneValidateCodeCheckResult;
import com.hnykl.bp.web.system.pojo.base.TSPhoneValidateCode;
import com.hnykl.bp.web.system.service.PhoneValidateCodeService; 

@Service("phoneValidateCodeServiceImpl")
@Transactional
public class PhoneValidateCodeServiceImpl extends CommonServiceImpl implements PhoneValidateCodeService {

	@Override
	public PhoneValidateCodeCheckResult checkPhoneValidateCode(TSPhoneValidateCode tSPhoneValidateCode) {
		String query = "from TSPhoneValidateCode v where v.validateCode=:validateCode and v.mobilephone = :mobilephone ";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("validateCode", tSPhoneValidateCode.getValidateCode()); 
		queryObject.setParameter("mobilephone", tSPhoneValidateCode.getMobilephone()); 
		
		List<TSPhoneValidateCode> lst = queryObject.list();

		if (lst.isEmpty()) {
			return PhoneValidateCodeCheckResult.NOT_EXSITS;
		} else {
			TSPhoneValidateCode obj = lst.get(0); 
			Timestamp createTime=obj.getCreateTime();
			long btTime = System.currentTimeMillis() - createTime.getTime();
			long maxtime = BaseConfigMgr.getUserSmsSendMaxtime()*60*1000;
			if (btTime > maxtime) {
				return PhoneValidateCodeCheckResult.OVERDUE;
			} else {
				return PhoneValidateCodeCheckResult.SUCCESS;
			}
		}
	}

}
