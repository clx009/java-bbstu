package com.hnykl.bp.web.system.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.core.util.PasswordUtil;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.school.dto.SchoolOrderDTO;
import com.hnykl.bp.web.system.pojo.base.TSRoleUser;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.UserService;

/**
 * 
 * @author
 * 
 */
@Service("userService")
@Transactional
public class UserServiceImpl extends CommonServiceImpl implements UserService {

	public TSUser checkUserExits(TSUser user) {
		return this.commonDao.getUserByUserIdAndUserNameExits(user);
	}

	public String getUserRole(TSUser user) {
		return this.commonDao.getUserRole(user);
	}

	public void pwdInit(TSUser user, String newPwd) {
		this.commonDao.pwdInit(user, newPwd);
	}

	public int getUsersOfThisRole(String id) {
		Criteria criteria = getSession().createCriteria(TSRoleUser.class);
		criteria.add(Restrictions.eq("TSRole.id", id));
		int allCounts = ((Long) criteria.setProjection(Projections.rowCount())
				.uniqueResult()).intValue();
		return allCounts;
	}

	/**
	 * 第三方登录
	 * */
	@Override
	public TSUser checkUserExsits(String openId,String thirdpartyLoginType) {
		String query = "";
		if("1".equals(thirdpartyLoginType)){  
			query = "from TSUser u where u.openId = :openId ";
		}else{
			query = "from TSUser u where  u.wxOpenId=:openId ";
		}
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("openId", openId);
		@SuppressWarnings("unchecked")
		List<TSUser> lst = queryObject.list();
		if (lst.isEmpty()) {
			return null;
		} else {
			return (TSUser) lst.get(0);
		}
	}

	@Override
	public void changeProfile(TSUser user) {
		// TODO Auto-generated method stub
		String query = "from TSUser u where u.userName = :username ";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("username", user.getUserName());
		@SuppressWarnings("unchecked")
		List<TSUser> users = queryObject.list();

		if (!users.isEmpty()) {
			TSUser tsuser = users.get(0);
			if (user.getTimeZone() != null && !"".equals(user.getTimeZone()))
				tsuser.setTimeZone(user.getTimeZone());

			if (user.getNickname() != null && !"".equals(user.getNickname()))
				tsuser.setNickname(user.getNickname());

			if (user.getMessageAlert() != null
					&& !"".equals(user.getMessageAlert()))
				tsuser.setMessageAlert(user.getMessageAlert());

			if (user.getRealName() != null && !"".equals(user.getRealName()))
				tsuser.setRealName(user.getRealName());
			
			if (user.getEmail() != null && !"".equals(user.getEmail()))
				tsuser.setEmail(user.getEmail());
			
			if (user.getHeadPortraitUrl() != null && !"".equals(user.getHeadPortraitUrl()))
				tsuser.setHeadPortraitUrl(user.getHeadPortraitUrl());
			
			if (user.getFirstName() != null && !"".equals(user.getFirstName()))
				tsuser.setFirstName(user.getFirstName());
			
			if (user.getLastName() != null && !"".equals(user.getLastName()))
				tsuser.setLastName(user.getLastName());

			save(tsuser);
		}
	}

	@Override
	public void changeUserName(TSUser user, String newPhoneNumber) {
		// TODO Auto-generated method stub
		String query = "from TSUser u where u.userName = :username ";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("username", user.getUserName());
		@SuppressWarnings("unchecked")
		List<TSUser> users = queryObject.list();

		if (!users.isEmpty()) {
			TSUser tsuser = users.get(0);
			tsuser.setUserName(newPhoneNumber);
			tsuser.setModifyTime(new Date()); 
			save(tsuser);
		}
	}
	
	
	public void updateLastPosition(String userId,String lastLongitude,String lastLatitude,String timeZone,String createTime){
		if(StringUtils.isEmpty(userId))return;
		String query = "from TSUser u where u.id = :userId ";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("userId", userId);
		@SuppressWarnings("unchecked")
		List<TSUser> users = queryObject.list();

		if (!users.isEmpty()) {
			TSUser tsuser = users.get(0);
			tsuser.setLastLatitude(lastLatitude);
			tsuser.setLastLongitude(lastLongitude);
			tsuser.setLastPositionTimeZone(timeZone);
			tsuser.setPositionUploadTime( Timestamp.valueOf(createTime));
			tsuser.setModifyTime(new Date()); 
			save(tsuser);
		}
	}
	
	public void updateOpenId(String userId,String openId,String thirdpartyLoginType){
		if(StringUtils.isEmpty(userId))return;
		String query = "from TSUser u where u.id = :userId ";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("userId", userId);
		@SuppressWarnings("unchecked")
		List<TSUser> users = queryObject.list();

		if (!users.isEmpty()) {
			TSUser tsuser = users.get(0);
			tsuser.setThirdpartyLoginType(thirdpartyLoginType);
			if("1".equals(thirdpartyLoginType)){
				tsuser.setOpenId(openId);
			}else{
				tsuser.setWxOpenId(openId);
			}
			tsuser.setModifyTime(new Date()); 
			save(tsuser);
		}
	}
	
	
	public void pwdInitByUserId(TSUser user,String newPwd){
		this.commonDao.pwdInitByUserId(user, newPwd);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TSUser> findStudentListByUser(String userId,String type) {
		String sql = " SELECT tsbu.id AS 'id', tsbu.realname AS 'realName', tsu.nickname AS 'nickname', tsu.sex AS 'sex', tsu.email AS 'email', tsu.head_portrait_url AS 'headPortraitUrl', tsu.MOBILEPHONE AS 'mobilePhone'" +
				" FROM t_s_base_user tsbu " +
				" JOIN t_s_user tsu ON (tsbu.id = tsu.id) " + 
				" WHERE " +
				" tsbu.TYPE = :type and tsu.id IN ( SELECT user_id FROM t_f_family_user WHERE family_id IN " +
				" ( SELECT family_id FROM t_f_family_user WHERE user_id = :userId ) ) ";
		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql).addScalar("id").addScalar("nickname").addScalar("realName").addScalar("sex").addScalar("email").addScalar("headPortraitUrl").addScalar("mobilePhone");  
		sqlQuery.setParameter("type", type);
		sqlQuery.setParameter("userId", userId);
		sqlQuery.setResultTransformer(Transformers.aliasToBean(TSUser.class));
		
		return sqlQuery.list(); 
	} 
	
	@SuppressWarnings("unchecked")
	@Override
	public TSUser findUserInfo(String userId) {
		String sql = " SELECT * FROM t_s_user tsbu " +
		        " WHERE id = '"+userId+"' ";
		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.setResultTransformer(Transformers.aliasToBean(TSUser.class));
		return (TSUser) sqlQuery.list().get(0); 
		
		//return (TSUser) commonDao.findByQueryString(sql).get(0);
	} 
	
}
