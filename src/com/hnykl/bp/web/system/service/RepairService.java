package com.hnykl.bp.web.system.service;

import com.hnykl.bp.base.core.common.service.CommonService;

/** 
 * @Description 修复数据库Service
 * @ClassName: RepairService
 * @author 
 * @date 2013-7-19 下午01:31:00  
 */ 
public interface RepairService  extends CommonService{

	/** 
	 * @Description  修复数据库
	 * @author  2013-7-19  
	 */
	public void repair();

	/** 
	 * @Description  先清空数据库，然后再修复数据库
	 * @author  2013-7-19  
	 */
	  	
	public void deleteAndRepair();

}
