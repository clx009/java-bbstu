package com.hnykl.bp.web.system.dao.repair;

import com.hnykl.bp.base.minidao.annotation.MiniDao;

/**
 * 工程修复
 * @author 
 * @date 2013-11-11
 * @version 1.0
 */
@MiniDao
public interface RepairDao {
	
	public void batchRepairTerritory();

}
