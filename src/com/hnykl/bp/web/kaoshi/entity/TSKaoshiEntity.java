package com.hnykl.bp.web.kaoshi.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: t_s_kaoshi
 * @author onlineGenerator
 * @date 2016-10-05 16:47:17
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_kaoshi", schema = "")
@SuppressWarnings("serial")
public class TSKaoshiEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**subjectname*/
	private java.lang.String subjectName;
	/**testtime*/
	private java.lang.String testtime;
	/**score*/
	private java.lang.String score;
	/**userId*/
	private java.lang.String userId;
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  testtime
	 */
	@Column(name ="TESTTIME",nullable=false)
	public java.lang.String getTesttime(){
		return this.testtime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  testtime
	 */
	public void setTesttime(java.lang.String testtime){
		this.testtime = testtime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  score
	 */
	@Column(name ="SCORE",nullable=false)
	public java.lang.String getScore(){
		return this.score;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  score
	 */
	@Column(name ="subjectname",nullable=false)
	public java.lang.String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(java.lang.String subjectName) {
		this.subjectName = subjectName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  score
	 */
	public void setScore(java.lang.String score){
		this.score = score;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  userId
	 */
	@Column(name ="USER_ID",nullable=false)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  userId
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
}
