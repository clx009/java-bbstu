package com.hnykl.bp.web.kaoshi.service.impl;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.character.entity.TSCharactertestEntity;
import com.hnykl.bp.web.kaoshi.service.TSKaoshiServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.kaoshi.entity.TSKaoshiEntity;

import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("tSKaoshiService")
@Transactional
public class TSKaoshiServiceImpl extends CommonServiceImpl implements TSKaoshiServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((TSKaoshiEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((TSKaoshiEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((TSKaoshiEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TSKaoshiEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TSKaoshiEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TSKaoshiEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,TSKaoshiEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{subjectid}",String.valueOf(t.getSubjectName()));
 		sql  = sql.replace("#{testtime}",String.valueOf(t.getTesttime()));
 		sql  = sql.replace("#{score}",String.valueOf(t.getScore()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
 	
	public List<TSKaoshiEntity> findTestResults(String userId){
		String query = " select f.* from t_s_kaoshi f  where f.user_id = :userId order by subjectname";
		SQLQuery queryObject = getSession().createSQLQuery(query);
		queryObject.setParameter("userId", userId); 
		queryObject.addEntity(TSKaoshiEntity.class);
		List<TSKaoshiEntity> familys = queryObject.list(); 
		return familys;
	}
}