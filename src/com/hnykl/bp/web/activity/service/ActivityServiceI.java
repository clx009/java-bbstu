package com.hnykl.bp.web.activity.service;

import java.sql.Timestamp;
import java.util.List;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.activity.entity.ActivityEntity;

public interface ActivityServiceI extends CommonService {

	void modifyActivity(ActivityEntity entity);

	List<ActivityEntity> findActivity(String userId, Timestamp startTime, Timestamp endTime);

	void saveActivities(List<ActivityEntity> entities); 

	void modifyActivities(List<ActivityEntity> entities);

	void deleteActivities(List<String> ids);
	public void work();
}
