package com.hnykl.bp.web.activity.service.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.bean.BeanUtils;
import com.hnykl.bp.base.tool.date.DateUtil;
import com.hnykl.bp.easemob.server.api.SendMessageAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.TextMessageBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;
import com.hnykl.bp.web.activity.entity.ActivityEntity;
import com.hnykl.bp.web.activity.service.ActivityServiceI;
import com.hnykl.bp.web.family.service.TFMemberServiceI;
import com.hnykl.bp.web.position.service.TPPositionServiceI;
import com.hnykl.bp.web.system.entity.MessageEntity;
import com.hnykl.bp.web.system.entity.MessageUserEntity;
import com.hnykl.bp.web.system.service.MessageServiceI;

/**
 * 
 * @author Administrator
 * 
 */
@Service("activityService")
@Transactional
public class ActivityServiceImpl extends CommonServiceImpl implements
		ActivityServiceI {
	 private static Logger logger = Logger.getLogger(ActivityServiceImpl.class);
	EasemobRestAPIFactory factory = ClientContext.getInstance()
			.init(ClientContext.INIT_FROM_PROPERTIES).getAPIFactory();
	@Autowired
	private TFMemberServiceI tFMemberService;
	@Autowired
	private MessageServiceI messageService;
	@Autowired
	private TPPositionServiceI tPPositionService;
	@Override
	public void modifyActivity(ActivityEntity entity) {
		// TODO Auto-generated method stub
		ActivityEntity activity = this.commonDao.findUniqueByProperty(
				ActivityEntity.class, "id", entity.getId());
		BeanUtils.populateBean(activity, entity);
		this.save(activity);
	}

	@Override
	public List<ActivityEntity> findActivity(String userId,
			Timestamp startTime, Timestamp endTime) {

		String query = " from ActivityEntity a where a.userId = :userId and a.time between :startTime and :endTime ";
		Query hq = this.getSession().createQuery(query);
		hq.setParameter("userId", userId);
		hq.setParameter("startTime", startTime);
		hq.setParameter("endTime", endTime);
		@SuppressWarnings("unchecked")
		List<ActivityEntity> list = hq.list();
		return list;

	}

	public List<ActivityEntity> findTodayActivity() {
		
		String query = " select user_id userId,name,time,cn_time cnTime,remind_time remindTime,type,address,longitude,latitude  from t_a_activity a where DATE_FORMAT(a.time,'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d') ";
		Query hq = this.getSession().createSQLQuery(query).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map> list = hq.list();
		Map dataMap = new HashMap();
		ActivityEntity activityEntity = null;
		List<ActivityEntity> activityEntityList = new ArrayList<ActivityEntity>();
		for(int i=0;list != null && i<list.size();i++){
			activityEntity = new ActivityEntity();
			BeanUtils.populate(activityEntity, list.get(i));
			activityEntityList.add(activityEntity);
		}
		return activityEntityList;
	}
	
	@Override
	public void saveActivities(List<ActivityEntity> entities) {
		for (ActivityEntity entity : entities) {
			this.save(entity);
		}
	}

	@Override
	public void modifyActivities(List<ActivityEntity> entities) {
		for (ActivityEntity entity : entities) {
			if(entity.getId()==null||"".equals(entity.getId())){//新增
				this.save(entity);
			}else{//修改
				ActivityEntity activity = this.commonDao.findUniqueByProperty(
						ActivityEntity.class, "id", entity.getId());
				BeanUtils.populateBean(activity, entity);
				this.save(activity);
			}
		}
	}

	@Override
	public void deleteActivities(List<String> ids) {
		// TODO Auto-generated method stub
		for(String id:ids){
			this.deleteEntityById(ActivityEntity.class, id);
		}
	}
	//定时处理活动
	public void work(){
		List<ActivityEntity> activityEntityList = findTodayActivity();
		logger.info("=========>start job,today activity size:"+((activityEntityList != null)?activityEntityList.size():0));
		ActivityEntity activityEntity = null;
		SendMessageAPI senMessage = (SendMessageAPI) factory
				.newInstance(EasemobRestAPIFactory.SEND_MESSAGE_CLASS);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(int i=0;activityEntityList != null && i<activityEntityList.size();i++){
			activityEntity = activityEntityList.get(i);
			long l = DateUtil.getDistanceMinutes(new Date(activityEntity.getTime().getTime()),new Date());
			logger.info("=====>activityName:"+activityEntity.getName()+",activityEntity.getTime():"+activityEntity.getTime()+",activityEntity.getRemindTime():"+activityEntity.getRemindTime()+",distanceMinutes:"+l);
			if(l > 0 && l == activityEntity.getRemindTime() ){//发消息
				List<Map> familyDataMap = tFMemberService.findAdultMemberByUserId(activityEntity.getUserId());
				MessageEntity messageEntity = new MessageEntity(activityEntity.getName(),
						sdf.format(activityEntity.getTime())+" "+activityEntity.getAddress()+"-"+activityEntity.getName(),new Timestamp(new Date().getTime()),activityEntity.getUserId(),
						"1");
				messageService.save(messageEntity);
				Map<String, String> ext = new HashMap<String, String>();
				ext.put("action", "activityRemind");
				ext.put("activityName", activityEntity.getName());
				ext.put("activityTime", sdf.format(activityEntity.getTime()));
				ext.put("activityAddress", activityEntity.getAddress());
				ext.put("longitude", activityEntity.getLongitude());
				ext.put("latitude", activityEntity.getLatitude());
				
				String[] targets = new String[familyDataMap.size()];
				MessageUserEntity messageUserEntity = new MessageUserEntity();
				List<MessageUserEntity> messageUserEntityList = new ArrayList<MessageUserEntity>();
				for(int j=0;j<familyDataMap.size();j++){
					targets[j] = (String)familyDataMap.get(j).get("username");
					messageUserEntity = new MessageUserEntity();
					messageUserEntity.setMessageId(messageEntity.getId());
					messageUserEntity.setFromUserId(activityEntity.getUserId() );
					messageUserEntity.setToUserId((String)familyDataMap.get(j).get("userId"));
					messageUserEntityList.add(messageUserEntity);
				} 
				tPPositionService.batchSave(messageUserEntityList);
				BodyWrapper textMessageBody = new TextMessageBody("users",
						targets, null, ext, ""); 
				@SuppressWarnings("unused")
				ResponseWrapper sendMessage = (ResponseWrapper) senMessage
						.sendMessage(textMessageBody);
			}
		}
		logger.info("=========>end job");
	}
}
