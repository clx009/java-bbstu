package com.hnykl.bp.web.activity.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.hnykl.bp.base.core.common.entity.IdEntity;

/**
 * 
 * @author Administrator
 * 
 */
@Entity
@Table(name = "t_a_activity") 
public class ActivityEntity extends IdEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private String name;
	private Timestamp time;
	private Timestamp cnTime;
	private double remindTime;
	private String remindTimeUnit;
	private String type;
	private String address;
	private String longitude;
	private String latitude;
	private String remindSwitch;
	private String arriveRemindSwitch;

	public ActivityEntity() {
		super();
	}

	public ActivityEntity(String userId, String name, Timestamp time,
			double remindTime, String remindTimeUnit, String type) {
		super();
		this.userId = userId;
		this.name = name;
		this.time = time;
		this.remindTime = remindTime;
		this.remindTimeUnit = remindTimeUnit;
		this.type = type;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "time")
	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "cn_time")
	public Timestamp getCnTime() {
		return cnTime;
	}

	public void setCnTime(Timestamp cnTime) {
		this.cnTime = cnTime;
	}
	
	
	@Column(name = "remind_time")
	public double getRemindTime() {
		return remindTime;
	}

	public void setRemindTime(double remindTime) {
		this.remindTime = remindTime;
	}

	@Column(name = "remind_time_unit")
	public String getRemindTimeUnit() {
		return remindTimeUnit;
	}

	public void setRemindTimeUnit(String remindTimeUnit) {
		this.remindTimeUnit = remindTimeUnit;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name="address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name="longitude")
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name="latitude")
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name="remind_switch")
	public String getRemindSwitch() {
		return remindSwitch;
	}

	public void setRemindSwitch(String remindSwitch) {
		this.remindSwitch = remindSwitch;
	}

	@Column(name="arrive_remind_switch")
	public String getArriveRemindSwitch() {
		return arriveRemindSwitch;
	}

	public void setArriveRemindSwitch(String arriveRemindSwitch) {
		this.arriveRemindSwitch = arriveRemindSwitch;
	}
	
	

}
