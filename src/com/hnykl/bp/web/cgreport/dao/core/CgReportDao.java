package com.hnykl.bp.web.cgreport.dao.core;

import java.util.List;
import java.util.Map;

import com.hnykl.bp.base.minidao.annotation.Arguments;
import com.hnykl.bp.base.minidao.annotation.MiniDao;

/**
 * 
 * @author zhangdaihao
 *
 */
@MiniDao
public interface CgReportDao{

	@Arguments("configId")
	List<Map<String,Object>> queryCgReportItems(String configId);
	
	@Arguments("id")
	Map queryCgReportMainConfig(String id);
}
