package com.hnykl.bp.web.plan.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: t_s_plan
 * @author onlineGenerator
 * @date 2016-10-07 19:58:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_plan", schema = "")
@SuppressWarnings("serial")
public class TSPlanEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**userId*/
	private java.lang.String userId;
	/**entrancescore*/
	private java.lang.String entrancescore;
	/**entrancedesc*/
	private java.lang.String entrancedesc;
	/**teacher*/
	private java.lang.String teacher;
	/**totaldesc*/
	private java.lang.String totaldesc;
	private java.lang.String season;
	
	private java.lang.String path;
	private java.lang.String note;
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  userId
	 */
	@Column(name ="USER_ID",nullable=false)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  userId
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  entrancescore
	 */
	@Column(name ="ENTRANCESCORE",nullable=false)
	public java.lang.String getEntrancescore(){
		return this.entrancescore;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  entrancescore
	 */
	public void setEntrancescore(java.lang.String entrancescore){
		this.entrancescore = entrancescore;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  entrancedesc
	 */
	@Column(name ="ENTRANCEDESC",nullable=false)
	public java.lang.String getEntrancedesc(){
		return this.entrancedesc;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  entrancedesc
	 */
	public void setEntrancedesc(java.lang.String entrancedesc){
		this.entrancedesc = entrancedesc;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  teacher
	 */
	@Column(name ="TEACHER",nullable=false)
	public java.lang.String getTeacher(){
		return this.teacher;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  teacher
	 */
	public void setTeacher(java.lang.String teacher){
		this.teacher = teacher;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  totaldesc
	 */
	@Column(name ="TOTALDESC",nullable=false)
	public java.lang.String getTotaldesc(){
		return this.totaldesc;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  totaldesc
	 */
	public void setTotaldesc(java.lang.String totaldesc){
		this.totaldesc = totaldesc;
	}
	@Column(name ="SEASON",nullable=false)
	public java.lang.String getSeason() {
		return season;
	}

	public void setSeason(java.lang.String season) {
		this.season = season;
	}
	@Column(name ="PATH",nullable=false)
	public java.lang.String getPath() {
		return path;
	}

	public void setPath(java.lang.String path) {
		this.path = path;
	}
	@Column(name ="NOTE",nullable=false)
	public java.lang.String getNote() {
		return note;
	}

	public void setNote(java.lang.String note) {
		this.note = note;
	}
	
	
}
