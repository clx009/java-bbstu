package com.hnykl.bp.web.plan.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: t_s_plan_content
 * @author onlineGenerator
 * @date 2016-10-07 19:58:37
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_plan_content", schema = "")
@SuppressWarnings("serial")
public class TSPlanContentEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**userId*/
	private java.lang.String userId;
	/**学年*/
	private java.lang.String schoolYear;
	/**学期*/
	private java.lang.String term;
	/**name*/
	private java.lang.String name;
	/**entrancescore*/
	private java.lang.String entrancescore;
	/**teacher*/
	private java.lang.String teacher;
	/**result*/
	private java.lang.String result;
	
	private String path;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  userId
	 */
	@Column(name ="USER_ID",nullable=false)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  userId
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学年
	 */
	@Column(name ="SCHOOL_YEAR",nullable=false)
	public java.lang.String getSchoolYear(){
		return this.schoolYear;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学年
	 */
	public void setSchoolYear(java.lang.String schoolYear){
		this.schoolYear = schoolYear;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学期,外键关联到数据字典
	 */
	@Column(name ="TERM",nullable=false)
	public java.lang.String getTerm(){
		return this.term;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学期,外键关联到数据字典
	 */
	public void setTerm(java.lang.String term){
		this.term = term;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  name
	 */
	@Column(name ="NAME",nullable=false)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  name
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  entrancescore
	 */
	@Column(name ="ENTRANCESCORE",nullable=false)
	public java.lang.String getEntrancescore(){
		return this.entrancescore;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  entrancescore
	 */
	public void setEntrancescore(java.lang.String entrancescore){
		this.entrancescore = entrancescore;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  teacher
	 */
	@Column(name ="TEACHER",nullable=false)
	public java.lang.String getTeacher(){
		return this.teacher;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  teacher
	 */
	public void setTeacher(java.lang.String teacher){
		this.teacher = teacher;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  result
	 */
	@Column(name ="RESULT",nullable=false)
	public java.lang.String getResult(){
		return this.result;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  result
	 */
	public void setResult(java.lang.String result){
		this.result = result;
	}

	
	@Column(name ="PATH",nullable=false)
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
