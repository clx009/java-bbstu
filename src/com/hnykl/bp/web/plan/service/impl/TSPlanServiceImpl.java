package com.hnykl.bp.web.plan.service.impl;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.plan.service.TSPlanServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.plan.entity.TSPlanEntity;
import com.hnykl.bp.web.shijian.entity.TSShsjEntity;

import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("tSPlanService")
@Transactional
public class TSPlanServiceImpl extends CommonServiceImpl implements TSPlanServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((TSPlanEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((TSPlanEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((TSPlanEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TSPlanEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TSPlanEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TSPlanEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,TSPlanEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{user_id}",String.valueOf(t.getUserId()));
 		sql  = sql.replace("#{entrancescore}",String.valueOf(t.getEntrancescore()));
 		sql  = sql.replace("#{entrancedesc}",String.valueOf(t.getEntrancedesc()));
 		sql  = sql.replace("#{teacher}",String.valueOf(t.getTeacher()));
 		sql  = sql.replace("#{totaldesc}",String.valueOf(t.getTotaldesc()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}

	@Override
	public TSPlanEntity findStudyPlan(String userId) {
		String query = " select f.* from t_s_plan f  where f.user_id = :userId ";
		SQLQuery queryObject = getSession().createSQLQuery(query);
		queryObject.setParameter("userId", userId); 
		queryObject.addEntity(TSPlanEntity.class);
		List<TSPlanEntity> results = queryObject.list(); 
		if(null!=results&&results.size()>0)
		      return results.get(0);
		else
			return new TSPlanEntity();
	}
 	
 	
}