package com.hnykl.bp.web.plan.service.impl;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.plan.service.TSPlanContentServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.plan.entity.TSPlanContentEntity;
import com.hnykl.bp.web.plan.entity.TSPlanEntity;

import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("tSPlanContentService")
@Transactional
public class TSPlanContentServiceImpl extends CommonServiceImpl implements TSPlanContentServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((TSPlanContentEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((TSPlanContentEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((TSPlanContentEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TSPlanContentEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TSPlanContentEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TSPlanContentEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,TSPlanContentEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{user_id}",String.valueOf(t.getUserId()));
 		sql  = sql.replace("#{name}",String.valueOf(t.getName()));
 		sql  = sql.replace("#{result}",String.valueOf(t.getResult()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}

	@Override
	public List<TSPlanContentEntity> findStudyPlanContents(String userId) {
		String query = " select f.* from t_s_plan_content f  where f.user_id = :userId ";
		SQLQuery queryObject = getSession().createSQLQuery(query);
		queryObject.setParameter("userId", userId); 
		queryObject.addEntity(TSPlanContentEntity.class);
		List<TSPlanContentEntity> results = queryObject.list(); 
		return results;
	}
 	
 	
}