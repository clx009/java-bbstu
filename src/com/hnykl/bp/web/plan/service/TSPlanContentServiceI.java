package com.hnykl.bp.web.plan.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.plan.entity.TSPlanContentEntity;
import com.hnykl.bp.web.plan.entity.TSPlanEntity;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SQLQuery;

public interface TSPlanContentServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TSPlanContentEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TSPlanContentEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TSPlanContentEntity t);
 	

	public List<TSPlanContentEntity> findStudyPlanContents(String userId) ;
 	
}
