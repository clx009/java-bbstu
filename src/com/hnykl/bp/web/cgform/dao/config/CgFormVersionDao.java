package com.hnykl.bp.web.cgform.dao.config;

import com.hnykl.bp.web.cgform.entity.config.CgFormHeadEntity;

import com.hnykl.bp.base.minidao.annotation.Arguments;
import com.hnykl.bp.base.minidao.annotation.MiniDao;
/**
 * 
 * @Title:CgFormFieldDao
 * @description:
 * @author 
 * @date Aug 24, 2013 11:33:33 AM
 * @version V1.0
 */
@MiniDao
public interface CgFormVersionDao {
	@Arguments("tableName")
	public String  getCgFormVersionByTableName(String tableName);
	@Arguments("id")
	public String  getCgFormVersionById(String id);
	@Arguments({"newVersion","formId"})
	public void  updateVersion(String newVersion,String formId);
	
	@Arguments({"id"})
	public CgFormHeadEntity  getCgFormById(String id);
}
