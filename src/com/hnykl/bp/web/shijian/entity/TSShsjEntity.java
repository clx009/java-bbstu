package com.hnykl.bp.web.shijian.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: t_s_shsj
 * @author onlineGenerator
 * @date 2016-10-07 16:33:37
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_s_shsj", schema = "")
@SuppressWarnings("serial")
public class TSShsjEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**type*/
	private java.lang.String type;
	/**name*/
	private java.lang.String name;
	/**description*/
	private java.lang.String description;
	/**createtime*/
	private java.lang.String createtime;
	private java.lang.String endtime;
	/**result*/
	private java.lang.String result;
	/**userId*/
	private java.lang.String userId;
	
	private String path;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  type
	 */
	@Column(name ="TYPE",nullable=false)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  type
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  name
	 */
	@Column(name ="NAME",nullable=false)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  name
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  description
	 */
	@Column(name ="DESCRIPTION",nullable=false)
	public java.lang.String getDescription(){
		return this.description;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  description
	 */
	public void setDescription(java.lang.String description){
		this.description = description;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  createtime
	 */
	@Column(name ="CREATETIME",nullable=false)
	public java.lang.String getCreatetime(){
		return this.createtime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  createtime
	 */
	public void setCreatetime(java.lang.String createtime){
		this.createtime = createtime;
	}
	
	@Column(name ="ENDTIME",nullable=false)
	public java.lang.String getEndtime() {
		return endtime;
	}

	public void setEndtime(java.lang.String endtime) {
		this.endtime = endtime;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  result
	 */
	@Column(name ="RESULT",nullable=false)
	public java.lang.String getResult(){
		return this.result;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  result
	 */
	public void setResult(java.lang.String result){
		this.result = result;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  userId
	 */
	@Column(name ="USER_ID",nullable=false)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  userId
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	
	@Column(name ="PATH",nullable=false)
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
