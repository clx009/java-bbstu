package com.hnykl.bp.web.demo.page;

import java.util.ArrayList;
import java.util.List;

import com.hnykl.bp.web.demo.entity.test.BpOrderCustomEntity;
import com.hnykl.bp.web.demo.entity.test.BpOrderProductEntity;

/**   
 * @Title: Entity
 * @Description: 订单信息 VO
 * @author 
 * @date 2013-03-19 22:01:34
 * @version V1.0   
 *
 */
@SuppressWarnings("serial")
public class BpOrderMainPage implements java.io.Serializable {
	/**订单客户明细*/
	private List<BpOrderCustomEntity> bpOrderCustomList = new ArrayList<BpOrderCustomEntity>();
	public List<BpOrderCustomEntity> getBpOrderCustomList() {
		return bpOrderCustomList;
	}
	public void setBpOrderCustomList(List<BpOrderCustomEntity> bpOrderCustomList) {
		this.bpOrderCustomList = bpOrderCustomList;
	}
	/**订单产品明细*/
	private List<BpOrderProductEntity> bpOrderProductList = new ArrayList<BpOrderProductEntity>();
	public List<BpOrderProductEntity> getBpOrderProductList() {
		return bpOrderProductList;
	}
	public void setBpOrderProductList(List<BpOrderProductEntity> bpOrderProductList) {
		this.bpOrderProductList = bpOrderProductList;
	}
}
