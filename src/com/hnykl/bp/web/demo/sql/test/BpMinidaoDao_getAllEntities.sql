SELECT * FROM bp_minidao WHERE 1=1
<#if bpMinidao.userName ?exists && bpMinidao.userName ?length gt 0>
	and user_name = :bpMinidao.userName
</#if>
<#if bpMinidao.mobilePhone ?exists && bpMinidao.mobilePhone ?length gt 0>
	and mobile_phone = :bpMinidao.mobilePhone
</#if>
<#if bpMinidao.officePhone ?exists && bpMinidao.officePhone ?length gt 0>
	and office_phone = :bpMinidao.officePhone
</#if>
<#if bpMinidao.email ?exists && bpMinidao.email ?length gt 0>
	and email = :bpMinidao.email
</#if>
<#if bpMinidao.age ?exists && bpMinidao.age ?length gt 0>
	and age = :bpMinidao.age
</#if>
<#if bpMinidao.salary ?exists && bpMinidao.salary ?length gt 0>
	and salary = :bpMinidao.salary
</#if>
<#if bpMinidao.sex ?exists && bpMinidao.sex ?length gt 0>
	and sex = :bpMinidao.sex
</#if>
<#if bpMinidao.status ?exists && bpMinidao.status ?length gt 0>
	and status = :bpMinidao.status
</#if>
