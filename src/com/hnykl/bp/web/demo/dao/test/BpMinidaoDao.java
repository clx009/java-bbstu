package com.hnykl.bp.web.demo.dao.test;

import java.util.List;
import java.util.Map;

import com.hnykl.bp.base.minidao.annotation.Arguments;
import com.hnykl.bp.base.minidao.annotation.MiniDao;
import com.hnykl.bp.base.minidao.annotation.ResultType;
import com.hnykl.bp.base.minidao.annotation.Sql;
import com.hnykl.bp.base.minidao.hibernate.MiniDaoSupportHiber;
import com.hnykl.bp.web.demo.entity.test.BpMinidaoEntity;

/**
 * Minidao例子
 * @author fancq
 * 
 */
@MiniDao
public interface BpMinidaoDao extends MiniDaoSupportHiber<BpMinidaoEntity> {
	@Arguments({"bpMinidao", "page", "rows"})
	public List<Map> getAllEntities(BpMinidaoEntity bpMinidao, int page, int rows);

	@Arguments({"bpMinidao", "page", "rows"})
	@ResultType("com.hnykl.bp.web.demo.entity.test.BpMinidaoEntity")
	public List<BpMinidaoEntity> getAllEntities2(BpMinidaoEntity bpMinidao, int page, int rows);

	//@Arguments("id")
	//BpMinidaoEntity getBpMinidao(String id);

	@Sql("SELECT count(*) FROM bp_minidao")
	Integer getCount();

	@Sql("SELECT SUM(salary) FROM bp_minidao")
	Integer getSumSalary();

	/*@Arguments("bpMinidao")
	int update(BpMinidaoEntity bpMinidao);

	@Arguments("bpMinidao")
	void insert(BpMinidaoEntity bpMinidao);

	@Arguments("bpMinidao")
	void delete(BpMinidaoEntity bpMinidao);*/
}
