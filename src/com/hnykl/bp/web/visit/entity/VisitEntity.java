package com.hnykl.bp.web.visit.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import com.hnykl.bp.web.system.pojo.base.TSUser;

/**
 * @Title: Entity
 * @Description: 访问管里
 * @author onlineGenerator
 * @date 2016-06-17 16:10:30
 * @version V1.0
 * 
 */
@Entity
@Table(name = "t_v_visit", schema = "")
@SuppressWarnings("serial")
public class VisitEntity implements java.io.Serializable {
	/** id */
	private java.lang.String id; 
	/**标题*/
	private String title;
	/** 补充 */
	private java.lang.String addenda;
	/** 期望访问时间 */
	private Date expectTime;
	/** 创建时间 */
	private Date createTime;
	/** 创建人id，外键关联到t_s_user.id */
	private TSUser creator = new TSUser();
	//委托人
//	private TSUser client = new TSUser();
	private String client;
	/** 状态：0-待接单，1-已接单，2-已访问，3，已归档 */
	private java.lang.String status;
	/** 类型：1-家访，2-校访，3-专访（专家咨询） **/
	//private TSType type = new TSType();
	private String type;
	
	private String addendaResult;

	private String interviewee;

	private Date visitTime;

	List<VisitOptionEntity> visitOptions = new ArrayList<VisitOptionEntity>();

	public VisitEntity() {
		super();
	} 
	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", nullable = false)
	public java.lang.String getId() {
		return this.id;
	}
	
	
	
	@Column(name = "title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 补充
	 */
	@Column(name = "ADDENDA", nullable = false)
	public java.lang.String getAddenda() {
		return this.addenda;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 补充
	 */
	public void setAddenda(java.lang.String addenda) {
		this.addenda = addenda;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 期望访问时间
	 */
	@Column(name = "EXPECT_TIME", nullable = false)
	public Date getExpectTime() {
		return this.expectTime;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 期望访问时间
	 */
	public void setExpectTime(Date expectTime) {
		this.expectTime = expectTime;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 创建时间
	 */
	@Column(name = "CREATE_TIME", nullable = false)
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
 

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 状态：0-待接单，1-已接单，2-已访问，3，已归档
	 */
	@Column(name = "STATUS", nullable = false)
	public java.lang.String getStatus() {
		return this.status;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 状态：0-待接单，1-已接单，2-已访问，3，已归档
	 */
	public void setStatus(java.lang.String status) {
		this.status = status;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "addenda_result")
	public String getAddendaResult() {
		return addendaResult;
	}

	public void setAddendaResult(String addendaResult) {
		this.addendaResult = addendaResult;
	}

	@Column(name = "interviewee")
	public String getInterviewee() {
		return interviewee;
	}

	public void setInterviewee(String interviewee) {
		this.interviewee = interviewee;
	}

	@Column(name = "visit_time")
	public Date getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}  

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "visit")
	public List<VisitOptionEntity> getVisitOptions() {
		return visitOptions;
	}

	public void setVisitOptions(List<VisitOptionEntity> visitOptions) {
		this.visitOptions = visitOptions;
	}

	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name="creator_id")
	public TSUser getCreator() {
		if(creator == null) return new TSUser();
		return creator;
	}

	public void setCreator(TSUser creator) {
		this.creator = creator;
	}
	
	@Column(name="client")
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	

	

}
