package com.hnykl.bp.web.visit.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hnykl.bp.web.system.pojo.base.TSAttachment;
import com.hnykl.bp.web.system.pojo.base.TSType;

@Entity
@Table(name = "t_v_material", schema = "")
@SuppressWarnings("serial")
public class MaterialEntity extends TSAttachment implements Serializable {
	private TSType TSType;//文档分类
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "type_id",referencedColumnName="TYPECODE") 
	public TSType getTSType() {
		return TSType;
	}
	public void setTSType(TSType tSType) {
		TSType = tSType;
	}

}
