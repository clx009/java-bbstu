package com.hnykl.bp.web.visit.entity;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.JoinColumn; 
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

/**   
 * @Title: Entity
 * @Description: 访问关注内容项管理
 * @author onlineGenerator
 * @date 2016-06-17 16:09:48
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_v_visit_option", schema = "")
@SuppressWarnings("serial")
public class VisitOptionEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**visitId*/
	private VisitEntity visit;
	/**optionId*/
	private OptionEntity option;
	
	
	
	
	public VisitOptionEntity() {
		super();
	}
 

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
 
	 

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "option_id")
	public OptionEntity getOption() {
		return option;
	}

	public void setOption(OptionEntity option) {
		this.option = option;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "visit_id")
	public VisitEntity getVisit() {
		return visit;
	}


	public void setVisit(VisitEntity visit) {
		this.visit = visit;
	}
	
	
}
