package com.hnykl.bp.web.visit.entity;

 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

/**   
 * @Title: Entity
 * @Description: 访问关注内容项管理
 * @author onlineGenerator
 * @date 2016-06-17 17:19:16
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_v_option", schema = "")
@SuppressWarnings("serial")
public class OptionEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**名称*/
	private java.lang.String name;
	/**描述*/
	private java.lang.String description;
	/**类型：1-家访，2-校访，3-专访（专家咨询）*/
	private java.lang.String type;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  名称
	 */
	@Column(name ="NAME",nullable=false)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  描述
	 */
	@Column(name ="DESCRIPTION",nullable=false)
	public java.lang.String getDescription(){
		return this.description;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  描述
	 */
	public void setDescription(java.lang.String description){
		this.description = description;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  类型：1-家访，2-校访，3-专访（专家咨询）
	 */
	@Column(name ="TYPE",nullable=false)
	public java.lang.String getType(){
		return this.type;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  类型：1-家访，2-校访，3-专访（专家咨询）
	 */
	public void setType(java.lang.String type){
		this.type = type;
	}
}
