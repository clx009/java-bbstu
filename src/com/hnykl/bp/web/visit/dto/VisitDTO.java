package com.hnykl.bp.web.visit.dto;

import java.util.Date; 
import java.util.List;
 
import javax.persistence.Id;

import com.hnykl.bp.web.visit.entity.OptionEntity; 
 
public class VisitDTO {
	/** id */
	private java.lang.String id;
	/** 委托人id，外键：关联到t_s_user.id */
	private java.lang.String clientId;
	/** 委托时间 */
	private Date entrustTime;
	/** 补充 */
	private java.lang.String addenda;
	/** 期望访问时间 */
	private Date expectTime;
	/** 创建时间 */
	private Date createTime;
	/** 创建人id，外键关联到t_s_user.id */
	private java.lang.String creatorId;
	/** 状态：0-待接单，1-已接单，2-已访问，3，已归档 */
	private java.lang.String status;
	/** 类型：1-家访，2-校访，3-专访（专家咨询） **/
	private java.lang.String type;
	
	
	private String addendaResult;

	private String interviewee;
	
	private Date visitTime;
	
	/**
	 * 访问关注内容
	 */
	private List<OptionEntity> visitOptions;

	public VisitDTO() {
		super();
	}
 
	
	
	public VisitDTO(String id, String clientId, Date entrustTime,
			String addenda, Date expectTime, Date createTime,
			String creatorId, String status, String type, String addendaResult,
			String interviewee, Date visitTime, List<OptionEntity> visitOptions) {
		super();
		this.id = id;
		this.clientId = clientId;
		this.entrustTime = entrustTime;
		this.addenda = addenda;
		this.expectTime = expectTime;
		this.createTime = createTime;
		this.creatorId = creatorId;
		this.status = status;
		this.type = type;
		this.addendaResult = addendaResult;
		this.interviewee = interviewee;
		this.visitTime = visitTime;
		this.visitOptions = visitOptions;
	}


	@Id
	public java.lang.String getId() {
		return id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getClientId() {
		return clientId;
	}

	public void setClientId(java.lang.String clientId) {
		this.clientId = clientId;
	}

	public Date getEntrustTime() {
		return entrustTime;
	}

	public void setEntrustTime(Date entrustTime) {
		this.entrustTime = entrustTime;
	}

	public java.lang.String getAddenda() {
		return addenda;
	}

	public void setAddenda(java.lang.String addenda) {
		this.addenda = addenda;
	}

	public Date getExpectTime() {
		return expectTime;
	}

	public void setExpectTime(Date expectTime) {
		this.expectTime = expectTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public java.lang.String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(java.lang.String creatorId) {
		this.creatorId = creatorId;
	}

	public java.lang.String getStatus() {
		return status;
	}

	public void setStatus(java.lang.String status) {
		this.status = status;
	}

	public java.lang.String getType() {
		return type;
	}

	public void setType(java.lang.String type) {
		this.type = type;
	}

	public List<OptionEntity> getVisitOptions() {
		return visitOptions;
	}

	public void setVisitOptions(List<OptionEntity> visitOptions) {
		this.visitOptions = visitOptions;
	}

	public String getAddendaResult() {
		return addendaResult;
	}

	public void setAddendaResult(String addendaResult) {
		this.addendaResult = addendaResult;
	}

	public String getInterviewee() {
		return interviewee;
	}

	public void setInterviewee(String interviewee) {
		this.interviewee = interviewee;
	}

	public Date getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}
	
	

}
