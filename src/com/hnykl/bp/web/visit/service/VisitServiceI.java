package com.hnykl.bp.web.visit.service;

import com.hnykl.bp.base.core.common.service.CommonService; 
import com.hnykl.bp.web.visit.entity.OptionEntity;
import com.hnykl.bp.web.visit.entity.VisitEntity;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface VisitServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(VisitEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(VisitEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(VisitEntity t);
 	 

	public void createVisit(VisitEntity visit);
	
	public List<Map> statisticsVisit(String userId);
	
	public List<OptionEntity> findOptions(String type);
	
	public List<VisitEntity> findVisitList(VisitEntity visitEntity);

	public List<VisitEntity> findVisitListByUserIds(String[] studentIds, String type);

	public void appointHandler(VisitEntity t);
 
}
