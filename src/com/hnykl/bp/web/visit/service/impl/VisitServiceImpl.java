package com.hnykl.bp.web.visit.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.rt.entity.RealTimeEntity;
import com.hnykl.bp.web.rt.service.RealTimeServiceI;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.UserService;
import com.hnykl.bp.web.visit.entity.OptionEntity;
import com.hnykl.bp.web.visit.entity.VisitEntity;
import com.hnykl.bp.web.visit.entity.VisitOptionEntity;
import com.hnykl.bp.web.visit.service.OptionServiceI;
import com.hnykl.bp.web.visit.service.VisitOptionServiceI;
import com.hnykl.bp.web.visit.service.VisitServiceI;

@Service("visitService")
@Transactional
public class VisitServiceImpl extends CommonServiceImpl implements
		VisitServiceI {

	@Autowired
	private VisitOptionServiceI visitOptionService;
	@Autowired
	OptionServiceI optionService;
	@Autowired
	private UserService userService;
	@Autowired
	RealTimeServiceI realTimeService;

	public <T> void delete(T entity) {
		super.delete(entity);
		// 执行删除操作配置的sql增强
		this.doDelSql((VisitEntity) entity);
	}

	public <T> Serializable save(T entity) {
		Serializable t = super.save(entity);
		// 执行新增操作配置的sql增强
		this.doAddSql((VisitEntity) entity);
		return t;
	}

	public <T> void saveOrUpdate(T entity) {
		super.saveOrUpdate(entity);
		// 执行更新操作配置的sql增强
		this.doUpdateSql((VisitEntity) entity);
	}

	/**
	 * 默认按钮-sql增强-新增操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doAddSql(VisitEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-更新操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doUpdateSql(VisitEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-删除操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doDelSql(VisitEntity t) {
		return true;
	}

	/**
	 * 替换sql中的变量
	 * 
	 * @param sql
	 * @return
	 */
	public String replaceVal(String sql, VisitEntity t) {
		sql = sql.replace("#{id}", String.valueOf(t.getId()));
		sql = sql.replace("#{addenda}", String.valueOf(t.getAddenda()));
		sql = sql.replace("#{expect_time}", String.valueOf(t.getExpectTime()));
		sql = sql.replace("#{create_time}", String.valueOf(t.getCreateTime()));
		sql = sql.replace("#{status}", String.valueOf(t.getStatus()));
		sql = sql.replace("#{UUID}", UUID.randomUUID().toString());
		return sql;
	}

	@Override
	public void createVisit(VisitEntity visit) {
		List<VisitOptionEntity> visitOptions = visit.getVisitOptions();
		visit.setVisitOptions(null);
		String visitId = (String) this.save(visit);

		String optStr = "";
		for (VisitOptionEntity v : visitOptions) {
			VisitEntity visitEntity = new VisitEntity();
			visitEntity.setId(visitId);
			v.setVisit(visitEntity);
			visitOptionService.save(v);
			OptionEntity optionEntity = optionService.get(OptionEntity.class, v.getOption().getId());
			optStr+=optionEntity.getName()+"、";
		}
		
		TSUser user = (TSUser)userService.getEntity(TSUser.class, visit.getCreator().getId());
		RealTimeEntity realTimeEntity = new RealTimeEntity();
		realTimeEntity.setBusinessId(visit.getId());
		realTimeEntity.setTableName("VisitEntity"); 
		realTimeEntity.setType("2");
		realTimeEntity.setCreateTime(new Date());
		realTimeEntity.setUserId(visit.getCreator().getId());
		if("1".equals(visit.getType())){
			realTimeEntity.setTitle("家访申请");
			realTimeEntity.setContent(user.getNickname()+" 申请了家访，包含的项目有："+optStr);
		}
		if("2".equals(visit.getType())){
			realTimeEntity.setTitle("校访申请");
			realTimeEntity.setContent(user.getNickname()+" 申请了校访，包含的项目有："+optStr);
		}
		if("3".equals(visit.getType())){
			realTimeEntity.setTitle("专家咨询申请");
			realTimeEntity.setContent(user.getNickname()+" 申请了专家咨询，包含的项目有："+optStr);
		}
		
		realTimeService.save(realTimeEntity);
	}

	public List<OptionEntity> findOptions(String type) {
		String hql = "from OptionEntity where type=:type ";
		Query queryObject = getSession().createQuery(hql);
		queryObject.setParameter("type", type);
		return queryObject.list();
	}

	public List<VisitEntity> findVisitList(VisitEntity visitEntity) {
		if (visitEntity.getType() == null)
			return new ArrayList<VisitEntity>();
		String[] types = visitEntity.getType().split(",");
		String hql = "from VisitEntity where creator.id=:userId and type in("
				+ StringUtils.array2Str(types) + ") order by createTime desc  ";
		Query queryObject = getSession().createQuery(hql);
		queryObject.setParameter("userId", visitEntity.getCreator().getId());
		return queryObject.list();
	}

	public List<Map> statisticsVisit(String userId) {
		List<Map> dataList = new ArrayList<Map>();
		if (StringUtils.isEmpty(userId))
			return dataList;
		StringBuffer sbStr = new StringBuffer();
		sbStr.append(" select case when DATE_FORMAT(create_time,'%m') >= '01' and DATE_FORMAT(create_time,'%m') <= '03' then '春季'    ");
		sbStr.append(" when DATE_FORMAT(create_time,'%m') >= '04' and DATE_FORMAT(create_time,'%m') <= '06' then '夏季'                ");
		sbStr.append(" when DATE_FORMAT(create_time,'%m') >= '07' and DATE_FORMAT(create_time,'%m') <= '09' then '秋季'                ");
		sbStr.append(" when DATE_FORMAT(create_time,'%m') >= '10' and DATE_FORMAT(create_time,'%m') <= '12' then '冬季'                ");
		sbStr.append(" end season,count(*) totalNum                                                                                                   ");
		sbStr.append("   from t_v_visit where creator_id = :userId  group by                                ");
		sbStr.append(" case when DATE_FORMAT(create_time,'%m') >= '01' and DATE_FORMAT(create_time,'%m') <= '03' then '春季'            ");
		sbStr.append(" when DATE_FORMAT(create_time,'%m') >= '04' and DATE_FORMAT(create_time,'%m') <= '06' then '夏季'                ");
		sbStr.append(" when DATE_FORMAT(create_time,'%m') >= '07' and DATE_FORMAT(create_time,'%m') <= '09' then '秋季'                ");
		sbStr.append(" when DATE_FORMAT(create_time,'%m') >= '10' and DATE_FORMAT(create_time,'%m') <= '12' then '冬季'                ");
		sbStr.append(" end                                                                                                             ");
		Query queryObject = getSession().createSQLQuery(sbStr.toString())
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		queryObject.setParameter("userId", userId);
		dataList = queryObject.list();
		return dataList;
	}

	@Override
	public List<VisitEntity> findVisitListByUserIds(String[] studentIds,String type) {
		String hql = "from VisitEntity where creator.id in ("
				+ StringUtils.array2Str(studentIds)
				+ ") and type='"+type+"'  order by createTime desc ";
		Query queryObject = getSession().createQuery(hql);
		return queryObject.list();
	}

	@Override
	public void appointHandler(VisitEntity t) {
		String sql = "update t_v_visit v set v.client = :client,v.status=:status where v.id = :id ";
		SQLQuery q = getSession().createSQLQuery(sql);
		q.setParameter("client", t.getClient());
		q.setParameter("id", t.getId());
		q.setParameter("status", t.getStatus());
		q.executeUpdate();
	}

}