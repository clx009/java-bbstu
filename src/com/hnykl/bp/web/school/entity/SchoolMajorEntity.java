package com.hnykl.bp.web.school.entity;

 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator; 

import com.hnykl.bp.poi.excel.annotation.Excel;

/**   
 * @Title: Entity
 * @Description: 学校信息管理
 * @author onlineGenerator
 * @date 2016-06-20 15:49:42
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_sch_school_major", schema = "")
@SuppressWarnings("serial")
public class SchoolMajorEntity implements java.io.Serializable {
	
	@Excel(exportName="专业编号")
 	private java.lang.String majorCode;

	@Excel(exportName="学校编号")
 	private java.lang.String schoolCode;
 	

	
	/**id*/
	private java.lang.String id;
	/**学校id*/
	private SchoolEntity school;
	/**专业id*/
	private MajorEntity major; 
	/**排名*/
	@Excel(exportName="排名")
	private java.lang.String ranking;  
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="school_id")
	public SchoolEntity getSchool() {
		return school;
	}

	public void setSchool(SchoolEntity school) {
		this.school = school;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="major_id")
	public MajorEntity getMajor() {
		return major;
	}

	public void setMajor(MajorEntity major) {
		this.major = major;
	}

	@Column(name="ranking")
	public java.lang.String getRanking() {
		return ranking;
	}

	public void setRanking(java.lang.String ranking) {
		this.ranking = ranking;
	}

	@Transient
	public java.lang.String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(java.lang.String schoolCode) {
		this.schoolCode = schoolCode;
	}

	@Transient
	public java.lang.String getMajorCode() {
		return majorCode;
	}

	public void setMajorCode(java.lang.String majorCode) {
		this.majorCode = majorCode;
	}
	 
	
}
