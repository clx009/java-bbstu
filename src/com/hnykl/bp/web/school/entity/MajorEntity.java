package com.hnykl.bp.web.school.entity; 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.hnykl.bp.poi.excel.annotation.Excel;
 

/**   
 * @Title: Entity
 * @Description: 专业课程管理
 * @author onlineGenerator
 * @date 2016-06-20 15:59:37
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_sch_major", schema = "")
@SuppressWarnings("serial") 
public class MajorEntity implements java.io.Serializable {
	/**主键*/
	private java.lang.String id;
	
	private MajorEntity parent;

	/**编号*/
	@Excel(exportName="专业编号")
	private java.lang.String code;
	/**名称*/
	@Excel(exportName="专业名称")
	private java.lang.String name; 
	
	
	@Excel(exportName="是否热门：0-否，1-是")
	private String isHot;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	 
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  编号
	 */
	@Column(name ="CODE",nullable=false)
	public java.lang.String getCode(){
		return this.code;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  编号
	 */
	public void setCode(java.lang.String code){
		this.code = code;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  名称
	 */
	@Column(name ="NAME",nullable=false)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}

	@Column(name="is_hot")
	public String getIsHot() {
		return isHot;
	}

	public void setIsHot(String isHot) {
		this.isHot = isHot;
	}

	
	@ManyToOne(fetch=FetchType.LAZY) 
	@JoinColumn(name="parent_id")
	@NotFound(action=NotFoundAction.IGNORE)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public MajorEntity getParent() {
		return parent;
	}

	public void setParent(MajorEntity parent) {
		this.parent = parent;
	} 
	/*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "parent")
	public List<MajorEntity> getMajorEntityList() {
		return majorEntityList;
	}

	public void setMajorEntityList(List<MajorEntity> majorEntityList) {
		this.majorEntityList = majorEntityList;
	}*/
	 
}
