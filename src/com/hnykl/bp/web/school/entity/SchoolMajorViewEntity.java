package com.hnykl.bp.web.school.entity;
  
import java.lang.String; 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

/**   
 * @Title: Entity
 * @Description: v_sch_school_major
 * @author onlineGenerator
 * @date 2016-06-23 17:46:35
 * @version V1.0   
 *
 */
@Entity
@Table(name = "v_sch_school_major", schema = "")
@SuppressWarnings("serial")
public class SchoolMajorViewEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	
	private String schoolId;
	
	private String majorId;
	/**名称*/
	private java.lang.String majorName;
	/**专业综合排名*/
	private java.lang.String comprehensiveRanking;
	/**排名*/
	private java.lang.String ranking;
	/**英文名称*/
	private java.lang.String englishName;
	/**中文名称*/
	private java.lang.String chineseName;
	/**学校类型：0-公立，1-私立*/
	private java.lang.String publicOrPrivate;
	/**建校时间*/
	private java.lang.String builtUpTime;
	/**SAT要求*/
	private java.lang.String satReqMin;

	private java.lang.String satReqMax;
	/**托福要求*/
	private java.lang.String toeflRequirement;
	/**学费*/
	private java.lang.String tuition;
	/**地址*/
	private java.lang.String address;
	/**简介*/
	private java.lang.String introduction;
	
	private String cityId;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	
	@Column(name="school_id")
	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	@Column(name="major_id")
	public String getMajorId() {
		return majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  名称
	 */
	@Column(name ="MAJOR_NAME",nullable=false)
	public java.lang.String getMajorName(){
		return this.majorName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  名称
	 */
	public void setMajorName(java.lang.String majorName){
		this.majorName = majorName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  专业综合排名
	 */
	@Column(name ="COMPREHENSIVE_RANKING",nullable=false)
	public java.lang.String getComprehensiveRanking(){
		return this.comprehensiveRanking;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  专业综合排名
	 */
	public void setComprehensiveRanking(java.lang.String comprehensiveRanking){
		this.comprehensiveRanking = comprehensiveRanking;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  排名
	 */
	@Column(name ="RANKING",nullable=false)
	public java.lang.String getRanking(){
		return this.ranking;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  排名
	 */
	public void setRanking(java.lang.String ranking){
		this.ranking = ranking;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  英文名称
	 */
	@Column(name ="ENGLISH_NAME",nullable=false)
	public java.lang.String getEnglishName(){
		return this.englishName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  英文名称
	 */
	public void setEnglishName(java.lang.String englishName){
		this.englishName = englishName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  中文名称
	 */
	@Column(name ="CHINESE_NAME",nullable=false)
	public java.lang.String getChineseName(){
		return this.chineseName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  中文名称
	 */
	public void setChineseName(java.lang.String chineseName){
		this.chineseName = chineseName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学校类型：0-公立，1-私立
	 */
	@Column(name ="PUBLIC_OR_PRIVATE",nullable=false)
	public java.lang.String getPublicOrPrivate(){
		return this.publicOrPrivate;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学校类型：0-公立，1-私立
	 */
	public void setPublicOrPrivate(java.lang.String publicOrPrivate){
		this.publicOrPrivate = publicOrPrivate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  建校时间
	 */
	@Column(name ="BUILT_UP_TIME",nullable=false)
	public java.lang.String getBuiltUpTime(){
		return this.builtUpTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  建校时间
	 */
	public void setBuiltUpTime(java.lang.String builtUpTime){
		this.builtUpTime = builtUpTime;
	}
 
	@Column(name="SAT_REQ_MIN")
	public java.lang.String getSatReqMin() {
		return satReqMin;
	}

	public void setSatReqMin(java.lang.String satReqMin) {
		this.satReqMin = satReqMin;
	}
	
	@Column(name="SAT_REQ_MAX")
	public java.lang.String getSatReqMax() {
		return satReqMax;
	}

	public void setSatReqMax(java.lang.String satReqMax) {
		this.satReqMax = satReqMax;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  托福要求
	 */
	@Column(name ="TOEFL_REQUIREMENT",nullable=false)
	public java.lang.String getToeflRequirement(){
		return this.toeflRequirement;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  托福要求
	 */
	public void setToeflRequirement(java.lang.String toeflRequirement){
		this.toeflRequirement = toeflRequirement;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学费
	 */
	@Column(name ="TUITION",nullable=false)
	public java.lang.String getTuition(){
		return this.tuition;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学费
	 */
	public void setTuition(java.lang.String tuition){
		this.tuition = tuition;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  地址
	 */
	@Column(name ="ADDRESS",nullable=false)
	public java.lang.String getAddress(){
		return this.address;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  地址
	 */
	public void setAddress(java.lang.String address){
		this.address = address;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  简介
	 */
	@Column(name ="INTRODUCTION",nullable=false)
	public java.lang.String getIntroduction(){
		return this.introduction;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  简介
	 */
	public void setIntroduction(java.lang.String introduction){
		this.introduction = introduction;
	}

	@Column(name="CITY_ID")
	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
}
