package com.hnykl.bp.web.school.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

import com.hnykl.bp.poi.excel.annotation.Excel;
import com.hnykl.bp.poi.excel.annotation.ExcelTarget;

/**
 * @Title: Entity
 * @Description: 学校信息管理
 * @author onlineGenerator
 * @date 2016-06-20 15:51:32
 * @version V1.0
 * 
 */
@Entity
@Table(name = "t_sch_school", schema = "")
@SuppressWarnings("serial")
@ExcelTarget(id = "schoolEntity")
public class SchoolEntity implements java.io.Serializable {
	/** id */
	private java.lang.String id;
	@Excel(exportName = "学校编号")
	private java.lang.String code;
	/** 英文名称 */
	@Excel(exportName = "英文名称")
	private java.lang.String englishName;
	/** 中文名称 */
	@Excel(exportName = "中文名称")
	private java.lang.String chineseName;
	/** 建校时间 */
	@Excel(exportName = "建校时间")
	private java.lang.String builtUpTime;
	/** 综合排名 */
	@Excel(exportName = "综合排名")
	private Integer comprehensiveRanking;
	/** 本科公立大学排名 */
	@Excel(exportName = "学校类型：0-公立，1-私立")
	private String publicOrPrivate;
	/** 本科私立大学排名 */
	@Excel(exportName = "排名")
	private Integer ranking;
	/** 托福要求 */
	@Excel(exportName = "托福要求")
	private Integer toeflRequirement;
	/** SAT要求 */
	@Excel(exportName = "SAT最小值")
	private Integer satReqMin;
	@Excel(exportName = "SAT最大值")
	private Integer satReqMax;
	/** 录取比例 */
	@Excel(exportName = "录取比例")
	private Double acceptanceRate;
	/** 地址 */
	@Excel(exportName = "地址")
	private java.lang.String address;
	@Excel(exportName = "学费")
	private Double tuition;
	@Excel(exportName = "简介")
	private String introduction;

	@Excel(exportName = "城市编号")
	private String cityId;

	@Excel(exportName = "特色课程")
	private String featureMajor;
	@Excel(exportName = "经度")
	private String longitude;
	@Excel(exportName = "纬度")
	private String latitude;
	@Excel(exportName = "邮箱")
	private String email;

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", nullable = false)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 学校编号
	 */
	@Column(name = "CODE", nullable = false, unique = true)
	public java.lang.String getCode() {
		return code;
	}

	public void setCode(java.lang.String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 英文名称
	 */
	@Column(name = "ENGLISH_NAME", nullable = false)
	public java.lang.String getEnglishName() {
		return this.englishName;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 英文名称
	 */
	public void setEnglishName(java.lang.String englishName) {
		this.englishName = englishName;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 中文名称
	 */
	@Column(name = "CHINESE_NAME", nullable = false)
	public java.lang.String getChineseName() {
		return this.chineseName;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 中文名称
	 */
	public void setChineseName(java.lang.String chineseName) {
		this.chineseName = chineseName;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 建校时间
	 */
	@Column(name = "BUILT_UP_TIME", nullable = false)
	public java.lang.String getBuiltUpTime() {
		return this.builtUpTime;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 建校时间
	 */
	public void setBuiltUpTime(java.lang.String builtUpTime) {
		this.builtUpTime = builtUpTime;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 综合排名
	 */
	@Column(name = "COMPREHENSIVE_RANKING", nullable = false)
	public Integer getComprehensiveRanking() {
		return this.comprehensiveRanking;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 综合排名
	 */
	public void setComprehensiveRanking(Integer comprehensiveRanking) {
		this.comprehensiveRanking = comprehensiveRanking;
	}

	@Column(name = "public_or_private")
	public String getPublicOrPrivate() {
		return publicOrPrivate;
	}

	public void setPublicOrPrivate(String publicOrPrivate) {
		this.publicOrPrivate = publicOrPrivate;
	}

	@Column(name = "ranking")
	public Integer getRanking() {
		return ranking;
	}

	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 托福要求
	 */
	@Column(name = "TOEFL_REQUIREMENT", nullable = false)
	public Integer getToeflRequirement() {
		return this.toeflRequirement;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 托福要求
	 */
	public void setToeflRequirement(Integer toeflRequirement) {
		this.toeflRequirement = toeflRequirement;
	}

	@Column(name = "SAT_REQ_MIN")
	public Integer getSatReqMin() {
		return satReqMin;
	}

	public void setSatReqMin(Integer satReqMin) {
		this.satReqMin = satReqMin;
	}

	@Column(name = "SAT_REQ_MAX")
	public Integer getSatReqMax() {
		return satReqMax;
	}

	public void setSatReqMax(Integer satReqMax) {
		this.satReqMax = satReqMax;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 录取比例
	 */
	@Column(name = "ACCEPTANCE_RATE", nullable = false)
	public Double getAcceptanceRate() {
		return this.acceptanceRate;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 录取比例
	 */
	public void setAcceptanceRate(Double acceptanceRate) {
		this.acceptanceRate = acceptanceRate;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 地址
	 */
	@Column(name = "ADDRESS", nullable = false)
	public java.lang.String getAddress() {
		return this.address;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 地址
	 */
	public void setAddress(java.lang.String address) {
		this.address = address;
	}

	@Column(name = "TUITION", nullable = false)
	public Double getTuition() {
		return tuition;
	}

	public void setTuition(Double tuition) {
		this.tuition = tuition;
	}

	@Column(name = "INTRODUCTION", nullable = false)
	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	@Column(name = "CITY_ID")
	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	@Column(name = "FEATURE_MAJOR")
	public String getFeatureMajor() {
		return featureMajor;
	}

	public void setFeatureMajor(String featureMajor) {
		this.featureMajor = featureMajor;
	}

	@Column(name = "longitude")
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name = "latitude")
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
