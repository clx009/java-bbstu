package com.hnykl.bp.web.school.entity;
 
import java.util.Date; 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue; 
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator; 

import com.hnykl.bp.web.system.pojo.base.TSUser;

/**   
 * @Title: Entity
 * @Description: 学校信息管理
 * @author onlineGenerator
 * @date 2016-06-20 15:49:42
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_sch_user_school_collect", schema = "")
@SuppressWarnings("serial")
public class SchoolCollectEntity implements java.io.Serializable { 
	
	
	private java.lang.String id; 
	private java.lang.String schoolId; 
	private java.lang.String userId; 
	private Date createTime; 
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  学校id
	 */
	@Column(name ="SCHOOL_ID",nullable=false)
	public java.lang.String getSchoolId(){
		return this.schoolId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  学校id
	 */
	public void setSchoolId(java.lang.String schoolId){
		this.schoolId = schoolId;
	}
	
	
	@Column(name="user_id")
	public java.lang.String getUserId() {
		return userId;
	}

	public void setUserId(java.lang.String userId) {
		this.userId = userId;
	}
	
	@Column(name="create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}  
	
}
