package com.hnykl.bp.web.school.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.school.entity.TSchTeacherEntity;
import java.io.Serializable;

public interface TeacherServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TSchTeacherEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TSchTeacherEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TSchTeacherEntity t);
}
