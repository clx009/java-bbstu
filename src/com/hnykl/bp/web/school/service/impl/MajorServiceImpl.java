package com.hnykl.bp.web.school.service.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.school.service.MajorServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.school.entity.MajorEntity;

import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("majorService")
@Transactional
public class MajorServiceImpl extends CommonServiceImpl implements MajorServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((MajorEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((MajorEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((MajorEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(MajorEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(MajorEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(MajorEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,MajorEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId())); 
 		sql  = sql.replace("#{code}",String.valueOf(t.getCode()));
 		sql  = sql.replace("#{name}",String.valueOf(t.getName())); 
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MajorEntity> getMajorSelectItems(String q) {
		// TODO Auto-generated method stub
		String query = " from MajorEntity m where 1=1 ";
		if(q!=null&&!"".equals(q)){
			query+=" and m.name like '%"+q+"%'";
		}
		Query hq = this.getSession().createQuery(query);   
		return hq.list();
	}
 

	@SuppressWarnings("unchecked")
	@Override
	public List<MajorEntity> findMajor(String pid, String isHot) {
		// TODO Auto-generated method stub
		Criteria criteria = this.getSession().createCriteria(MajorEntity.class);
		if(pid!=null&&!"".equals(pid))
			criteria.add(Restrictions.eq("parent.id", pid));
		if(isHot!=null&&!"".equals(isHot))
			criteria.add(Restrictions.eq("isHot", isHot));
		return criteria.list();
	}
}