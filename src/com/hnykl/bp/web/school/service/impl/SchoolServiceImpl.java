package com.hnykl.bp.web.school.service.impl;
 
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.school.dto.RankingQueryDTO;
import com.hnykl.bp.web.school.dto.SchoolOrderDTO;
import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.school.service.SchoolServiceI;

@Service("schoolService")
@Transactional
public class SchoolServiceImpl extends CommonServiceImpl implements
		SchoolServiceI {

	public <T> void delete(T entity) {
		super.delete(entity);
		// 执行删除操作配置的sql增强
		this.doDelSql((SchoolEntity) entity);
	}

	public <T> Serializable save(T entity) {
		Serializable t = super.save(entity);
		// 执行新增操作配置的sql增强
		this.doAddSql((SchoolEntity) entity);
		return t;
	}

	public <T> void saveOrUpdate(T entity) {
		super.saveOrUpdate(entity);
		// 执行更新操作配置的sql增强
		this.doUpdateSql((SchoolEntity) entity);
	}

	/**
	 * 默认按钮-sql增强-新增操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doAddSql(SchoolEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-更新操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doUpdateSql(SchoolEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-删除操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doDelSql(SchoolEntity t) {
		return true;
	}

	/**
	 * 替换sql中的变量
	 * 
	 * @param sql
	 * @return
	 */
	public String replaceVal(String sql, SchoolEntity t) {
		sql = sql.replace("#{id}", String.valueOf(t.getId()));
		sql = sql
				.replace("#{english_name}", String.valueOf(t.getEnglishName()));
		sql = sql
				.replace("#{chinese_name}", String.valueOf(t.getChineseName()));
		sql = sql.replace("#{built_up_time}",
				String.valueOf(t.getBuiltUpTime()));
		sql = sql.replace("#{comprehensive_ranking}",
				String.valueOf(t.getComprehensiveRanking()));
		sql = sql.replace("#{public_or_private}",
				String.valueOf(t.getPublicOrPrivate()));
		sql = sql.replace("#{ranking}", String.valueOf(t.getRanking()));
		sql = sql.replace("#{toefl_requirement}",
				String.valueOf(t.getToeflRequirement()));
		sql = sql.replace("#{sat_req_min}", String.valueOf(t.getSatReqMin()));
		sql = sql.replace("#{sat_req_max}", String.valueOf(t.getSatReqMax()));
		sql = sql.replace("#{acceptance_rate}",
				String.valueOf(t.getAcceptanceRate()));
		sql = sql.replace("#{address}", String.valueOf(t.getAddress()));
		sql = sql.replace("#{UUID}", UUID.randomUUID().toString());
		return sql;
	}

	@Override
	public List<SchoolEntity> getSchoolSelectItems(String q) {
		String query = " from SchoolEntity s where 1=1";
		if (q != null && !"".equals(query))
			query += " and (s.chineseName like '%" + q
					+ "%' or s.englishName like '%" + q + "%')";
		Query hq = this.getSession().createQuery(query);
		@SuppressWarnings("unchecked")
		List<SchoolEntity> list = hq.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SchoolOrderDTO> getSchoolRanking(RankingQueryDTO rq) { 
		String sql = "select s.comprehensive_ranking comprehensiveRanking,s.ranking,s.id as schoolId,s.chinese_name as chineseName,s.english_name as englishName,if(c.id is null,0,1) isCollect "
				+ " from t_sch_school s "
				+ " LEFT JOIN (select * from t_sch_user_school_collect where  user_id = '"+rq.getUserId()+"') c on (s.id = c.school_id) "
				+ " where 1=1";
		
		if (rq.getSchoolType() != null && !"".equals(rq.getSchoolType())) {
			
			sql = sql + " and s.public_or_private = '" + rq.getSchoolType() + "' ";
			
			sql = sql + " or s.public_or_private is null";
//			if(rq.getOrderType()!=null && !"".equals(rq.getOrderType())){
//				sql = sql + " order by s.ranking "+rq.getOrderType();
//			}else {
//				sql = sql + " order by s.ranking asc";
//			}
			//
			if(rq.getOrderType()!=null && !"".equals(rq.getOrderType())){
				sql = sql + " order by s.comprehensive_ranking is null,s.comprehensive_ranking "+rq.getOrderType();
			}else {
				sql = sql + " order by s.comprehensive_ranking is null,s.comprehensive_ranking asc";
			} 
		}else{
			if(rq.getOrderType()!=null && !"".equals(rq.getOrderType())){
				sql = sql + " order by s.comprehensive_ranking is null,s.comprehensive_ranking "+rq.getOrderType();
			}else {
				sql = sql + " order by s.comprehensive_ranking is null,s.comprehensive_ranking asc";
			} 
		}
		
		if(rq.getFirstResult()!=null&&!"".equals(rq.getFirstResult()) && rq.getMaxResult()!=null&&!"".equals(rq.getMaxResult())){
			sql = sql + " limit "+rq.getFirstResult()+" , "+rq.getMaxResult();
		} 
		
		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.addEntity(SchoolOrderDTO.class);
		return sqlQuery.list();

	}
}