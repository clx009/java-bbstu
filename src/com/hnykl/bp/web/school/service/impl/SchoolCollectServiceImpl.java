package com.hnykl.bp.web.school.service.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.school.dto.SchoolCollectionDTO; 
import com.hnykl.bp.web.school.service.SchoolCollectServiceI;

@Service("schoolCollectService")
@Transactional
public class SchoolCollectServiceImpl extends CommonServiceImpl implements
		SchoolCollectServiceI {

	@Override
	public List<SchoolCollectionDTO> findCollectSchoolByFamilyId(String familyId) {
		// TODO Auto-generated method stub
		String sql = " SELECT tsu.nickname,tsbu.username,tsbu.realname,tsbu.id userId,us.id, us.create_time createTime,s.id as schoolId, s.chinese_name AS englishName, s.english_name AS chineseName, s.ranking,"
				+ " s.public_or_private publicOrPrivate, s.comprehensive_ranking AS comprehensiveRanking "
				+ " FROM t_s_user tsu,t_s_base_user tsbu, t_sch_user_school_collect us, t_sch_school s,t_f_family_user tffu "
				+ " WHERE  tsbu.id=tsu.id  and us.school_id = s.id and tsbu.id=us.user_id and tffu.user_id = us.user_id and tffu.family_id= :familyId  ";
		SQLQuery query = this.getSession().createSQLQuery(sql);
		query.setParameter("familyId", familyId);
		query.addEntity(SchoolCollectionDTO.class);
		@SuppressWarnings("unchecked")
		List<SchoolCollectionDTO> list = query.list();
		return list;
	}

	@Override
	public void deleteCollect(String userId, String schoolId) {
		// TODO Auto-generated method stub
		String sql = " delete from t_sch_user_school_collect where user_id=:userId and school_id = :schoolId ";
		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.setParameter("userId", userId);
		sqlQuery.setParameter("schoolId", schoolId);
		sqlQuery.executeUpdate();
	}

}
