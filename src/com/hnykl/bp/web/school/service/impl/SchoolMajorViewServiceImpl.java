package com.hnykl.bp.web.school.service.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.school.service.SchoolMajorViewServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.school.dto.ChooseSchoolRankingQueryDTO;
import com.hnykl.bp.web.school.dto.MajorRankingQueryDTO; 
import com.hnykl.bp.web.school.entity.SchoolMajorViewEntity;

import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@Service("schoolMajorViewService")
@Transactional
public class SchoolMajorViewServiceImpl extends CommonServiceImpl implements
		SchoolMajorViewServiceI {

	public <T> void delete(T entity) {
		super.delete(entity);
		// 执行删除操作配置的sql增强
		this.doDelSql((SchoolMajorViewEntity) entity);
	}

	public <T> Serializable save(T entity) {
		Serializable t = super.save(entity);
		// 执行新增操作配置的sql增强
		this.doAddSql((SchoolMajorViewEntity) entity);
		return t;
	}

	public <T> void saveOrUpdate(T entity) {
		super.saveOrUpdate(entity);
		// 执行更新操作配置的sql增强
		this.doUpdateSql((SchoolMajorViewEntity) entity);
	}

	/**
	 * 默认按钮-sql增强-新增操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doAddSql(SchoolMajorViewEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-更新操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doUpdateSql(SchoolMajorViewEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-删除操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doDelSql(SchoolMajorViewEntity t) {
		return true;
	}

	/**
	 * 替换sql中的变量
	 * 
	 * @param sql
	 * @return
	 */
	public String replaceVal(String sql, SchoolMajorViewEntity t) {
		sql = sql.replace("#{id}", String.valueOf(t.getId()));
		sql = sql.replace("#{major_name}", String.valueOf(t.getMajorName()));
		sql = sql.replace("#{comprehensive_ranking}",
				String.valueOf(t.getComprehensiveRanking()));
		sql = sql.replace("#{ranking}", String.valueOf(t.getRanking()));
		sql = sql
				.replace("#{english_name}", String.valueOf(t.getEnglishName()));
		sql = sql
				.replace("#{chinese_name}", String.valueOf(t.getChineseName()));
		sql = sql.replace("#{public_or_private}",
				String.valueOf(t.getPublicOrPrivate()));
		sql = sql.replace("#{built_up_time}",
				String.valueOf(t.getBuiltUpTime()));
		sql = sql.replace("#{sat_req_min}", String.valueOf(t.getSatReqMin()));
		sql = sql.replace("#{sat_req_max}", String.valueOf(t.getSatReqMax()));
		sql = sql.replace("#{toefl_requirement}",
				String.valueOf(t.getToeflRequirement()));
		sql = sql.replace("#{tuition}", String.valueOf(t.getTuition()));
		sql = sql.replace("#{address}", String.valueOf(t.getAddress()));
		sql = sql.replace("#{introduction}",
				String.valueOf(t.getIntroduction()));
		sql = sql.replace("#{UUID}", UUID.randomUUID().toString());
		return sql;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SchoolMajorViewEntity> getSchoolMajorRanking(MajorRankingQueryDTO rq) {
		Criteria criteria = this.getSession()
				.createCriteria(SchoolMajorViewEntity.class)
				.setFirstResult(rq.getFirstResult())
				.setMaxResults(rq.getMaxResult()).add(Restrictions.eq("majorId", rq.getMajorId()));

		if ("asc".equals(rq.getOrderType())) {
			if (rq.getSchoolType() != null && !"".equals(rq.getSchoolType())) {
				criteria.add(
						Restrictions.eqOrIsNull("publicOrPrivate",
								rq.getSchoolType())).addOrder(
						Order.asc("ranking"));
			} else {
				criteria.addOrder(Order.asc("comprehensiveRanking"));
			}
		}else{
			if (rq.getSchoolType() != null && !"".equals(rq.getSchoolType())) {
				criteria.add(
						Restrictions.eqOrIsNull("publicOrPrivate",
								rq.getSchoolType())).addOrder(
						Order.desc("ranking"));
			} else {
				criteria.addOrder(Order.desc("comprehensiveRanking"));
			}
		}
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SchoolMajorViewEntity> getSchoolMajorRanking(
			ChooseSchoolRankingQueryDTO cs) {
		// TODO Auto-generated method stub
		Criteria criteria = this
				.getSession()
				.createCriteria(SchoolMajorViewEntity.class)
				.setFirstResult(cs.getFirstResult())
				.setMaxResults(cs.getMaxResult())
				.add(Restrictions.eqOrIsNull("cityId", cs.getCityId()))
				.add(Restrictions.between("tuition", cs.getTuitionMin(),
						cs.getTuitionMax()))
				.add(Restrictions.eqOrIsNull("majorId", cs.getMajorId()))
				.add(Restrictions.between("toeflRequirement",
						cs.getToeflRequirementMin(),
						cs.getToeflRequirementMax()))
				.add(Restrictions
						.or(Restrictions.gt("satReqMax",
								cs.getSatRequirementMax())).add(
								Restrictions.lt("satReqMin",
										cs.getSatRequirementMin())));

		if ("asc".equals(cs.getOrderType())) {
			if (cs.getSchoolType() != null && !"".equals(cs.getSchoolType())) {
				criteria.add(
						Restrictions.eqOrIsNull("publicOrPrivate",
								cs.getSchoolType())).addOrder(
						Order.asc("ranking"));
			} else {
				criteria.addOrder(Order.asc("comprehensiveRanking"));
			}
		}else{
			if (cs.getSchoolType() != null && !"".equals(cs.getSchoolType())) {
				criteria.add(
						Restrictions.eqOrIsNull("publicOrPrivate",
								cs.getSchoolType())).addOrder(
						Order.desc("ranking"));
			} else {
				criteria.addOrder(Order.desc("comprehensiveRanking"));
			}
		}
		return criteria.list();
	}
}