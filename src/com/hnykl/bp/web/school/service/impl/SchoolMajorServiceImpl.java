package com.hnykl.bp.web.school.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.bean.BeanUtils;
import com.hnykl.bp.web.school.dto.ChooseSchoolRankingQueryDTO;
import com.hnykl.bp.web.school.dto.MajorRankingQueryDTO;
import com.hnykl.bp.web.school.dto.SchoolOrderDTO;
import com.hnykl.bp.web.school.entity.SchoolMajorEntity;
import com.hnykl.bp.web.school.service.SchoolMajorServiceI;

@Service("schoolMajorService")
@Transactional
public class SchoolMajorServiceImpl extends CommonServiceImpl implements
		SchoolMajorServiceI {

	public <T> void delete(T entity) {
		super.delete(entity);
		// 执行删除操作配置的sql增强
		this.doDelSql((SchoolMajorEntity) entity);
	}

	public <T> Serializable save(T entity) {
		Serializable t = super.save(entity);
		// 执行新增操作配置的sql增强
		this.doAddSql((SchoolMajorEntity) entity);
		return t;
	}

	public <T> void saveOrUpdate(T entity) {
		super.saveOrUpdate(entity);
		// 执行更新操作配置的sql增强
		this.doUpdateSql((SchoolMajorEntity) entity);
	}

	/**
	 * 默认按钮-sql增强-新增操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doAddSql(SchoolMajorEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-更新操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doUpdateSql(SchoolMajorEntity t) {
		return true;
	}

	/**
	 * 默认按钮-sql增强-删除操作
	 * 
	 * @param id
	 * @return
	 */
	public boolean doDelSql(SchoolMajorEntity t) {
		return true;
	}

	/**
	 * 替换sql中的变量
	 * 
	 * @param sql
	 * @return
	 */
	public String replaceVal(String sql, SchoolMajorEntity t) {
		sql = sql.replace("#{id}", String.valueOf(t.getId()));
		sql = sql.replace("#{major_id}", String.valueOf(t.getMajor().getId()));
	/*	sql = sql.replace("#{comprehensive_ranking}",
				String.valueOf(t.getComprehensiveRanking()));*/
		sql = sql.replace("#{ranking}", String.valueOf(t.getRanking()));
		sql = sql.replace("#{UUID}", UUID.randomUUID().toString());
		return sql;
	}

	@Override
	public SchoolMajorEntity isExists(SchoolMajorEntity schoolMajor) {

		String query = " from SchoolMajorEntity sm where sm.school.id = :schoolId and sm.major.id = :majorId";

		Query hq = this.getSession().createQuery(query);
		hq.setParameter("schoolId", schoolMajor.getSchool().getId());
		hq.setParameter("majorId", schoolMajor.getMajor().getId());
		@SuppressWarnings("unchecked")
		List<SchoolMajorEntity> list = hq.list();
		if (list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SchoolOrderDTO> getSchoolMajorRanking(MajorRankingQueryDTO rq) {
		String sql = "select sm.ranking,sm.comprehensive_ranking comprehensiveRanking,s.id as schoolId,s.english_name as englishName,s.chinese_name as chineseName,IF(scol.id is null,0,1) isCollect "
				+ " from t_sch_school s "
				+ " join t_sch_school_major sm on (sm.school_id = s.id) "
				+ " JOIN t_sch_major m on (sm.major_id = m.id) "
				+ " left join (select * from t_sch_user_school_collect where  user_id = '"
				+ rq.getUserId()
				+ "') scol on (s.id = scol.school_id ) where 1=1 ";

		if(StringUtils.isNotEmpty(rq.getMajorId())){
			sql = sql + " and sm.major_id = '"+ rq.getMajorId() + "' ";
		}
		
//		if (rq.getSchoolType() != null && !"".equals(rq.getSchoolType())) {
//
//			sql = sql + " and s.public_or_private = '" + rq.getSchoolType()
//					+ "' ";
//		}
		if (rq.getOrderType() != null && !"".equals(rq.getOrderType())) {
			sql = sql + " order by sm.ranking " + rq.getOrderType();
		} else {
			sql = sql + " order by sm.ranking asc";
		}
		/* else {
			if (rq.getOrderType() != null && !"".equals(rq.getOrderType())) {
				sql = sql + " order by s.comprehensive_ranking "
						+ rq.getOrderType();
			} else {
				sql = sql + " order by s.comprehensive_ranking asc";
			}
		}*/

		if (rq.getFirstResult() != null && !"".equals(rq.getFirstResult())
				&& rq.getMaxResult() != null && !"".equals(rq.getMaxResult())) {
			sql = sql + " limit " + rq.getFirstResult() + " , "
					+ rq.getMaxResult();
		}

		SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
		sqlQuery.addEntity(SchoolOrderDTO.class);

		return sqlQuery.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SchoolOrderDTO> getSchoolMajorRanking(
			ChooseSchoolRankingQueryDTO rq) {
		String sql = "select s.id as schoolId,s.english_name as englishName,s.chinese_name as chineseName,IF(scol.id is null,'0','1') isCollect "
				+ " from t_sch_school s "
				+ " left join (select * from t_sch_user_school_collect where  user_id = '"
				+ rq.getUserId()
				+ "') scol on (s.id = scol.school_id ) where 1=1 ";
		if(StringUtils.isNotEmpty(rq.getMajorId())){
			sql = sql + " AND EXISTS (select 1 from t_sch_school_major sm where sm.school_id = s.id and sm.major_id IN  ("+ com.hnykl.bp.base.tool.character.StringUtils.array2Str(rq.getMajorId().split(",")) + ")) ";
		}
		
		if (rq.getCityId() != null && !"".equals(rq.getCityId())) {
			sql = sql + " and s.city_id = '" + rq.getCityId() + "'";
		}

		if (StringUtils.isNotEmpty(rq.getToeflRequirementMin()) && StringUtils.isNotEmpty(rq.getToeflRequirementMax() ) ) {
			String[] sArray = rq.getToeflRequirementMin().split(",");
			if(sArray.length == 1){
				sql = sql + " and  ( s.TOEFL_requirement >= " + rq.getToeflRequirementMin().split(",")[0];
				sql = sql + " and s.TOEFL_requirement <= " + rq.getToeflRequirementMax().split(",")[0] +")";
			}else{
				for(int  i=0;sArray != null && i<sArray.length;i++){
					if(i==0){
						sql = sql + " and ( ( s.TOEFL_requirement >= " + rq.getToeflRequirementMin().split(",")[i];
						sql = sql + " and s.TOEFL_requirement <= " + rq.getToeflRequirementMax().split(",")[i] +")";
					}else if(i == sArray.length -1){
						sql = sql + " or ( s.TOEFL_requirement >= " + rq.getToeflRequirementMin().split(",")[i];
						sql = sql + " and s.TOEFL_requirement <= " + rq.getToeflRequirementMax().split(",")[i] +") )";
					}else{
						sql = sql + " or ( s.TOEFL_requirement >= " + rq.getToeflRequirementMin().split(",")[i];
						sql = sql + " and s.TOEFL_requirement <= " + rq.getToeflRequirementMax().split(",")[i] +") ";
					}
				}
			}
		} 
		
		if (StringUtils.isNotEmpty(rq.getSatRequirementMin()) && StringUtils.isNotEmpty(rq.getSatRequirementMax() ) ) {
			String[] sArray = rq.getSatRequirementMin().split(",");
			if(sArray.length == 1){
				sql = sql + " and  ( s.SAT_req_min >= " + rq.getSatRequirementMin().split(",")[0];
				sql = sql + " and s.SAT_req_max <= " + rq.getSatRequirementMax().split(",")[0] +")";
			}else{
				for(int  i=0;sArray != null && i<sArray.length;i++){
					if(i==0){
						sql = sql + " and ( ( s.SAT_req_min <= " + rq.getSatRequirementMin().split(",")[i];
						sql = sql + " and s.SAT_req_max >= " + rq.getSatRequirementMax().split(",")[i] +")";
					}else if(i == sArray.length -1){
						sql = sql + " or ( s.SAT_req_min <= " + rq.getSatRequirementMin().split(",")[i];
						sql = sql + " and s.SAT_req_max >= " + rq.getSatRequirementMax().split(",")[i] +") )";
					}else{
						sql = sql + " or ( s.SAT_req_min <= " + rq.getSatRequirementMin().split(",")[i];
						sql = sql + " and s.SAT_req_max >= " + rq.getSatRequirementMax().split(",")[i] +") ";
					}
				}
			}
		} 
		 
		if (StringUtils.isNotEmpty(rq.getTuitionMin()) && StringUtils.isNotEmpty(rq.getTuitionMax() ) ) {
			String[] tuitionMinArray = rq.getTuitionMin().split(",");
			
			if(tuitionMinArray.length == 1){
				sql = sql + " and  ( s.tuition >= " + rq.getTuitionMin().split(",")[0];
				sql = sql + " and s.tuition <= " + rq.getTuitionMax().split(",")[0] +") ";
			}else{
				for(int  i=0;tuitionMinArray != null && i<tuitionMinArray.length;i++){
					if(i==0){
						sql = sql + " and ( ( s.tuition >= " + rq.getTuitionMin().split(",")[i];
						sql = sql + " and s.tuition <= " + rq.getTuitionMax().split(",")[i] +")";
					}else if(i == tuitionMinArray.length -1){
						sql = sql + " or ( s.tuition >= " + rq.getTuitionMin().split(",")[i];
						sql = sql + " and s.tuition <= " + rq.getTuitionMax().split(",")[i] +") )";
					}else{
						sql = sql + " or ( s.tuition >= " + rq.getTuitionMin().split(",")[i];
						sql = sql + " and s.tuition <= " + rq.getTuitionMax().split(",")[i] +") ";
					}
				}
			}
		}  
		
		if(StringUtils.isNotEmpty(rq.getPublicOrPrivate())){
			sql = sql + " and s.public_or_private in (" + com.hnykl.bp.base.tool.character.StringUtils.array2Str(rq.getPublicOrPrivate().split(",")) + ") ";
		}
		
		if (rq.getOrderType() != null && !"".equals(rq.getOrderType())) {
			sql = sql + " order by s.ranking " + rq.getOrderType();
		} else {
			sql = sql + " order by s.ranking asc";
		}
		/*
		if (rq.getSchoolType() != null && !"".equals(rq.getSchoolType())) {

		} else {
			if (rq.getOrderType() != null && !"".equals(rq.getOrderType())) {
				sql = sql + " order by s.comprehensive_ranking "
						+ rq.getOrderType();
			} else {
				sql = sql + " order by s.comprehensive_ranking asc";
			}
		}*/

		if (rq.getFirstResult() != null && !"".equals(rq.getFirstResult())
				&& rq.getMaxResult() != null && !"".equals(rq.getMaxResult())) {
			sql = sql + " limit " + rq.getFirstResult() + " , "
					+ rq.getMaxResult();
		}
		System.out.println("======>getSchoolMajorRanking sql:"+sql);
		Query sqlQuery = this.getSession().createSQLQuery(sql).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		SchoolOrderDTO schoolOrderDTO = null;
		Map dataMap = new HashMap();
		List<Map> dataList = sqlQuery.list();
		List<SchoolOrderDTO> schoolOrderDTOList = new ArrayList<SchoolOrderDTO>();
		for(int i=0;dataList != null && i<dataList.size();i++){
			schoolOrderDTO = new SchoolOrderDTO();
			BeanUtils.populate(schoolOrderDTO, dataList.get(i));
			schoolOrderDTOList.add(schoolOrderDTO);
		}
		return schoolOrderDTOList;
	}
}