package com.hnykl.bp.web.school.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.school.dto.ChooseSchoolRankingQueryDTO;
import com.hnykl.bp.web.school.dto.MajorRankingQueryDTO;
import com.hnykl.bp.web.school.dto.SchoolOrderDTO;
import com.hnykl.bp.web.school.entity.SchoolMajorEntity;
import java.io.Serializable; 
import java.util.List;

public interface SchoolMajorServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(SchoolMajorEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(SchoolMajorEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(SchoolMajorEntity t);

	public SchoolMajorEntity isExists(SchoolMajorEntity schoolMajor);

	public List<SchoolOrderDTO> getSchoolMajorRanking(
			MajorRankingQueryDTO rankingQueryDTO);

	public List<SchoolOrderDTO> getSchoolMajorRanking(
			ChooseSchoolRankingQueryDTO chooseSchoolRankingQueryDTO); 
}
