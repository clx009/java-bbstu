package com.hnykl.bp.web.school.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.school.dto.RankingQueryDTO;
import com.hnykl.bp.web.school.dto.SchoolOrderDTO;
import com.hnykl.bp.web.school.entity.SchoolEntity;
import java.io.Serializable;
import java.util.List;

public interface SchoolServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(SchoolEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(SchoolEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(SchoolEntity t);

	public List<SchoolEntity> getSchoolSelectItems(String q); 

	public List<SchoolOrderDTO> getSchoolRanking(RankingQueryDTO rankingQueryDTO);
}
