package com.hnykl.bp.web.school.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.school.entity.MajorEntity;
import java.io.Serializable;
import java.util.List;

public interface MajorServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(MajorEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(MajorEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(MajorEntity t);
 	
 	public List<MajorEntity> getMajorSelectItems(String q);

	public List<MajorEntity> findMajor(String pid, String isHot);
}
