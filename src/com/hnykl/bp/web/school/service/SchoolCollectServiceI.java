package com.hnykl.bp.web.school.service;

import java.util.List;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.school.dto.SchoolCollectionDTO; 

public interface SchoolCollectServiceI extends CommonService{

	public List<SchoolCollectionDTO> findCollectSchoolByFamilyId(String familyId);

	void deleteCollect(String userId, String schoolId);

}
