package com.hnykl.bp.web.school.dto;

public class MajorRankingQueryDTO extends RankingQueryDTO {
	private String majorId;

	public MajorRankingQueryDTO() {
		super();
	}

	public MajorRankingQueryDTO(String majorId) {
		super();
		this.majorId = majorId;
	}

	public String getMajorId() {
		return majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}

}
