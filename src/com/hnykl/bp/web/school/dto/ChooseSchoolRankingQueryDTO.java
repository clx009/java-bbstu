package com.hnykl.bp.web.school.dto;

public class ChooseSchoolRankingQueryDTO extends RankingQueryDTO {
	private String cityId;
	private String tuitionMin;
	private String tuitionMax;
	private String majorId;
	private String toeflRequirementMin;
	private String toeflRequirementMax;
	private String satRequirementMin;
	private String satRequirementMax;

	public ChooseSchoolRankingQueryDTO() {
		super();
	}

	public ChooseSchoolRankingQueryDTO(String cityId, String tuitionMin,
			String tuitionMax, String majorId, String toeflRequirementMin,
			String toeflRequirementMax, String satRequirementMin,
			String satRequirementMax) {
		super();
		this.cityId = cityId;
		this.tuitionMin = tuitionMin;
		this.tuitionMax = tuitionMax;
		this.majorId = majorId;
		this.toeflRequirementMin = toeflRequirementMin;
		this.toeflRequirementMax = toeflRequirementMax;
		this.satRequirementMin = satRequirementMin;
		this.satRequirementMax = satRequirementMax;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getTuitionMin() {
		return tuitionMin;
	}

	public void setTuitionMin(String tuitionMin) {
		this.tuitionMin = tuitionMin;
	}

	public String getTuitionMax() {
		return tuitionMax;
	}

	public void setTuitionMax(String tuitionMax) {
		this.tuitionMax = tuitionMax;
	}

	public String getMajorId() {
		return majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}

	public String getToeflRequirementMin() {
		return toeflRequirementMin;
	}

	public void setToeflRequirementMin(String toeflRequirementMin) {
		this.toeflRequirementMin = toeflRequirementMin;
	}

	public String getToeflRequirementMax() {
		return toeflRequirementMax;
	}

	public void setToeflRequirementMax(String toeflRequirementMax) {
		this.toeflRequirementMax = toeflRequirementMax;
	}

	public String getSatRequirementMin() {
		return satRequirementMin;
	}

	public void setSatRequirementMin(String satRequirementMin) {
		this.satRequirementMin = satRequirementMin;
	}

	public String getSatRequirementMax() {
		return satRequirementMax;
	}

	public void setSatRequirementMax(String satRequirementMax) {
		this.satRequirementMax = satRequirementMax;
	}

}
