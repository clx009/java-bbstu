package com.hnykl.bp.web.school.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SchoolOrderDTO {
	private String comprehensiveRanking;
	private String schoolId;
	private String chineseName;
	private String englishName;
	private String isCollect;
	private String ranking;
	
	@Id
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getChineseName() {
		return chineseName;
	}
	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getIsCollect() {
		return isCollect;
	}
	public void setIsCollect(String isCollect) {
		this.isCollect = isCollect;
	}
	public String getComprehensiveRanking() {
		return comprehensiveRanking;
	}
	public void setComprehensiveRanking(String comprehensiveRanking) {
		this.comprehensiveRanking = comprehensiveRanking;
	}
	public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	}
	
}
