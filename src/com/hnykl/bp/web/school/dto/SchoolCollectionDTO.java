package com.hnykl.bp.web.school.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class SchoolCollectionDTO {
	
	private String id;
	
	private Date createTime; 
	
	private String schoolId;
	
	/**英文名称*/
	private java.lang.String englishName;
	/**中文名称*/
	private java.lang.String chineseName;
	
	private Integer comprehensiveRanking;
	
	private String publicOrPrivate;
	
	private Integer ranking;
	
	private String username;
	
	private String realname;
	
	private String nickname;
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	private String userId;
	
	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public java.lang.String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(java.lang.String englishName) {
		this.englishName = englishName;
	}

	public java.lang.String getChineseName() {
		return chineseName;
	}

	public void setChineseName(java.lang.String chineseName) {
		this.chineseName = chineseName;
	}

	public Integer getComprehensiveRanking() {
		return comprehensiveRanking;
	}

	public void setComprehensiveRanking(Integer comprehensiveRanking) {
		this.comprehensiveRanking = comprehensiveRanking;
	}

	public String getPublicOrPrivate() {
		return publicOrPrivate;
	}

	public void setPublicOrPrivate(String publicOrPrivate) {
		this.publicOrPrivate = publicOrPrivate;
	}

	public Integer getRanking() {
		return ranking;
	}

	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	

}
