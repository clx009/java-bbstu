package com.hnykl.bp.web.school.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MajorDTO {
	/** 主键 */
	private java.lang.String id;
	/** 父专业id */
	private java.lang.String parentId;
	/** 编号 */
	private java.lang.String code;
	/** 名称 */
	private java.lang.String name;
	/** 类型:1-专业，2-科目 */
	private java.lang.String type;

	private String parentText;

	private List<Map> childrenList = new ArrayList<Map>();
	


	public List<Map> getChildrenList() {
		return childrenList;
	}

	public void setChildrenList(List<Map> childrenList) {
		this.childrenList = childrenList;
	}

	public MajorDTO() {
		super();
	}

	public MajorDTO(String id, String parentId, String code, String name,
			String type, String parentText) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.code = code;
		this.name = name;
		this.type = type;
		this.parentText = parentText;
	}

	public java.lang.String getId() {
		return id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getParentId() {
		return parentId;
	}

	public void setParentId(java.lang.String parentId) {
		this.parentId = parentId;
	}

	public java.lang.String getCode() {
		return code;
	}

	public void setCode(java.lang.String code) {
		this.code = code;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getType() {
		return type;
	}

	public void setType(java.lang.String type) {
		this.type = type;
	}

	public String getParentText() {
		return parentText;
	}

	public void setParentText(String parentText) {
		this.parentText = parentText;
	}

}
