package com.hnykl.bp.web.school.dto;

public class RankingQueryDTO {
	private String userId;
	private Integer firstResult;
	private Integer maxResult;
	private String schoolType;
	private String orderType;
	private String publicOrPrivate;

	public String getPublicOrPrivate() {
		return publicOrPrivate;
	}

	public void setPublicOrPrivate(String publicOrPrivate) {
		this.publicOrPrivate = publicOrPrivate;
	}

	public RankingQueryDTO() {
		super();
	}

	public RankingQueryDTO(String userId,Integer firstResult, Integer maxResult,
			String schoolType,String orderType) {
		super();
		this.userId = userId;
		this.firstResult = firstResult;
		this.maxResult = maxResult;
		this.schoolType = schoolType;
		this.orderType = orderType;
	}

	public Integer getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(Integer firstResult) {
		this.firstResult = firstResult;
	}

	public Integer getMaxResult() {
		return maxResult;
	}

	public void setMaxResult(Integer maxResult) {
		this.maxResult = maxResult;
	}

	public String getSchoolType() {
		return schoolType;
	}

	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
