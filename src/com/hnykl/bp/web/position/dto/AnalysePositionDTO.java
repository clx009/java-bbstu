package com.hnykl.bp.web.position.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class AnalysePositionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1166504239088939650L;
	private String id;
	private String countInHome;
	private String countInSchool;
	private String countTotal;
	private String percentInHome;
	private String percentInSchool;
	private String percentOther;
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCountInHome() {
		return countInHome;
	}
	public void setCountInHome(String countInHome) {
		this.countInHome = countInHome;
	}
	public String getCountInSchool() {
		return countInSchool;
	}
	public void setCountInSchool(String countInSchool) {
		this.countInSchool = countInSchool;
	}
	public String getCountTotal() {
		return countTotal;
	}
	public void setCountTotal(String countTotal) {
		this.countTotal = countTotal;
	}
	public String getPercentInHome() {
		return percentInHome;
	}
	public void setPercentInHome(String percentInHome) {
		this.percentInHome = percentInHome;
	}
	public String getPercentInSchool() {
		return percentInSchool;
	}
	public void setPercentInSchool(String percentInSchool) {
		this.percentInSchool = percentInSchool;
	}
	public String getPercentOther() {
		return percentOther;
	}
	public void setPercentOther(String percentOther) {
		this.percentOther = percentOther;
	}
}
