package com.hnykl.bp.web.position.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.hnykl.bp.base.core.common.entity.IdEntity;

/**   
 * @Title: Entity
 * @Description: t_p_position
 * @author onlineGenerator
 * @date 2016-05-30 14:07:58
 * @version V1.0   
 *
 */  
@Entity
@Table(name = "t_p_position")
@PrimaryKeyJoinColumn(name = "id")
public class TPPositionEntity extends IdEntity { 
	
	/**用户id*/
	private java.lang.String userId;
	/**longitude*/
	private java.lang.String longitude;
	/**纬度*/
	private java.lang.String latitude;
	/**位置截图路径*/
	private java.lang.String positionScreenshotUrl;
	/** 停留时间*/
	private java.lang.Long stayTime;
	/**文字描述*/
	private java.lang.String description;
	/**创建时间*/
	private Timestamp createTime; 
	
	private String timeZone;
	
	private String type;
	
	private String isInHome;
	
	private String isInSchool;
	
	
	
	

	public TPPositionEntity() {
		super();
	}

	public TPPositionEntity(String userId, String longitude, String latitude,
			String positionScreenshotUrl, Long stayTime, String description,
			Timestamp createTime, String timeZone,String type) {
		super();
		this.userId = userId;
		this.longitude = longitude;
		this.latitude = latitude;
		this.positionScreenshotUrl = positionScreenshotUrl;
		this.stayTime = stayTime;
		this.description = description;
		this.createTime = createTime;
		this.timeZone = timeZone;
		this.type = type;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户id
	 */
	@Column(name ="user_id",nullable=false)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户id
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  longitude
	 */
	@Column(name ="longitude",nullable=false)
	public java.lang.String getLongitude(){
		return this.longitude;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  longitude
	 */
	public void setLongitude(java.lang.String longitude){
		this.longitude = longitude;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  纬度
	 */
	@Column(name ="latitude",nullable=false)
	public java.lang.String getLatitude(){
		return this.latitude;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  纬度
	 */
	public void setLatitude(java.lang.String latitude){
		this.latitude = latitude;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  位置截图路径
	 */
	@Column(name ="position_screenshot_url",nullable=false)
	public java.lang.String getPositionScreenshotUrl(){
		return this.positionScreenshotUrl;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  位置截图路径
	 */
	public void setPositionScreenshotUrl(java.lang.String positionScreenshotUrl){
		this.positionScreenshotUrl = positionScreenshotUrl;
	}
	
	/**
	 *方法: 取得java.lang.Long
	 *@return: java.lang.Long  停留时间 （秒）
	 */
	@Column(name = "stay_time", nullable=true)
	public java.lang.Long getStayTime() {
		return this.stayTime;
	}
	
	public void setStayTime(java.lang.Long stayTime) {
		this.stayTime = stayTime;
	}
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  文字描述
	 */
	@Column(name ="description",nullable=false)
	public java.lang.String getDescribe(){
		return this.description;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  文字描述
	 */
	public void setDescribe(java.lang.String description){
		this.description = description;
	}
	/**
	 *方法: 取得Timestamp
	 *@return: Timestamp 创建时间
	 */
	@Column(name ="create_time",nullable=false)
	public Timestamp getCreateTime(){
		return this.createTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建时间
	 */
	public void setCreateTime(Timestamp createTime){
		this.createTime = createTime;
	}

	/**
	 * 
	 * */
	@Column(name = "time_zone")
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	@Column(name="type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name="is_in_home")
	public String getIsInHome() {
		return isInHome;
	}

	public void setIsInHome(String isInHome) {
		this.isInHome = isInHome;
	}
	
	@Column(name="is_in_school")
	public String getIsInSchool() {
		return isInSchool;
	}

	public void setIsInSchool(String isInSchool) {
		this.isInSchool = isInSchool;
	}
}
