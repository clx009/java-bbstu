package com.hnykl.bp.web.position.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

import com.hnykl.bp.base.core.common.entity.IdEntity;

import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: t_f_common_address
 * @author onlineGenerator
 * @date 2016-06-02 09:33:47
 * @version V1.0   
 *
 */
@Entity
@Table(name = "t_f_common_address", schema = "") 
public class CommonAddressEntity extends IdEntity implements java.io.Serializable { 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**成员id，关联到t_f_member.id*/
	private java.lang.String userId;
	/**地址*/
	private java.lang.String address;
	/**中心点经度*/
	private java.lang.String longitude;
	/**中心点纬度*/
	private java.lang.String latitude;
	/**半径*/
	private java.lang.String radius;
	/**记录创建时间*/
	private Timestamp createTime;
	/**记录创建人id*/
	private java.lang.String creatorId;
	/**最后修改时间*/
	private Timestamp lastModifyTime;
	/**最后修改人id*/
	private java.lang.String lastModifyPerson;
	
	private String name;
	
	private String type;
	
	
	
	


	public CommonAddressEntity() {
		super();
	}

	public CommonAddressEntity(String userId, String address,
			String longitude, String latitude, String radius,
			Timestamp createTime, String creatorId, Timestamp lastModifyTime,
			String lastModifyPerson,String name,String type) {
		super(); 
		this.userId = userId;
		this.address = address;
		this.longitude = longitude;
		this.latitude = latitude;
		this.radius = radius;
		this.createTime = createTime;
		this.creatorId = creatorId;
		this.lastModifyTime = lastModifyTime;
		this.lastModifyPerson = lastModifyPerson;
		this.name = name;
		this.type = type;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  成员id，关联到t_f_member.id
	 */
	@Column(name ="user_id",nullable=false)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  成员id，关联到t_f_member.id
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  地址
	 */
	@Column(name ="ADDRESS",nullable=false)
	public java.lang.String getAddress(){
		return this.address;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  地址
	 */
	public void setAddress(java.lang.String address){
		this.address = address;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  中心点经度
	 */
	@Column(name ="LONGITUDE",nullable=false)
	public java.lang.String getLongitude(){
		return this.longitude;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  中心点经度
	 */
	public void setLongitude(java.lang.String longitude){
		this.longitude = longitude;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  中心点纬度
	 */
	@Column(name ="LATITUDE",nullable=false)
	public java.lang.String getLatitude(){
		return this.latitude;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  中心点纬度
	 */
	public void setLatitude(java.lang.String latitude){
		this.latitude = latitude;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  半径
	 */
	@Column(name ="RADIUS",nullable=false)
	public java.lang.String getRadius(){
		return this.radius;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  半径
	 */
	public void setRadius(java.lang.String radius){
		this.radius = radius;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  记录创建时间
	 */
	@Column(name ="CREATE_TIME",nullable=false)
	public Timestamp getCreateTime(){
		return this.createTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  记录创建时间
	 */
	public void setCreateTime(Timestamp createTime){
		this.createTime = createTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  记录创建人id
	 */
	@Column(name ="CREATOR_ID",nullable=false)
	public java.lang.String getCreatorId(){
		return this.creatorId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  记录创建人id
	 */
	public void setCreatorId(java.lang.String creatorId){
		this.creatorId = creatorId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  最后修改时间
	 */
	@Column(name ="LAST_MODIFY_TIME",nullable=false)
	public Timestamp getLastModifyTime(){
		return this.lastModifyTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  最后修改时间
	 */
	public void setLastModifyTime(Timestamp lastModifyTime){
		this.lastModifyTime = lastModifyTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  最后修改人id
	 */
	@Column(name ="LAST_MODIFY_PERSON",nullable=false)
	public java.lang.String getLastModifyPerson(){
		return this.lastModifyPerson;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  最后修改人id
	 */
	public void setLastModifyPerson(java.lang.String lastModifyPerson){
		this.lastModifyPerson = lastModifyPerson;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column(name="type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
