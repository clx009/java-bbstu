package com.hnykl.bp.web.position.service;

import java.util.List;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.position.entity.CommonAddressEntity;

public interface CommonAddressServiceI extends CommonService {

	/**
	 * 根据家人圈成员id找出该成员所有常用地址信息
	 * @param memberId 家人圈成员id
	 * @return
	 */
	public List<CommonAddressEntity> findMemeberCommonAddress(String familyUserId);
	
	/**
	 * 编辑常用地址
	 * @param commonAddressEntity 常用地址实体对象
	 * @return 
	 */
	public void modifyCommonAddress(CommonAddressEntity commonAddressEntity); 
	
	public CommonAddressEntity saveCommonAddress(CommonAddressEntity commonAddressEntity);
}
