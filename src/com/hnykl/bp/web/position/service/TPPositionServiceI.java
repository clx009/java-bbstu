package com.hnykl.bp.web.position.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.position.entity.TPPositionEntity;

public interface TPPositionServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TPPositionEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TPPositionEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TPPositionEntity t);

	public List<TPPositionEntity> findMemberPositions(String userId,
			Timestamp start, Timestamp end);
	public List<TPPositionEntity> findMemberPositions(String userId);
	
	public void savePosition(TPPositionEntity entity);
	
	public Map analysePosition(String userId,String startTime,String endTime);
}
