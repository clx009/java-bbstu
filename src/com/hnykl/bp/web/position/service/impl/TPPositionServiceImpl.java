package com.hnykl.bp.web.position.service.impl;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.position.entity.CommonAddressEntity;
import com.hnykl.bp.web.position.entity.TPPositionEntity;
import com.hnykl.bp.web.position.service.CommonAddressServiceI;
import com.hnykl.bp.web.position.service.TPPositionServiceI;
import com.hnykl.bp.web.rt.entity.RealTimeEntity;
import com.hnykl.bp.web.rt.service.RealTimeServiceI;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.UserService;

@Service("tPPositionService")
@Transactional
public class TPPositionServiceImpl extends CommonServiceImpl implements TPPositionServiceI {
	
	@Autowired
	private UserService userService;
	@Autowired
	private CommonAddressServiceI commonAddressService;
	
	@Autowired
	RealTimeServiceI realTimeService;
	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((TPPositionEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((TPPositionEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((TPPositionEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(TPPositionEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(TPPositionEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(TPPositionEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,TPPositionEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{user_id}",String.valueOf(t.getUserId()));
 		sql  = sql.replace("#{longitude}",String.valueOf(t.getLongitude()));
 		sql  = sql.replace("#{latitude}",String.valueOf(t.getLatitude()));
 		sql  = sql.replace("#{position_screenshot_url}",String.valueOf(t.getPositionScreenshotUrl()));
 		sql  = sql.replace("#{describe}",String.valueOf(t.getDescribe()));
 		sql  = sql.replace("#{create_time}",String.valueOf(t.getCreateTime()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}

	@Override
	public List<TPPositionEntity> findMemberPositions(String userId,
			Timestamp start, Timestamp end) {
		String query = " from TPPositionEntity p where p.userId = :userId and p.createTime between :start and :end  order by p.createTime asc";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("userId", userId);
		queryObject.setParameter("start", start);
		queryObject.setParameter("end", end);
		@SuppressWarnings("unchecked")
		List<TPPositionEntity> list = queryObject.list();
		return list;
	}
	
	public List<TPPositionEntity> findMemberPositions(String userId) {
		String query = " from TPPositionEntity p where p.userId = :userId  order by p.createTime desc";
		Query queryObject = getSession().createQuery(query);
		queryObject.setParameter("userId", userId);
		@SuppressWarnings("unchecked")
		List<TPPositionEntity> list = queryObject.list();
		return list;
	}
	
	public void savePosition(TPPositionEntity entity){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<CommonAddressEntity> commonAddressEntityList =   commonAddressService.findMemeberCommonAddress(entity.getUserId());
		String isIn= "N";
		Map dataMap = new HashMap();
		for(int i=0; commonAddressEntityList != null && i<commonAddressEntityList.size();i++){
			CommonAddressEntity commonAddressEntity = commonAddressEntityList.get(i);
			try {
				if(commonAddressEntity.getType() != null){
					double ss = StringUtils.getDistance(Double.parseDouble(commonAddressEntity.getLatitude()), Double.parseDouble(commonAddressEntity.getLongitude()), 
							Double.parseDouble(entity.getLatitude()), Double.parseDouble(entity.getLongitude()));
					if((ss*1000) <= Double.parseDouble(commonAddressEntity.getRadius()))isIn="Y";
					else isIn="N";
					dataMap.put(commonAddressEntity.getType(),isIn);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		if(dataMap.get("01") != null)entity.setIsInHome(dataMap.get("01").toString());
		if(dataMap.get("02") != null)entity.setIsInSchool(dataMap.get("02").toString());
		save(entity);
		userService.updateLastPosition(entity.getUserId(),entity.getLongitude(),entity.getLatitude(),entity.getTimeZone(),sdf.format(entity.getCreateTime()) );
		
		TSUser user = (TSUser)userService.getEntity(TSUser.class, entity.getUserId());
		
		if("1".equals(entity.getType())){
			
			RealTimeEntity realTimeEntity = new RealTimeEntity();
			realTimeEntity.setBusinessId(entity.getId());
			realTimeEntity.setTableName("TPPositionEntity"); 
			realTimeEntity.setTitle("报平安");
			realTimeEntity.setType("1");
			realTimeEntity.setCreateTime(new Date());
			realTimeEntity.setUserId(entity.getUserId());
			realTimeEntity.setContent(user.getNickname()+" 报平安 ，位置："+entity.getDescribe());
			realTimeService.save(realTimeEntity);
			
		}
		
	}
	
	/**
	 * 分析足迹
	 * @param userId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Map analysePosition(String userId,String startTime,String endTime){
		StringBuffer sbStr = new StringBuffer();
		sbStr.append(" select user_id userId,                                                                                                                                                                               ");
		sbStr.append(" sum(case when is_in_home = 'Y' then 1 else 0 end) countInHome,                                                                                                                        ");
		sbStr.append(" sum(case when is_in_school = 'Y' then 1 else 0 end) countInSchool,                                                                                                                    ");
		sbStr.append(" count(*) countTotal,                                                                                                                                                                  ");
		sbStr.append(" concat(round(sum(case when is_in_home = 'Y' then 1 else 0 end)/count(*),2)*100,'%') percentInHome,                                                                                    ");
		sbStr.append(" concat(round(sum(case when is_in_school = 'Y' then 1 else 0 end)/count(*),2)*100,'%') percentInSchool,                                                                                ");
		sbStr.append(" concat(  if( (1- round(sum(case when is_in_home = 'Y' then 1 else 0 end)/count(*),2) - round(sum(case when is_in_school = 'Y' then 1 else 0 end)/count(*),2) ) >0,(1- round(sum(case when is_in_home = 'Y' then 1 else 0 end)/count(*),2) - round(sum(case when is_in_school = 'Y' then 1 else 0 end)/count(*),2) ),0) *100,'%') percentOther       ");
		sbStr.append("  from t_p_position tpp where  user_id = :userId  and DATE_FORMAT(create_time,'%Y-%m-%d')>=:startTime and DATE_FORMAT(create_time,'%Y-%m-%d')<=:endTime                                 ");
		Query sqlQuery = getSession().createSQLQuery(sbStr.toString()).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		sqlQuery.setParameter("userId", userId);
		sqlQuery.setParameter("startTime", startTime);
		sqlQuery.setParameter("endTime", endTime);
		List<Map> analysePositionDTOList =  sqlQuery.list();
		Map dataMap = new HashMap();
		if(analysePositionDTOList != null){
			dataMap = analysePositionDTOList.get(0);
			if(dataMap.get("countInHome") == null)dataMap.put("countInHome",0);
			if(dataMap.get("countInSchool") == null)dataMap.put("countInSchool",0);
			if(dataMap.get("countTotal") == null)dataMap.put("countTotal",0);
			if(dataMap.get("percentInHome") == null)dataMap.put("percentInHome",0);
			if(dataMap.get("percentInSchool") == null)dataMap.put("percentInSchool",0);
			if(dataMap.get("percentOther") == null)dataMap.put("percentOther",0);
		}
		return dataMap;
	}
	
}