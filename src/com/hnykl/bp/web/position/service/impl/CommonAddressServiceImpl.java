package com.hnykl.bp.web.position.service.impl;
 
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.bean.BeanUtils;
import com.hnykl.bp.web.position.entity.CommonAddressEntity;
import com.hnykl.bp.web.position.entity.TPPositionEntity;
import com.hnykl.bp.web.position.service.CommonAddressServiceI;
import com.hnykl.bp.web.position.service.TPPositionServiceI;

@Service("commonAddressService")
@Transactional
public class CommonAddressServiceImpl extends CommonServiceImpl implements CommonAddressServiceI{
	
	@Autowired
	private TPPositionServiceI tPPositionService; 
	
	@Override
	public List<CommonAddressEntity> findMemeberCommonAddress(String familyUserId) {
		// TODO Auto-generated method stub
		String query = " from CommonAddressEntity  c where  c.userId = :familyUserId";
		Query commonAddressQuery = this.getSession().createQuery(query);
		commonAddressQuery.setParameter("familyUserId", familyUserId);
		@SuppressWarnings("unchecked")
		List<CommonAddressEntity> list = commonAddressQuery.list();
		return list;
	}

	@Override
	public void modifyCommonAddress(CommonAddressEntity commonAddressEntity) {
		CommonAddressEntity cae = this.commonDao.getEntity(CommonAddressEntity.class,commonAddressEntity.getId());
		if(cae == null)return;
		BeanUtils.populateBean(cae, commonAddressEntity);
		this.saveOrUpdate(cae); 
		String isIn = "N";
		//批量更新在家还是在校
		if(StringUtils.isNotBlank(cae.getUserId() )){
			List<TPPositionEntity> tPPositionEntityList = tPPositionService.findMemberPositions(cae.getUserId());
			for(int i=0;tPPositionEntityList != null && i<tPPositionEntityList.size();i++){
				double ss = com.hnykl.bp.base.tool.character.StringUtils.getDistance(Double.parseDouble(tPPositionEntityList.get(i).getLatitude()), Double.parseDouble(tPPositionEntityList.get(i).getLongitude()), 
						Double.parseDouble(commonAddressEntity.getLatitude()), Double.parseDouble(commonAddressEntity.getLongitude()));
				if((ss*1000) <= Double.parseDouble(commonAddressEntity.getRadius()))isIn="Y";
				else isIn="N";
				if("01".equals(commonAddressEntity.getType() )){
					tPPositionEntityList.get(i).setIsInHome(isIn);
				}
				if("02".equals(commonAddressEntity.getType()) ){
					tPPositionEntityList.get(i).setIsInSchool(isIn);
				}
			}
			batchSave(tPPositionEntityList);
		}
	}
	
	public CommonAddressEntity saveCommonAddress(CommonAddressEntity commonAddressEntity) {
		String id = (String) save(commonAddressEntity);
		commonAddressEntity.setId(id);
		//更新
		String userId = commonAddressEntity.getUserId();
		String isIn = "N";
		//批量更新在家还是在校
		if(StringUtils.isNotBlank(userId)){
			List<TPPositionEntity> tPPositionEntityList = tPPositionService.findMemberPositions(userId);
			for(int i=0;tPPositionEntityList != null && i<tPPositionEntityList.size();i++){
				tPPositionEntityList.get(i).setIsInHome(isIn);
				tPPositionEntityList.get(i).setIsInSchool(isIn);
				double ss = com.hnykl.bp.base.tool.character.StringUtils.getDistance(Double.parseDouble(tPPositionEntityList.get(i).getLatitude()), Double.parseDouble(tPPositionEntityList.get(i).getLongitude()), 
						Double.parseDouble(commonAddressEntity.getLatitude()), Double.parseDouble(commonAddressEntity.getLongitude()));
				if((ss*1000) <= Double.parseDouble(commonAddressEntity.getRadius()))isIn="Y";
				else isIn="N";
				if("01".equals(commonAddressEntity.getType() )){
					tPPositionEntityList.get(i).setIsInHome(isIn);
				}
				if("02".equals(commonAddressEntity.getType()) ){
					tPPositionEntityList.get(i).setIsInSchool(isIn);
				}
			}
			batchSave(tPPositionEntityList);
		}
		return commonAddressEntity;
	}
	

}
