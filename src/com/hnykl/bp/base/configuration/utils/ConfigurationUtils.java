package  com.hnykl.bp.base.configuration.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.log4j.Logger;
import com.hnykl.bp.base.configuration.ConfigurationRegister;
/**
 * <p>
 * 配置管理的工具类
 * </p>
 */
public class ConfigurationUtils {
    /**
     * Logger
     */
    private static final Logger logger = Logger
            .getLogger(ConfigurationRegister.class);

    /** Prefix for system property placeholders: "${" */
    public static final String PLACEHOLDER_PREFIX = "${";

    /** Suffix for system property placeholders: "}" */
    public static final String PLACEHOLDER_SUFFIX = "}";

    /**
     * 私有构造函数
     */
    private ConfigurationUtils() {

    }

    /**
     * 把指定文件的路径解析为URL
     * 
     * @param filePath 文件路径（绝对或相对的）
     * @return 文件的URL
     */
    public static URL resolveFileURL(String filePath) {
        filePath = resolvePlaceholders(filePath);

        URL url = locate(null, filePath);
        if (url != null) {
            return url;
        }

        if (ConfigurationRegister.isWebApplication()) {
            try {
                url = ConfigurationRegister.getWebApplicationServletContext()
                        .getResource(filePath);
            } catch (MalformedURLException e) {
                // TODO 处理异常
            }
        }

        return url;
    }

    /**
     * Return the location of the specified resource by searching the user home
     * directory, the current classpath and the system classpath.
     * 
     * @param base the base path of the resource
     * @param name the name of the resource
     * 
     * @return the location of the resource
     */
    public static URL locate(String base, String name) {
        if (logger.isDebugEnabled()) {
            StringBuffer buf = new StringBuffer();
            buf.append("ConfigurationUtils.locate(): base is ").append(base);
            buf.append(", name is ").append(name);
            logger.debug(buf.toString());
        }

        if (name == null) {
            // undefined, always return null
            return null;
        }

        URL url = null;

        // attempt to create an URL directly
        try {
            if (base == null) {
                url = new URL(name);
            } else {
                URL baseURL = new URL(base);
                url = new URL(baseURL, name);

                // check if the file exists
                InputStream in = null;
                try {
                    in = url.openStream();
                } finally {
                    if (in != null) {
                        in.close();
                    }
                }
            }

            logger.debug("Loading configuration from the URL " + url);
        } catch (IOException e) {
            url = null;
        }

        // attempt to load from an absolute path
        if (url == null) {
            File file = new File(name);
            if (file.isAbsolute() && file.exists()) { // already absolute?
                try {
                    url = file.toURL();
                    logger
                            .debug("Loading configuration from the absolute path "
                                    + name);
                } catch (MalformedURLException e) {
                    logger.warn("Could not obtain URL from file", e);
                }
            }
        }

        // attempt to load from the base directory
        if (url == null) {
            try {
                File file = constructFile(base, name);
                if (file != null && file.exists()) {
                    url = file.toURL();
                }

                if (url != null) {
                    logger.debug("Loading configuration from the path " + file);
                }
            } catch (MalformedURLException e) {
                logger.warn("Could not obtain URL from file", e);
            }
        }

        // attempt to load from the user home directory
        if (url == null) {
            try {
                File file = constructFile(System.getProperty("user.home"), name);
                if (file != null && file.exists()) {
                    url = file.toURL();
                }

                if (url != null) {
                    logger.debug("Loading configuration from the home path "
                            + file);
                }

            } catch (MalformedURLException e) {
                logger.warn("Could not obtain URL from file", e);
            }
        }

        // attempt to load from classpath
        if (url == null) {
            url = locateFromClasspath(name);
        }
        return url;
    }

    /**
     * Tries to find a resource with the given name in the classpath.
     * 
     * @param resourceName the name of the resource
     * @return the URL to the found resource or <b>null</b> if the resource
     *         cannot be found
     */
    static URL locateFromClasspath(String resourceName) {
        URL url = null;
        // attempt to load from the context classpath
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        if (loader != null) {
            url = loader.getResource(resourceName);

            if (url != null) {
                logger
                        .debug("Loading configuration from the context classpath ("
                                + resourceName + ")");
            }
        }

        // attempt to load from the system classpath
        if (url == null) {
            url = ClassLoader.getSystemResource(resourceName);

            if (url != null) {
                logger
                        .debug("Loading configuration from the system classpath ("
                                + resourceName + ")");
            }
        }
        return url;
    }

    public static URL getURL(String basePath, String file)
            throws MalformedURLException {
        File f = new File(file);
        if (f.isAbsolute()) { // already absolute?
            return f.toURL();
        }

        try {
            if (basePath == null) {
                return new URL(file);
            } else {
                URL base = new URL(basePath);
                return new URL(base, file);
            }
        } catch (MalformedURLException uex) {
            return constructFile(basePath, file).toURL();
        }
    }

    /**
     * Helper method for constructing a file object from a base path and a file
     * name. This method is called if the base path passed to
     * <code>getURL()</code> does not seem to be a valid URL.
     * 
     * @param basePath the base path
     * @param fileName the file name
     * @return the resulting file
     */
    static File constructFile(String basePath, String fileName) {
        File file = null;

        File absolute = null;
        if (fileName != null) {
            absolute = new File(fileName);
        }

        if (basePath == null || (absolute != null && absolute.isAbsolute())) {
            file = new File(fileName);
        } else {
            StringBuffer fName = new StringBuffer();
            fName.append(basePath);

            // My best friend. Paranoia.
            if (!basePath.endsWith("/")) {
                fName.append("/");
            }
            if (fileName.startsWith("." + "/")) {
                fName.append(fileName.substring(2));
            } else {
                fName.append(fileName);
            }

            file = new File(fName.toString());
        }

        return file;
    }

    /**
     * 获取数据源
     * 
     * @param dataSourceProvider 数据源提供者的实现类
     * @param dataSourceName 数据源名称
     * @return 数据源对象
     * @throws ConfigurationException 获取数据源异常
    public static DataSource getDataSourceByProvider(String dataSourceProvider,
            String dataSourceName) throws ConfigurationException {
        try {
            Class providerClass = Class.forName(dataSourceProvider);

            if (DataSourceProvider.class.isAssignableFrom(providerClass)) {
                DataSourceProvider provider = (DataSourceProvider) providerClass
                        .newInstance();
                return provider.getDataSource(dataSourceName);
            } else {
                throw new ConfigurationException(
                        "Fail to get dataSource by provider, provider not iplement DataSourceProvider.");
            }
        } catch (ClassNotFoundException e) {
            throw new ConfigurationException(
                    "Fail to get dataSource by provider, not found provider.",
                    e);
        } catch (IllegalAccessException e) {
            throw new ConfigurationException(
                    "Fail to get dataSource by provider.", e);
        } catch (InstantiationException e) {
            throw new ConfigurationException(
                    "Fail to get dataSource by provider.", e);
        }
    }
    */

    /**
     * Resolve ${...} placeholders in the given text, replacing them with
     * corresponding system property values.
     * 
     * @param text the String to resolve
     * @return the resolved String
     * @see #PLACEHOLDER_PREFIX
     * @see #PLACEHOLDER_SUFFIX
     */
    public static String resolvePlaceholders(String text) {
        StringBuffer buf = new StringBuffer(text);

        int startIndex = buf.indexOf(PLACEHOLDER_PREFIX);
        while (startIndex != -1) {
            int endIndex = buf.indexOf(PLACEHOLDER_SUFFIX, startIndex
                    + PLACEHOLDER_PREFIX.length());
            if (endIndex != -1) {
                String placeholder = buf.substring(startIndex
                        + PLACEHOLDER_PREFIX.length(), endIndex);
                int nextIndex = endIndex + PLACEHOLDER_SUFFIX.length();
                try {
                    String propVal = System.getProperty(placeholder);
                    if (propVal == null) {
                        // Fall back to searching the system environment.
                        propVal = System.getenv(placeholder);
                    }
                    if (propVal != null) {
                        buf.replace(startIndex, endIndex
                                + PLACEHOLDER_SUFFIX.length(), propVal);
                        nextIndex = startIndex + propVal.length();
                    } else {
                        logger
                                .warn("Could not resolve placeholder '"
                                        + placeholder
                                        + "' in ["
                                        + text
                                        + "] as system property: neither system property nor environment variable found");
                    }
                } catch (Throwable ex) {
                    logger
                            .warn("Could not resolve placeholder '"
                                    + placeholder
                                    + "' in ["
                                    + text
                                    + "] as system property: neither system property nor environment variable found");
                }
                startIndex = buf.indexOf(PLACEHOLDER_PREFIX, nextIndex);
            } else {
                startIndex = -1;
            }
        }

        return buf.toString();
    }

    /**
     * Get configuration file.
     * @param file Full path of configuration file.
     * @return Opened file input stream.
     * @throws IOException configuration file not founded.
     */
    public static InputStream getResourceAsStream(String file) throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream in = null;
        if (loader != null) {
            in = loader.getResourceAsStream(file);
        }
        if (in == null) {
            in = ClassLoader.getSystemResourceAsStream(file);
        }
        if (in == null) {
            throw new IOException("Could not found resource " + file);
        }
        return in;
    }
}
