package com.hnykl.bp.base.configuration;

/**
 * <p>
 * The interface of ConfigureRepository
 * </p>
 * 
 */
public interface IConfigurationItem {
    /**
     * 配置项名称：文件
     */
    public static final String CONFIGURE_ITEM_FILE = "file";

    /**
     * 配置项名称：编码
     */
    public static final String CONFIGURE_ITEM_ENCODING = "encoding";

    /**
     * 配置项名称：类型
     */
    public static final String CONFIGURE_ITEM_TYPE = "type";

    /**
     * 配置项名称：配置器
     */
    public static final String CONFIGURE_ITEM_CONFIGURER = "configurer";

    /**
     * 配置项名称：配置方法
     */
    public static final String CONFIGURE_ITEM_CONFIGMETHOD = "configmethod";

    /**
     * @return the mainConfigName
     */
    public String getMainConfigName();

    /**
     * @param mainConfigName the mainConfigName to set
     */
    public void setMainConfigName(String mainConfigName);

    /**
     * @return the name of this configuration item
     */
    public String getName();

    /**
     * @param name the name of this configuration item
     */
    public void setName(String name);

    /**
     * @return the configmethod
     */
    public String getConfigmethod();

    /**
     * @param configmethod the configmethod to set
     */
    public void setConfigmethod(String configmethod);

    /**
     * @return the configurer
     */
    public String getConfigurer();

    /**
     * @param configurer the configurer to set
     */
    public void setConfigurer(String configurer);

    /**
     * @return the encoding
     */
    public String getEncoding();

    /**
     * @param encoding the encoding to set
     */
    public void setEncoding(String encoding);

    /**
     * @return the file
     */
    public String getFile();

    /**
     * @param file the file to set
     */
    public void setFile(String file);

    /**
     * @return the type
     */
    public String getType();

    /**
     * @param type the type to set
     */
    public void setType(String type);

    /**
     * add extend property into the configure repository
     * 
     * @param name name of property
     * @param value value of property
     */
    public void setProperty(String name, String value);

    /**
     * get extend property from the configure repository
     * 
     * @param name name of property
     * @return value of property
     */
    public String getProperty(String name);
}
