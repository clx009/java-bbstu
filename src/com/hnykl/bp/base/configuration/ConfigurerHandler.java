package com.hnykl.bp.base.configuration;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.apache.log4j.Logger;

/**
 * <p>
 * 自定义配置器的处理类，所有自定义的配置类都通过该类加载
 * </p>
 *
 */
public class ConfigurerHandler implements Configurer {

    /**
     *  Logger
     */
    private static final Logger logger = Logger.getLogger(ConfigurerHandler.class);

    /**
     * the unique instance of CustomConfigurator
     */
    private static final ConfigurerHandler instance = new ConfigurerHandler();

    /**
     * private CustomConfigurator
     */
    private ConfigurerHandler() {

    }

    /**
     * get instance of CustomConfigurator
     * @return instance of CustomConfigurator
     */
    public static Configurer getInstance() {
        return instance;
    }

    /**
     * @see common.configuration.Configurer#configure(IConfiguratorItem)
     */
    public void configure(IConfigurationItem confItem) throws ConfigurationException {

        // 检查参数的合法性
        if (confItem == null || confItem.getName() == null || confItem.getConfigurer() == null) {
            ConfigurationException cex = new ConfigurationException(
                    "Irregular parameter confItem, confItem or confItem.getName() or "
                            + "confItem.getConfigurer() could not be null.");
            cex.log(logger);
            throw cex;
        }

        // 取得配置项的名称
        String name = confItem.getName();

        try {
            // 加载配置项的类
            Class configuratorClass = Class.forName(confItem.getConfigurer());
            Object configurator = null;

            if (ConfigurerHandler.class.isAssignableFrom(configuratorClass)) {
                // 指定的配置类不能是CustomConfigurator，否则抛出异常
                ConfigurationException cex = new ConfigurationException("Fail to configure " + name
                        + ", the configurator could not be " + ConfigurerHandler.class.getName());
                cex.log(logger);
                throw cex;

            } else if (Configurer.class.isAssignableFrom(configuratorClass)) {
                // 如果配置项的类实现了IConfigurator接口,则调用configure方法
                ((Configurer) configuratorClass.newInstance()).configure(confItem);

            } else {
                // 使用没有实现IConfigurator接口的类进行初始化
                try {
                    if (confItem.getEncoding() != null) {
                        // 如果编码不为空，则检查是否有两个参数的方法

                        Method method = configuratorClass.getDeclaredMethod(confItem.getConfigmethod(), new Class[] {
                                String.class, String.class});

                        if (method != null) {
                            // 如果方法不是静态的，则实例化该类
                            if (!Modifier.isStatic(method.getModifiers())) {
                                configurator = configuratorClass.newInstance();
                            }

                            // 调用方法进行初始化，并返回
                            method.invoke(configurator, new Object[] {confItem.getFile(), confItem.getEncoding()});
                            return;
                        }
                    }

                    // 如果编码为空或没有两个参数的方法，则检查是否有一个参数的方法
                    Method method = configuratorClass.getDeclaredMethod(confItem.getConfigmethod(),
                            new Class[] {String.class});

                    if (method != null) {
                        // 如果方法不是静态的，则实例化该类
                        if (!Modifier.isStatic(method.getModifiers())) {
                            configurator = configuratorClass.newInstance();
                        }

                        // 调用方法进行初始化，并返回
                        method.invoke(configurator, new Object[] {confItem.getFile()});
                        return;
                    }

                    // 如果，没有找到有参数的方法 ，则检查是有无参数的方法，如果找不到则抛出异常
                    method = configuratorClass.getDeclaredMethod(confItem.getConfigmethod(), null);
                    if (method == null) {
                        ConfigurationException cex = new ConfigurationException("Fail to configure " + name
                                + ", not found method " + confItem.getConfigmethod() + " in class "
                                + confItem.getConfigurer());
                        cex.log(logger);
                        throw cex;
                    }

                } catch (SecurityException e) {
                    ConfigurationException cex = new ConfigurationException("Fail to configure " + name
                            + ", denied access to method '" + confItem.getConfigmethod() + "' in class '"
                            + confItem.getConfigurer() + "'.", e);
                    cex.log(logger);
                    throw cex;
                } catch (NoSuchMethodException e) {
                    ConfigurationException cex = new ConfigurationException("Fail to configure " + name
                            + ", not found natching method '" + confItem.getConfigmethod() + "' in class '"
                            + confItem.getConfigurer() + "'.", e);
                    cex.log(logger);
                    throw cex;
                } catch (IllegalArgumentException e) {
                    ConfigurationException cex = new ConfigurationException("Fail to configure " + name
                            + ", could not instantiation class '" + confItem.getConfigurer() + "'.", e);
                    cex.log(logger);
                    throw cex;
                } catch (InvocationTargetException e) {
                    ConfigurationException cex = new ConfigurationException("Fail to configure " + name
                            + ", fail to confirue with class '" + confItem.getConfigurer() + "' and method '"
                            + confItem.getConfigmethod() + "'.", e);
                    cex.log(logger);
                    throw cex;
                }
            }
        } catch (InstantiationException e) {
            ConfigurationException cex = new ConfigurationException("Fail to configure " + name
                    + ", fail to instantiation class '" + confItem.getConfigurer() + "'.", e);
            cex.log(logger);
            throw cex;
        } catch (IllegalAccessException e) {
            ConfigurationException cex = new ConfigurationException("Fail to configure " + name
                    + ", fail to confirue with class '" + confItem.getConfigurer() + "' and method '"
                    + confItem.getConfigmethod() + "'.", e);
            cex.log(logger);
            throw cex;
        } catch (ClassNotFoundException e) {
            ConfigurationException cex = new ConfigurationException("Fail to configure " + name + ", not found class '"
                    + confItem.getConfigurer() + "'", e);
            cex.log(logger);
            throw cex;
        }

    }

    /*
     * (non-Javadoc)
     * @see common.configuration.IConfigurator#release()
     */
    public void release() {
    }

    /*
     * (non-Javadoc)
     * @see common.configuration.IConfigurator#supportRelease()
     */
    public boolean supportRelease() {
        return false;
    }
}
