package com.hnykl.bp.base.configuration;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

public class BasedataConfigMgr {
	  /**
     *  Logger
     */
    private static final Logger logger = Logger.getLogger(BaseConfigMgr.class);

    private static Configuration basedataConf;

    public static Configuration getBasedataConfigure() {
        if (basedataConf == null) {
            try {
                ConfigurationRegister confRegister = ConfigurationRegister
                        .getInstance();
                confRegister.refreshConfiguration("basedata");
                basedataConf = confRegister.getConfiguration("basedata");
            } catch (ConfigurationException ex) {
                return null;
            }
        }
        return basedataConf;
    }
    /** 数据库名称. */
    public static String getDataBaseName() {
        return getBasedataConfigure().getString("util.dbquery.dbmstype");
    }
}
