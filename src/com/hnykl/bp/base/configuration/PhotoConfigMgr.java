package com.hnykl.bp.base.configuration;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

public class PhotoConfigMgr {
	/**
	 * Logger
	 */
	private static final Logger logger = Logger.getLogger(ProductConfigMgr.class);

	private static Configuration baseConf;

	public static Configuration getBaseConfigure() {
		if (baseConf == null) {
			try {
				ConfigurationRegister confRegister = ConfigurationRegister.getInstance();
				confRegister.refreshConfiguration("photo");
				baseConf = confRegister.getConfiguration("photo");
			} catch (ConfigurationException ex) {
				return null;
			}
		}
		return baseConf;
	}

	public static boolean refresh() {
		try {
			ConfigurationRegister confRegister = ConfigurationRegister.getInstance();
			confRegister.refreshConfiguration("product");
			return true;
		} catch (ConfigurationException ex) {
			return false;
		}
	}

	/**
	 * 相册图片地址
	 * 
	 * @return
	 */
	public static String getCnffPhotoPath() {
		String value = "";
		try {
			value = getBaseConfigure().getString("cnff.file.photo.path");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// logger.warn("得到比价器访问地址出现异常:" + e.getMessage());
		}
		return value;
	}
}
