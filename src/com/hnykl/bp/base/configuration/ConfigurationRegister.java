package com.hnykl.bp.base.configuration;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;


import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.log4j.Logger;

import com.hnykl.bp.base.configuration.utils.ConfigurationUtils;

/**
 * 配置信息注册类 作为各种应用配置信息的入口
 * 
 * 配置项的加载基于Apache的commons-configuration（{@link org.apache.commons.configuration}）实现
 * 
 * 使用配置管理器之前首先需要初始化，初始化方式有以下几种：
 * 1、在Web应用里面通过Listener启动，在web.xml里面增加下面的Listener配置即可。
 *   <listener>
 *       <listener-class>ConfigurationLoaderListener</listener-class>
 *   </listener>
 *   
 * 2、在Web应用里面通过Servlet启动，在web.xml里面增加下面的Servlet配置即可。
 *   <servlet>
 *       <servlet-name>configurationLoaderServlet</servlet-name>
 *       <servlet-class>ConfigurationLoaderServlet</servlet-class>
 *       <load-on-startup>0</load-on-startup>
 *   </servlet>
 *   
 *  3、在非Web应用里面启动，调用{@link ConfigurationRegister#getInstance()} 
 *     或 {@link ConfigurationRegister#getInstance(String)}
 *     或 {@link ConfigurationRegister#init()}
 *     
 *  4、如果需要在其他Web应用启动器里面初始化，可以调用 {@link ConfigurationRegister#init(ServletContext, String)} 
 *
 * @see org.apache.commons.configuration.Configuration
 * @see org.apache.commons.configuration.PropertiesConfiguration
 * @see org.apache.commons.configuration.CompositeConfiguration
 * @see ConfigurationLoaderListener
 * @see ConfigurationLoaderServlet
 * 
 */
public class ConfigurationRegister {

    /**
     *  Logger
     */
    private static final Logger LOGGER = Logger.getLogger(ConfigurationRegister.class);

    /**
     * 主配置信息,记录了系统当前所有的配置信息的引用
     */
    private Configuration mainConfig;

    /**
     * 主配置的名称
     */
    private String mainConfigName;

    /**
     * 配置信息清单
     */
    private Map configMap;

    /**
     * 包含配置文件列表
     */
    private List includeConfigurations = new ArrayList();

    /**
     * 公共的包含配置文件列表
     */
    private static List globalIncludeConfigurations = new ArrayList();

    /**
     * 配置信息常量表
     */
    /**
     * 默认主配置文件名
     */
    private static String DEFAULT_MAIN_CONFIG_FILE = "bp.properties";

    /**
     * 主配置文件名后缀
     */
    private static final String MAIN_CONFIG_FILE_SUFFIX = ".properties";

    /**
     * 主配置文件列表配置项名称
     */
    private static final String MAIN_CONFIGS = "mainConfigurations";

    /**
     * 配置信息项名称
     */
    private static final String CONFIG_ITEM = "configuration";

    /**
     * 配置文件路径
     */
    private static final String CONFIG_ITEM_FILE = "file";

    /**
     * 配置文件类型(xml,properties,path)
     */
    private static final String CONFIG_ITEM_TYPE = "type";

    /**
     * 配置文件编码
     */
    private static final String CONFIG_ITEM_ENCODING = "encoding";

    /**
     * 配置文件加载器
     */
    private static final String CONFIG_ITEM_CONFIGURER = "configurer";

    /**
     * 配置文件为xml类型
     */
    private static final String CONFIG_ITEM_TYPE_XML = "xml";

    /**
     * 配置文件为properties类型
     */
    private static final String CONFIG_ITEM_TYPE_PROPERTIES = "properties";

    /**
     * 配置文件为database类型
     */
    private static final String CONFIG_ITEM_TYPE_DATABASE = "database";

    /**
     * 配置文件为properties类型
     */
    private static final String CONFIG_ITEM_TYPE_CUSTOM = "custom";

    /**
     * 配置文件为properties类型
     */
    private static final String CONFIG_ITEM_TYPE_PATH = "path";

    /**
     * 应用上下文属性名称：webapp的根目录
     */
    public static final String CONTEXT_ATTRIBUTE_WEBAPP_ROOT = "context.attribute.webapp.root";

    /**
     * 应用上下文属性名称：webapp的ServletContext对象
     */
    public static final String CONTEXT_ATTRIBUTE_WEBAPP_SERVLETCONTEXT = "context.attribute.webapp.servletcontext";

    /**
     * 是否是Web Application
     */
    private static boolean isWebApplication = false;

    /**
     * 配置是否已经加载
     */
    private static boolean configurationLoaded = false;

    /**
     * 应用上下文属性列表
     */
    private static Map contextAttributes = new HashMap();

    /**
     * 该类实例的Map，对应到多个主配置文件
     */
    private static Map instances = new HashMap();
    

    /**
     * 私有构造函数
     *
     * @param mainCfg 配置信息中心注册文件路径
     * @throws ConfigurationException 配置信息异常
     */
    private ConfigurationRegister(String mainCfg) throws ConfigurationException {
        //进行数据初始化工作
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Create configuration:" + mainCfg);
        }

        if ((mainCfg == null) || ("".equals(mainCfg.trim()))) {
            mainCfg = DEFAULT_MAIN_CONFIG_FILE;
        }

        mainConfigName = mainCfg;

        //获得主配置信息
        try {
            URL url = ConfigurationUtils.resolveFileURL(mainCfg);
            mainConfig = new PropertiesConfiguration(url);
            mainConfig.addProperty(CONTEXT_ATTRIBUTE_WEBAPP_ROOT, (String) contextAttributes.get(CONTEXT_ATTRIBUTE_WEBAPP_ROOT));
        } catch (org.apache.commons.configuration.ConfigurationException e) {
            instances.remove(mainCfg);
            ConfigurationException confException = new ConfigurationException("Fail to load main configuration: "
                    + mainCfg, e);
            confException.error(LOGGER);
            throw confException;
        }

        String configName = mainConfig.getString("name");
        if (configName == null) {
            configName = mainCfg;
        }

        instances.put(configName, this);
        if (!mainCfg.equals(configName)) {
            instances.put(mainCfg, this);
        }
        
        //如果文件中存在"includes"项则加载指定include文件
        loadIncludeConfigurations();        
        
        //将includes指定的配置文件里以system.开头的配置项设置到系统环境里面
        setSystemProperties();

        if (!includeConfigurations.isEmpty()) {
            mainConfig = new CompositeConfiguration(mainConfig, includeConfigurations);
        }

        //注册表重新构造
        configMap = new Hashtable();
        this.refreshAllConfiguration();

        // 如果文件中存在"mainConfigurations"项则加载子主配置文件
        loadSubMainConfigurations();

        LOGGER.info("Loading '" + mainCfg + "' success");
    }

    /**
     * 加载includes指定的包含配置文件
     * @throws ConfigurationException 配置异常
     */
    private void loadIncludeConfigurations() throws ConfigurationException {
        Iterator includeFiles = mainConfig.getList("includes").iterator();
        while (includeFiles.hasNext()) {
            String includeFile = (String) includeFiles.next();

            if (includeFile != null) {
                try {
                    Configuration includeConfiguration = null;
                    URL url = ConfigurationUtils.resolveFileURL(includeFile);
                    includeConfiguration = new PropertiesConfiguration(url);

                    if (includeConfiguration == null) {
                        includeConfiguration = new PropertiesConfiguration();
                    }

                    if (isWebApplication()) {
                        includeConfiguration.addProperty("webroot", getContextAttribute(CONTEXT_ATTRIBUTE_WEBAPP_ROOT));
                    }

                    includeConfigurations.add(includeConfiguration);
                } catch (org.apache.commons.configuration.ConfigurationException e) {
                    ConfigurationException confException = new ConfigurationException("Load include file fail.", e);
                    confException.error(LOGGER);
                    throw confException;
                }
            }
        }

        if (includeConfigurations.isEmpty() && !globalIncludeConfigurations.isEmpty()) {
            includeConfigurations.addAll(globalIncludeConfigurations);
        }

        // 加到公共的里面
        if (globalIncludeConfigurations.isEmpty()) {
            globalIncludeConfigurations.addAll(includeConfigurations);
        }
    }

    /**
     * 将includes指定的配置文件里以system.开头的配置项设置到系统环境里面
     */
    private void setSystemProperties() {
        Iterator confIterator = includeConfigurations.iterator();
        while (confIterator.hasNext()) {
            Configuration conf = (Configuration) confIterator.next();
            Iterator keyIterator = conf.getKeys();
            while (keyIterator.hasNext()) {
                String key = (String) keyIterator.next();
                String value = conf.getString(key);
                if (value != null && key.startsWith("system.")) {
                    System.setProperty(key, value);
                }
            }
        }
    }

    /**
     * 加载附属的主配置
     * 
     * @throws ConfigurationException 配置信息异常
     */
    private void loadSubMainConfigurations() throws ConfigurationException {
        Iterator subMainConfigurations = mainConfig.getList(MAIN_CONFIGS).iterator();
        while (subMainConfigurations.hasNext()) {
            String subMainConfiguration = (String) subMainConfigurations.next();
            ConfigurationRegister.getInstance(subMainConfiguration);
        }
    }

    /**
     * 取得配置信息注册类实例 我们通过一个中心配置文件来记录整个系统所用到的所有配置信息.
     * 该配置文件保存在classpath下
     *
     * @return 配置信心注册类实例
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public static ConfigurationRegister getInstance() throws ConfigurationException {
        return getInstance(null);
    }

    /**
     * 取得配置信息注册类实例 我们通过一个中心配置文件来记录整个系统所用到的所有配置信息.
     *
     * @param mainCfg 配置信息中心注册文件路径
     * @return 配置信心注册类实例
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public static synchronized ConfigurationRegister getInstance(String mainCfg) throws ConfigurationException {
    	// 不传入参数取默认主配置文件
        if (mainCfg == null) {
            mainCfg = DEFAULT_MAIN_CONFIG_FILE;
        }

        // 尝试从缓存中获取主配置文件实例,能得到则返回
        ConfigurationRegister register = (ConfigurationRegister) instances.get(mainCfg);
        if (register != null) {
            return register;
        }

        // 不能获取则,并与主配置文件后缀不同则尝试添加主配置文件后缀后再从缓存中获取
        if (!mainCfg.endsWith(MAIN_CONFIG_FILE_SUFFIX)) {
            mainCfg = mainCfg + MAIN_CONFIG_FILE_SUFFIX;
            register = (ConfigurationRegister) instances.get(mainCfg);
        }
        
        // 依然不能获取则新建一个
        if (register == null) {
            register = new ConfigurationRegister(mainCfg);
        }

        return register;
    }

    /**
     * 按名字取得指定的配置信息
     *
     * @param cfgName 配置信息名称
     * @return 配置信息类实例
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public Configuration getConfiguration(String cfgName) throws ConfigurationException {
        //从Map注册表中获得指定名字的配置信息实例
        return (Configuration) configMap.get(cfgName);
    }

    /**
     * 在注册表中增加注册信息
     *
     * @param cfgName 配置信息名称
     * @param config 被加入注册表的配置信息
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public void addConfiguration(String cfgName, Configuration config) throws ConfigurationException {
        //加入Map注册表中
        configMap.put(cfgName, config);
    }

    /**
     * 从注册表中删除注册信息
     *
     * @param cfgName 配置信息名称
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public void removeConfiguration(String cfgName) throws ConfigurationException {
        //从Map注册表中删除
        configMap.remove(cfgName);
    }

    /**
     * 初始化配置，将加载所有主配置文件
     * 
     * @throws ConfigurationException 配置信息异常
     */
    public static void init() throws ConfigurationException {
        init(null, null);
    }

    /**
     * 初始化配置，将加载所有主配置文件
     * 
     * @throws ConfigurationException 配置信息异常
     */
    public static void init(ServletContext servletContext, String webRoot) throws ConfigurationException {
        if (configurationLoaded) {
            LOGGER.error("ConfigurationRegister has loaded, reload is not allowed.");
            return;
        }
        if (servletContext != null) {
            isWebApplication = true;
            setContextAttribute(CONTEXT_ATTRIBUTE_WEBAPP_SERVLETCONTEXT, servletContext);
            System.setProperty("web.fileroot", webRoot);
            if (webRoot != null) {
                setContextAttribute(CONTEXT_ATTRIBUTE_WEBAPP_ROOT, webRoot);
            }
        }
        getInstance();
        configurationLoaded = true;
    }

    /**
     * 释放所有的主配置
     */
    public static void releaseAll() {
    	/*
        Iterator mainConfigs = instances.values().iterator();
        while (mainConfigs.hasNext()) {
            ConfigurationRegister mainConfig = (ConfigurationRegister) mainConfigs.next();
            mainConfig.release();
        }*/
    }

    /**
     * 释放所有的配置项
     */
    public void release() {
        Iterator configs = this.configMap.values().iterator();
        while (configs.hasNext()) {
            Configurer conf = (Configurer) configs.next();
            conf.release(); 
        }
    }

    /**
     * 在本地配置文件修改后,刷新内存中的配置信息
     *
     * @param cfgName 配置信息名称
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public void refreshConfiguration(String cfgName) throws ConfigurationException {
        //获取配置项信息
        String itemFile = null;
        String itemType = null;
        //如果配置文件类型为xml，构造xml配置实例，并加入注册表
        Configuration itemConfiguration = null;

        //处理参数异常
        if (cfgName == null) {
            return;
        }

        //获得该配置项文件路径
        itemFile = mainConfig.getString(cfgName + "." + CONFIG_ITEM_FILE);
        if (itemFile != null) {
            String webappRoot = (String) contextAttributes.get(CONTEXT_ATTRIBUTE_WEBAPP_ROOT);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Refresh configuration " + cfgName + " with webappRoot: " + webappRoot);
            }

            if (itemFile.startsWith(".") && webappRoot != null) {
                itemFile = webappRoot + itemFile.substring(1);
                mainConfig.setProperty(cfgName + "." + CONFIG_ITEM_FILE, itemFile);
            }
        }

        //获得该配置项类型
        itemType = mainConfig.getString(cfgName + "." + CONFIG_ITEM_TYPE);

        try {
            //如果配置文件类型为properties或xml，构造配置实例，并加入注册表
            if (itemType == null) {
                LOGGER.warn("not supported null type, skip this configuration item.");
            } else if (itemType.equalsIgnoreCase(CONFIG_ITEM_TYPE_XML)) {
                if (itemFile == null) {
                    LOGGER.warn("Config file is null for configuration" + cfgName + " , skip this configuration item.");
                    return;
                }
                LOGGER.info("Loading '" + itemFile + "' ...");

                URL url = ConfigurationUtils.resolveFileURL(itemFile);
                itemConfiguration = new XMLConfiguration(url);
                itemConfiguration.addProperty(CONTEXT_ATTRIBUTE_WEBAPP_ROOT, (String) contextAttributes.get(CONTEXT_ATTRIBUTE_WEBAPP_ROOT));
                LOGGER.info("Loading '" + itemFile + "' success");
            } else if (itemType.equalsIgnoreCase(CONFIG_ITEM_TYPE_PROPERTIES)) {
                if (itemFile == null) {
                    LOGGER.warn("Config file is null for configuration" + cfgName + " , skip this configuration item.");
                    return;
                }
                LOGGER.info("Loading '" + itemFile + "' ...");

                URL url = ConfigurationUtils.resolveFileURL(itemFile);
                itemConfiguration = new PropertiesConfiguration(url);
                itemConfiguration.addProperty(CONTEXT_ATTRIBUTE_WEBAPP_ROOT, (String) contextAttributes.get(CONTEXT_ATTRIBUTE_WEBAPP_ROOT));
                LOGGER.info("Loading '" + itemFile + "' success");
            } else if (itemType.equalsIgnoreCase(CONFIG_ITEM_TYPE_DATABASE)) {
                LOGGER.info("Loading '" + cfgName + "' ...");
                /**
                 * 暂时屏蔽掉数据的加载 
                itemConfiguration = loadDatabaseConfiguration(cfgName);
                 */
//                itemConfiguration.addProperty(CONTEXT_ATTRIBUTE_WEBAPP_ROOT, (String) contextAttributes.get(CONTEXT_ATTRIBUTE_WEBAPP_ROOT));
                LOGGER.info("Loading '" + cfgName + "' success");
            } else if (itemType.equalsIgnoreCase(CONFIG_ITEM_TYPE_CUSTOM)) {
                loadCustomConfiguration(cfgName);
//                itemConfiguration.addProperty(CONTEXT_ATTRIBUTE_WEBAPP_ROOT, (String) contextAttributes.get(CONTEXT_ATTRIBUTE_WEBAPP_ROOT));
                LOGGER.info("Loading '" + cfgName + "' success");
            } else if (!itemType.equalsIgnoreCase(CONFIG_ITEM_TYPE_PATH)) {
                LOGGER.error("not supported type : " + itemType + " , skip this configuration item.");
            }
        } catch (org.apache.commons.configuration.ConfigurationException e) {
            ConfigurationException confException = new ConfigurationException("Fail to load configuration: " + cfgName,
                    e);
            confException.error(LOGGER);
            throw confException;
        }

        //获得配置信息成功后，加入注册表中
        if (itemConfiguration != null) {
            if (this.includeConfigurations.size() > 0) {
                itemConfiguration = new CompositeConfiguration(itemConfiguration, includeConfigurations);
            }
            //清除旧有的信息
            this.removeConfiguration(cfgName);
            //新增新的信息
            this.addConfiguration(cfgName, itemConfiguration);
        }
    }

    /**
     * 加载自定义配置
     * 
     * @param cfgName 配置名称
     * @throws ConfigurationException 配置异常
     */
    private void loadCustomConfiguration(String cfgName) throws ConfigurationException {
        String configurator = mainConfig.getString(cfgName + "." + CONFIG_ITEM_CONFIGURER);
        if (configurator == null) {
            LOGGER.error("Loading '" + cfgName + "' fail, configurer is null.");
            return;
        }

        LOGGER.info("Loading '" + cfgName + "' with configurer '" + configurator + "' ...");

        IConfigurationItem confItem = new ConfigurationItem();
        confItem.setMainConfigName(mainConfigName);
        confItem.setName(cfgName);
        Iterator ite = mainConfig.getKeys();
        while (ite.hasNext()) {
            String cfgItemName = (String) ite.next();
            if (cfgItemName.startsWith(cfgName + ".")) {
                String cfgItemValue = mainConfig.getString(cfgItemName);
                cfgItemName = cfgItemName.substring((cfgName + ".").length());
                confItem.setProperty(cfgItemName, cfgItemValue);
            }
        }

        Configurer customConf = ConfigurerHandler.getInstance();
        if (customConf.supportRelease()) {
            configMap.put(cfgName, customConf);
        }

        try {
            customConf.configure(confItem);
        } catch (ConfigurationException e) {
           ConfigurationException cex = new ConfigurationException(
                    "Loading '" + cfgName + "' fail.", e);
            cex.log(LOGGER);
            throw cex;
        }
    }

    /**
     * 加载数据库配置
     * 
     * @param cfgName 主配置名称
     * @return 数据库配置对象
     * @throws ConfigurationException 配置异常
     
    private Configuration loadDatabaseConfiguration(String cfgName) throws ConfigurationException {

        String dataSourceProvider = mainConfig.getString(cfgName + "." + "dataSourceProvider");
        String dataSourceName = mainConfig.getString(cfgName + "." + "dataSourceName");
        String jdbcUrl = mainConfig.getString(cfgName + "." + "jdbcUrl");
        String jdbcDriver = mainConfig.getString(cfgName + "." + "jdbcDriver");
        String userName = mainConfig.getString(cfgName + "." + "userName");
        String password = mainConfig.getString(cfgName + "." + "password");

        String jndiName = mainConfig.getString(cfgName + "." + "jndiName");
        String jndiUrl = mainConfig.getString(cfgName + "." + "jndiUrl");
        String jndiFactory = mainConfig.getString(cfgName + "." + "jndiFactory");

        String table = mainConfig.getString(cfgName + "." + "table");
        String keyColumn = mainConfig.getString(cfgName + "." + "keyColumn");
        String valueColumn = mainConfig.getString(cfgName + "." + "valueColumn");

        if (dataSourceProvider != null && dataSourceName != null) {
            DataSource dataSource = ConfigurationUtils.getDataSourceByProvider(dataSourceProvider, dataSourceName);
            return new common.configuration.DatabaseConfiguration(dataSource, table, keyColumn, valueColumn);
        } else if (jndiName != null && jndiName.trim().length() > 0) {
            return new common.configuration.DatabaseConfiguration(jndiName, jndiUrl, jndiFactory, userName,
                    password, table, keyColumn, valueColumn);
        } else {
            return new common.configuration.DatabaseConfiguration(jdbcDriver, jdbcUrl, userName, password,
                    table, keyColumn, valueColumn);
        }
    }*/

    /**
     * 在本地配置文件修改后,刷新内存中的配置信息
     *
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public void refreshAllConfiguration() throws ConfigurationException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Begin to refresh all configuration.");
        }
        //获得配置信息项，通过迭代子获得相应配置信息实例，并加入注册表
        String itemPrefix = null;

        if (configMap == null) {
            configMap = new Hashtable();
        }
        //请空Map
        configMap.clear();

        Iterator ir = mainConfig.getList(CONFIG_ITEM).iterator();
        while (ir.hasNext()) {
            //获得配置项名
            itemPrefix = (String) ir.next();
            refreshConfiguration(itemPrefix);
        } //while 结束

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Finish refresh all configuration.");
        }
    }

    /**
     * 取得指定名称配置信息的配置文件路径
     *
     * @param cfgName 配置信息名称
     * @return 配置文件路径
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public String getConfigurationFile(String cfgName) throws ConfigurationException {
        String confFile = mainConfig.getString(cfgName + "." + CONFIG_ITEM_FILE);

        String webappRoot = (String) contextAttributes.get(CONTEXT_ATTRIBUTE_WEBAPP_ROOT);
        if (confFile.startsWith(".") && webappRoot != null) {
            confFile = webappRoot + confFile.substring(1);
        }

        return confFile;
    }

    /**
     * 取得指定名称配置信息的配置文件类型
     *
     * @param cfgName 配置信息名称
     * @return 配置文件类型
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public String getConfigurationType(String cfgName) throws ConfigurationException {
        return mainConfig.getString(cfgName + "." + CONFIG_ITEM_TYPE);
    }

    /**
     * 取得指定名称配置信息的配置文件编码
     *
     * @param cfgName 配置信息名称
     * @return 配置文件类型
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public String getConfigurationEncoding(String cfgName) throws ConfigurationException {
        return mainConfig.getString(cfgName + "." + CONFIG_ITEM_ENCODING);
    }

    /**
     * 取得指定名称配置信息的扩展属性
     *
     * @param cfgName 配置信息名称
     * @param propName 扩展属性名称
     * @return 配置文件扩展属性
     * @throws ConfigurationException 配置信息异常
     * @test *
     */
    public String getConfigurationProperty(String cfgName, String propName) throws ConfigurationException {
        return mainConfig.getString(cfgName + "." + propName);
    }

    /**
     * 设置应用上下文属性
     * @param key 属性主键
     * @param value 属性值
     */
    public static void setContextAttribute(String key, Object value) {
        contextAttributes.put(key, value);
    }

    /**
     * 获取应用上下文属性
     * @param key 属性主键
     * @return 属性值
     */
    public static Object getContextAttribute(String key) {
        return contextAttributes.get(key);
    }

    /**
     * @return 是否是Web应用
     */
    public static boolean isWebApplication() {
        return isWebApplication;
    }

    /**
     * @return Web应用的ServletContext对象
     */
    public static ServletContext getWebApplicationServletContext() {
        return (ServletContext) getContextAttribute(CONTEXT_ATTRIBUTE_WEBAPP_SERVLETCONTEXT);
    }

    /**
     * @return Web应用的根目录
     */
    public static ServletContext getWebApplicationRoot() {
        return (ServletContext) getContextAttribute(CONTEXT_ATTRIBUTE_WEBAPP_ROOT);
    }

    /**
     * @return the copy of includeConfigurations
     */
    public List getIncludeConfigurations() {
        List result = new ArrayList();
        result.addAll(includeConfigurations);
        return result;
    }

	public static void setDEFAULT_MAIN_CONFIG_FILE(String default_main_config_file) {
		DEFAULT_MAIN_CONFIG_FILE = default_main_config_file;
	}
}