package com.hnykl.bp.base.configuration;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import com.hnykl.bp.base.tool.character.StringUtils;

public class ProductConfigMgr {
	/**
	 * Logger
	 */
	private static final Logger logger = Logger.getLogger(ProductConfigMgr.class);

	private static Configuration baseConf;

	public static Configuration getBaseConfigure() {
		if (baseConf == null) {
			try {
				ConfigurationRegister confRegister = ConfigurationRegister.getInstance();
				confRegister.refreshConfiguration("product");
				baseConf = confRegister.getConfiguration("product");
			} catch (ConfigurationException ex) {
				return null;
			}
		}
		return baseConf;
	}

	public static boolean refresh() {
		try {
			ConfigurationRegister confRegister = ConfigurationRegister.getInstance();
			confRegister.refreshConfiguration("product");
			return true;
		} catch (ConfigurationException ex) {
			return false;
		}
	}

	/**
	 * 产品图片上传相对路径
	 * 
	 * @return 产品图片上传相对路径
	 */
	public static String getProductFilePath() {
		String value = "";
		try {
			value = getBaseConfigure().getString("cnff.file.product.path");
		} catch (Exception e) {
			logger.warn("获取配置中产品图片上传相对路径:" + e.getMessage());
		}
		if (StringUtils.isEmpty(value)) {
			value = "";
		}
		return value;
	}

	/**
	 * 产品供应一键采集临时目录
	 * 
	 * @return 产品供应一键采集临时目录
	 */
	public static String getProductOneKeyCollectPath() {
		String value = "";
		try {
			value = getBaseConfigure().getString("cnff.file.product.onekey.collect");
		} catch (Exception e) {
			logger.warn("产品供应一键采集临时目录:" + e.getMessage());
		}
		if (StringUtils.isEmpty(value)) {
			value = "";
		}
		return value;
	}

}
