package com.hnykl.bp.base.configuration;
import com.hnykl.bp.base.exception.CctirRuntimeException;



public class ConfigurationException extends CctirRuntimeException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4841784799981831495L;

    /**
     * 构造无信息的异常。随后可以利用Throwable.initCause填充异常信息。
     */
    public ConfigurationException() {
        super();
    }

    /**
     * 利用提供的异常信息构造异常
     * 
     * @param msg 异常的信息
     */
    public ConfigurationException(String msg) {
        super(msg);
    }

    /**
     * 利用已有异常构建新异常,并加入自己的异常信息
     * 
     * @param msg 异常信息
     * @param e 已有异常
     */
    public ConfigurationException(String msg, Exception e) {
        super(msg, e);
    }
}