package com.hnykl.bp.base.configuration.web;

import java.net.MalformedURLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;

import com.hnykl.bp.base.configuration.ConfigurationException;
import com.hnykl.bp.base.configuration.ConfigurationRegister;


/**
 * <p>
 * 配置启动器的Web监听器
 * </p>
 * 使用配置管理器之前首先需要初始化，初始化方式有以下几种：
 * 1、在Web应用里面通过Listener启动，在web.xml里面增加下面的Listener配置即可，也可以。 <listener>
 * <listener-class>ConfigurationLoaderListener</listener-class>
 * </listener> 2、
 * 
 */
public class ConfigurationLoaderListener implements ServletContextListener {

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent event) {
        try {
            init(event);
        } catch (ServletException e) {
            event.getServletContext().log(
                    "ConfigurationLoaderListener fail to start.");
            throw new RuntimeException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent event) {
        ConfigurationRegister.releaseAll();
    }

    /**
     * Servlet loaded, initialize environment 初始化配置信息
     * 及在其中被包含的其他公共配置信息(类型为xml,properties的,类型为Path的不需要初始化)
     * 
     * @param event ServletContextEvent
     * @throws ServletException Servlet异常
     */
    public void init(ServletContextEvent event) throws ServletException {
        // 定义servlet上下文
        ServletContext sc = event.getServletContext();
        String sn = "ConfigurationLoaderListener";
        sc.log("Listener '" + sn + "' init starting");
        sc.log("Listener Context info: " + sc.getServerInfo());
        sc.log("Listener version: " + sc.getMajorVersion() + "."
                + sc.getMinorVersion());

        // 如果应用程序不是打包部署，可以直接用sc.getRealPath("/")取得webappRoot，否则需要根据web.xml的路径取得
        String webappRoot = null;
        webappRoot = sc.getRealPath("/");
        if (webappRoot == null) {
            String requestMappingsURL = null;
            try {
                requestMappingsURL = sc.getResource("/WEB-INF/web.xml")
                        .toString();
            } catch (MalformedURLException e) {
                sc.log(e.getMessage());
            }

            if (requestMappingsURL != null) {
                webappRoot = requestMappingsURL.substring(0, requestMappingsURL
                        .length()
                        - "/WEB-INF/web.xml".length());
                sc.log("webappRoot:" + webappRoot);
            }
        }

        sc.log("Listener Context real root: " + webappRoot);

        if (webappRoot != null) {
            // webappRoot = StringUtils.strReplace(webappRoot, "\\", "/");
            webappRoot = webappRoot.replace('\\', '/'); // .replaceAll("\\",
                                                        // "/");
            if (webappRoot.endsWith("/")) {
                webappRoot = webappRoot.substring(0, webappRoot.length() - 1);
            }
        }

        try {
            // 初始化默认主配置
            ConfigurationRegister.init(sc, webappRoot);
        } catch (ConfigurationException e) {
            sc.log(e.getLocalizedMessage(), e);
            System.err.println(e);
        }
        sc.log("Listener '" + sn + "' init finished");
    }

}
