package com.hnykl.bp.base.configuration.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hnykl.bp.base.configuration.ConfigurationException;
import com.hnykl.bp.base.configuration.ConfigurationRegister;

/**
 *
 * 配置信息初始化类
 * 通过在web.xml里面配置,让这个类自动初始化相关配置信息
 */
public class ConfigurationLoaderServlet extends HttpServlet {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5399792174260797844L;

    /**
     * 初始化是否成功标记
     */
    private boolean initSuccess;

    /**
     * 出错信息
     */
    private String errorMessage;

    /**
     * 装载时间
     */
    private static long loadTime;

    /**
     * Servlet loaded, initialize environment
     * 初始化配置信息,包括
     * 及在其中被包含的其他公共配置信息(类型为xml,properties的,类型为Path的不需要初始化)
     * @throws ServletException Servlet异常
     */
    public void init() throws ServletException {

        //定义servlet上下文
        ServletContext sc = getServletConfig().getServletContext();
        String sn = getServletName();
        sc.log("Servlet '" + sn + "' init starting");

        sc.log("Servlet Context info: " + sc.getServerInfo());
        sc.log("Servlet version: " + sc.getMajorVersion() + "." + sc.getMinorVersion());

        // 如果应用程序不是打包部署，可以直接用sc.getRealPath("/")取得webappRoot，否则需要根据web.xml的路径取得
        String webappRoot = sc.getRealPath("/");
        if (webappRoot == null) {
            String requestMappingsURL = null;
            try {
                requestMappingsURL = sc.getResource("/WEB-INF/web.xml").toString();
            } catch (MalformedURLException e) {
                sc.log(e.getMessage());
            }

            if (requestMappingsURL != null) {
                webappRoot = requestMappingsURL.substring(0, requestMappingsURL.length() - "/WEB-INF/web.xml".length());
                sc.log("webappRoot:" + webappRoot);
            }
        }

        sc.log("Servlet Context real root: " + webappRoot);

        if (webappRoot != null) {
            webappRoot = webappRoot.replace('\\', '/');
            if (webappRoot.endsWith("/")) {
                webappRoot = webappRoot.substring(0, webappRoot.length() - 1);
            }
        }

        try {
            // 出发其进行初始化
            ConfigurationRegister.init(sc, webappRoot);

            // 标记成功标志
            initSuccess = true;
            errorMessage = "";
        } catch (ConfigurationException e) {
            //标记失败标志
            initSuccess = false;
            errorMessage = e.getLocalizedMessage();
            if (errorMessage == null) {
                errorMessage = "no message";
            }
            sc.log(errorMessage, e);
            System.err.println(e);
        }
        sc.log("Servlet '" + sn + "' init finished");
    }

    /**
     * 返回当前系统的配置信息
     * Servlet request, return environment & related information
     * @see javax.servlet.http.HttpServlet
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<head>");
        out.println("<title>GlobalInit Servlet Page</title>");
        out.println("</head>");
        out.println("<body>");

        out.println("<h1 align=\"center\">GlobalInit Servlet Running</h1>");
        if (initSuccess) {
            out.println("<h1 align=\"center\">GlobalInit Servlet init success</h1>");
        } else {
            out.println("<h1 align=\"center\">GlobalInit Servlet init fail: " + errorMessage + "</h1>");
        }

        // output servlet context information
        out.println("<ul>");
        ServletContext sc = getServletContext();
        out.println("<li>Servlet Context info: " + sc.getServerInfo() + "</li>");
        out.println("<li>Servlet version: " + sc.getMajorVersion() + "." + sc.getMinorVersion() + "</li>");
        out.println("<li>Servlet Context real root: " + sc.getRealPath("/") + "</li>");
        out.println("<li>Servlet Context real root: " + sc.getRealPath("/") + "</li>");
        out.println("</ul>");

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSz");
        out.println("<h1 align=\"center\">Servlet load time: " + fmt.format(new Date(loadTime)) + "</h1>");
        out.println("<h1 align=\"center\">Current server time: " + fmt.format(new Date()) + "</h1>");

        out.println("</body>");
        out.println("</html>");
    }
}
