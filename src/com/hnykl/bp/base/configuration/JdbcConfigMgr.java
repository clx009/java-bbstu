package com.hnykl.bp.base.configuration;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 直接读取jdbc的相关链接
 * @author powerquanc
 *
 */
public class JdbcConfigMgr {
	  /**
     *  Logger
     */
    private static final Logger logger = Logger.getLogger(JdbcConfigMgr.class);

    private static Configuration jdbcConf;

    public static Configuration getJdbcConfigure() {
        if (jdbcConf == null) {
            try {
                ConfigurationRegister confRegister = ConfigurationRegister
                        .getInstance();
                confRegister.refreshConfiguration("jdbc");
                jdbcConf = confRegister.getConfiguration("jdbc");
            } catch (ConfigurationException ex) {
                return null;
            }
        }
        return jdbcConf;
    }

    public static boolean refresh() {
        try {
            ConfigurationRegister confRegister = ConfigurationRegister
                    .getInstance();
            confRegister.refreshConfiguration("jdbc");
            return true;
        } catch (ConfigurationException ex) {
            return false;
        }
    }

    public static String getHibernateDialect(){
    	  return getJdbcConfigure().getString("hibernate.dialect");
    }
    public static String getValidationQuerySqlserver(){
    	return getJdbcConfigure().getString("validationQuery.sqlserver");
    }
    public static String getJdbcUrl(){
    	return getConfigureValue("jdbc.url","jdbc:mysql://192.168.199.230:3306/bbstu?useUnicode=true&characterEncoding=UTF-8");
    }
    public static String getJdbcUsername(){
    	return getConfigureValue("jdbc.username","root");
    }
    public static String getJdbcPassword(){
    	return getConfigureValue("jdbc.password","root");
    }
    public static String getJdbcDbType(){
    	return getConfigureValue("jdbc.dbType","mysql");
    }
    public static String getDiverName(){
    	return getConfigureValue("diver.name","com.mysql.jdbc.Driver");
    }
    public static String getJdbcDatabase(){
    	return getConfigureValue("jdbc.database","bbstu");
    }
    public static String getConfigureValue(String param,String defaultValue){
    	String value = defaultValue;
    	try {
			value = getJdbcConfigure().getString(param);
			if(StringUtils.isEmpty(value)){
				value = defaultValue;
			}
		} catch (Exception e) {
		}
    	return value;
    }
}
