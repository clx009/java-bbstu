package com.hnykl.bp.base.configuration;

/**
 * <p>配置加载器接口</p>
 *
 */
public interface Configurer {

    /**
     * Configure
     * @param configFile IConfigurationItem
     * @throws ConfigurationException throw if any exception throws
     */
    public void configure(IConfigurationItem confItem) throws ConfigurationException;

    /**
     * 获取是否之后release方法
     * @return
     */
    public boolean supportRelease();

    /**
     * 撤销配置对象
     */
    public void release();

}
