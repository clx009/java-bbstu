package com.hnykl.bp.base.configuration;

import java.util.HashMap;

/**
 * <p>
 * ConfigurationItem, holds all the properties of the configuration item
 * </p>
 * 
 */
public class ConfigurationItem implements IConfigurationItem {

    /**
     * 存放配置项的HashMap
     */
    private final HashMap properties;

    /**
     * 当前配置项的名称
     */
    private String name;

    /**
     * 所属主配置的名称
     */
    private String mainConfigName;

    /**
     * 构造函数
     */
    public ConfigurationItem() {
        properties = new HashMap();
    }

    /**
     * @return the name of this configuration item
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name of this configuration item
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the configmethod
     */
    public String getConfigmethod() {
        return (String) properties.get(CONFIGURE_ITEM_CONFIGMETHOD);
    }

    /**
     * @param configmethod the configmethod to set
     */
    public void setConfigmethod(String configmethod) {
        properties.put(CONFIGURE_ITEM_CONFIGMETHOD, configmethod);
    }

    /**
     * @return the configurator
     */
    public String getConfigurer() {
        return (String) properties.get(CONFIGURE_ITEM_CONFIGURER);
    }

    /**
     * @param configurer the configurer to set
     */
    public void setConfigurer(String configurer) {
        properties.put(CONFIGURE_ITEM_CONFIGURER, configurer);
    }

    /**
     * @return the encoding
     */
    public String getEncoding() {
        return (String) properties.get(CONFIGURE_ITEM_ENCODING);
    }

    /**
     * @param encoding the encoding to set
     */
    public void setEncoding(String encoding) {
        properties.put(CONFIGURE_ITEM_ENCODING, encoding);
    }

    /**
     * @return the file
     */
    public String getFile() {
        return (String) properties.get(CONFIGURE_ITEM_FILE);
    }

    /**
     * @param file the file to set
     */
    public void setFile(String file) {
        properties.put(CONFIGURE_ITEM_FILE, file);
    }

    /**
     * @return the type
     */
    public String getType() {
        return (String) properties.get(CONFIGURE_ITEM_TYPE);
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        properties.put(CONFIGURE_ITEM_TYPE, type);
    }

    /**
     * add property into the configuration item
     * 
     * @param name name of property
     * @param value value of property
     */
    public void setProperty(String name, String value) {
        properties.put(name, value);
    }

    /**
     * get property from the configuration item
     * 
     * @param name name of property
     * @return value of property
     */
    public String getProperty(String name) {
        return (String) properties.get(name);
    }

    /**
     * @return the mainConfigName
     */
    public String getMainConfigName() {
        return mainConfigName;
    }

    /**
     * @param mainConfigName the mainConfigName to set
     */
    public void setMainConfigName(String mainConfigName) {
        this.mainConfigName = mainConfigName;
    }

}
