package com.hnykl.bp.base.configuration;


import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.hnykl.bp.web.cgform.common.CgAutoListConstant;
/**
 * 基本配置
 * @author powerquanc
 *  2013-9-6
 */
public class BaseConfigMgr {
	
	  /**
     *  Logger
     */
    private static final Logger logger = Logger.getLogger(BaseConfigMgr.class);

    private static Configuration baseConf;

    public static Configuration getBaseConfigure() {
        if (baseConf == null) {
            try {
                ConfigurationRegister confRegister = ConfigurationRegister
                        .getInstance();
                confRegister.refreshConfiguration("base");
                baseConf = confRegister.getConfiguration("base");
            } catch (ConfigurationException ex) {
                return null;
            }
        }
        return baseConf;
    }

    public static boolean refresh() {
        try {
            ConfigurationRegister confRegister = ConfigurationRegister
                    .getInstance();
            confRegister.refreshConfiguration("base");
            return true;
        } catch (ConfigurationException ex) {
            return false;
        }
    }

    /** 数据库名称. */
    public static String getDataBaseName() {
        return getBaseConfigure().getString("util.dbquery.dbmstype");
    }

    /** 数据库字符集. */
    public static String getDataBaseCharset() {
        return getConfigureValue("database.charset","UTF-8");
    }

    public static String getWebPageInCharset() {
        return getConfigureValue("web.page.incharset","UTF-8");
    }

    public static String getWebPageOutCharset() {
        return getConfigureValue("web.page.outcharset","UTF-8");
    }
    
    public static String getWebRootPath() {
        return getBaseConfigure().getString("web.root.path");
    }
    
    public static String getWebVisitUrl() {
        return getBaseConfigure().getString("web.visit.url");
    }
    
    public static String getWebBaseUrl() {
        return getBaseConfigure().getString("web.base.url");
    }
    
    public static String getFilebrowserBrowseUrl() {
        return getBaseConfigure().getString("filebrowserBrowseUrl");
    }
    public static String getFilebrowserImageBrowseUrl() {
        return getBaseConfigure().getString("filebrowserImageBrowseUrl");
    }
    public static String getFilebrowserFlashBrowseUrl() {
        return getBaseConfigure().getString("filebrowserFlashBrowseUrl");
    }
    public static String getFilebrowserUploadUrl() {
        return getBaseConfigure().getString("filebrowserUploadUrl");
    }
    public static String getFilebrowserImageUploadUrl() {
        return getBaseConfigure().getString("filebrowserImageUploadUrl");
    }
    public static String getFilebrowserFlashUploadUrl() {
        return getBaseConfigure().getString("filebrowserFlashUploadUrl");
    }
    public static String getSqlReadMode() {
    	return getConfigureValue(CgAutoListConstant.SYS_MODE_KEY,"PUB");
    }
    public static String getAutoScanMenuFlag() {
        return getBaseConfigure().getString("auto.scan.menu.flag");
    }
    public static String getUploadpath() {
        return getBaseConfigure().getString("uploadpath");
    }
    public static String getTemplatepath() {
        return getBaseConfigure().getString("templatepath");
    }
    public static String getButtonAuthority() {
        return getBaseConfigure().getString("button.authority");
    }
    
    public static String getOfficeHome() {
        return getBaseConfigure().getString("office_home");//openOffice安装路径不一定在项目下面
    }
    
    public static String getCkUserfiles() {
        return getWebVisitUrl() + getBaseConfigure().getString("ck.userfiles");
    }
    public static String getCkBaseDir() {
        return getWebRootPath();
    }
    public static String getRandCodeType() {
        return getBaseConfigure().getString("randCodeType");
    }
    
    public static String getRandCodeLength() {
        return getBaseConfigure().getString("randCodeLength");
    }
    /*****************************************代码生成的相关配置 start*************************************************/
    /**
     * 生成的代码前缀路径
     */
    public static String getProjectPath() {
        return getBaseConfigure().getString("project.path");
    }
    
    /**
     * java源代码的路径
     */
    public static String getJavaRootPackage() {
        return getConfigureValue("java.root.package","src");
    }
    /**
     * java业务路径
     */
    public static String getJavaBusinessPackage() {
        return getConfigureValue("java.business.package","com.hnykl.bp.web");
    }
    
    /**
     * web源代码的路径
     * @return
     */
    public static String getWebPackage() {
        return getConfigureValue("web.package","WebRoot.webpage"); 
    }
    /**
     * 生成的代码模板路径
     * @return
     */
    public static String getCodeTemplatePath() {
        return getConfigureValue("codeTemplate.path","vfs_home/codeTemplate");
    }
    /**
     * 
     * @return
     */
    public static String getBpFiledConvert() {
        return getConfigureValue("bp.filed.convert","true");
    }
    /**
     * id
     * @return
     */
    public static String getGenerateTableId() {
        return getConfigureValue("generate.table.id","id");
    }
    /**
     * 
     * @return
     */
    public static String getUiSearchFiledNum() {
        return getConfigureValue("ui.search.filed.num","1");
    }
    /**
     * 
     * @return
     */
    public static String getUiFilterFields() {
        return getConfigureValue("ui_filter_fields","create_date,create_by,create_name,update_date,update_by,update_name");
    }
    /**
     * bean注入的路径
     * @return
     */
    public static String getBeanHibernateXmlPath() {
        return getBaseConfigure().getString("bean.hibernate.xml.path");
    }
    /**
     * 获取发送短信版本
     * @return
     */
    public static String getUserSmsSendVersion(){
    	String value = "";
    	try {
    		value = getBaseConfigure().getString("user.sms.send.version");
    	} catch (Exception e) {
    		logger.warn("获取发送短信版本出现异常:" + e.getMessage());
    	}
    	return value;
    }
    /**
     * 获取发送短信服务地址
     * @return
     */
    public static String getUserSmsSendServerurl(){
    	String value = "";
    	try {
    		value = getBaseConfigure().getString("user.sms.send.serverurl");
    	} catch (Exception e) {
    		logger.warn("获取发送短信帐号id出现异常:" + e.getMessage());
    	}
    	return value;
    }
    /**
     * 获取发送短信帐号id
     * @return
     */
    public static String getUserSmsSendAccountSid(){
    	String value = "";
    	try {
    		value = getBaseConfigure().getString("user.sms.send.accountSid");
    	} catch (Exception e) {
    		logger.warn("获取发送短信帐号id出现异常:" + e.getMessage());
    	}
    	return value;
    }
    /**
     * 获取发送短信权限认证token
     * @return
     */
    public static String getUserSmsSendAuthToken(){
    	String value = "";
    	try {
    		value = getBaseConfigure().getString("user.sms.send.authToken");
    	} catch (Exception e) {
    		logger.warn("获取发送短信权限认证token出现异常:" + e.getMessage());
    	}
    	return value;
    }
    /**
     * 忘记密码发送短信短信模板
     * @return
     */
    public static String getUserSmsSendForgetpwdTemplateId(){
    	String value = "";
    	try {
    		value = getBaseConfigure().getString("user.sms.send.forgetpwd.templateId");
    	} catch (Exception e) {
    		logger.warn("获取发送短信模板出现异常:" + e.getMessage());
    	}
    	return value;
    }
    /**
     * 注册发送短信短信模板
     * @return
     */
    public static String getUserSmsSendRegTemplateId(){
    	String value = "";
    	try {
    		value = getBaseConfigure().getString("user.sms.send.reg.templateId");
    	} catch (Exception e) {
    		logger.warn("获取发送短信模板出现异常:" + e.getMessage());
    	}
    	return value;
    }
    /**
     * 获取发送短信app_id
     * @return
     */
    public static String getUserSmsSendAppId(){
    	String value = "";
    	 try {
             value = getBaseConfigure().getString("user.sms.send.appId");
         } catch (Exception e) {
         	logger.warn("获取发送短信应用id出现异常:" + e.getMessage());
         }
    	return value;
    }
    /**
     * 获取短信验证码发送最大有效时间
     * @return
     */
    public static long getUserSmsSendMaxtime(){
    	long value = 0;
    	try {
    		value = getBaseConfigure().getLong("user.sms.send.maxtime");
    	} catch (Exception e) {
    		logger.warn("获取发送短信验证码发送最大有效时间出现异常:" + e.getMessage());
    	}
    	return value;
    }
    public static String getConfigureValue(String param,String defaultValue){
    	String value = defaultValue;
    	try {
			value = getBaseConfigure().getString(param);
			if(StringUtils.isEmpty(value)){
				value = defaultValue;
			}
		} catch (Exception e) {
		}
    	return value;
    }

	 /**
     * 获取注册创建群组默认名称
     * @return
     */
    public static String getUserRegisterDefaultFamilyName(){
    	String value = "";
    	try {
    		value = getBaseConfigure().getString("user.register.default.familyname");
    	} catch (Exception e) {
    		logger.warn("获取注册创建群组默认名称出现异常:" + e.getMessage());
    	}
    	return value;
    }


      /**
     * 获取邀请码发送最大有效时间
     * @return
     */
    public static long getUserInviteCodeMaxtime(){
    	long value = 0;
    	try {
    		value = getBaseConfigure().getLong("user.invite.code.maxtime");
    	} catch (Exception e) {
    		logger.warn("获取邀请码发送最大有效时间出现异常:" + e.getMessage());
    	}
    	return value;
    }
    

    /**
   * 获取默认头像路径
   * @return
   */
  public static String getUserRegisterDefaultHeadPortraitPath(){
  	String value="";
  	try {
  		value = getBaseConfigure().getString("user.register.default.headPortrait.path");
  	} catch (Exception e) {
  		logger.warn("获取注册默认头像路径出现异常:" + e.getMessage());
  	}
  	return value;
  }
    
    /*****************************************代码生成的相关配置 end*************************************************/
}
