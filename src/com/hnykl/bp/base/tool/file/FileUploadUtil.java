package com.hnykl.bp.base.tool.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import com.hnykl.bp.base.exception.FileUploadException;

public  class FileUploadUtil {
    /**
     * 上传文件请求
     */
    private HttpServletRequest m_request;
    /**
     * @see m_request
     */
    public FileUploadUtil(HttpServletRequest request) {
        this.m_request = request;
    }
    /**
     * 文件上传
     * @param MAX_UPLOAD_FILE_SIZE 最大文件大小
     * @param Threshold_Size 缓冲文件大小
     * @param repositoryPath 缓冲区路径
     * @param savePath 文件保存路径
     * @return 文件名列表
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public List upload(int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
                       String repositoryPath, String savePath) throws FileUploadException {
        List res = new ArrayList();
        try {
            DiskFileUpload fu = new DiskFileUpload();
            fu.setSizeMax(MAX_UPLOAD_FILE_SIZE);
            fu.setSizeThreshold(Threshold_Size);
            fu.setRepositoryPath(repositoryPath);
            List items = null; 
            try {
                items = fu.parseRequest(m_request);
            } catch (Exception e) {
                throw new FileUploadException("上传失败，请检查文件大小是否超出限制: ",e);
            }
            for (int i = 0; i < items.size(); i++) {
                FileItem item = (FileItem) items.get(i);
                if (!item.isFormField()) { //非普通表单字段
                    String name = item.getName();
                    if (name.lastIndexOf("\\") != -1) {
                        name = name.substring(name.lastIndexOf("\\") + 1);
                    }
                    if (name.lastIndexOf(":") != -1) {
                        name = name.substring(name.lastIndexOf(":") + 1);
                    }
                    long time = System.currentTimeMillis();
                    name =  time+name;
                    File saveFile = new File(savePath);
                    if(!saveFile.exists()){
                    	saveFile.mkdirs();
                    }
                    item.write(new java.io.File(savePath + "/" + name));
                    res.add(name);
                    item.delete();
                }
            }
        } catch (Exception e) {
        	e.printStackTrace();
            throw new FileUploadException("上传文件失败！-"+e.getMessage(),e);
        }
        return res;
    }
    
    
    
    /**
     * 文件上传
     * @param MAX_UPLOAD_FILE_SIZE 最大文件大小
     * @param Threshold_Size 缓冲文件大小
     * @param repositoryPath 缓冲区路径
     * @param savePath 文件保存路径
     * @return 文件名列表
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public List upload(String destFileName, int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
                       String repositoryPath, String savePath) throws FileUploadException {
        List res = new ArrayList();
        try {
            DiskFileUpload fu = new DiskFileUpload();
            fu.setSizeMax(MAX_UPLOAD_FILE_SIZE);
            fu.setSizeThreshold(Threshold_Size);
            fu.setRepositoryPath(repositoryPath);
            List items = null; 
            try {
                items = fu.parseRequest(m_request);
            } catch (Exception e) {
                throw new FileUploadException("上传失败，请检查文件大小是否超出限制: ");
            }
            for (int i = 0; i < items.size(); i++) {
                FileItem item = (FileItem) items.get(i);
                if (!item.isFormField()) { //非普通表单字段
//                    String name = item.getName();
//                    if (name.lastIndexOf("\\") != -1) {
//                        name = name.substring(name.lastIndexOf("\\") + 1);
//                    }
//                    if (name.lastIndexOf(":") != -1) {
//                        name = name.substring(name.lastIndexOf(":") + 1);
//                    }
                    //long time = System.currentTimeMillis();
                    //name = time + name;
                    item.write(new java.io.File(savePath + "/" + destFileName));
                    res.add(destFileName);
                    item.delete();
                    break;
                }
            }
        } catch (Exception e) {
            throw new FileUploadException("上传文件失败！");
        }
        return res;
    }
    
    
    /**
     * 文件上传
     * @param MAX_UPLOAD_FILE_SIZE 最大文件大小
     * @param Threshold_Size 缓冲文件大小
     * @param repositoryPath 缓冲区路径
     * @param savePath 文件保存路径
     * @param encode 网页上传文件的编码
     * @return 文件名列表
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public List upload(int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
                       String repositoryPath, String savePath, String encode) throws FileUploadException {
        List res = new ArrayList();
        try {
            DiskFileUpload fu = new DiskFileUpload();
            fu.setSizeMax(MAX_UPLOAD_FILE_SIZE);
            fu.setSizeThreshold(Threshold_Size);
            fu.setRepositoryPath(repositoryPath);
            fu.setHeaderEncoding(encode);
            List items = null; 
            try {
                items = fu.parseRequest(m_request);
            } catch (Exception e) {
                throw new FileUploadException("上传失败，请检查文件大小是否超出限制: ");
            }
            for (int i = 0; i < items.size(); i++) {
                FileItem item = (FileItem) items.get(i);
                if (!item.isFormField()) { //非普通表单字段
                    String name = item.getName();
                    if (name.lastIndexOf("\\") != -1) {
                        name = name.substring(name.lastIndexOf("\\") + 1);
                    }
                    if (name.lastIndexOf(":") != -1) {
                        name = name.substring(name.lastIndexOf(":") + 1);
                    }
                    long time = System.currentTimeMillis();
                    name = time + name;
                    item.write(new java.io.File(savePath + "/" + name));
                    res.add(name);
                    item.delete();
                }
            }
        } catch (Exception e) {
            throw new FileUploadException("上传文件失败！");
        }
        return res;
    }
    
    /**
     * 获取文件html元素的列表
     * @param MAX_UPLOAD_FILE_SIZE 最大文件大小
     * @param Threshold_Size 缓冲文件大小
     * @param repositoryPath 缓冲区路径
     * @return html元素的列表
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public List getUploadItems(int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
            String repositoryPath) throws FileUploadException {
       return getUploadItems(MAX_UPLOAD_FILE_SIZE, Threshold_Size, repositoryPath, null);
    }
    
    
    /**
     * 获取文件html元素的列表
     * @param MAX_UPLOAD_FILE_SIZE 最大文件大小
     * @param Threshold_Size 缓冲文件大小
     * @param repositoryPath 缓冲区路径
     * @param encode 网页上传文件的编码
     * @return html元素的列表
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public List getUploadItems(int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
            String repositoryPath, String encode) throws FileUploadException {
        DiskFileUpload fu = new DiskFileUpload();
        fu.setSizeMax(MAX_UPLOAD_FILE_SIZE);
        fu.setSizeThreshold(Threshold_Size);
        fu.setRepositoryPath(repositoryPath);
        if (null != encode && !"".equals(encode.trim())) {
            fu.setHeaderEncoding(encode.trim());
        }
        List items = new ArrayList();
        try {
            items = fu.parseRequest(m_request);
        } catch (Exception e) {
            throw new FileUploadException("获取文件html元素的列表,请检查表单类型:enctype 是否为 'multipart/form-data' ");
        }
        return items;
    }
   
    
    /**
     * 文件上传
     * @param items  items
     * @param savePath 文件保存路径
     * @return 文件名列表
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public HashMap upload(List items, String savePath) throws FileUploadException {
        
        HashMap res = new HashMap();
        try {
                
            for (int i = 0; i < items.size(); i++) {
                FileItem item = (FileItem) items.get(i);
                if (!item.isFormField()) { //非普通表单字段
                    String paramName = item.getName();
                    String paramValue = item.getName();
                    if (paramValue.lastIndexOf("\\") != -1) {
                        paramValue = paramValue.substring(paramValue.lastIndexOf("\\") + 1);
                    }
                    if (paramValue.lastIndexOf(":") != -1) {
                        paramValue = paramValue.substring(paramValue.lastIndexOf(":") + 1);
                    }
                    long time = System.currentTimeMillis();
                    paramValue = time + paramValue;
                    item.write(new java.io.File(savePath + "/" + paramValue));
                    res.put(paramName, paramValue);
                    item.delete();                    
                }
            }
        } catch (Exception e) {
            throw new FileUploadException("上传文件失败！");
        }
        
        return res;
      }
      
    public String upload(FileItem item, String savePath) throws FileUploadException {        
        String paramValue = item.getName();
        try {                            
            if (!item.isFormField()) { //非普通表单字段
                //String paramName = item.getName();                    
                if (paramValue.lastIndexOf("\\") != -1) {
                    paramValue = paramValue.substring(paramValue.lastIndexOf("\\") + 1);
                }
                if (paramValue.lastIndexOf(":") != -1) {
                    paramValue = paramValue.substring(paramValue.lastIndexOf(":") + 1);
                }
                long time = System.currentTimeMillis();
                paramValue = time + paramValue;
                item.write(new java.io.File(savePath + "/" + paramValue));                
            }
        } catch (Exception e) {
            throw new FileUploadException("上传文件失败！");
        }
        
        return paramValue;
      }   
    
    
    /**
     * 文件上传
     * @param MAX_UPLOAD_FILE_SIZE 最大文件大小
     * @param Threshold_Size 缓冲文件大小
     * @param repositoryPath 缓冲区路径
     * @param savePath 文件保存路径
     * @param encode 网页上传文件的编码
     * @return 文件名列表
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public List uploadProgress(int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
                       String repositoryPath, String savePath, String encode, FileItemFactory factory) throws FileUploadException {
        List res = new ArrayList();
        
        //UploadListener listener = new UploadListener(m_request, 30);
        // Create a factory for disk-based file items
       // FileItemFactory factory = new MonitoredDiskFileItemFactory(listener);
        
        try {
            DiskFileUpload fu = new DiskFileUpload();
            fu.setSizeMax(MAX_UPLOAD_FILE_SIZE);
            fu.setSizeThreshold(Threshold_Size);
            fu.setRepositoryPath(repositoryPath);
            fu.setHeaderEncoding(encode);
            fu.setFileItemFactory(factory);
            List items = null; 
            try {
                items = fu.parseRequest(m_request);
            } catch (Exception e) {
                throw new FileUploadException("上传失败，请检查文件大小是否超出限制: ");
            }
            for (int i = 0; i < items.size(); i++) {
                FileItem item = (FileItem) items.get(i);
                if (!item.isFormField()) { //非普通表单字段
                    String name = item.getName();
                    if (name.lastIndexOf("\\") != -1) {
                        name = name.substring(name.lastIndexOf("\\") + 1);
                    }
                    if (name.lastIndexOf(":") != -1) {
                        name = name.substring(name.lastIndexOf(":") + 1);
                    }
                    long time = System.currentTimeMillis();
                    name = time + name;
                    item.write(new java.io.File(savePath + "/" + name));
                    res.add(name);
                    item.delete();
                }
            }
        } catch (Exception e) {
            throw new FileUploadException("上传文件失败！");
        }
        return res;
    }
    
    /**
     * 文件上传,并且获取其他普通表单字段
     * @param destFileName 目标文件名
     * @param MAX_UPLOAD_FILE_SIZE 最大文件大小
     * @param Threshold_Size 缓冲文件大小
     * @param repositoryPath 缓冲区路径
     * @param savePath 文件保存路径
     * @return List[0]文件名列表(List), List[1]普通表单字段(Map)
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public List uploadFileAndGetOtherFiled(String destFileName,
			int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
			String repositoryPath, String savePath) throws FileUploadException {
	
		List fileList = new ArrayList();
		Map fieldMap = new HashMap();

		try {
			DiskFileUpload fu = new DiskFileUpload();
			fu.setSizeMax(MAX_UPLOAD_FILE_SIZE);
			fu.setSizeThreshold(Threshold_Size);
			fu.setRepositoryPath(repositoryPath);
			List items = null;
			try {
				items = fu.parseRequest(m_request);
			} catch (Exception e) {
				throw new FileUploadException("上传失败，请检查文件大小是否超出限制: ");
			}
			for (int i = 0; i < items.size(); i++) {
				FileItem item = (FileItem) items.get(i);
				if (!item.isFormField()) { //非普通表单字段
					String name = item.getName();
					if (name.lastIndexOf("\\") != -1) {
						name = name.substring(name.lastIndexOf("\\") + 1);
					}
					if (name.lastIndexOf(":") != -1) {
						name = name.substring(name.lastIndexOf(":") + 1);
					}
					item.write(new java.io.File(savePath + "/" + destFileName));
					fileList.add(destFileName);
					item.delete();
				} else {
					String name = item.getFieldName();
					fieldMap.put(name, item.getString());
				}
			}
		} catch (Exception e) {
			throw new FileUploadException("上传文件失败！");
		}

		List result = new ArrayList();
		result.add(fileList);
		result.add(fieldMap);
		return result;

	}
    
    
    /**
     * 文件上传,并且获取其他普通表单字段
     * @param destFileName 目标文件名，如果该目录下有同名的文件，则在文件名后面添加"_1, _2, _3, _n'等字符
     * @param MAX_UPLOAD_FILE_SIZE 最大文件大小
     * @param Threshold_Size 缓冲文件大小
     * @param repositoryPath 缓冲区路径
     * @param savePath 文件保存路径
     * @return List[0]文件名列表(List), List[1]普通表单字段(Map)
     * @throws FileUploadException 假如上传失败，抛出异常
     */
    public List uploadFileAndGetOtherFiled2nd(Map destFileNameMap,
			int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
			String repositoryPath, String savePath) throws FileUploadException {
	
		List fileList = new ArrayList();
		Map fieldMap = new HashMap();

		try {
			DiskFileUpload fu = new DiskFileUpload();
			fu.setSizeMax(MAX_UPLOAD_FILE_SIZE);
			fu.setSizeThreshold(Threshold_Size);
			fu.setRepositoryPath(repositoryPath);
			List items = null;
			try {
				items = fu.parseRequest(m_request);
			} catch (Exception e) {
				throw new FileUploadException("上传失败，请检查文件大小是否超出限制: ",e);
			}
			for (int i = 0; i < items.size(); i++) {
				FileItem item = (FileItem) items.get(i);
				if (!item.isFormField()) { //非普通表单字段
					String name = item.getName();
					if (name.lastIndexOf("\\") != -1) {
						name = name.substring(name.lastIndexOf("\\") + 1);
					}
					if (name.lastIndexOf(":") != -1) {
						name = name.substring(name.lastIndexOf(":") + 1);
					}
					String fieldname = item.getFieldName();
					String destFileName = "";
					if(destFileNameMap.get(fieldname) != null){
						destFileName = (String)destFileNameMap.get(fieldname);
					}else{
						destFileName = fieldname;
					}
					//看看目录下是否有相似的文件
					List list = FileUtil.getFileList(savePath, destFileName);
					int size = list.size();
					if (size >= 1) {
						destFileName = destFileName + "_" + size;
					}
					
					item.write(new java.io.File(savePath + "/" + destFileName));
					
					
					fileList.add(destFileName);
					item.delete();
				} else {
					String name = item.getFieldName();
					fieldMap.put(name, item.getString());
				}
			}
		} catch (Exception e) {
			throw new FileUploadException("上传文件失败！",e);
		}

		List result = new ArrayList();
		result.add(fileList);
		result.add(fieldMap);
		return result;
	}
    public InputStream getInputStream(int MAX_UPLOAD_FILE_SIZE, int Threshold_Size,
			String repositoryPath, String fieldName) throws FileUploadException {
    	InputStream ret = null;
		try {
			DiskFileUpload fu = new DiskFileUpload();
			fu.setSizeMax(MAX_UPLOAD_FILE_SIZE);
			fu.setSizeThreshold(Threshold_Size);
			fu.setRepositoryPath(repositoryPath);
			List items = null;
			try {
				items = fu.parseRequest(m_request);
			} catch (Exception e) {
				e.printStackTrace();
				throw new FileUploadException("上传失败，请检查文件大小是否超出限制: ");
			}
			for (int i = 0; i < items.size(); i++) {
				FileItem item = (FileItem) items.get(i);
				if (!item.isFormField()) { //非普通表单字段
					String name = item.getFieldName();
					if (name.equalsIgnoreCase(fieldName)) {
						ret=item.getInputStream();
					}
					item.delete();
				}
			}
		} catch (Exception e) {
			throw new FileUploadException("上传文件失败！");
		}
		return ret;
	} 

	/**
	 * 没有struts2的标签,struts2的文件上传
	 * @param myFile
	 * @param uploadPath
	 * @param fileName
	 * @param maxUploadFileSize
	 * @throws Exception
	 */
	public static boolean struts2FileUpload(File myFile,String uploadPath,String fileName) throws Exception{
		boolean flag = false;
		InputStream is = null;
		OutputStream os = null;
		try{
			//基于myFile创建一个文件输入流
			 is = new FileInputStream(myFile);
			// 设置目标文件
			File toFile = new File(uploadPath, fileName);
			// 创建一个输出流
			 os = new FileOutputStream(toFile);
			//设置缓存
			byte[] buffer = new byte[1024];
			int length = 0;
			//读取myFile文件输出到toFile文件中
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			flag = true;
		}catch(Exception ex){
			flag = false;
			throw new Exception(ex.getMessage());
		}finally{
			//关闭输入流
			if(is != null){
				is.close();
				is = null;
			}
			//关闭输出流
			if(os != null){
				os.close();
				os = null;
			}
		}
		return flag;
	}
    
}
