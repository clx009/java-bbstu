package com.hnykl.bp.base.tool.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hnykl.bp.base.exception.CctirRuntimeException;

/**
 * 文件操作工具
 */
public class FileUtil {
    private static Logger log = Logger.getLogger(FileUtil.class);
    public FileUtil() {
    }

    /**
     * 创建目录
     * 
     * @param folderName
     *            目录名
     * @param path
     *            创建目录的当前路径
     */
    public static void createFolder(String folderName, String path) {

        File discFolder = new File(path + folderName);
        if (!discFolder.exists()) {
            boolean success = discFolder.mkdirs();
            if (!success) {
                // ����ʧ����Ϣ
            }
        }
    }

    /**
     * 删除文件
     * 
     * @param fileName
     *            文件名
     * @param path
     *            文件所在路径
     */
    public static void removeFile(String fileName, String path) {

        File discFile = new File(path + fileName);

        if (discFile.exists()) {
            boolean success = discFile.delete();
            if (!success) {
                // ����ʧ����Ϣ
            }
        }
    }

    /**
     * 写文件
     * 
     * @param fileName
     *            文件名
     * @param path
     *            文件所在路径
     * @param contents
     *            文件内容
     */
    public static void writeFile(String fileName, String path, byte[] contents) {

        File discFile = new File(path + fileName);
        try {
            OutputStream s = new FileOutputStream(discFile);
            s.write(contents);
            s.close();
        } catch (IOException e) {
            throw new CctirRuntimeException(e);
        }
    }
    
    public static void writeFile(String fileName, byte[] contents) {

        File discFile = new File(fileName);
        try {
            OutputStream s = new FileOutputStream(discFile);
            s.write(contents);
            s.close();
        } catch (IOException e) {
            throw new CctirRuntimeException(e);
        }
    }

    /**
     * 读文件
     * 
     * @param fileName
     *            文件名
     * @param path
     *            文件所在路径
     * @return 文件内容
     * @throws CustomException
     */
    public static byte[] readFile(String fileName, String path) {
        try {
            FileInputStream fis = new FileInputStream(path + fileName);
            byte[] b = new byte[fis.available()];
            fis.read(b);
            return b;
        } catch (IOException e) {
            throw new CctirRuntimeException(e);
        }
    }

    /**
     * 读文件
     * 
     * @param path
     *            文件路径(包含文件名)
     * @return 文件内容
     */
    public static byte[] readFile(String path) {

        try {
            FileInputStream fis = new FileInputStream(path);
            byte[] b = new byte[fis.available()];
            fis.read(b);
            return b;
        } catch (IOException e) {
            throw new CctirRuntimeException(e);
        }
    }
    public static synchronized  boolean fileExists(String sFilePathName) {
        File file = new File(sFilePathName); 
        
       return fileExists(file);
        
    }


      /**
     * 判断文件是否存在
     *
     * @param oFile 文件
     * @return 如果存在则返回true，否则false
     */
    public static synchronized  boolean fileExists(File oFile) {
           if (null == oFile) {
            return false;
        } else {
            return oFile.exists();
        }
      }

    /**
     * 删除文件
     *
     * @param dir_path 文件
     * @throws FileNotFoundException 文件不存在异常
     */
    public static synchronized void deleteFiles(String dir_path) throws FileNotFoundException {
        File file = new File(dir_path);
        if (file != null && !file.exists()) {
            log.warn("删除文件时没有找到文件");
            throw new FileNotFoundException("删除文件时没有找到文件");
        }
        if (file.isDirectory()) {
            File[] fe = file.listFiles();
            for (int i = 0; null != fe && i < fe.length; i++) {
                deleteFiles(fe[i].toString());
                if (null != fe[i]) {
                    fe[i].delete(); //删除已经是空的子目录
                }
            }
        }
        if (null != file) {
            file.delete(); //删除总目录
        }
    }

    /**
     * 创建目录
     * @param path path
     * @return 是否创建成功
     */
    public static  synchronized boolean  createDir(String path) {
        //如果目录不存在，则创建
        File sfo = new File(path);
        if (!sfo.exists()) {
            sfo.mkdirs();
        }
        return fileExists(sfo);
    }
    
    /**
     * 查出在某个文件目录下，以likeFileName文件名为开头的文件列表，
     * 使用时要确保文件目录下的文件不多，不然会有性能问题。
     * @param path path 文件目录
     * @param likeFileName 要查询的文件名
     * @return 和likeFileName相似的文件列表
     */
    public static  synchronized List getFileList(String path, String likeFileName) {
    	List list = new ArrayList();
    	if(!path.endsWith("/"))path = path + "/";
    	if (!fileExists(path + likeFileName)) {
    		return list;
    	}
    	File file = new File(path);
    	
    	FilenameFilter filter = new SameFilenameFilter(likeFileName);
    	
    	if (file.isDirectory()) {
            File[] fe = file.listFiles(filter);
            for (int i = 0; i < fe.length; i++) {
        		list.add(fe[i].getName());
        	}
    	}
    	
    	return list;
    }

    
    /**
     * 取得目录下文件个数，不计算嵌套文件的个数
     * @param dir
     * @return
     */
    public static int getNotNestedFilesCount(File dir) {
    		if (null != dir &&  dir.isDirectory()) {
    			return dir.listFiles().length;
    		}  else {
    			return 0;
    		}
    }
    
}