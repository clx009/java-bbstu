package com.hnykl.bp.base.tool.file;

import java.io.File;
import java.io.FilenameFilter;

public class SameFilenameFilter implements FilenameFilter {
	private String likeFileName;
	public SameFilenameFilter(String likeFileName) {
		this.likeFileName = likeFileName;
	}

	public boolean accept(File dir, String name) {
		name = (name.indexOf(".")>-1)?name.substring(0,name.lastIndexOf(".")):name;
		likeFileName = (likeFileName.indexOf(".")> -1)? likeFileName.substring(0,likeFileName.lastIndexOf(".")):likeFileName;
		if (name != null && name.startsWith(likeFileName)) {
			return true;
		}
		return false;
	}
	
	public static void main(String []args){
		String filename = "aaa.jpg";
		System.out.println(filename.indexOf("."));
	}
	
}
