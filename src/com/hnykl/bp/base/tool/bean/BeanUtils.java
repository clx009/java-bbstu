
package com.hnykl.bp.base.tool.bean;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * <p>
 * JavaBean创建或转换工具类
 * </p>
 */
public class BeanUtils {
    /**
     * 当前类的logger
     */
    private static Logger logger = Logger.getLogger(BeanUtils.class);

    /**隐藏构造函数.*/
    private BeanUtils() { }
    
    /**
     * 以某个bean为模板创建新的bean实例
     * @param beanClass 目标bean类型
     * @param fromBean 数据来源bean
     * @return 新bean
     */
    public static Object createBean(Class beanClass, Object fromBean) {
        try {
            Object toBean = beanClass.newInstance();
            populateBean(toBean, fromBean);
            return toBean;
        } catch (InstantiationException e) {
            logger.debug(e);
        } catch (IllegalAccessException e) {
            logger.debug(e);
        }
        return null;
    }

    /**
     * 以页面请求为数据来源创建新的bean实例
     * @param beanClass 目标bean类型
     * @param request 页面请求
     * @return 新bean
     */
    public static Object createBean(Class beanClass, HttpServletRequest request) {
        try {
            Object toBean = beanClass.newInstance();
            createBean(toBean, request);
            return toBean;
        } catch (InstantiationException e) {
            logger.debug(e);
        } catch (IllegalAccessException e) {
            logger.debug(e);
        }
        return null;
    }

    /**
     * 从request中获取参数初始化bean
     * 
     * @param bean Benn 类
     * @param request 请求
     * @return Benn 类
     */
    public static Object createBean(Object bean, HttpServletRequest request) {
        Map properties = new HashMap();
        Enumeration names = null;
        names = request.getParameterNames();
        while (names.hasMoreElements()) {
            String name = (String) names.nextElement();
            properties.put(name, request.getParameter(name));
        }
        populate(bean, properties);
        return bean;
    }

    /**
     * 从一个对象向另一个对象填充数据.
     * @param toBean 目标对象
     * @param fromBean 数据来源对象
     * @return 目标对象
     */
    public static Object populateBean(Object toBean, Object fromBean) {
        Field[] fields = fromBean.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            Class type = field.getType();
            String name = field.getName();
            name = (name.substring(0, 1)).toUpperCase() + name.substring(1);
            String setMethodName = "set" + name;
            String getMethodName = "get" + name;

            Method getMethod = null;
            try {
                getMethod = fromBean.getClass().getMethod(getMethodName, null);
            } catch (Exception e) {
                if (logger.isDebugEnabled()) {
                    logger.debug(e);
                }
            }

            if (getMethod == null && type == Boolean.TYPE) {
                try {
                    getMethod = fromBean.getClass()
                            .getMethod("is" + name, null);
                } catch (Exception e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug(e);
                    }
                }
            }

            if (getMethod == null) {
                continue;
            }

            try {
                Method setmethod = toBean.getClass().getMethod(setMethodName,
                        new Class[] {type});
                if (setmethod == null) {
                    continue;
                }

                Object value = getMethod.invoke(fromBean, null);
                if (value == null) {
                    continue;
                }
                /*
                 * if (type.isPrimitive()) { if (type == Integer.TYPE) { value =
                 * new Integer(value.toString()); } else if (type == Long.TYPE) {
                 * value = new Long(value.toString()); } else if (type ==
                 * Float.TYPE) { value = new Float(value.toString()); } else if
                 * (type == Double.TYPE) { value = new Double(value.toString()); } }
                 */

                setmethod.invoke(toBean, new Object[] {value});
            } catch (Exception e) {
                if (logger.isDebugEnabled()) {
                    logger.debug(e);
                }
            }
        }
        return toBean;
    }

    /**
     * createBean函数的辅助函数. 改为public,liji
     * 
     * @param bean 目标对象
     * @param properties 存储属性列表的对象
     */
    public static void populate(Object bean, Map properties) {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            Class type = field.getType();
            String name = field.getName();
            String methodName = "set" + (name.substring(0, 1)).toUpperCase()
                    + name.substring(1);
            try {
                Object value = properties.get(name);
                // System.out.println(name + ":" + value);
                if (value == null) {
                    continue;
                }

                if (value instanceof String[] && type != String[].class) {
                    String[] ss = (String[]) value;
                    String temp = "";
                    for (int j = 0; j < ss.length; j++) {
                        temp += "," + ss[j];
                    }
                    if (temp.length() > 0) {
                        value = temp.substring(1);
                    }
                }

                if (type.isPrimitive()) {
                    if (type == Integer.TYPE) {
                        value = new Integer(value.toString());
                    } else if (type == Long.TYPE) {
                        value = new Long(value.toString());
                    } else if (type == Float.TYPE) {
                        value = new Float(value.toString());
                    } else if (type == Double.TYPE) {
                        value = new Double(value.toString());
                    } else if (type == Boolean.TYPE) {
                        if (value.toString().equalsIgnoreCase("true")
                                || value.toString().equalsIgnoreCase("t")
                                || value.toString().equalsIgnoreCase("y")) {
                            value = Boolean.TRUE;
                        } else {
                            value = Boolean.FALSE;
                        }
                    }
                } else if (type == Integer.class) {
                    value = new Integer(value.toString());
                } else if (type == Long.class) {
                    value = new Long(value.toString());
                } else if (type == Float.class) {
                    value = new Float(value.toString());
                } else if (type == Double.class) {
                    value = new Double(value.toString());
                } else if (type == Boolean.class) {
                    if (value.toString().equalsIgnoreCase("true")
                            || value.toString().equalsIgnoreCase("t")
                            || value.toString().equalsIgnoreCase("y")) {
                        value = Boolean.TRUE;
                    } else {
                        value = Boolean.FALSE;
                    }
                } else if (type == java.util.Date.class) {
                    if (value.toString().length() <= 10) {
                        value = new SimpleDateFormat("yyyy-MM-dd").parse(value
                                .toString());
                    } else if(value.toString().length() > 10&&value.toString().length() <= 16){
                    	value = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(value
                                .toString());
                    }else {
                        value = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                                .parse(value.toString());
                    }
                }
                Method method = bean.getClass().getMethod(methodName,
                        new Class[] {type});
                method.invoke(bean, new Object[] {value});
            } catch (Exception e) {
                logger.debug(e);
            }
        }
    }

    /**
     * 从对象中反射获得某个属性的值
     * @param bean 对象
     * @param propertyName 属性名
     * @return 指定属性的值
     */
    public static Object getBeanValue(Object bean, String propertyName) {
        propertyName = (propertyName.substring(0, 1)).toUpperCase()
                + propertyName.substring(1);
        String getMethodName = "get" + propertyName;

        Method getMethod = null;
        try {
            getMethod = bean.getClass().getMethod(getMethodName, null);
        } catch (Exception e) {
            if (logger.isDebugEnabled()) {
                logger.debug(e);
            }
        }
        if (getMethod == null) {
            try {
                getMethod = bean.getClass()
                        .getMethod("is" + propertyName, null);
            } catch (Exception e) {
                if (logger.isDebugEnabled()) {
                    logger.debug(e);
                }
            }
        }

        if (getMethod == null) {
            throw new IllegalArgumentException("在bean中取得属性:" + propertyName
                    + "失败,没有属性对应的获取方法");
        }

        Object value = null;
        try {
            value = getMethod.invoke(bean, null);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("在bean中取得属性:" + propertyName
                    + "失败," + e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("在bean中取得属性:" + propertyName
                    + "失败," + e);
        } catch (InvocationTargetException e) {
            throw new IllegalArgumentException("在bean中取得属性:" + propertyName
                    + "失败," + e);
        }

        return value;
    }
    /**
     * 判断类型是否属于基本类型
     * @param clazz 类型
     * @return true或false
     */
    public static boolean isBasicTypeOfClass(Class clazz) {
        return clazz.equals(String.class) || clazz.equals(Integer.class) || clazz.equals(Byte.class)
                || clazz.equals(Long.class) || clazz.equals(Double.class) || clazz.equals(Float.class)
                || clazz.equals(Character.class) || clazz.equals(Short.class) || clazz.equals(BigDecimal.class)
                || clazz.equals(BigInteger.class) || clazz.equals(Boolean.class) || clazz.isPrimitive()
                || java.util.Date.class.isAssignableFrom(clazz);
    }
    /**
     * 判断对象是否属于基本类型
     * @param obj 对象
     * @return true或false
     */
    public static boolean isBaseTypeOfObject(Object obj) {
        Class clazz = obj.getClass();
        return isBasicTypeOfClass(clazz);
    }
    /**
     * 获取对象所有字段的值
     * @param bean 目标对象
     * @return 字段名称与字段值的集合
     */
    public static Map getAllFieldValues(Object bean) {
        Map properties = new HashMap();
        if(bean == null) return properties;
        Field[] fields = bean.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            Class type = field.getType();
            String name = field.getName();
            String methodName = "get" + (name.substring(0, 1)).toUpperCase() + name.substring(1);
            try {
                Method method = null;

                try {
                    method = bean.getClass().getMethod(methodName, new Class[] {});
                } catch (NoSuchMethodException e) {
                    if (type == Boolean.TYPE) {
                        if (name.startsWith("is")) {
                            methodName = name;
                        } else {
                            methodName = "is" + (name.substring(0, 1)).toUpperCase() + name.substring(1);
                        }

                        try {
                            method = bean.getClass().getMethod(methodName, new Class[] {});
                        } catch (NoSuchMethodException e2) {
                        }
                    }
                }

                // No method found 
                if (method == null || method.getModifiers() != 1) {
                    continue;
                }

                Object value = method.invoke(bean, new Object[] {});

                if (logger.isDebugEnabled()) {
                    //System.out.println(name + ":" + value);
                }

                if (value == null) {
                    // value = "";
                } else if (type.isPrimitive()) {
                    if (type == Integer.TYPE) {
                        value = value.toString();
                    } else if (type == Long.TYPE) {
                        value = value.toString();
                    } else if (type == Float.TYPE) {
                        value = value.toString();
                    } else if (type == Double.TYPE) {
                        value = value.toString();
                    } else if (type == Boolean.TYPE) {
                        value = value.toString();
                    }
                } else if (type == java.util.Date.class || java.util.Date.class.isAssignableFrom(type)) {
                    value = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((java.util.Date) value);
                    if (value.toString().endsWith("00:00:00")) {
                        value = value.toString().substring(0, 10);
                    }
                } else if (type == byte[].class) {
                    value = new String((byte[]) value);
                } else if (isBasicTypeOfClass(type)) {
                    value = value.toString();
                } else {
                    // value = value;
                }

                properties.put(name, value);
            } catch (Exception e) {
                logger.warn(e);
            }
        }
        return properties;
    }
    /**
     * 判断Map里面所有的元素是否都是原生类型的对象。

     * @param map 对象集合
     * @return true或false
     */
    public static boolean isBaseTypeOfMap(Map map) {
        if (map == null || map.isEmpty()) {
            return false;
        }

        Iterator valueIte = map.values().iterator();
        while (valueIte.hasNext()) {
            Object value = valueIte.next();
            if (value != null && !isBaseTypeOfObject(value)) {
                return false;
            }
        }

        return true;
    }
    
    /**
     * 获取类的默认实现类，一般用于返回java.util.Map或java.util.List的默认实现类
     * @param clazz 类的类型
     * @return 默认实现类的类型
     */
    public static Class getDefaultImplementClass(Class clazz) {
        if (!clazz.isInterface()) {
            return clazz;
        }

        if (Map.class.isAssignableFrom(clazz)) {
            return HashMap.class;
        } else if (Collection.class.isAssignableFrom(clazz)) {
            return ArrayList.class;
        }

        return null;
    }
    
    /**
     * 获取类的默认实现类，一般用于返回java.util.Map或java.util.List的默认实现类
     * @param className 类名
     * @return 默认实现类的类型
     */
    public static Class getDefaultImplementClass(String className) {
        Class clazz;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            logger.warn(e);
            return null;
        }

        if (Map.class.isAssignableFrom(clazz)) {
            return HashMap.class;
        } else if (Collection.class.isAssignableFrom(clazz)) {
            return ArrayList.class;
        }

        return clazz;
    }
    /**
     * 根据类的类型创建类的实例
     * @param clazz 类的类型
     * @return 类的实例，如果找不到对应的类或创建失败则返回null
     */
    public static Object createBean(Class clazz) {
        try {
            Object bean = clazz.newInstance();
            return bean;
        } catch (InstantiationException e) {
            logger.warn(e);
        } catch (IllegalAccessException e) {
            logger.warn(e);
        }
        return null;
    }
    /**
     * 获取对象的指定字段的类型
     * @param bean 对象
     * @param name 字段名称
     * @return 字段类型
     */
    public static Class getFieldType(Object bean, String name) {
        return getFieldType(bean.getClass(), name);
    }
    
    /**
     * 根据类名创建类的实例
     * @param className 完整的类型
     * @return 类的实例，如果找不到对应的类或创建失败则返回null
     */
    public static Object createBean(String className) {
        try {
            return Class.forName(className).newInstance();
        } catch (InstantiationException e) {
            logger.warn(e);
        } catch (IllegalAccessException e) {
            logger.warn(e);
        } catch (ClassNotFoundException e) {
            logger.warn(e);
        }
        return null;
    }
}
