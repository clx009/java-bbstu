package com.hnykl.bp.base.tool.bean;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


public class BeanFactory implements ApplicationContextAware {

	private static BeanFactory instance = null;
	
    private static ApplicationContext context = null;

    public static BeanFactory getInstance() {
    	if (instance == null)
    		instance = new BeanFactory();
    	return instance;
    }
    
    public static Object getBean(String name) {
        return context.getBean(name);
    }

    public static Map getBeansOfType(Class beanClass) {
        return context.getBeansOfType(beanClass);
    }

    public void setApplicationContext(ApplicationContext applicationContext)  {
        context = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return context;
    }
    
}
