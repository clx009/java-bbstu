package com.hnykl.bp.base.tool.bean;
import com.hnykl.bp.base.exception.IllegalArgumentExceptionDefine;
import com.hnykl.bp.base.exception.IllegalArgumentException;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 
 * <p>
 *  持久化基类，采用java.lang.ref包的内省机制覆盖父类的toString()、equals(Object o)、hasCode()方法<br/>
 *  hibernate持久层调用的方法
 * </p>
 */
public abstract class BaseObject implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6565656566L;

    /**
     * 覆盖父类的toString()
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * 覆盖父类的equals(Object o)方法
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    /**
     * 覆盖父类的hasCode()方法
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
