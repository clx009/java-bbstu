package com.hnykl.bp.base.tool.bean;

import java.util.List;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;

import com.hnykl.bp.base.exception.IllegalArgumentExceptionDefine;
import com.hnykl.bp.base.exception.IllegalArgumentException;

/**
 * 提供一组静态方法来检查参数是否为无效参数/非法参数/不满足要求的参数，如果参数无效则抛IllegalArgumentException
 */
public class ArgumentCheckingUtil {
    /**
     * 缺省构造函数
     */
    private ArgumentCheckingUtil() {
    }

    /**
     * 检查参数是否非null
     * @param argumentName 参数名
     * @param argument 参数
     * @throws IllegalArgumentException 非法异常（参数argument为null）
     */
    public static void assertNotNull(String argumentName, Object argument) throws IllegalArgumentException {
        if (argument == null) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_NULL, "argument "
                    + argumentName + " is null");
        }
    }

    /**
     * 检查参数是否为空，即null或空串或全空格
     * @param argumentName 参数名
     * @param argument 参数
     * @throws IllegalArgumentException 非法异常（参数argument为null）
     */
    public static void assertNotEmpty(String argumentName, String argument) throws IllegalArgumentException {
        assertNotNull(argumentName, argument);
        if (argument.trim().length() == 0) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_EMPTY, "argument "
                    + argumentName + " is null");
        }
    }

    /**
     * 检查参数是否不为'\0'
     * @param argumentName 参数名
     * @param argument 参数
     * @throws IllegalArgumentException 非法异常（参数argument为'\0'）
     */
    public static void assertNot0(String argumentName, char argument) throws IllegalArgumentException {
        if ('\0' == argument) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.CHAR_TYPE_ARGUMENT_IS_ZERO, "argument "
                    + argumentName + " is \'\\0\'");
        }
    }

    /**
     * 检查参数是否不为0
     * @param argumentName 参数名
     * @param argument 参数
     * @throws IllegalArgumentException 非法异常（参数argument为'\0'）
     */
    public static void assertNot0(String argumentName, long argument) throws IllegalArgumentException {
        if (0 == argument) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.CHAR_TYPE_ARGUMENT_IS_ZERO, "argument "
                    + argumentName + " is \'\\0\'");
        }
    }

    /**
     * 检查参数是否为指定类或其子类的实例
     * @param argumentName 参数名
     * @param argument 参数
     * @param clazz 指定的类
     * @throws IllegalArgumentException 非法异常（参数argument不为指定类或其子类的实例）
     */
    public static void assertInstance(String argumentName, Object argument, Class clazz)
            throws IllegalArgumentException {
        if (argument != null && clazz != null && !clazz.isInstance(argument)) {
            List superclasses = ClassUtils.getAllSuperclasses(clazz);
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_NOT_INSTANCE, "argument "
                    + argumentName + " is not instance of " + clazz.getName() + ", it is instance of "
                    + argument.getClass().getName() + ", and all of its super-classes is/are \""
                    + StringUtils.join(superclasses.toArray(), ",") + "\"");
        }
    }

    /**
     * 检查参数是否为指定类或其子类
     * @param argumentName 参数名
     * @param argument 参数
     * @param clazz 指定的类
     * @throws IllegalArgumentException 非法异常（参数argument不为指定类或其子类
     * 或argument为接口）
     */
    public static void assertSubClass(String argumentName, Class argument, Class clazz) throws IllegalArgumentException {
        if (argument != null && clazz != null && !clazz.getName().equals(argument.getName())) {
            if (argument.isInterface()) {
                throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_NOT_SUB_CLASS,
                        "argument " + argumentName + " is not class/sub-class of " + clazz.getName()
                                + ", it is Interface : " + argument.getName());
            }
            Class superClass = argument.getSuperclass();
            String fullyQualifyNameOfClassOfClazz = clazz.getName();
            StringBuffer superClassNames = new StringBuffer(clazz.getName());
            while (!superClass.getName().equals(Object.class.getName())) {
                if (fullyQualifyNameOfClassOfClazz.equals(superClass.getName())) {
                    return;
                } else {
                    superClassNames.append(",");
                    superClassNames.append(superClass.getName());
                    // 获取超类的直接超类
                    superClass = superClass.getSuperclass();
                }
            }
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_NOT_SUB_CLASS, "argument "
                    + argumentName + " is not class/sub-class of " + clazz.getName() + ", it is class of "
                    + argument.getName() + ", and all of its super-classes is/are \"" + superClassNames.toString()
                    + "\"");
        }
    }

    /**
     * 检查参数是否为指定接口或其子接口
     * @param argumentName 参数名
     * @param argument 参数
     * @param interface0 指定的接口
     * @throws IllegalArgumentException 非法异常（参数argument不为指定接口或其子接口
     * 或argument不为接口）
     */
    public static void assertSubInterface(String argumentName, Class argument, Class interface0)
            throws IllegalArgumentException {
        if (argument != null && interface0 != null && !interface0.getName().equals(argument.getName())) {
            if (!argument.isInterface()) {
                throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_NOT_SUB_INTERFACE,
                        "argument " + argumentName + " is not Interface " + ", it is Class : " + argument.getName());
            }
            List superInterfaces = ClassUtils.getAllInterfaces(argument);
            String fullyQualifyNameOfClassOfInterface0 = interface0.getName();
            StringBuffer superInterfaceNames = new StringBuffer(interface0.getName());
            Class superInterface = null;
            for (int i = 0; i < superInterfaces.size(); i++) {
                superInterface = (Class) superInterfaces.get(i);
                if (fullyQualifyNameOfClassOfInterface0.equals(superInterface.getName())) {
                    return;
                } else {
                    superInterfaceNames.append(",");
                    superInterfaceNames.append(superInterface.getName());
                }
            }
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_NOT_SUB_INTERFACE,
                    "argument " + argumentName + " is not Interface/sub-Interface of "
                            + fullyQualifyNameOfClassOfInterface0 + ", it is Interface: " + argument.getName()
                            + ", and all of its super-Interface is/are \"" + superInterfaceNames.toString() + "\"");
        }
    }

    /**
     * 检查参数是否为指定接口的实现
     * @param argumentName 参数名
     * @param argument 参数
     * @param interface0 指定的接口
     * @throws IllegalArgumentException 非法异常（参数argument不为指定接口的实现）
     */
    public static void assertImplement(String argumentName, Class argument, Class interface0)
            throws IllegalArgumentException {
        if (argument != null && interface0 != null) {
            if (argument.isInterface()) {
                throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_NOT_INSTANCE, "argument "
                        + argumentName + " is an Interface : " + argument.getName());
            }
            List superInterfaces = ClassUtils.getAllInterfaces(argument);
            String fullyQualifyNameOfClassOfInterface0 = interface0.getName();
            StringBuffer superInterfaceNames = new StringBuffer(interface0.getName());
            Class superInterface = null;
            for (int i = 0; i < superInterfaces.size(); i++) {
                superInterface = (Class) superInterfaces.get(i);
                if (fullyQualifyNameOfClassOfInterface0.equals(superInterface.getName())) {
                    return;
                } else {
                    superInterfaceNames.append(",");
                    superInterfaceNames.append(superInterface.getName());
                }
            }
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_NOT_INSTANCE, "argument "
                    + argumentName + " is not implement of " + fullyQualifyNameOfClassOfInterface0 + ", it is : "
                    + argument.getName() + ", and all of its super-Interface is/are \""
                    + superInterfaceNames.toString() + "\"");
        }
    }

    /**
     * 检查参数数组元素个数是否不小于某个值
     * @param argumentName 参数名
     * @param arguments 参数数组
     * @param targetLength 目标长度
     * @throws IllegalArgumentException 非法异常（参数arguments数组元素个数小于目标个数/长度）
     */
    public static void assertNotLengthOfArrayLower(String argumentName, Object[] arguments, int targetLength)
            throws IllegalArgumentException {
        if (arguments.length < targetLength) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARRAY_TYPE_ARGUMENT_LENGTH_IS_LOWER,
                    "the length of array of argument " + argumentName + " is less than " + targetLength);
        }
    }

    /**
     * 检查参数是否相等
     * @param argumentName1 参数1的名称
     * @param argument1 参数1
     * @param argumentName2 参数2的名称
     * @param argument2 参数2
     * @throws IllegalArgumentException 非法异常（参数argument不等于目标值）
     */
    public static void assertEquals(String argumentName1, int argument1, String argumentName2, int argument2)
            throws IllegalArgumentException {
        if (argument1 != argument2) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_DO_NOT_EQUAL, "argument "
                    + argumentName1 + " != " + argumentName2 + ", " + argumentName1 + " = \"" + argument1 + "\" and "
                    + argumentName2 + " = \"" + argument2 + "\"");
        }
    }

    /**
     * 检查参数是否相等
     * @param argumentName1 参数1的名称
     * @param argument1 参数1
     * @param argumentName2 参数2的名称
     * @param argument2 参数2
     * @throws IllegalArgumentException 非法异常（参数argument不等于目标值）
     */
    public static void assertEquals(String argumentName1, long argument1, String argumentName2, long argument2)
            throws IllegalArgumentException {
        if (argument1 != argument2) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_DO_NOT_EQUAL, "argument "
                    + argumentName1 + " != " + argumentName2 + ", " + argumentName1 + " = \"" + argument1 + "\" and "
                    + argumentName2 + " = \"" + argument2 + "\"");
        }
    }

    /**
     * 检查参数是否相等
     * @param argumentName1 参数1的名称
     * @param argument1 参数1
     * @param argumentName2 参数2的名称
     * @param argument2 参数2
     * @throws IllegalArgumentException 非法异常（参数argument不等于目标值）
     */
    public static void assertEquals(String argumentName1, float argument1, String argumentName2, float argument2)
            throws IllegalArgumentException {
        if (argument1 != argument2) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_DO_NOT_EQUAL, "argument "
                    + argumentName1 + " != " + argumentName2 + ", " + argumentName1 + " = \"" + argument1 + "\" and "
                    + argumentName2 + " = \"" + argument2 + "\"");
        }
    }

    /**
     * 检查参数是否不小于某个数
     * @param argumentName 参数名
     * @param argument 参数
     * @param targetValue 目标值
     * @throws IllegalArgumentException 非法异常（参数argument小于目标值）
     */
    public static void assertNotLower(String argumentName, int argument, int targetValue)
            throws IllegalArgumentException {
        if (argument < targetValue) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_LOWER, "argument "
                    + argumentName + " < " + targetValue + ", value of " + argumentName + " is \"" + argument + "\"");
        }
    }

    /**
     * 检查参数是否不小于某个数
     * @param argumentName 参数名
     * @param argument 参数
     * @param targetValue 目标值
     * @throws IllegalArgumentException 非法异常（参数argument小于目标值）
     */
    public static void assertNotLower(String argumentName, long argument, long targetValue)
            throws IllegalArgumentException {
        if (argument < targetValue) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_LOWER, "argument "
                    + argumentName + " < " + targetValue + ", value of " + argumentName + " is \"" + argument + "\"");
        }
    }

    /**
     * 检查参数是否不小于某个数
     * @param argumentName 参数名
     * @param argument 参数
     * @param targetValue 目标值
     * @throws IllegalArgumentException 非法异常（参数argument小于目标值）
     */
    public static void assertNotLower(String argumentName, float argument, float targetValue)
            throws IllegalArgumentException {
        if (argument < targetValue) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_LOWER, "argument "
                    + argumentName + " < " + targetValue + ", value of " + argumentName + " is \"" + argument + "\"");
        }
    }

    /**
     * 检查参数是否不大于某个数
     * @param argumentName 参数名
     * @param argument 参数
     * @param targetValue 目标值
     * @throws IllegalArgumentException 非法异常（参数argument大于目标值）
     */
    public static void assertNotUpper(String argumentName, int argument, int targetValue)
            throws IllegalArgumentException {
        if (argument > targetValue) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_UPPER, "argument "
                    + argumentName + " > " + targetValue + ", value of " + argumentName + " is \"" + argument + "\"");
        }
    }

    /**
     * 检查参数是否不大于某个数
     * @param argumentName 参数名
     * @param argument 参数
     * @param targetValue 目标值
     * @throws IllegalArgumentException 非法异常（参数argument大于目标值）
     */
    public static void assertNotUpper(String argumentName, long argument, long targetValue)
            throws IllegalArgumentException {
        if (argument > targetValue) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_UPPER, "argument "
                    + argumentName + " > " + targetValue + ", value of " + argumentName + " is \"" + argument + "\"");
        }
    }

    /**
     * 检查参数是否不大于某个数
     * @param argumentName 参数名
     * @param argument 参数
     * @param targetValue 目标值
     * @throws IllegalArgumentException 非法异常（参数argument大于目标值）
     */
    public static void assertNotUpper(String argumentName, float argument, float targetValue)
            throws IllegalArgumentException {
        if (argument > targetValue) {
            throw new IllegalArgumentException(IllegalArgumentExceptionDefine.ARGUMENT_IS_UPPER, "argument "
                    + argumentName + " > " + targetValue + ", value of " + argumentName + " is \"" + argument + "\"");
        }
    }
}
