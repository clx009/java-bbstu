package com.hnykl.bp.base.tool.bean;

/**
 * 动态加载类
 * @author foreser
 *
 */
public class ClassUtil {
	  /**
	    * Load a class with a given name.
	    *
	    * It will try to load the class in the following order:
	    * <ul>
	    *  <li>From Thread.currentThread().getContextClassLoader()
	    *  <li>Using the basic Class.forName()
	    *  <li>From ClassLoaderUtil.class.getClassLoader()
	    *  <li>From the callingClass.getClassLoader()
	    * </ul>
	    *
	    * @param className The name of the class to load
	    * @param callingClass The Class object of the calling object
	    * @throws ClassNotFoundException If the class cannot be found anywhere.
	    */
	    public static Class loadClass(String className, Class callingClass) throws ClassNotFoundException {
	        try {
	            return Thread.currentThread().getContextClassLoader().loadClass(className);
	        } catch (ClassNotFoundException e) {
	            try {
	                return Class.forName(className);
	            } catch (ClassNotFoundException ex) {
	                try {
	                    return ClassUtil.class.getClassLoader().loadClass(className);
	                } catch (ClassNotFoundException exc) {
	                    return callingClass.getClassLoader().loadClass(className);
	                }
	            }
	        }
	    }
	    
	    public static Object getObject(String className, Class callingClass) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
	    	Class loadClass = ClassUtil.loadClass(className, callingClass);
			return loadClass.newInstance();
	    }
}