package com.hnykl.bp.base.tool.mail;

import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
/**
 * 发送邮件java类
 * @author powerquanc
 *  2013-7-12
 */
public class SendMail {
	private String host;
	private String user;
	private String pwd;
	private String from;
	private String to;
	
	public SendMail(String host,String user,String pwd){
		this.host = host;
		this.user = user;
		this.pwd = pwd;
	}
	
	public void setAddress(String from, String to) {
		this.from = from;
		this.to = to;
	}

	public void send(String subject,String txt) {
		Properties props = new Properties(); // 设置发送邮件的邮件服务器的属性（这里使用网易的smtp服务器）
		props.put("mail.smtp.host", host); // 需要经过授权，也就是有户名和密码的校验，这样才能通过验证（一定要有这一条）
		props.put("mail.smtp.auth", "true"); // 用刚刚设置好的props对象构建一个session
		Session session = Session.getDefaultInstance(props); // 有了这句便可以在发送邮件的过程中在console处显示过程信息，供调试使
																// //
																// 用（你可以在控制台（console)上看到发送邮件的过程）
		session.setDebug(true); // 用session为参数定义消息对象
		MimeMessage message = new MimeMessage(session);
		try { // 加载发件人地址
			message.setFrom(new InternetAddress(from)); // 加载收件人地址
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); // 加载标题
			message.setSubject(subject); // 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
			Multipart multipart = new MimeMultipart(); // 设置邮件的文本内容
			BodyPart contentPart = new MimeBodyPart();
			contentPart.setText(txt);
			multipart.addBodyPart(contentPart);
			message.setContent(multipart); // 保存邮件
			message.saveChanges(); // 发送邮件
			Transport transport = session.getTransport("smtp"); // 连接服务器的邮箱
			transport.connect(host, user, pwd); // 把邮件发送出去
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String host = "smtp.163.com"; // smtp服务器
		String user = "powerquanc"; // 填写个人用户名
		String pwd = "myoqw4a1fieldwy"; // 填写个人密码
		SendMail cn = new SendMail(host,user,pwd); // 设置发件人地址、收件人地址和邮件标题
		cn.setAddress("powerquanc@163.com", "powerquanc@gmail.com");
		cn.send("邮件标题333","邮件内容");
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}


}