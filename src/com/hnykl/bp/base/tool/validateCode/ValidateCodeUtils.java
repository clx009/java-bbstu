package com.hnykl.bp.base.tool.validateCode;

import org.apache.commons.lang.math.RandomUtils;

/**
 * 验证码生成工具类
 */
public class ValidateCodeUtils {
	/**
	 * 随机得到4位的验证码
	 * @return
	 */
	public static String get4ValidateCode(){
		String code2 = null;
		code2 = String.valueOf(RandomUtils.nextInt(10000));
		code2 = org.apache.commons.lang.StringUtils.leftPad(code2, 4, '0');
		String codenum2 = "";
		codenum2 += org.apache.commons.lang.StringUtils.right(String.valueOf(Integer.parseInt(String.valueOf(code2.charAt(0))) + 1), 1);
		codenum2 += org.apache.commons.lang.StringUtils.right(String.valueOf(Integer.parseInt(String.valueOf(code2.charAt(1))) + 2), 1);
		codenum2 += org.apache.commons.lang.StringUtils.right(String.valueOf(Integer.parseInt(String.valueOf(code2.charAt(2))) + 3), 1);
		codenum2 += org.apache.commons.lang.StringUtils.right(String.valueOf(Integer.parseInt(String.valueOf(code2.charAt(3))) + 4), 1);
		return codenum2;
	}
}
