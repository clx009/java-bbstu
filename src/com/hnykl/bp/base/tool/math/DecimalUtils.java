package com.hnykl.bp.base.tool.math;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * <p>
 * ��ֵ������
 * </p>
 *
 */
public class DecimalUtils {

    /**
     * ��ʽ����ֵ
     * @param number ��ֵ����
     * @param format ��ʽ���ַ�
     * @return ��ʽ������ַ�
     */
    public static String format(Number number, String format) {
        DecimalFormat formater = new DecimalFormat(format);
        return formater.format(number);
    }

    /**
     * ���ַ����Ϊ��ֵ����
     * @param text ��ֵ�ַ�
     * @param format ��ʽ���ַ�
     * @return ��ֵ����
     */
    public static Number parse(String text, String format) {
        DecimalFormat formater = new DecimalFormat(format);
        try {
            return formater.parse(text);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * ���ַ�תΪ��ֵ����֮����и�ʽ��
     * @param number ��ֵ�ַ�
     * @param format ��ʽ���ַ�
     * @return ��ʽ������ַ�
     */
    public static String format(String number, String format) {
        if (number == null) {
            return null;
        }

        number = number.replaceAll(",", "");
        BigDecimal decimal = new BigDecimal(number);
        DecimalFormat formater = new DecimalFormat(format);
        return formater.format(decimal);
    }

}
