package com.hnykl.bp.base.tool.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.tool.validateCode.ValidateCodeUtils;

public class SmsTemplate {
	private static Logger logger = Logger.getLogger(SmsTemplate.class);
	
	public static String version = "";
	public static String serverUrl="";
	public static String accountSid="";
	public static String authToken="";
	public static String appId="";
	static{
		version = BaseConfigMgr.getUserSmsSendVersion();
		serverUrl = BaseConfigMgr.getUserSmsSendServerurl();
		accountSid = BaseConfigMgr.getUserSmsSendAccountSid();
		authToken = BaseConfigMgr.getUserSmsSendAuthToken();
		appId = BaseConfigMgr.getUserSmsSendAppId();
	}
	
	
	public static String to="";
	public static String para="";

	public static String getSignature(String accountSid, String authToken,String timestamp,EncryptUtil encryptUtil) throws Exception{
		String sig = accountSid + authToken + timestamp;
		String signature = encryptUtil.md5Digest(sig);
		return signature;
	}
	
	/*
	public static HttpResponse get(String cType,String accountSid,String authToken,String timestamp,String url,DefaultHttpClient httpclient,EncryptUtil encryptUtil) throws Exception{
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader("Accept", cType);//
		httpget.setHeader("Content-Type", cType+";charset=utf-8");
		String src = accountSid + ":" + timestamp;
		String auth = encryptUtil.base64Encoder(src);
		httpget.setHeader("Authorization",auth);
		HttpResponse response = httpclient.execute(httpget);
		return response;
	}
	*/
	
	public static String post(String cType,String accountSid,String authToken,String timestamp,String url,EncryptUtil encryptUtil,String body) throws Exception{
		InputStream in = null;
		BufferedReader buffer = null;
		PostMethod postMethod = null;
		try{
			HttpClient client = new HttpClient();
			postMethod = new PostMethod(url);
			postMethod.addRequestHeader("Content-type", "application/json;charset=utf-8");
			postMethod.addRequestHeader("Accept", cType);
			String src = accountSid + ":" + timestamp;
			String auth = encryptUtil.base64Encoder(src);
			postMethod.addRequestHeader("Authorization", auth);
//			NameValuePair content = new NameValuePair("content", body);
//		    NameValuePair[] params = {content};
		    postMethod.setRequestBody(body);
		    int statusCode = client.executeMethod(postMethod);
	        if (statusCode != 200) {
		        throw new RuntimeException("Bad response status, " + 
		          postMethod.getStatusLine());
	        }
	        StringBuffer temp = new StringBuffer();
	        in = postMethod.getResponseBodyAsStream();
	        buffer = new BufferedReader(new InputStreamReader(in, "UTF-8"));
	        for (String tempstr = ""; (tempstr = buffer.readLine()) != null; ) {
	           temp = temp.append(tempstr);
	        }
	        return temp.toString().trim();
	     }catch (HttpException e) {
	      e.printStackTrace();
	      throw new RuntimeException(e.getMessage());
	    } catch (IOException e) {
	      e.printStackTrace();
	      throw new RuntimeException(e.getMessage());
	    }catch(Exception ex){
	      ex.printStackTrace();
	      throw new RuntimeException(ex.getMessage());
	    }finally {
	      try {
	        if (buffer != null)
	          buffer.close();
	        if (in != null)
	          in.close();
	        if (postMethod != null)
	          postMethod.releaseConnection();
	      } catch (IOException e) {
	        e.printStackTrace();
	      }
	    }
	}
	
	//转换时间格式
	public static String dateToStr(Date date,String pattern) {
	       if (date == null || date.equals(""))
	    	 return null;
	       SimpleDateFormat formatter = new SimpleDateFormat(pattern);
	       return formatter.format(date);
 } 
	
	public static String sendSMS(String to, String templateId,String param) {
		String result = "";
		try {
			//MD5加密
			EncryptUtil encryptUtil = new EncryptUtil();
			//获取时间戳
			String timestamp = dateToStr(new Date(), "yyyyMMddHHmmss");//获取时间戳
			String signature =getSignature(accountSid,authToken,timestamp,encryptUtil);
			StringBuffer sb = new StringBuffer("https://");
			sb.append(serverUrl);
			String url = sb.append("/").append(version)
					.append("/Accounts/").append(accountSid)
					.append("/Messages/templateSMS")
					.append("?sig=").append(signature).toString();
			TemplateSMS templateSMS=new TemplateSMS();
			templateSMS.setAppId(appId);
			templateSMS.setTemplateId(templateId);
			templateSMS.setTo(to);
			templateSMS.setParam(param);
			Gson gson = new Gson();
			String body = gson.toJson(templateSMS);
			body="{\"templateSMS\":"+body+"}";
			System.out.println("post bpdy is: " + body);
			String responseStr = post("application/json",accountSid, authToken, timestamp, url, encryptUtil, body);
			logger.info("======>send msg return info:"+responseStr);
			result = responseStr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 短信验证码简易接口*/
//	public static String templateSimpleSMS(String accountSid, String authToken,
//			String appId, String templateId, String to, String param) {
//		// TODO Auto-generated method stub
//		String result = "";
//		String url = "http://www.ucpaas.com/maap/sms/code";
//		String timestamp = dateToStr(new Date(), "yyyyMMddHHmmssSSS");//获取时间戳
//		DefaultHttpClient httpclient=new DefaultHttpClient();
//		try {
//			//MD5加密
//			EncryptUtil encryptUtil = new EncryptUtil();
//			String signature =getSignature(accountSid,timestamp,authToken,encryptUtil);
//			StringBuffer sb = new StringBuffer(url);
//			url = sb.append("?")
//					.append("&sid=").append(accountSid)
//					.append("&appId=").append(appId)
//					.append("&time=").append(timestamp)
//					.append("&sign=").append(signature.toLowerCase())
//					.append("&to=").append(to)
//					.append("&templateId=").append(templateId)
//					.append("&param=").append(param).toString();
//			System.out.println("templateSimpleSMS url = "+url);
//			HttpGet httpget = new HttpGet(url);
//
//			HttpResponse response = httpclient.execute(httpget);
//			HttpEntity entity = response.getEntity();
//			if (entity != null) {
//				result = EntityUtils.toString(entity, "UTF-8");
//				System.out.println("templateSimpleSMS Response content is: " + result);
//			}
//			EntityUtils.consume(entity);
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally{
//			// 关闭连接
//		    httpclient.getConnectionManager().shutdown();
//		}
//		return result;
//	}
	
	/**
	 * main
	 */
	public static void main(String[] args) throws IOException {
		
		version = "2014-06-30";
		serverUrl = "api.ucpaas.com";
		accountSid = "14654054157aa86cede284d3aa6822d6";
		authToken = "8dafaf890c936ef49453383298ceab1e";
		appId = "be55c02622c14596bf5decbf4880f820";
		String validateCode =  ValidateCodeUtils.get4ValidateCode();
		String userSmsSendRegTemplateId =  BaseConfigMgr.getUserSmsSendRegTemplateId();
		userSmsSendRegTemplateId="24260";
		String retStr =  sendSMS("15700758231",userSmsSendRegTemplateId ,validateCode+",2");
		System.out.println(retStr);
	}
}
