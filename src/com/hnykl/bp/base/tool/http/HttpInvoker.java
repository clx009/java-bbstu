package com.hnykl.bp.base.tool.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import com.hnykl.bp.base.tool.character.StringUtils;
/**
 * http请求数据
 * @author powerquanc
 *  2013-6-27
 */
public class HttpInvoker {
	/**
	 * http的post方法
	 * @param pUrl
	 * @param paraStr
	 * @throws IOException
	 */
	public static String postData(String pUrl,String paraStr) throws IOException {
		BufferedReader br = null;
		HttpURLConnection connection = null;
		DataOutputStream out = null;
		String content = null;
		try{
			URL postUrl = new URL(pUrl);
			// 打开连接
			connection = (HttpURLConnection) postUrl.openConnection();
			//设置连接超时间时间为3s
			connection.setConnectTimeout(60000);
			//设置读取超时时间为3s
			connection.setReadTimeout(60000);
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			
			connection.setRequestProperty("Content-type", "text/html");
			connection.setRequestProperty("Accept-Charset", "UTF-8");
			connection.setRequestProperty("contentType", "UTF-8");
			
			connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			connection.connect();
			out = new DataOutputStream(connection.getOutputStream());
			out.writeBytes(paraStr);
			out.flush();
			out.close(); 
			//out = null;
			br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			while(br.read() != -1){
				sb.append(br.readLine());
			}
			content = new String(sb);
			//content = (StringUtils.isNotEmpty(content) && content.indexOf("http://tempuri.org/") > -1)?content.substring(content.indexOf("http://tempuri.org/")+21,content.indexOf("</")):null;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(out != null)out.close();
			if(br != null)br.close();
			if(connection != null)connection.disconnect();
		}
		return content;
	}
	/**
	 * http的post方法
	 * @param pUrl
	 * @param paraStr
	 * @throws IOException
	 */
	public static String getHttpData(String pUrl,String paraStr,String method) throws IOException {
		BufferedReader br = null;
		HttpURLConnection connection = null;
		DataOutputStream out = null;
		String content = null;
		try{
			URL postUrl = new URL(pUrl);
			// 打开连接
			connection = (HttpURLConnection) postUrl.openConnection();
			//设置连接超时间时间为3s
			connection.setConnectTimeout(60000);
			//设置读取超时时间为3s
			connection.setReadTimeout(60000);
			connection.setDoOutput(true);
			connection.setDoInput(true);
			if(StringUtils.isNotEmpty(method)){
				connection.setRequestMethod(method);
			}else{
				connection.setRequestMethod("POST");
			}
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			
			connection.setRequestProperty("Content-type", "text/html");
			connection.setRequestProperty("Accept-Charset", "UTF-8");
			connection.setRequestProperty("contentType", "UTF-8");
			
			connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			//connection.setRequestProperty("Content-Type","application/json");
			connection.connect();
			out = new DataOutputStream(connection.getOutputStream());
			if(StringUtils.isNotEmpty(paraStr) ){
				out.writeBytes(paraStr);
			}
			out.flush();
			out.close(); 
//			out = null;
			br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			while(br.read() != -1){
				sb.append(br.readLine());
			}
			content = new String(sb);
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(out != null)out.close();
			if(br != null)br.close();
			if(connection != null)connection.disconnect();
		}
		return content;
	}
	 /**
	  * get方法处理数据
	  * 
	  * */
	 public static String sendGet(String url, String param) {
	        String result = "";
	        BufferedReader in = null;
	        try {
	            String urlNameString = url + "?" + param;
	            URL realUrl = new URL(urlNameString);
	            // 打开和URL之间的连接
	            URLConnection connection = realUrl.openConnection();
	            // 设置通用的请求属性
	            connection.setRequestProperty("accept", "*/*");
	            connection.setRequestProperty("connection", "Keep-Alive");
	            connection.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	            // 建立实际的连接
	            connection.connect();
	            // 获取所有响应头字段
	            Map<String, List<String>> map = connection.getHeaderFields();
	            // 遍历所有的响应头字段
	            for (String key : map.keySet()) {
	                System.out.println(key + "--->" + map.get(key));
	            }
	            // 定义 BufferedReader输入流来读取URL的响应
	            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	            String line;
	            while ((line = in.readLine()) != null) {
	                result += line;
	            }
	        } catch (Exception e) {
	            System.out.println("发送GET请求出现异常！" + e);
	            e.printStackTrace();
	        } finally {
	            try {
	                if (in != null) {
	                    in.close();
	                }
	            } catch (Exception e2) {
	                e2.printStackTrace();
	            }
	        }
	        return result;
	    }

	public static void main(String []arg){
		try{
//			String url = "https://graph.qq.com/user/get_user_info";
//			String paramStr = "oauth_consumer_key=100330589&access_token=FDF484C8077D2C13B73AD950A1036D70&openid=A5EF28F5A7CC763AC8E8B0D4EA1FD51D&format=json";
//			String retStr = HttpInvoker.postData(url,paramStr);
			
			String url = "https://graph.qq.com/oauth2.0/me";
			String retStr = HttpInvoker.getHttpData("https://graph.qq.com/oauth2.0/me", "access_token=DCF66B2AF0830C6A213E0AEA59773923", "GET");
			System.out.println(retStr);
		}catch(Exception ex){
			 ex.printStackTrace();
		 }
	}

}