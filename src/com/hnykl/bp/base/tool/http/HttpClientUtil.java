package com.hnykl.bp.base.tool.http;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClientUtil
{
  private static int TIME_OUT = 60000;
  private static String CHARSET = "UTF-8";

  

  public static String get(String url)
  {
    HttpURLConnection conn = null;
    DataOutputStream output = null;
    OutputStream os = null;
    InputStream is = null;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try
    {
      URL siteUrl = new URL(url);
      conn = (HttpURLConnection)siteUrl.openConnection();
      conn.setConnectTimeout(120000);
      conn.setDoInput(true);
      conn.setDoOutput(true);
      conn.setUseCaches(false);
      conn.setRequestMethod("GET");
      conn.setRequestProperty("Connection", "keep-alive");

      conn.connect();
      os = conn.getOutputStream();
      os.flush();
      os.close();
      int code = conn.getResponseCode();
      System.out.println(conn.getResponseMessage());
      if (code != 200) {
        throw new RuntimeException("请求‘" + url + "’失败！");
      }

      is = conn.getInputStream();
      int len = 0;
      byte[] buffer = new byte[''];
      while (len != -1) {
        len = is.read(buffer);
        if (len == -1) {
          break;
        }
        baos.write(buffer, 0, len);
      }

      byte[] responData = baos.toByteArray();
      String strRes = new String(responData, "UTF-8");

      String str1 = strRes;
      return str1;
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    finally {
      try {
        if (output != null) {
          output.close();
        }
        if (baos != null) {
          baos.close();
        }
        if (is != null)
          is.close();
      }
      catch (IOException e) {
        throw new RuntimeException(e);
      }

      if (conn != null)
        conn.disconnect();
    }
  }
}