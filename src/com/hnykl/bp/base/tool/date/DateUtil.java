package com.hnykl.bp.base.tool.date;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * <p>
 * 日期处理工具类
 * </p>
 * 
 */
public class DateUtil {

	/**
	 * 取得系统日期
	 * 
	 * @return 系统时间
	 */
	public static Date getSysDate() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		return new Date(cal.getTime().getTime());
	}

	public static Timestamp getStartTimeStamp(int year, int month, int day) {
		Calendar cal = getStartOfDay(year, month, day);

		return new Timestamp(cal.getTimeInMillis());
	}

	public static Calendar getStartOfDay(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DATE, day);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		return cal;
	}

	public static Timestamp getEndTimeStamp(int year, int month, int day) {
		Calendar cal = getEndOfDay(year, month, day);

		return new Timestamp(cal.getTime().getTime());
	}

	public static Calendar getEndOfDay(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DATE, day);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal;
	}

	/**
	 * 取得系统时间
	 * 
	 * @return 系统时间
	 */
	public static Timestamp getShortSysTimeStamp() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);

		return new Timestamp(cal.getTime().getTime());
	}

	// /**
	// * 取得系统时间(YYYYMMDDHHMMSSS)格式的系统时间，主要用于查询
	// * @return 系统时间
	// */
	// public static Timestamp getYYYYMMDDHHMMSSSysTimeStamp() {
	// Calendar cal = Calendar.getInstance();
	// cal.set(Calendar.MILLISECOND, 0);
	// return new Timestamp(cal.getTime().getTime());
	// }

	/**
	 * get current calendar
	 * 
	 * @return
	 */
	public static Calendar getCurrentCalendar() {
		Calendar cal = Calendar.getInstance();
		return cal;
	}

	/**
	 * 取得系统时间
	 * 
	 * @return 系统时间
	 */
	public static Timestamp getSysTimeStamp() {
		long time = Calendar.getInstance().getTime().getTime();

		return new Timestamp(time);
	}

	public static String formatDate(String format, Date date) {
		DateFormat df = new SimpleDateFormat(format);

		return df.format(date);
	}

	public static String formatDate(String format, java.util.Date date) {
		DateFormat df = new SimpleDateFormat(format);

		return df.format(date);
	}

	private static final SimpleDateFormat fmtYYYYMMDD = new SimpleDateFormat(
			"yyyy-MM-dd");

	private static final SimpleDateFormat fmtYYYYMMDDHHMMSS = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	private static final SimpleDateFormat fmtYYYYMMDDHH24MMSS = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	// private static final SimpleDateFormat fmtYYYYMMDDHHMMSSMS = new
	// SimpleDateFormat("yyyy-MM-dd HH:mm:ss:MS");

	public static SimpleDateFormat getFmtYYYYMMDD() {
		// DateFormat fmto = DateFormat.getDateTimeInstance();
		return (SimpleDateFormat) fmtYYYYMMDD.clone();
	}

	public static SimpleDateFormat getFmtYYYYMMDDHHMMSS() {
		return (SimpleDateFormat) fmtYYYYMMDDHHMMSS.clone();
	}

	public static SimpleDateFormat getFmtYYYYMMDDHH24MMSS() {
		return (SimpleDateFormat) fmtYYYYMMDDHH24MMSS.clone();
	}
	
	
	/**
	 * 取得startOf Day
	 * 
	 * @return
	 */
	public static Timestamp getStartOfDay() {
		return getShortSysTimeStamp();
	}

	/**
	 * 格式化为YYYYMMDDHHMMSS的日期
	 * 
	 * @param time
	 * @return
	 */
	public static String formatYYYYMMDDHHMMSS(Timestamp time) {
		return fmtYYYYMMDDHHMMSS.format(time);
	}

	/**
	 * 格式化为YYYYMMDDHH24MMSS的日期
	 * 
	 * @param time
	 * @return
	 */
	public static String formatYYYYMMDDHH24MMSS(Timestamp time) {
		return fmtYYYYMMDDHH24MMSS.format(time);
	}

	/**
	 * 格式化为YYYYMMDDHHMMSS的日期
	 * 
	 * @param time
	 * @return
	 */
	public static String formatYYYYMMDD(Timestamp time) {
		return fmtYYYYMMDD.format(time);
	}

	/**
	 * oracle time to_char function
	 * 
	 * @param time
	 * @return
	 */
	public static String getOracleTodateStr(Timestamp time) {
		StringBuffer sb = new StringBuffer(100);
		sb.append("to_date('");
		sb.append(formatYYYYMMDDHHMMSS(time));
		sb.append("','yyyy-MM-dd HH24:mi:ss')");

		return sb.toString();
	}

	/**
	 * 获取当前时间处理
	 * 
	 * @return
	 */
	public static String getOracleTodateStr() {
		Timestamp time = getSysTimeStamp();
		return getOracleTodateStr(time);
	}

	/**
	 * 根据时间获取字符串.
	 * 
	 * @param date
	 *            日期
	 * @return 日期字符串
	 */
	public static String getOracleTodateStr(java.util.Date date) {
		Timestamp time = new Timestamp(date.getTime());
		return getOracleTodateStr(time);
	}

	/**
	 * oracle time to_char function
	 * 
	 * @param time
	 * @return
	 */
	public static String getOracleToShortdateStr(Timestamp time) {
		StringBuffer sb = new StringBuffer(100);
		sb.append("to_date('");
		sb.append(formatYYYYMMDD(time));
		sb.append("','yyyy-MM-dd')");

		return sb.toString();
	}

	/**
	 * get between and date string
	 * 
	 * @param start
	 *            开始时间
	 * @param end
	 *            结束时间
	 * @return 时间段
	 */
	public static String getBetweenAndDateStr(Timestamp start, Timestamp end) {
		Timestamp startTime = start;
		if (null == startTime) {
			// 默认当天
			startTime = getStartOfDay();
		}

		Timestamp endTime = end;
		if (null == endTime) {
			// 默认当天
			endTime = getEndOfDay();
		}
		StringBuffer sb = new StringBuffer(100);
		sb.append(" between ");
		sb.append(getOracleTodateStr(startTime));
		sb.append(" and ");
		sb.append(getOracleTodateStr(endTime));

		return sb.toString();
	}
	
	/**
	 * get end of day
	 * 
	 * @return
	 */
	public static Timestamp getEndOfDay() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.MILLISECOND, 999);
		cal.set(Calendar.SECOND, 59);
		return new Timestamp(cal.getTime().getTime());
	}

	/**
	 * 加小时
	 * 
	 * @param date
	 * @param hour
	 * @return
	 */
	public static Calendar addHourForDate(java.util.Date date, int hour) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, hour);
		return cal;
	}
	
	/**
	 * 加天
	 * 
	 * @param date
	 * @param hour
	 * @return
	 */
	public static Calendar addDayForDate(java.util.Date date, int day) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, day);
		return cal;
	}

	/**
	 * 加月
	 * 
	 * @param date
	 * @param hour
	 * @return
	 */
	public static Calendar addMonthForDate(java.util.Date date, int month) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, month);
		return cal;
	}
	/**
	 * 加年
	 * 
	 * @param date
	 * @param hour
	 * @return
	 */
	public static Calendar addYearForDate(java.util.Date date, int year) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, year);
		return cal;
	}
	
	/**
	 * 得到相差的时间
	 * @param time
	 * @return
	 */
	public static String getBetweenTimeStr(long time) {
		int theDay, theHour, theMinutes;
		theDay = 24 * 60 * 60;// 一天
		theHour = 60 * 60; // 一小时
		theMinutes = 60; // 一分钟
		int nowTime = 0; // 当前的总秒数
		// 现在的天数，小时，分，秒
		int nowDay = 0, nowHour = 0, nowMinutes = 0, nowSenconds = 0;
		StringBuffer sb = new StringBuffer();
		if (time - theDay >= 0)// 显示：天，小时
		{
			nowDay = (int)time / theDay;
			nowTime = (int)time % theDay;
			if (nowTime - theHour >= 0){
				nowHour = nowTime / theHour;
			}else{
				theHour = 0;
			}
			sb.append(nowDay + "天");
			sb.append(nowHour + "小时");
		}else if (time - theHour >= 0) {// 显示：小时，分钟
			nowHour = (int)time / theHour;
			nowTime = (int)time % theHour;
			if (nowTime - theMinutes >= 0)
				nowMinutes = nowTime / theMinutes;
			else
				nowMinutes = 0;
			sb.append(nowHour + "小时");
			sb.append(nowMinutes + "分钟");

		} else if (time - theHour < 0 && time - theMinutes >= 0) {// 显示：分钟
			nowMinutes = (int)time / theMinutes;
			sb.append("0小时" + nowMinutes + "分");
		} else {
			nowSenconds = (int)time;
			sb.append("0分" + nowSenconds + "秒");
		}
		return sb.toString();
	}
	/**
	 * 查询相差分钟
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static long getDistanceMinutes(java.util.Date bigDate,java.util.Date smallDate) {
		if(bigDate != null && smallDate != null && bigDate.before(smallDate))return -1;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long t = 0;
		try {
			t = sdf.parse(sdf.format(bigDate)).getTime() - sdf.parse(sdf.format(smallDate)).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return t/(60*1000);
	}

	public static void main(String[] arg) {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			java.util.Date productPassTime = sdf.parse("2013-9-23 16:03:13");
			long l = DateUtil.addHourForDate(productPassTime,240).getTimeInMillis(); 
			long nowl = System.currentTimeMillis();
			String timeStr = DateUtil.getBetweenTimeStr((l - nowl)/1000);
			System.out.println(timeStr);
		}catch(Exception ex){
			
		}
	}
}
