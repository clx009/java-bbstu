package com.hnykl.bp.base.tool.date;

import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

/**
 * <p>
 * ����ʱ�乤����
 * </p>

 */
public class DateTimeUtils {

    /**
     * ��־��¼��
     */
    private static final Logger LOGGER = Logger.getLogger(DateTimeUtils.class);

    /**
     * ���ڸ�ʽ��
     */
    private static SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("yyyy-MM-dd");
    
    /**
     * ����ʱ���ʽ��
     */
    private static SimpleDateFormat FORMAT_DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //private static SimpleDateFormat FORMAT_DATETIMEMS = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss.fffffffff");
    
    /**
     * ʱ���ʽ��
     */
    private static SimpleDateFormat FORMAT_TIME = new SimpleDateFormat("HH:mm:ss");
    
    /**
     * ���ڸ�ʽ�����޷ָ��
     */
    private static SimpleDateFormat FORMAT_SHORTDATE = new SimpleDateFormat("yyyyMMdd");
    
    /**
     * ���ڸ�ʽ�����޷ָ���죩
     */
    private static SimpleDateFormat FORMAT_YYYYMM = new SimpleDateFormat("yyyyMM");
    
    /**
     * Ĭ�����ڸ�ʽ��
     */
    private static SimpleDateFormat FORMAT_DEFAULT = new SimpleDateFormat();

    /**
     * ˽�й��캯��
     */
    private DateTimeUtils() {
    }

    /**
     * �������ַ�ת��Ϊ��Ӧ�����ڶ���
     * @param date �����ַ�
     * @param format ��ʽ�ַ�
     * @return ת��������ڶ���
     */
    public static java.util.Date parseDate(String date, String format) {

        if (date == null || date.trim().equals("") == true) {
            return null;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            return simpleDateFormat.parse(date);
        } catch (Exception e) {
            LOGGER.error("Parse Date Error.", e);
            return null;
        }
    }

    /**
     * �������ַ�ת��Ϊ��Ӧ�����ڶ���
     * @param date �����ַ�
     * @return ת��������ڶ���
     */
    public static java.util.Date parseDate(String date) {

        if (date == null || date.trim().equals("") == true) {
            return null;
        }

        date = date.trim();
        int dateLen = date.length();
        date = date.replaceAll("/", "-");
        if (dateLen < 20) {
            date = date.replace('.', '-');
        }

        try {
            if (dateLen == 6) {
                return FORMAT_YYYYMM.parse(date);
            } else if (dateLen == 8) {
                return FORMAT_SHORTDATE.parse(date);
            } else if (dateLen == 10) {
                return FORMAT_DATE.parse(date);
            } else if (dateLen == 19) {
                return FORMAT_DATETIME.parse(date);
            } else {
                return java.sql.Timestamp.valueOf(date);
            }
        } catch (Exception e) {
            LOGGER.error("Parse Date Error.", e);
        }

        try {
            return FORMAT_DEFAULT.parse(date);
        } catch (Exception e) {
            try {
                return new java.util.Date(date);
            } catch (Exception e2) {
                LOGGER.error("Parse Date Error.", e2);
            }
        }

        return null;
    }

    /**
     * �����ڶ���ת��ΪĬ�ϵĸ�ʽ
     * @param date ���ڶ���
     * @return ��ʽ������ַ�
     */
    public static String formatDate(java.util.Date date) {
        if (date == null) {
            return "";
        }

        if (date instanceof java.sql.Timestamp) {
            return FORMAT_DATETIME.format(date);
        } else if (date instanceof java.sql.Date) {
            return FORMAT_DATE.format(date);
        } else if (date instanceof java.sql.Time) {
            return FORMAT_TIME.format(date);
        } else {
            return FORMAT_DATETIME.format(date);
        }
    }

    /**
     * �����ڶ����ʽ��Ϊָ���ĸ�ʽ�ַ�
     * @param date ���ڶ���
     * @param format ��ʽ�ַ�
     * @return ��ʽ������ַ�
     */
    public static String formatDate(java.util.Date date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            return date.toString();
        }

    }

}
