package com.hnykl.bp.base.tool.encrypt;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;


public class EncryptURL {
    /**
     * 跳转的目的页面
     */
    private String objectivePagePath = "";

    /**
     * 组装后的URL地址
     */
    private String uniteURL = "";

    /**
     * DES加密对象
     */
    private DES des = null;

    /**
     * @throws Exception 异常
     */
    public EncryptURL() throws Exception {
        des = new DES();
    }

    /**
     * @param desKey 密钥
     * @throws Exception 异常
     */
    public EncryptURL(String desKey) throws Exception {
        des = new DES(desKey);
    }

    /**
     * 设置跳转目的的页面路径
     *
     * @param objectivePagePath 参数
     */
    public void setObjectivePagePath(String objectivePagePath) {
        this.objectivePagePath = objectivePagePath;
    }


    /**
     * 获取跳转目的的页面路径
     *
     * @return 返回
     */
    public String getObjectivePagePath() {
        return this.objectivePagePath;
    }


    /**
     * 初始uniteURL值
     *
     * @param uniteURL uniteURL
     */
    public void setUniteURL(String uniteURL) {
        this.uniteURL = uniteURL;
    }


    /**
     * 获取组装的URL地址
     *
     * @return String
     * @throws Exception Exception
     */
    public String getUniteURL() throws Exception {
        return this.uniteURL;
    }


    /**
     * 获取加密后的URL地址
     *
     * @return String
     * @throws Exception Exception
     */
    public String getEncryptURL() throws Exception {
        return (des.encrypt(uniteURL));
    }


    /**
     * 根据请求的页面表单来获取参数列表
     *
     * @param request request
     * @return List
     * @throws Exception Exception
     */
    public List getEntityList(HttpServletRequest request) throws Exception {
        List list = new ArrayList();
        String url = "";
        String isEncrypt = request.getParameter("isEncrypt");
        String queryString = request.getParameter("queryString");
        if (isEncrypt != null && isEncrypt.equals("false")) { //URL为不加密的
            url = queryString;
            /* 取出所有request中的参数
             * 不加密的是用表单的形式提交,在此要获取表单的参数
             */
            Enumeration tagList = request.getParameterNames();
            while (tagList.hasMoreElements()) {
                String tagName = (String) tagList.nextElement();
                if (!tagName.equalsIgnoreCase("queryString")) {
                    String[] values = request.getParameterValues(tagName); //获取参数列表值
                    for (int i = 0; i < values.length; i++) {
                        EntityParameter entityParameter = new EntityParameter();
                        entityParameter.setParameterName(tagName);
                        entityParameter.setParameterValue(values[i]);
                        list.add(entityParameter);
                    }
                }
            }
        } else {
            url = des.decrypt(queryString); //解密URL地址
        }
        if (url.indexOf("?") <= 0) {
            objectivePagePath = url; //直接输入页面不带参数的提交页面路径
        } else {
            objectivePagePath = url.substring(0, url.indexOf("?")); //带参数的页面提交
        }
        objectivePagePath = url.substring(0, url.indexOf("?"));
        String parameterString = url.substring(url.indexOf("?") + 1);
        //分解参数列表
        String[] allParameters = parameterString.split("&");
        if (allParameters != null && allParameters.length > 0) {
            for (int i = 0; i < allParameters.length; i++) {
                String parameterNameValue = allParameters[i];
                //分解每个参数的值
                String[] temp = parameterNameValue.split("=");
                //给参数实体类付值
                if (temp.length == 2) { //并判断参数是否有值
                    EntityParameter entityParameter = new EntityParameter();
                    entityParameter.setParameterName(temp[0]);
                    entityParameter.setParameterValue(temp[1]);
                    list.add(entityParameter);
                }
            }
        }
        return list;
    }


    /**
     * 根据加密后的URL地址,来分解参数
     *
     * @param encryptURL encryptURL
     * @param isEncrypt  isEncrypt
     * @return List
     * @throws Exception Exception
     */
    public List getEntityList(String encryptURL, String isEncrypt) throws Exception {
        List list = new ArrayList();
        String url = "";
        if (isEncrypt != null && isEncrypt.equals("false")) { //URL为不加密的
            url = encryptURL;
        } else {
            url = des.decrypt(encryptURL); //解密URL地址
        }
        objectivePagePath = url.substring(0, url.indexOf("?"));
        String parameterString = url.substring(url.indexOf("?") + 1);
        //分解参数列表
        String[] allParameters = parameterString.split("&");
        if (allParameters != null && allParameters.length > 0) {
            for (int i = 0; i < allParameters.length; i++) {
                String parameterNameValue = allParameters[i];
                //分解每个参数的值
                String[] temp = parameterNameValue.split("=");
                //给参数实体类付值
                if (temp.length == 2) { //并判断参数是否有值
                    EntityParameter entityParameter = new EntityParameter();
                    entityParameter.setParameterName(temp[0]);
                    entityParameter.setParameterValue(temp[1]);
                    list.add(entityParameter);
                }
            }
        }
        return list;
    }
}
