package com.hnykl.bp.base.tool.encrypt;


public class EntityParameter {

    /**
     *
     */
    private String parameterName = "";

    /**
     *
     */
    private String parameterValue = "";

    /**
     *
     */
    public EntityParameter() {
    }

    /**
     * @return String
     */
    public String getParameterName() {
        return this.parameterName;
    }

    /**
     * @param parameterName parameterName
     */
    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    /**
     * @return String
     */
    public String getParameterValue() {
        return this.parameterValue;
    }

    /**
     * @param parameterValue parameterValue
     */
    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

}
