package com.hnykl.bp.base.tool.encrypt;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
/*
 * HMAC-SHA1加密，专为短信发送提供
 */
public class HmacSHA1Encrypt {
	 public static byte[] HmacSHA1Encrypt(String encryptText, String encryptKey)
	    throws Exception
	  {
	    String MAC_NAME = "HmacSHA1";
	    String ENCODING = "UTF-8";
	    byte[] data = encryptKey.getBytes(ENCODING);
	    SecretKey secretKey = new SecretKeySpec(data, MAC_NAME);
	    Mac mac = Mac.getInstance(MAC_NAME);
	    mac.init(secretKey);
	    byte[] text = encryptText.getBytes(ENCODING);
	    return mac.doFinal(text);
	  }
}
