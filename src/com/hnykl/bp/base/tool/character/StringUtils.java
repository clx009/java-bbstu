package  com.hnykl.bp.base.tool.character;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * 字符串操作工具
 */
public class StringUtils {

    private static final char[] QUOTE_ENCODE = "&quot;".toCharArray();

    private static final char[] AMP_ENCODE = "&amp;".toCharArray();

    private static final char[] LT_ENCODE = "&lt;".toCharArray();

    private static final char[] GT_ENCODE = "&gt;".toCharArray();

    /**
     * 被 this.hash() 方法调用
     */
    private static MessageDigest digest = null;

    /**
     * logger(log4j)
     */
    static Logger logger = Logger.getLogger(MessageDigest.class.getName());
    
    /**
     * 是否为空判断
     * @param str
     * @return 如果为空，返回true
     */
    public static boolean isEmpty(String str) {
        boolean empty = false;
        if (str == null || str.equals("") || str.toLowerCase().equals("null"))
            empty = true;
        return empty;
    }

    /**   
     * 是否为空的判断
     * @param str
     * @return
     * @history add by lf 2006-10-08
     */
     public static boolean isNotEmpty(String str) {
         return !isEmpty(str);

    }
     /**
      * 去除空的null值输出
      * @param str
      * @return
      */
     public static String delEmptyStr(String str){
    	 if(isEmpty(str)){
    		 return "";
    	 }else{
    		return str;
    	 }
    	 
     }
     
     /**
      * 
      * @return
      */
    public static String delNewlineChar(String str){
    	String ss = "";
    	if(isEmpty(str))return ss;
    	try{
    		ss = str.replaceAll("\r","");
    		ss = str.replaceAll("\n","");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return ss;
    }
    /**
     * 得到map当中的字符串
     * @param dataMap
     * @param key
     * @return
     */
    public static String getMapDataStr(Map dataMap,String key){
    	String ss = "";
    	try{
    		Object object = dataMap.get(key); 
	    	if(object != null){
	    		ss = (String)object;
	    	}
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return ss;
    }
    
    /**
     * 替换所有字符串
     * 
     * @param line
     * @param oldString
     * @param newString
     * @return
     */
    public static String strReplace(String line, String oldString,
            String newString) {
        if (line == null) {
            return null;
        }
        if (newString == null) {
            return line;
        }
        int i = 0;
        if ((i = line.indexOf(oldString, i)) >= 0) {
            char[] string2 = line.toCharArray();
            char[] newString2 = newString.toCharArray();
            int oLength = oldString.length();
            StringBuffer buf = new StringBuffer(string2.length);
            buf.append(string2, 0, i).append(newString2);
            i += oLength;
            int j;
            for (j = i; (i = line.indexOf(oldString, i)) > 0; j = i) {
                buf.append(string2, j, i - j).append(newString2);
                i += oLength;
            }

            buf.append(string2, j, string2.length - j);
            return buf.toString();
        } else {
            return line;
        }
    }

    /**
     * @param str
     * @return
     */
    public static String htmlEncoder(String str) {
        if (str == null) {
            return ("");
        }
        if (str.equals("")) {
            return ("");
        }
        // 建立一个StringBuffer来处理输入数据
        StringBuffer buf = new StringBuffer();
        char ch1 = '\n';
        char ch2 = '\n';

        for (int i = 0; i < str.length(); i++) {
            ch1 = str.charAt(i);

            if ((ch1 == ' ') && ((i + 1) < str.length())) { // 将两个空格转换为一个全角中文空格
                ch2 = str.charAt(i + 1);
                if (ch2 == ' ') {
                    buf.append("　");
                    i++;
                } else {
                    buf.append(ch1);
                }
            } else if (ch1 == '\n') {
                buf.append("<br>");
            } else if (ch1 == '\t') {
                buf.append("　　");
            } else if (ch1 == '<') {
                buf.append("&lt;");
            } else if (ch1 == '>') {
                buf.append("&gt;");
            } else if (ch1 == '&') {
                buf.append("&amp;");
            } else {
                buf.append(ch1);
            }
        }
        return buf.toString();
    }

    /**
     * 把"&lt;P align=center&gt;&amp;nbsp;&lt;/P&gt;"类似的字符转换成"
     * <P align=center>
     * &nbsp;
     * </P>"
     * 
     * @param str
     * @return
     */
    public static String htmlDecoder(String str) {
        if (str == null) {
            return ("");
        }
        if (str.equals("")) {
            return ("");
        }

        String strFirest = str.replaceAll("&lt;", "<");
        String strSecond = strFirest.replaceAll("&gt;", ">");
        String strthree = strSecond.replaceAll("&amp;", "&");
        String strFour = strthree.replaceAll("<br>", "\\\\n");
        String strFive = strFour.replaceAll("&quot;", "\'");

        return strFive;
    }


    /**
     * 使用 MD5 算法获取一个字符串的哈希摘要，并且把摘要转换成 16 进制字符串返回；为避免 创建过多的 MessageDigest
     * 对象，这个方法是线程同步的；
     * <p>
     * MD5 算法是单向不可逆的，输入一个字符串总能获得一个输出字符串，但是不能从输出字符串 反向得到输入字符串；
     * <p>
     * 使用 MD5 算法对口令加密，用户输入一个明文的密码字符串，系统调用 hash() 从明文获取
     * 哈希摘要，然后和保存在数据库的密码作比较，相等则可通过口令验证；
     * 
     * @param data 明文字符串
     * @return 经过 MD5 变换以后的密文字符串
     */
    public synchronized static final String hash(String data) {
        if (digest == null) {
            try {
                digest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException nsae) {
                logger.error("Failed to load the MD5 MessageDigest. ", nsae);
            }
        }

        try {
            digest.update(data.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            logger.error(e);
        }
        return encodeHex(digest.digest());
    }

    /**
     * 把一个 byte 数组转换成 16 进制字符串
     * <p>
     * 
     * @param bytes byte 数组
     * @return 16 进制字符串
     */
    public static final String encodeHex(byte[] bytes) {
        StringBuffer buf = new StringBuffer(bytes.length * 2);
        int i;

        for (i = 0; i < bytes.length; i++) {
            if (((int) bytes[i] & 0xff) < 0x10) {
                buf.append("0");
            }
            buf.append(Long.toString((int) bytes[i] & 0xff, 16));
        }
        return buf.toString();
    }

    /**
     * 对字符串中含有HTML Tag标签号进行转移
     * 
     * @param in 转义前的字符串
     * @return 转义后的字符串
     */
    public static final String escapeHTMLTags(String in) {
        if (in == null) {
            return null;
        }
        int i = 0;
        int last = 0;
        char[] input = in.toCharArray();
        int len = input.length;
        StringBuffer out = new StringBuffer((int) ((double) len * 1.3D));
        for (; i < len; i++) {
            char ch = input[i];
            if (ch <= '>') {
                if (ch == '<') {
                    if (i > last) {
                        out.append(input, last, i - last);
                    }
                    last = i + 1;
                    out.append(LT_ENCODE);
                } else if (ch == '>') {
                    if (i > last) {
                        out.append(input, last, i - last);
                    }
                    last = i + 1;
                    out.append(GT_ENCODE);
                }
            }
        }

        if (last == 0) {
            return in;
        }
        if (i > last) {
            out.append(input, last, i - last);
        }
        return out.toString();
    }

    /**
     * 对字符串中含有&lt和&gt标签号进行转移
     * 
     * @param in 转义前的字符串
     * @return 转义后的字符串
     */
    public static final String encodeHTMLTags(String in) {
        if (in == null) {
            return null;
        }

        return in.replaceAll(new String(GT_ENCODE), ">").replaceAll(
                new String(LT_ENCODE), "<");
    }

    /**
     * 对将要加入XML中的字符串进行转义
     * 
     * @param string 需要转义的字符串
     * @return 转义后的字符串
     */
    public static final String escapeForXML(String string) {
        if (string == null) {
            return null;
        }
        int i = 0;
        int last = 0;
        char[] input = string.toCharArray();
        int len = input.length;
        StringBuffer out = new StringBuffer((int) ((double) len * 1.3D));
        for (; i < len; i++) {
            char ch = input[i];
            if (ch <= '>') {
                if (ch == '<') {
                    if (i > last) {
                        out.append(input, last, i - last);
                    }
                    last = i + 1;
                    out.append(LT_ENCODE);
                } else if (ch == '&') {
                    if (i > last) {
                        out.append(input, last, i - last);
                    }
                    last = i + 1;
                    out.append(AMP_ENCODE);
                } else if (ch == '"') {
                    if (i > last) {
                        out.append(input, last, i - last);
                    }
                    last = i + 1;
                    out.append(QUOTE_ENCODE);
                }
            }
        }

        if (last == 0) {
            return string;
        }
        if (i > last) {
            out.append(input, last, i - last);
        }
        return out.toString();
    }

    /**
     * 对XML中已经转义的字符串进行解码
     * 
     * @param string 解码前的字符串
     * @return 解码后的字符串
     */
    public static final String unescapeFromXML(String string) {
        string = strReplace(string, "&lt;", "<");
        string = strReplace(string, "&gt;", ">");
        string = strReplace(string, "&quot;", "\"");
        return strReplace(string, "&amp;", "&");
    }

    /**
     * 编码一个URL，将其它的除ASCII码之外的内容全部编成默认的编码
     * 
     * @param input 输入的编码
     * @return 输出的编码
     */
    public static String encodeURI(String input) {
        if (input == null) {
            return input;
        }
        StringBuffer buf = new StringBuffer();
        char[] charArray = input.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            char sub = charArray[i];
            if ((int) sub > 255) {
                buf.append(URLEncoder.encode(String.valueOf(sub)));
            } else {
                buf.append(sub);
            }
        }
        return buf.toString();
    }

    /**
     * 将文件名中的汉字转为UTF8编码的串,以便下载时能正确显示另存的文件名. 纵横软件制作中心雨亦奇2003.08.01
     * 
     * @param s 原文件名
     * @return 重新编码后的文件名
     */
    public static String toUtf8String(String s) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 0 && c <= 255) {
                sb.append(c);
            } else {
                byte[] b;
                try {
                    b = String.valueOf(c).getBytes("utf-8");
                } catch (Exception ex) {
                    System.out.println(ex);
                    b = new byte[0];
                }
                for (int j = 0; j < b.length; j++) {
                    int k = b[j];
                    if (k < 0) {
                        k += 256;
                    }
                    sb.append("%" + Integer.toHexString(k).toUpperCase());
                }
            }
        }
        return sb.toString();
    }

    public static String fromUTF(String val) throws Exception {
        StringBuffer sb = new StringBuffer();
        byte[] buf = val.getBytes("ISO-8859-1");
        int i = 0, j, c, c2, c3;
        while (i < buf.length) {
            if ((char) buf[i] == '%') {
                c = convertHexDigit(buf[i + 1]) << 4
                        | convertHexDigit(buf[i + 2]);
                switch (c >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    /* 0xxxxxxx */
                    i += 3;
                    sb.append((char) c);
                    break;
                case 12:
                case 13:
                    /* 110x xxxx 10xx xxxx */
                    i += 3;
                    if (i + 3 > buf.length && (char) buf[i] != '%') {
                        throw new Exception("UTF String format error !");
                    }
                    c2 = convertHexDigit(buf[i + 1]) << 4
                            | convertHexDigit(buf[i + 2]);
                    i += 3;
                    if ((c2 & 0xC0) != 0x80) {
                        throw new Exception("UTF String format error !");
                    }
                    sb.append((char) (((c & 0x1F) << 6) | (c2 & 0x3F)));
                    break;
                case 14:
                    /* 1110 xxxx 10xx xxxx 10xx xxxx */
                    i += 3;
                    if (i + 6 > buf.length && (char) buf[i] != '%'
                            && (char) buf[i + 3] != '%') {
                        throw new Exception("UTF String format error !");
                    }
                    c2 = convertHexDigit(buf[i + 1]) << 4
                            | convertHexDigit(buf[i + 2]);
                    i += 3;
                    c3 = convertHexDigit(buf[i + 1]) << 4
                            | convertHexDigit(buf[i + 2]);
                    i += 3;
                    if (((c2 & 0xC0) != 0x80) || ((c3 & 0xC0) != 0x80)) {
                        throw new Exception("UTF String format error !");
                    }
                    sb
                            .append((char) (((c & 0x0F) << 12)
                                    | ((c2 & 0x3F) << 6) | ((c3 & 0x3F) << 0)));
                    break;
                default:
                    /* 10xx xxxx, 1111 xxxx */
                    throw new Exception("UTF String format error !");
                }
            } else {
                sb.append((char) buf[i]);
                i++;
            }
        }
        return sb.toString();
    }

    private static int convertHexDigit(byte b) {
        b &= 0x7F;
        if ((b >= '0') && (b <= '9')) {
            return b - '0';
        } else if ((b >= 'a') && (b <= 'f')) {
            return b - 'a' + 10;
        } else if ((b >= 'A') && (b <= 'F')) {
            return b - 'A' + 10;
        } else {
            return 0;
        }
    }

    public static String toUTF(String val) {
        final char[] xlat = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F'};
        int i = 0, len = (val == null) ? 0 : val.length();
        StringBuffer sb = new StringBuffer();
        int c, t;
        while (i < len) {
            c = val.charAt(i++);
            if ((c >= 0x0001) && (c <= 0x007F)) {
                sb.append((char) c);
            } else if (c > 0x07FF) {
                sb.append('%');
                t = 0xE0 | ((c >> 12) & 0x0F);
                sb.append(xlat[(t >> 4) & 0x0F]);
                sb.append(xlat[(t >> 0) & 0x0F]);

                sb.append('%');
                t = 0x80 | ((c >> 6) & 0x3F);
                sb.append(xlat[(t >> 4) & 0x0F]);
                sb.append(xlat[(t >> 0) & 0x0F]);

                sb.append('%');
                t = 0x80 | ((c >> 0) & 0x3F);
                sb.append(xlat[(t >> 4) & 0x0F]);
                sb.append(xlat[(t >> 0) & 0x0F]);
            } else {
                sb.append('%');
                t = 0xC0 | ((c >> 6) & 0x1F);
                sb.append(xlat[(t >> 4) & 0x0F]);
                sb.append(xlat[(t >> 0) & 0x0F]);

                sb.append('%');
                t = 0x80 | ((c >> 0) & 0x3F);
                sb.append(xlat[(t >> 4) & 0x0F]);
                sb.append(xlat[(t >> 0) & 0x0F]);
            }
        }
        return sb.toString();
    }

    /**
     * 将字符串数组转为以逗号分割的字段串
     * 
     * @param arr 字符串数组
     * @return 字符串
     */
    public static String array2Str(String[] arr) {
        if (arr == null || arr.length == 0) {
            return "''";
        }

        StringBuffer result = new StringBuffer();
        result.append("'").append(arr[0]).append("'");
        for (int i = 1; i < arr.length; i++) {
            result.append(",'").append(arr[i]).append("'");
        }

        return result.toString();
    }

    /**
     * 将整数数组转为以逗号分割的字段串
     * 
     * @param arr 字符串数组
     * @return 字符串
     */
    public static String array2Str(int[] arr) {
        if (arr == null || arr.length == 0) {
            return "''";
        }

        StringBuffer result = new StringBuffer();
        result.append("'").append(arr[0]).append("'");
        for (int i = 1; i < arr.length; i++) {
            result.append(",'").append(arr[i]).append("'");
        }

        return result.toString();
    }

    /**
     * 将整数数组转为以逗号分割的无引号字符串
     * 
     * @param arr 字符串数组
     * @return 字符串
     */
    public static String array2StrNodot(int[] arr) {
        if (arr == null || arr.length == 0) {
            return "";
        }

        StringBuffer result = new StringBuffer();
        result.append(arr[0]);
        for (int i = 1; i < arr.length; i++) {
            result.append(",").append(arr[i]);
        }

        return result.toString();
    }

    /**
     * 将html代码转换为js可读取的字符串.
     * 
     * @param html 源html
     * @param quote 引号类型
     * @return
     */
    public static String html2JS(String html, String quote) {
        return html.replaceAll(quote, "\\\\" + quote).replaceAll("\n", "\\\\n")
                .replaceAll("\r", "");
    }

    /**
     * 按给定分割符 delim 将字符串 stream 拆分
     * 
     * @param stream
     * @param delim
     * @return
     * @deprecated
     */
    public static final String[] dispartStringWithTokens(String stream,
            String delim) {
        int count = 0; // 记数
        for (int index = 0; index < stream.length() && index != -1; count++) {
            index = stream.indexOf(delim, index);
            if (index == -1) {
                continue; // 最后一项不带分隔符
            }
            index += delim.length();
        }

        String[] result = new String[count];
        for (int i = 0, begin = 0, end = 0; i < count; i++) {
            end = stream.indexOf(delim, begin);
            if (end == -1) {
                end = stream.length(); // 最后一项不带分隔符
            }
            result[i] = new String(stream.substring(begin, end));
            begin = end + delim.length();
        }

        return result;
    }

    /**
     * 左填充字符串
     * 
     * @param src 源字符串
     * @param padder 填充的字符串
     * @param padCount 填充的字符串个数
     * @return 处理后的字符串
     */
    public static String leftPad(String src, String padder, int padCount) {
        if (src == null) {
            src = "";
        }

        for (int i = 0; i < padCount; i++) {
            src = padder + src;
        }

        return src;
    }

    /** ****************************************************************** */
    /**
     * 内部类
     */
    private static class DiscardOutputStream extends OutputStream {
        /**
         * @see java.io.OutputStream
         */
        public void write(int b) throws IOException {
        }

        /**
         * @see java.io.OutputStream
         */
        public void write(byte[] b) throws IOException {
        }

        /**
         * @see java.io.OutputStream
         */
        public void write(byte[] b, int off, int len) throws IOException {
        }
    }

    private static String getBodyText(Node node) {
        NodeList nl = node.getChildNodes();
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < nl.getLength(); i++) {
            Node child = nl.item(i);
            switch (child.getNodeType()) {
            case Node.ELEMENT_NODE:
                buffer.append(getBodyText(child));
                buffer.append(" ");
                break;
            case Node.TEXT_NODE:
                buffer.append(((Text) child).getData());
                break;
            }
        }
        return buffer.toString();
    }
    /*
    public static String html2Text(String html) {
        Tidy tidy = new Tidy();
        tidy.setQuiet(true);
        tidy.setShowWarnings(false);
        tidy.setErrout(new PrintWriter(new DiscardOutputStream()));
        Document root = tidy.parseDOM(new StringReader(html), null);
        String body = "";
        if (root == null) {
            return body;
        }
        NodeList nl = root.getElementsByTagName("body");
        if (nl.getLength() > 0) {
            body = getBodyText(nl.item(0));
            if (body == null) {
                body = "";
            }
        }
        return body;
    }
	*/
    
    private static double rad(double d)
    {
        return d * Math.PI / 180.0;
    }
    public static double getDistance(double lat1, double lng1, double lat2, double lng2)
    {
    	double EARTH_RADIUS = 6378.137;
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + 
         Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
        return s;
    }
    
    public static void main(String[] args) {
    	double dd = getDistance(28.20428,112.965948,28.229562,112.869901);
    	System.out.println(dd);
	}
    
}