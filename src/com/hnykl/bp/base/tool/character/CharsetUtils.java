package com.hnykl.bp.base.tool.character;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
/**
 * 转码类
 * @author powerquanc
 *  2013-6-14
 */
public class CharsetUtils {
//  1.关于文件编码
	//
//	    　　一般文件保存(文本文件),在保存的时候按系统默认语言编码(字符集)保存, 在中文系统上是GBK,英文系统上是ISO8859_1, 日文系统上是MS932.
	//  在打开的时候同样是按当前系统默认语言(字符集)编码去解码，所以一些中文汉字在日文系统上会显示乱码的原因就是这个.
	//  说白了也就是打开文件时候不同平台使用的字符集不同，
	//  例如中文字符集包括 a,b,c, 日文字符集包括a,b，当在日文系统上解码汉字c的时候，就会出现所谓的乱码。
//	    2. java工作处理编码模式
	//
//	   　　(1)java在运行期一律以unicode来存储字符,这样有利的支持了多语言环境.
	//
//	   　　(2)java在读取文件的时候默认是按照系统默认语言(字符集)编码来解码文件，读取和保存时候的编码不一致也导致程序中参数值错误，用FileInputStream类读取文件可以指定编码读取
//	          UTF-8
	//将以"中文"两个字为例，经查表可以知道其GB2312编码是"d6d0 cec4"，Unicode编码为"4e2d 6587"，UTF编码就是"e4b8ad e69687"。注意，这两个字没有iso8859-1编码，但可以用iso8859-1编码来"表示"。
    public static String convert(String s1, String c, String c2) {
        String s2 = "";
        try {
        	if(StringUtils.isNotEmpty(s1)){
        		s2 = new String(s1.getBytes(c), c2);
        	}
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  
        }
        return s2;
    }

    public static String UTF2GBK(String old) {
        String snew = null;
        try {
        	if(StringUtils.isNotEmpty(old)){
        		snew = new String(old.getBytes("UTF-8"),"GBK");
        	}
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  
        }
        return snew;
    }

    public static String ISO2UTF(String old) {
        String snew = null;
        try {
        	if(StringUtils.isNotEmpty(old)){
        		snew = new String(old.getBytes("ISO8859-1"), "UTF-8");
        	}
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace(); 
        }
        return snew;
    }

    /**
     * 转换成GBK字符,主要用于保存数据的转换
     *
     * @param old
     * @return
     */
    public static String toGBK(String old) {
        String snew = null;
        try {
        	if(StringUtils.isNotEmpty(old)){
        		snew = new String(old.getBytes(), "GBK");
        	}
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  
        }
        return snew;
    }
    /**
     * 转换成UTF-8字符,主要用于保存数据的转换
     *
     * @param old
     * @return
     */
    public static String toUTF8(String old) {
    	String snew = null;
    	try {
    		if(StringUtils.isNotEmpty(old)){
    			snew = new String(old.getBytes(), "UTF-8");
    		}
    	} catch (UnsupportedEncodingException e) {
    		e.printStackTrace();  
    	}
    	return snew;
    }

    public static String ISO2GBK(String old) {
        String snew = null;
        try {
        	if(StringUtils.isNotEmpty(old)){
        		snew = new String(old.getBytes("ISO-8859-1"), "GBK");
        	}
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  
        }
        return snew;
    }
    
//    public static String convertCodeAndGetText(String str) {//转变编码 
//    	  
//        File file = new File(str_filepath);  
//        BufferedReader reader;  
//        String text = "";  
//        try {  
//              
//            FileInputStream fis = new FileInputStream(file);  
//            BufferedInputStream in = new BufferedInputStream(fis);  
//            in.mark(4);  
//            byte[] first3bytes = new byte[3];  
//            in.read(first3bytes);  
//            in.reset();  
//            if (first3bytes[0] == (byte) 0xEF && first3bytes[1] == (byte) 0xBB  
//                    && first3bytes[2] == (byte) 0xBF) {// utf-8  
//                  reader = new BufferedReader(new InputStreamReader(in, "utf-8"));  
//            } else if (first3bytes[0] == (byte) 0xFF  
//                    && first3bytes[1] == (byte) 0xFE) {  
//                  reader = new BufferedReader(new InputStreamReader(in, "unicode"));  
//            } else if (first3bytes[0] == (byte) 0xFE && first3bytes[1] == (byte) 0xFF) {  
//                  reader = new BufferedReader(new InputStreamReader(in,  "utf-16be"));  
//            } else if (first3bytes[0] == (byte) 0xFF  && first3bytes[1] == (byte) 0xFF) {  
//                  reader = new BufferedReader(new InputStreamReader(in,  "utf-16le"));  
//            } else {  
//                  reader = new BufferedReader(new InputStreamReader(in, "GBK"));  
//            }  
//            String str = reader.readLine();  
//  
//            while (str != null) {  
//                text = text + str + "\n";  
//                str = reader.readLine();  
//            }  
//            reader.close();  
//      } catch (FileNotFoundException e) {  
//            // TODO Auto-generated catch block  
//            e.printStackTrace();  
//        } catch (IOException e) {  
//            e.printStackTrace();  
//        }  
//        return text;  
//    }  

    /**
     * 把字符串转换成Unicode码
     *
     * @param strText 待转换的字符串
     * @param code    转换前字符串的编码，如"GBK"
     * @return 转换后的Unicode码字符串
     */
    public static String toUnicode(String strText, String code) throws UnsupportedEncodingException {
        char c;
        String strRet = "";
        int intAsc;
        String strHex;
        strText = new String(strText.getBytes("8859_1"), code);
        for (int i = 0; i < strText.length(); i++) {
            c = strText.charAt(i);
            intAsc = (int) c;
            if (intAsc > 128) {
                strHex = Integer.toHexString(intAsc);
                strRet = strRet + "&#x" + strHex + ";";
            } else {
                strRet = strRet + c;
            }
        }
        return strRet;
    }

    /**
     *
     * @param asc
     * @return
     */
    public static String asc2gb(String asc) {
        String ret;

        if (asc == null) return asc;
        try {
            ret = new String(asc.getBytes("ISO8859_1"), "GB2312");
        }
        catch (UnsupportedEncodingException e) {
            ret = asc;
        }
        return ret;
    }

    public static String gb2asc(String gb) {
        String ret;
        if (gb == null) return gb;
        try {
            ret = new String(gb.getBytes("GB2312"), "ISO8859_1");
        }
        catch (UnsupportedEncodingException e) {
            ret = gb;
        }
        return ret;
    }

    public static int byte2int(byte b) {
        return ((-1) >>> 24) & b;
    }
    
    /**
     * 判断是否对url进行encode过
     * @param ss
     * @return
     */
    public static boolean isUrlEncode(String ss){
    	boolean flag = false;
    	String desCodeChar[] =  {"%20"," %22","%23","%25","%26","%28","%29","%2B","%2C","%2F","%3A","%3B","%3C","%3D","%3E","%3F","%4o","%5C","%7C"};
    	for(int i=0;i<desCodeChar.length;i++ ){
    		if(ss.indexOf(desCodeChar[i]) > -1){
    			flag = true;
    			break;
    		}
    	}
    	return flag;
    }
    
    
    public static void main(String []arg){
    	String ss = "%20 %22  %23 %25  %26  %28dd  %29 d%2B1 %2C 2%2F3 %3A %3B %3C %3D %3E %3F %4o %5C %7C ";
    	
    }

}

