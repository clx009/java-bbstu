/*    */ package com.hnykl.bp.base.codegenerate.util;
/*    */ 
/*    */ import java.util.List;
/*    */ import org.apache.commons.lang.StringUtils;
/*    */ 
/*    */ public class CodeStringUtils
/*    */ {
/*    */   public static String getStringSplit(String[] val)
/*    */   {
/* 15 */     StringBuffer sqlStr = new StringBuffer();
/* 16 */     String[] arrayOfString = val; int j = val.length; for (int i = 0; i < j; i++) { String s = arrayOfString[i];
/* 17 */       if (StringUtils.isNotBlank(s)) {
/* 18 */         sqlStr.append(",");
/* 19 */         sqlStr.append("'");
/* 20 */         sqlStr.append(s.trim());
/* 21 */         sqlStr.append("'");
/*    */       }
/*    */     }
/* 24 */     return sqlStr.toString().substring(1);
/*    */   }
/*    */ 
/*    */   public static String getInitialSmall(String str)
/*    */   {
/* 30 */     if (StringUtils.isNotBlank(str)) {
/* 31 */       str = str.substring(0, 1).toLowerCase() + str.substring(1);
/*    */     }
/* 33 */     return str;
/*    */   }
/*    */ 
/*    */   public static Integer getIntegerNotNull(Integer t)
/*    */   {
/* 40 */     if (t == null) {
/* 41 */       return Integer.valueOf(0);
/*    */     }
/* 43 */     return t;
/*    */   }
/*    */ 
/*    */   public static boolean isIn(String substring, String[] source)
/*    */   {
/* 55 */     if ((source == null) || (source.length == 0)) {
/* 56 */       return false;
/*    */     }
/* 58 */     for (int i = 0; i < source.length; i++) {
/* 59 */       String aSource = source[i];
/* 60 */       if (aSource.equals(substring)) {
/* 61 */         return true;
/*    */       }
/*    */     }
/* 64 */     return false;
/*    */   }
/*    */ 
/*    */   public static boolean isIn(String substring, List<String> ls)
/*    */   {
/* 75 */     String[] source = new String[0];
/* 76 */     if (ls != null) {
/* 77 */       source = (String[])ls.toArray();
/*    */     }
/*    */ 
/* 80 */     if ((source == null) || (source.length == 0)) {
/* 81 */       return false;
/*    */     }
/* 83 */     for (int i = 0; i < source.length; i++) {
/* 84 */       String aSource = source[i];
/* 85 */       if (aSource.equals(substring)) {
/* 86 */         return true;
/*    */       }
/*    */     }
/* 89 */     return false;
/*    */   }
/*    */ }

