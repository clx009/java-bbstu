/*    */ package com.hnykl.bp.base.codegenerate.util.def;
/*    */ 
/*    */ public final class FtlDef
/*    */ {
/*  7 */   public static String FIELD_REQUIRED_NAME = "field_required_num";
/*    */ 
/*  9 */   public static String FIELD_ROW_NAME = "field_row_num";
/*    */ 
/* 12 */   public static String BP_TABLE_ID = "bp_table_id";
/*    */ 
/* 14 */   public static String SEARCH_FIELD_NUM = "search_field_num";
/*    */ 
/* 17 */   public static String BP_PRIMARY_KEY_POLICY = "bp_primary_key_policy";
/*    */ 
/* 19 */   public static String BP_SEQUENCE_CODE = "bp_sequence_code";
/*    */ }

