package com.hnykl.bp.base.codegenerate.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import com.hnykl.bp.base.codegenerate.pojo.Columnt;
import com.hnykl.bp.base.codegenerate.pojo.TableConvert;
import com.hnykl.bp.base.codegenerate.util.CodeResourceUtil;
import com.hnykl.bp.base.configuration.JdbcConfigMgr;

public class BpReadTable {
	private static final Log log = LogFactory.getLog(BpReadTable.class);
	private static final long serialVersionUID = -5324160085184088010L;
	private Connection conn;
	private Statement stmt;
	private String sql;
	private ResultSet rs;

	public static void main(String[] args) throws SQLException {
		try {
			List<Columnt> cls = new BpReadTable().readTableColumn("person");
			for (Columnt c : cls)
				System.out.println(c.getFieldName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(ArrayUtils.toString(new BpReadTable()
				.readAllTableNames()));
	}

	public List<String> readAllTableNames() throws SQLException {
		List tableNames = new ArrayList(0);
		try {
			Class.forName(JdbcConfigMgr.getDiverName());
			this.conn = DriverManager.getConnection(JdbcConfigMgr.getJdbcUrl(),
					JdbcConfigMgr.getJdbcUsername(),
					JdbcConfigMgr.getJdbcPassword());
			this.stmt = this.conn.createStatement(1005, 1007);
			String jdbcDbType = JdbcConfigMgr.getJdbcDbType();
			if (jdbcDbType.equals("mysql")) {
				String jdbcDatabase = JdbcConfigMgr.getJdbcDatabase();
				this.sql = "select table_name from information_schema.tables where table_schema='"
						+ jdbcDatabase + "'";
			}
			if (jdbcDbType.equals("oracle")) {
				this.sql = " select distinct colstable.table_name as  table_name from user_tab_cols colstable";
			}

			if (jdbcDbType.equals("postgresql")) {
				this.sql = "SELECT distinct c.relname AS  table_name FROM pg_class c";
			}
			if (jdbcDbType.equals("sqlserver")) {

				this.sql = "select distinct c.name as  table_name from sys.objects c ";
			}
			this.rs = this.stmt.executeQuery(this.sql);
			while (this.rs.next()) {
				String tableName = this.rs.getString(1);
				tableNames.add(tableName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (this.stmt != null) {
					this.stmt.close();
					this.stmt = null;
					System.gc();
				}
				if (this.conn != null) {
					this.conn.close();
					this.conn = null;
					System.gc();
				}
			} catch (SQLException ex) {
				throw ex;
			}
		} finally {
			try {
				if (this.stmt != null) {
					this.stmt.close();
					this.stmt = null;
					System.gc();
				}
				if (this.conn != null) {
					this.conn.close();
					this.conn = null;
					System.gc();
				}
			} catch (SQLException e) {
				throw e;
			}
		}
		return tableNames;
	}

	public List<Columnt> readTableColumn(String tableName) throws Exception {

		List<Columnt> columntList = new ArrayList();
		try {
			Class.forName(JdbcConfigMgr.getDiverName());
			this.conn = DriverManager.getConnection(JdbcConfigMgr.getJdbcUrl(),
					JdbcConfigMgr.getJdbcUsername(),
					JdbcConfigMgr.getJdbcPassword());
			this.stmt = this.conn.createStatement(1005, 1007);

			if ("mysql".equals(JdbcConfigMgr.getJdbcDbType())) {
				this.sql = ("select column_name,data_type,column_comment,0,0,character_maximum_length from information_schema.columns where table_name = '"
						+ tableName.toUpperCase()
						+ "'"
						+ " and table_schema = '"
						+ CodeResourceUtil.DATABASE_NAME + "'");
			}

			if ("oracle".equals(JdbcConfigMgr.getJdbcDbType())) {
				this.sql =

				("select colstable.column_name column_name, colstable.data_type data_type, commentstable.comments column_comment, colstable.Data_Precision column_precision, colstable.Data_Scale column_scale,colstable.Char_Length from user_tab_cols colstable  inner join user_col_comments commentstable  on colstable.column_name = commentstable.column_name  where colstable.table_name = commentstable.table_name  and colstable.table_name = '"
						+ tableName.toUpperCase() + "'");
			}

			this.rs = this.stmt.executeQuery(this.sql);
			this.rs.last();
			int fieldNum = this.rs.getRow();
			int n = fieldNum;
			if (n > 0) {
				Columnt columnt = new Columnt();
				columnt.setFieldName(formatField(this.rs.getString(1)
						.toLowerCase()));

				columnt.setFieldDbName(this.rs.getString(1).toUpperCase());
				columnt.setFieldType(formatField(this.rs.getString(2)
						.toLowerCase()));

				columnt.setPrecision(this.rs.getString(4));
				columnt.setScale(this.rs.getString(5));
				columnt.setCharmaxLength(this.rs.getString(6));

				formatFieldClassType(columnt);
				columnt.setFiledComment(this.rs.getString(3) == null ? columnt
						.getFieldName() : this.rs.getString(3));
				if ((!CodeResourceUtil.BP_GENERATE_TABLE_ID.equals(columnt
						.getFieldName()))
						&& (!"createDt".equals(columnt.getFieldName()))
						&& (!"modifyDt".equals(columnt.getFieldName()))
						&& (!"delflag".equals(columnt.getFieldName()))
						&& (!"crtuser".equals(columnt.getFieldName()))
						&& (!"crtuserName".equals(columnt.getFieldName()))
						&& (!"modifierName".equals(columnt.getFieldName()))
						&& (!"optip".equals(columnt.getFieldName()))
						&& (!"modifier".equals(columnt.getFieldName()))
						&& (!"delDt".equals(columnt.getFieldName()))
						&& (!"modifyip".equals(columnt.getFieldName()))) {
					columntList.add(columnt);
				}
				while (this.rs.previous()) {
					Columnt po = new Columnt();
					po.setFieldName(formatField(this.rs.getString(1)
							.toLowerCase()));

					po.setFieldDbName(this.rs.getString(1).toUpperCase());
					if ((!CodeResourceUtil.BP_GENERATE_TABLE_ID.equals(po
							.getFieldName()))
							&& (!"createDt".equals(po.getFieldName()))
							&& (!"modifyDt".equals(po.getFieldName()))
							&& (!"delflag".equals(po.getFieldName()))
							&& (!"crtuser".equals(po.getFieldName()))
							&& (!"crtuserName".equals(po.getFieldName()))
							&& (!"modifierName".equals(po.getFieldName()))
							&& (!"optip".equals(po.getFieldName()))
							&& (!"modifier".equals(po.getFieldName()))
							&& (!"delDt".equals(po.getFieldName()))
							&& (!"modifyip".equals(po.getFieldName()))) {
						po.setFieldType(formatField(this.rs.getString(2)
								.toLowerCase()));

						po.setPrecision(this.rs.getString(4));
						po.setScale(this.rs.getString(5));
						po.setCharmaxLength(this.rs.getString(6));

						formatFieldClassType(po);
						po.setFiledComment(this.rs.getString(3) == null ? po
								.getFieldName() : this.rs.getString(3));
						columntList.add(po);
					}
				}
			} else {
				throw new Exception("该表不存在或者表中没有字段");
			}
			System.out.println("读取表成功");
		} catch (ClassNotFoundException e) {
			throw e;
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (this.stmt != null) {
					this.stmt.close();
					this.stmt = null;
					System.gc();
				}
				if (this.conn != null) {
					this.conn.close();
					this.conn = null;
					System.gc();
				}
			} catch (SQLException e) {
				throw e;
			}
		}
		List<Columnt> rsList = new ArrayList();
		for (int i = columntList.size() - 1; i >= 0; i--) {
			Columnt ch = (Columnt) columntList.get(i);
			rsList.add(ch);
		}
		return rsList;
	}

	public List<Columnt> readOriginalTableColumn(String tableName)
			throws Exception {
		List<Columnt> columntList = new ArrayList();
		try {
			Class.forName(JdbcConfigMgr.getDiverName());
			this.conn = DriverManager.getConnection(JdbcConfigMgr.getJdbcUrl(),
					JdbcConfigMgr.getJdbcUsername(),
					JdbcConfigMgr.getJdbcPassword());
			this.stmt = this.conn.createStatement(1005, 1007);
			if ("oracle".equals(JdbcConfigMgr.getJdbcDbType())) {
				this.sql = ("select colstable.column_name column_name, colstable.data_type data_type, commentstable.comments column_comment, colstable.Data_Precision column_precision, colstable.Data_Scale column_scale,colstable.Char_Length from user_tab_cols colstable  inner join user_col_comments commentstable  on colstable.column_name = commentstable.column_name  where colstable.table_name = commentstable.table_name  and colstable.table_name = '"
						+ tableName.toUpperCase() + "'");
			} else if ("mysql".equals(JdbcConfigMgr.getJdbcDbType())) {
				this.sql = "show full columns from " + tableName.toUpperCase();
			}
			this.rs = this.stmt.executeQuery(this.sql);
			this.rs.last();
			int fieldNum = this.rs.getRow();
			int n = fieldNum;

			if ("oracle".equals(JdbcConfigMgr.getJdbcDbType())) {
				if (n > 0) {
					Columnt columnt = new Columnt();
					columnt.setFieldName(formatField(this.rs.getString(1)
							.toLowerCase()));

					columnt.setFieldDbName(this.rs.getString(1).toUpperCase());

					columnt.setPrecision(this.rs.getString(4));
					columnt.setScale(this.rs.getString(5));
					columnt.setCharmaxLength(this.rs.getString(6));

					columnt.setFieldType(formatDataType(this.rs.getString(2)
							.toLowerCase(), columnt.getPrecision(), columnt
							.getScale()));

					formatFieldClassType(columnt);
					columnt.setFiledComment(this.rs.getString(3) == null ? columnt
							.getFieldName() : this.rs.getString(3));

					columntList.add(columnt);
					while (this.rs.previous()) {
						Columnt po = new Columnt();
						po.setFieldName(formatField(this.rs.getString(1)
								.toLowerCase()));

						po.setFieldDbName(this.rs.getString(1).toUpperCase());

						po.setPrecision(this.rs.getString(4));
						po.setScale(this.rs.getString(5));
						po.setCharmaxLength(this.rs.getString(6));

						po.setFieldType(formatDataType(this.rs.getString(2)
								.toLowerCase(), columnt.getPrecision(), columnt
								.getScale()));

						formatFieldClassType(po);
						po.setFiledComment(this.rs.getString(3) == null ? po
								.getFieldName() : this.rs.getString(3));
						columntList.add(po);
					}
				} else {
					throw new Exception("该表不存在或者表中没有字段");
				}
			} else if ("mysql".equals(JdbcConfigMgr.getJdbcDbType())) {
				if (n > 0) {
					Columnt columnt = new Columnt();
					columnt.setFieldName(formatField(this.rs.getString("FIELD")
							.toLowerCase()));

					columnt.setFieldDbName(this.rs.getString("FIELD")
							.toUpperCase());

					if (this.rs.getString("TYPE").startsWith("decimal")) {
						columnt.setPrecision(this.rs
								.getString("TYPE")
								.substring(
										this.rs.getString("TYPE").indexOf("(") + 1,
										this.rs.getString("TYPE").indexOf(",")));
						columnt.setScale(this.rs.getString("TYPE").substring(
								this.rs.getString("TYPE").indexOf(",") + 1,
								this.rs.getString("TYPE").indexOf(")")));
					}
					if (this.rs.getString("TYPE").startsWith("char")
							&& this.rs.getString("TYPE").startsWith("varchar")) {
						columnt.setCharmaxLength(this.rs
								.getString("TYPE")
								.substring(
										this.rs.getString("TYPE").indexOf("(") + 1,
										this.rs.getString("TYPE").indexOf(")")));
					}

					columnt.setFieldType(formatDataType(formatField(this.rs
							.getString("FIELD").toLowerCase()), columnt
							.getPrecision(), columnt.getScale()));

					formatFieldClassType(columnt);
					columnt.setFiledComment(this.rs.getString("Comment") == null ? columnt
							.getFieldName() : this.rs.getString("Comment"));

					columntList.add(columnt);
					while (this.rs.previous()) {
						Columnt po = new Columnt();
						po.setFieldName(formatField(this.rs.getString("FIELD")
								.toLowerCase()));

						po.setFieldDbName(this.rs.getString("FIELD")
								.toUpperCase());

						if (this.rs.getString("TYPE").startsWith("decimal")) {
							po.setPrecision(this.rs.getString("TYPE")
									.substring(
											this.rs.getString("TYPE").indexOf(
													"(") + 1,
											this.rs.getString("TYPE").indexOf(
													",")));
							po.setScale(this.rs.getString("TYPE").substring(
									this.rs.getString("TYPE").indexOf(",") + 1,
									this.rs.getString("TYPE").indexOf(")")));
						}
						if (this.rs.getString("TYPE").startsWith("char")
								&& this.rs.getString("TYPE").startsWith(
										"varchar")) {
							po.setCharmaxLength(this.rs.getString("TYPE")
									.substring(
											this.rs.getString("TYPE").indexOf(
													"(") + 1,
											this.rs.getString("TYPE").indexOf(
													")")));
						}

						columnt.setFieldType(formatDataType(formatField(this.rs
								.getString("FIELD").toLowerCase()), po
								.getPrecision(), po.getScale()));

						formatFieldClassType(po);
						po.setFiledComment(this.rs.getString("Comment") == null ? po
								.getFieldName() : this.rs.getString("Comment"));
						columntList.add(po);
					}
				} else {
					throw new Exception("该表不存在或者表中没有字段");
				}
			}
			System.out.println("读取表成功");
		} catch (ClassNotFoundException e) {
			throw e;
		} catch (SQLException e) {
			throw e;
		} finally {
			try {
				if (this.stmt != null) {
					this.stmt.close();
					this.stmt = null;
					System.gc();
				}
				if (this.conn != null) {
					this.conn.close();
					this.conn = null;
					System.gc();
				}
			} catch (SQLException e) {
				throw e;
			}
		}
		List<Columnt> rsList = new ArrayList();
		for (int i = columntList.size() - 1; i >= 0; i--) {
			Columnt ch = (Columnt) columntList.get(i);
			rsList.add(ch);
		}
		return rsList;
	}

	/* 375 */public static String formatField(String field) {
		String[] strs = field.split("_");
		/* 376 */field = "";
		/* 377 */int m = 0;
		for (int length = strs.length; m < length; m++) {
			/* 378 */if (m > 0) {
				/* 379 */String tempStr = strs[m].toLowerCase();
				/* 380 */tempStr = tempStr.substring(0, 1).toUpperCase() +
				/* 381 */tempStr.substring(1, tempStr.length());
				/* 382 */field = field + tempStr;
				/*     */} else {
				/* 384 */field = field + strs[m].toLowerCase();
				/*     */}
			/*     */}
		/* 387 */return field;
		/*     */}

	/*     */
	/*     */public static String formatFieldCapital(String field)
	/*     */{
		/* 397 */String[] strs = field.split("_");
		/* 398 */field = "";
		/* 399 */int m = 0;
		for (int length = strs.length; m < length; m++) {
			/* 400 */if (m > 0) {
				/* 401 */String tempStr = strs[m].toLowerCase();
				/* 402 */tempStr = tempStr.substring(0, 1).toUpperCase() +
				/* 403 */tempStr.substring(1, tempStr.length());
				/* 404 */field = field + tempStr;
				/*     */} else {
				/* 406 */field = field + strs[m].toLowerCase();
				/*     */}
			/*     */}
		/* 409 */field = field.substring(0, 1).toUpperCase()
				+ field.substring(1);
		/* 410 */return field;
		/*     */}

	/*     */
	/*     */public boolean checkTableExist(String tableName)
	/*     */{
		/*     */try
		/*     */{
			/* 420 */System.out.println("数据库驱动: "
					+ JdbcConfigMgr.getDiverName());
			/* 421 */Class.forName(JdbcConfigMgr.getDiverName());
			/* 422 */this.conn = DriverManager.getConnection(
					JdbcConfigMgr.getJdbcUrl(),
					JdbcConfigMgr.getJdbcUsername(),
					JdbcConfigMgr.getJdbcPassword());
			/* 423 */this.stmt = this.conn.createStatement(1005,
			/* 424 */1007);
			/*     */String jdbcDbType = JdbcConfigMgr.getJdbcDbType();
			/* 428 */if (jdbcDbType.equals("mysql")) {
				this.sql = "show full columns from " + tableName.toUpperCase();
			}
			/*     */
			/* 434 */if (jdbcDbType.equals("oracle")) {
				/* 435 */this.sql =
				/* 442 */("select colstable.column_name column_name, colstable.data_type data_type, commentstable.comments column_comment from user_tab_cols colstable  inner join user_col_comments commentstable  on colstable.column_name = commentstable.column_name  where colstable.table_name = commentstable.table_name  and colstable.table_name = '"
						+
						/* 441 */tableName.toUpperCase() +
				/* 442 */"'");
				/*     */}
			/*     */
			/* 445 */if (jdbcDbType.equals("postgresql")) {
				/* 446 */this.sql = MessageFormat
						.format("SELECT a.attname AS  field,t.typname AS type,col_description(a.attrelid,a.attnum) as comment,null as column_precision,null as column_scale,null as Char_Length,a.attnotnull  FROM pg_class c,pg_attribute  a,pg_type t  WHERE c.relname = {0} and a.attnum > 0  and a.attrelid = c.oid and a.atttypid = t.oid  ORDER BY a.attnum ",
								new Object[] { TableConvert.getV(tableName
										.toLowerCase()) });
				/*     */}
			/* 448 */if (jdbcDbType.equals("sqlserver")) {
				/* 449 */this.sql = MessageFormat
						.format("select cast(a.name as varchar(50)) column_name,  cast(b.name as varchar(50)) data_type,  cast(e.value as varchar(200)) comment,  cast(ColumnProperty(a.object_id,a.Name,'''Precision''') as int) num_precision,  cast(ColumnProperty(a.object_id,a.Name,'''Scale''') as int) num_scale,  a.max_length,  (case when a.is_nullable=1 then '''y''' else '''n''' end) nullable   from sys.columns a left join sys.types b on a.user_type_id=b.user_type_id left join sys.objects c on a.object_id=c.object_id and c.type='''U''' left join sys.extended_properties e on e.major_id=c.object_id and e.minor_id=a.column_id and e.class=1 where c.name={0}",
								new Object[] { TableConvert.getV(tableName
										.toLowerCase()) });
				/*     */}
			/*     */
			/* 453 */this.rs = this.stmt.executeQuery(this.sql);
			/* 454 */this.rs.last();
			/* 455 */int fieldNum = this.rs.getRow();
			/* 456 */if (fieldNum > 0)
				/* 457 */return true;
			/*     */}
		/*     */catch (Exception e) {
			/* 460 */e.printStackTrace();
			/* 461 */return false;
			/*     */}
		/* 463 */return false;
		/*     */}

	/*     */
	/*     */private void formatFieldClassType(Columnt columnt)
	/*     */{
		/* 471 */String fieldType = columnt.getFieldType();
		/* 472 */String scale = columnt.getScale();
		/*     */
		/* 474 */columnt.setClassType("inputxt");
		/*     */
		/* 476 */if ("N".equals(columnt.getNullable())) {
			/* 477 */columnt.setOptionType("*");
			/*     */}
		/* 479 */if (("datetime".equals(fieldType))
				|| (fieldType.contains("time")))
			/* 480 */columnt.setClassType("easyui-datetimebox");
		/* 481 */else if ("date".equals(fieldType))
			/* 482 */columnt.setClassType("easyui-datebox");
		/* 483 */else if (fieldType.contains("int"))
			/* 484 */columnt.setOptionType("n");
		/* 485 */else if ("number".equals(fieldType)) {
			/* 486 */if ((StringUtils.isNotBlank(scale))
					&& (Integer.parseInt(scale) > 0))
				/* 487 */columnt.setOptionType("d");
			/*     */}
		/* 489 */else if (("float".equals(fieldType))
				|| ("double".equals(fieldType))
				|| ("decimal".equals(fieldType)))
			/* 490 */columnt.setOptionType("d");
		/* 491 */else if ("numeric".equals(fieldType))
			/* 492 */columnt.setOptionType("d");
		/*     */}

	/*     */
	/*     */private String formatDataType(String dataType, String precision,
			String scale)
	/*     */{
		/* 500 */if (dataType.contains("char"))
			/* 501 */dataType = "java.lang.String";
		/* 502 */else if (dataType.contains("int"))
			/* 503 */dataType = "java.lang.Integer";
		/* 504 */else if (dataType.contains("float"))
			/* 505 */dataType = "java.lang.Float";
		/* 506 */else if (dataType.contains("double"))
			/* 507 */dataType = "java.lang.Double";
		/* 508 */else if (dataType.contains("number")
				|| dataType.contains("decimal")) {
			/* 509 */if ((StringUtils.isNotBlank(scale))
					&& (Integer.parseInt(scale) > 0))
				/* 510 */dataType = "java.math.BigDecimal";
			/* 511 */else if ((StringUtils.isNotBlank(precision))
					&& (Integer.parseInt(precision) > 10))
				/* 512 */dataType = "java.lang.Long";
			/*     */else
				/* 514 */dataType = "java.lang.Integer";
			/*     */}
		/* 516 */else if (dataType.contains("decimal"))
			/* 517 */dataType = "BigDecimal";
		/* 518 */else if (dataType.contains("date"))
			/* 519 */dataType = "java.util.Date";
		/* 520 */else if (dataType.contains("time"))
		/*     */{
			/* 522 */dataType = "java.util.Date";
			/* 523 */} else if (dataType.contains("blob"))
			/* 524 */dataType = "byte[]";
		/* 525 */else if (dataType.contains("clob"))
			/* 526 */dataType = "java.sql.Clob";
		/* 527 */else if (dataType.contains("numeric"))
			/* 528 */dataType = "BigDecimal";
		/*     */else {
			/* 530 */dataType = "java.lang.Object";
			/*     */}
		/* 532 */return dataType;
		/*     */}
	/*     */
}
