/*    */ package com.hnykl.bp.base.codegenerate.pojo.onetomany;
/*    */ 
/*    */ import java.util.List;
/*    */ import com.hnykl.bp.base.codegenerate.pojo.Columnt;
/*    */ 
/*    */ public class SubTableEntity
/*    */ {
/*    */   private String entityPackage;
/*    */   private String tableName;
/*    */   private String entityName;
/*    */   private String primaryKeyPolicy;
/*    */   private String sequenceCode;
/*    */   private String ftlDescription;
/*    */   private String[] foreignKeys;
/*    */   private List<Columnt> subColums;
/*    */ 
/*    */   public String getEntityPackage()
/*    */   {
/* 32 */     return this.entityPackage;
/*    */   }
/*    */   public String getTableName() {
/* 35 */     return this.tableName;
/*    */   }
/*    */   public String getEntityName() {
/* 38 */     return this.entityName;
/*    */   }
/*    */   public String getFtlDescription() {
/* 41 */     return this.ftlDescription;
/*    */   }
/*    */   public List<Columnt> getSubColums() {
/* 44 */     return this.subColums;
/*    */   }
/*    */   public void setEntityPackage(String entityPackage) {
/* 47 */     this.entityPackage = entityPackage;
/*    */   }
/*    */   public void setTableName(String tableName) {
/* 50 */     this.tableName = tableName;
/*    */   }
/*    */   public void setEntityName(String entityName) {
/* 53 */     this.entityName = entityName;
/*    */   }
/*    */   public void setFtlDescription(String ftlDescription) {
/* 56 */     this.ftlDescription = ftlDescription;
/*    */   }
/*    */   public void setSubColums(List<Columnt> subColums) {
/* 59 */     this.subColums = subColums;
/*    */   }
/*    */   public String[] getForeignKeys() {
/* 62 */     return this.foreignKeys;
/*    */   }
/*    */   public void setForeignKeys(String[] foreignKeys) {
/* 65 */     this.foreignKeys = foreignKeys;
/*    */   }
/*    */   public String getPrimaryKeyPolicy() {
/* 68 */     return this.primaryKeyPolicy;
/*    */   }
/*    */   public String getSequenceCode() {
/* 71 */     return this.sequenceCode;
/*    */   }
/*    */   public void setPrimaryKeyPolicy(String primaryKeyPolicy) {
/* 74 */     this.primaryKeyPolicy = primaryKeyPolicy;
/*    */   }
/*    */   public void setSequenceCode(String sequenceCode) {
/* 77 */     this.sequenceCode = sequenceCode;
/*    */   }
/*    */ }

/* Location:           Z:\medicalTreatment\medicalInsurance\trunk\engineering\lib\codegenerate-3.4.4_02.jar
 * Qualified Name:     com.hnykl.bp.base.codegenerate.pojo.onetomany.SubTableEntity
 * JD-Core Version:    0.6.0
 */