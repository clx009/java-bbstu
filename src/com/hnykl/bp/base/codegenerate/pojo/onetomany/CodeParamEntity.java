/*    */ package com.hnykl.bp.base.codegenerate.pojo.onetomany;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ public class CodeParamEntity
/*    */ {
/*    */   private String entityPackage;
/*    */   private String tableName;
/*    */   private String entityName;
/*    */   private String ftlDescription;
/*    */   private String primaryKeyPolicy;
/*    */   private String sequenceCode;
/* 31 */   private String ftl_mode = "A";
/*    */   List<SubTableEntity> subTabParam;
/*    */ 
/*    */   public List<SubTableEntity> getSubTabParam()
/*    */   {
/* 37 */     return this.subTabParam;
/*    */   }
/*    */   public void setSubTabParam(List<SubTableEntity> subTabParam) {
/* 40 */     this.subTabParam = subTabParam;
/*    */   }
/*    */   public String getEntityPackage() {
/* 43 */     return this.entityPackage;
/*    */   }
/*    */   public String getTableName() {
/* 46 */     return this.tableName;
/*    */   }
/*    */   public String getEntityName() {
/* 49 */     return this.entityName;
/*    */   }
/*    */   public String getFtlDescription() {
/* 52 */     return this.ftlDescription;
/*    */   }
/*    */   public void setEntityPackage(String entityPackage) {
/* 55 */     this.entityPackage = entityPackage;
/*    */   }
/*    */   public void setTableName(String tableName) {
/* 58 */     this.tableName = tableName;
/*    */   }
/*    */   public void setEntityName(String entityName) {
/* 61 */     this.entityName = entityName;
/*    */   }
/*    */   public void setFtlDescription(String ftlDescription) {
/* 64 */     this.ftlDescription = ftlDescription;
/*    */   }
/*    */   public String getFtl_mode() {
/* 67 */     return this.ftl_mode;
/*    */   }
/*    */   public void setFtl_mode(String ftl_mode) {
/* 70 */     this.ftl_mode = ftl_mode;
/*    */   }
/*    */   public String getPrimaryKeyPolicy() {
/* 73 */     return this.primaryKeyPolicy;
/*    */   }
/*    */   public String getSequenceCode() {
/* 76 */     return this.sequenceCode;
/*    */   }
/*    */   public void setPrimaryKeyPolicy(String primaryKeyPolicy) {
/* 79 */     this.primaryKeyPolicy = primaryKeyPolicy;
/*    */   }
/*    */   public void setSequenceCode(String sequenceCode) {
/* 82 */     this.sequenceCode = sequenceCode;
/*    */   }
/*    */ }

/* Location:           Z:\medicalTreatment\medicalInsurance\trunk\engineering\lib\codegenerate-3.4.4_02.jar
 * Qualified Name:     com.hnykl.bp.base.codegenerate.pojo.onetomany.CodeParamEntity
 * JD-Core Version:    0.6.0
 */