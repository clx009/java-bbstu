package com.hnykl.bp.base.codegenerate.generate;

import java.util.Map;

public abstract interface ICallBack
{
  public abstract Map<String, Object> execute();
}