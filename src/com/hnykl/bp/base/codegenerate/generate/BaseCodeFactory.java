package com.hnykl.bp.base.codegenerate.generate;

import freemarker.template.Configuration;
import java.io.IOException;
import java.util.Locale;
import com.hnykl.bp.base.codegenerate.util.CodeResourceUtil;

public class BaseCodeFactory
{
  public Configuration getConfiguration()
    throws IOException
  {
    Configuration cfg = new Configuration();
    cfg.setClassForTemplateLoading(getClass(), CodeResourceUtil.FREEMARKER_CLASSPATH);
    cfg.setLocale(Locale.CHINA);
    cfg.setDefaultEncoding("UTF-8");
    return cfg;
  }
}

