/*    */ package com.hnykl.bp.base.minidao.spring.map;
/*    */ 
/*    */ import java.util.Iterator;
/*    */ import java.util.LinkedHashMap;
/*    */ import java.util.Locale;
/*    */ import java.util.Map;
/*    */ import java.util.Map.Entry;
/*    */ import java.util.Set;
/*    */ 
/*    */ public class MiniDaoLinkedMap extends LinkedHashMap<String, Object>
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   private final Locale locale;
/*    */ 
/*    */   public MiniDaoLinkedMap()
/*    */   {
/* 18 */     this(null);
/*    */   }
/*    */ 
/*    */   public MiniDaoLinkedMap(Locale locale) {
/* 22 */     this.locale = (locale == null ? Locale.getDefault() : locale);
/*    */   }
/*    */ 
/*    */   public MiniDaoLinkedMap(int initialCapacity) {
/* 26 */     this(initialCapacity, null);
/*    */   }
/*    */ 
/*    */   public MiniDaoLinkedMap(int initialCapacity, Locale locale) {
/* 30 */     super(initialCapacity);
/* 31 */     this.locale = (locale == null ? Locale.getDefault() : locale);
/*    */   }
/*    */ 
/*    */   public Object put(String key, Object value) {
/* 35 */     return super.put(convertKey(key), value);
/*    */   }
/*    */ 
/*    */   public void putAll(Map map) {
/* 39 */     if (map.isEmpty())
/* 40 */       return;
/*    */     Map.Entry entry;
/* 42 */     for (Iterator iterator = map.entrySet().iterator(); iterator.hasNext(); put(
/* 43 */       convertKey((String)entry.getKey()), entry.getValue()))
/*    */     {
/* 44 */       entry = (Map.Entry)iterator.next();
/*    */     }
/*    */   }
/*    */ 
/*    */   public boolean containsKey(Object key)
/*    */   {
/* 50 */     return ((key instanceof String)) && 
/* 50 */       (super.containsKey(convertKey((String)key)));
/*    */   }
/*    */ 
/*    */   public Object get(Object key) {
/* 54 */     if ((key instanceof String)) {
/* 55 */       return super.get(convertKey((String)key));
/*    */     }
/* 57 */     return null;
/*    */   }
/*    */ 
/*    */   public Object remove(Object key) {
/* 61 */     if ((key instanceof String)) {
/* 62 */       return super.remove(convertKey((String)key));
/*    */     }
/* 64 */     return null;
/*    */   }
/*    */ 
/*    */   public void clear() {
/* 68 */     super.clear();
/*    */   }
/*    */ 
/*    */   protected String convertKey(String key) {
/* 72 */     return key.toLowerCase(this.locale);
/*    */   }
/*    */ }

