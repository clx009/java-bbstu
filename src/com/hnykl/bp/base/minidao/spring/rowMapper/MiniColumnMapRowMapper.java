/*    */ package com.hnykl.bp.base.minidao.spring.rowMapper;
/*    */ 
/*    */ import java.sql.ResultSet;
/*    */ import java.sql.ResultSetMetaData;
/*    */ import java.sql.SQLException;
/*    */ import java.util.Map;
/*    */ import com.hnykl.bp.base.minidao.spring.map.MiniDaoLinkedMap;
/*    */ import org.springframework.jdbc.core.RowMapper;
/*    */ import org.springframework.jdbc.support.JdbcUtils;
/*    */ 
/*    */ public class MiniColumnMapRowMapper
/*    */   implements RowMapper<Map<String, Object>>
/*    */ {
/*    */   public Map<String, Object> mapRow(ResultSet resultset, int rowNum)
/*    */     throws SQLException
/*    */   {
/* 23 */     ResultSetMetaData rsmd = resultset.getMetaData();
/* 24 */     int columnCount = rsmd.getColumnCount();
/* 25 */     Map mapOfColValues = createColumnMap(columnCount);
/* 26 */     for (int i = 1; i <= columnCount; i++) {
/* 27 */       String key = getColumnKey(JdbcUtils.lookupColumnName(rsmd, i));
/* 28 */       Object obj = getColumnValue(resultset, i);
/* 29 */       mapOfColValues.put(key, obj);
/*    */     }
/*    */ 
/* 32 */     return mapOfColValues;
/*    */   }
/*    */ 
/*    */   protected Map<String, Object> createColumnMap(int columnCount) {
/* 36 */     return new MiniDaoLinkedMap(columnCount);
/*    */   }
/*    */ 
/*    */   protected String getColumnKey(String columnName) {
/* 40 */     return columnName;
/*    */   }
/*    */ 
/*    */   protected Object getColumnValue(ResultSet rs, int index) throws SQLException
/*    */   {
/* 45 */     return JdbcUtils.getResultSetValue(rs, index);
/*    */   }
/*    */ }

