/*    */ package com.hnykl.bp.base.minidao.spring.rowMapper;
/*    */ 
/*    */ import java.math.BigDecimal;
/*    */ import java.sql.ResultSet;
/*    */ import java.sql.ResultSetMetaData;
/*    */ import java.sql.SQLException;
/*    */ import java.util.Date;
/*    */ import org.apache.commons.beanutils.BeanUtils;
/*    */ import org.apache.commons.beanutils.ConvertUtils;
/*    */ import com.hnykl.bp.base.minidao.util.BigDecimalConverter;
/*    */ import com.hnykl.bp.base.minidao.util.CamelCaseUtils;
/*    */ import com.hnykl.bp.base.minidao.util.DateConverter;
/*    */ import com.hnykl.bp.base.minidao.util.IntegerConverter;
/*    */ import org.springframework.jdbc.core.RowMapper;
/*    */ import org.springframework.jdbc.support.JdbcUtils;
/*    */ 
/*    */ public class GenericRowMapper<T>
/*    */   implements RowMapper<T>
/*    */ {
/*    */   private Class<T> clazz;
/*    */ 
/*    */   public GenericRowMapper(Class<T> claz)
/*    */   {
/* 26 */     this.clazz = claz;
/*    */   }
/*    */ 
/*    */   public T mapRow(ResultSet resultset, int rowNum) throws SQLException {
/*    */     try {
/* 31 */       ResultSetMetaData rsmd = resultset.getMetaData();
/* 32 */       int columnCount = rsmd.getColumnCount();
/* 33 */       Object bean = this.clazz.newInstance();
/* 34 */       ConvertUtils.register(new DateConverter(), Date.class);
/* 35 */       ConvertUtils.register(new BigDecimalConverter(), BigDecimal.class);
/* 36 */       ConvertUtils.register(new IntegerConverter(), Integer.class);
/* 37 */       for (int i = 1; i <= columnCount; i++) {
/* 38 */         String key = getColumnKey(JdbcUtils.lookupColumnName(rsmd, i));
/* 39 */         Object obj = getColumnValue(resultset, i);
/* 40 */         String camelKey = CamelCaseUtils.toCamelCase(key);
/* 41 */         BeanUtils.setProperty(bean, camelKey, obj);
/*    */       }
/* 43 */       return (T)bean;
/*    */     }
/*    */     catch (Exception e) {
					throw new SQLException("mapRow error.", e);
/*    */     }
/* 47 */    
/*    */   }
/*    */ 
/*    */   protected String getColumnKey(String columnName)
/*    */   {
/* 52 */     return columnName;
/*    */   }
/*    */ 
/*    */   protected Object getColumnValue(ResultSet rs, int index) throws SQLException
/*    */   {
/* 57 */     return JdbcUtils.getResultSetValue(rs, index);
/*    */   }
/*    */ }

