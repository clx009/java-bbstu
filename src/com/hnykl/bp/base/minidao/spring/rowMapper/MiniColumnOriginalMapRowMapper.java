/*    */ package com.hnykl.bp.base.minidao.spring.rowMapper;
/*    */ 
/*    */ import java.sql.ResultSet;
/*    */ import java.sql.ResultSetMetaData;
/*    */ import java.sql.SQLException;
/*    */ import java.util.Map;
/*    */ import org.apache.commons.collections.map.LinkedMap;
/*    */ import org.springframework.jdbc.core.RowMapper;
/*    */ import org.springframework.jdbc.support.JdbcUtils;
/*    */ 
/*    */ public class MiniColumnOriginalMapRowMapper
/*    */   implements RowMapper<Map<String, Object>>
/*    */ {
/*    */   public Map<String, Object> mapRow(ResultSet resultset, int rowNum)
/*    */     throws SQLException
/*    */   {
/* 24 */     ResultSetMetaData rsmd = resultset.getMetaData();
/* 25 */     int columnCount = rsmd.getColumnCount();
/* 26 */     Map mapOfColValues = createColumnMap(columnCount);
/* 27 */     for (int i = 1; i <= columnCount; i++) {
/* 28 */       String key = getColumnKey(JdbcUtils.lookupColumnName(rsmd, i));
/* 29 */       Object obj = getColumnValue(resultset, i);
/* 30 */       mapOfColValues.put(key, obj);
/*    */     }
/*    */ 
/* 33 */     return mapOfColValues;
/*    */   }
/*    */ 
/*    */   protected Map<String, Object> createColumnMap(int columnCount) {
/* 37 */     return new LinkedMap(columnCount);
/*    */   }
/*    */ 
/*    */   protected String getColumnKey(String columnName) {
/* 41 */     return columnName;
/*    */   }
/*    */ 
/*    */   protected Object getColumnValue(ResultSet rs, int index) throws SQLException
/*    */   {
/* 46 */     return JdbcUtils.getResultSetValue(rs, index);
/*    */   }
/*    */ }
