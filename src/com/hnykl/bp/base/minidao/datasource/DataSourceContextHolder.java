/*    */ package com.hnykl.bp.base.minidao.datasource;
/*    */ 
/*    */ public class DataSourceContextHolder
/*    */ {
/* 15 */   private static final ThreadLocal contextHolder = new ThreadLocal();
/*    */ 
/*    */   public static void setDataSourceType(DataSourceType dataSourceType) {
/* 18 */     contextHolder.set(dataSourceType);
/*    */   }
/*    */ 
/*    */   public static DataSourceType getDataSourceType() {
/* 22 */     return (DataSourceType)contextHolder.get();
/*    */   }
/*    */ 
/*    */   public static void clearDataSourceType() {
/* 26 */     contextHolder.remove();
/*    */   }
/*    */ }

