/*    */ package com.hnykl.bp.base.minidao.datasource;
/*    */ 
/*    */ import java.util.Map;
/*    */ import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
/*    */ import org.springframework.jdbc.datasource.lookup.DataSourceLookup;
/*    */ 
/*    */ public class DynamicDataSource extends AbstractRoutingDataSource
/*    */ {
/*    */   protected Object determineCurrentLookupKey()
/*    */   {
/* 19 */     DataSourceType dataSourceType = DataSourceContextHolder.getDataSourceType();
/* 20 */     return dataSourceType;
/*    */   }
/*    */ 
/*    */   public void setDataSourceLookup(DataSourceLookup dataSourceLookup)
/*    */   {
/* 25 */     super.setDataSourceLookup(dataSourceLookup);
/*    */   }
/*    */ 
/*    */   public void setDefaultTargetDataSource(Object defaultTargetDataSource)
/*    */   {
/* 30 */     super.setDefaultTargetDataSource(defaultTargetDataSource);
/*    */   }
/*    */ 
/*    */   public void setTargetDataSources(Map targetDataSources)
/*    */   {
/* 35 */     super.setTargetDataSources(targetDataSources);
/*    */   }
/*    */ }

