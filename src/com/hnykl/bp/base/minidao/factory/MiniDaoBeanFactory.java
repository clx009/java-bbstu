/*    */ package com.hnykl.bp.base.minidao.factory;
/*    */ 
/*    */ import java.util.List;
/*    */ import java.util.Set;
/*    */ import org.apache.commons.lang.StringUtils;
/*    */ import org.apache.log4j.Logger;
/*    */ import com.hnykl.bp.base.minidao.annotation.MiniDao;
/*    */ import com.hnykl.bp.base.minidao.util.MiniDaoUtil;
/*    */ import org.springframework.aop.framework.ProxyFactoryBean;
/*    */ import org.springframework.beans.BeansException;
/*    */ import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
/*    */ import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
/*    */ 
/*    */ public class MiniDaoBeanFactory
/*    */   implements BeanFactoryPostProcessor
/*    */ {
/* 25 */   private static final Logger logger = Logger.getLogger(MiniDaoBeanFactory.class);
/*    */   private List<String> packagesToScan;
/*    */ 
/*    */   public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)
/*    */     throws BeansException
/*    */   {
/* 32 */     logger.debug("................MiniDaoBeanFactory................ContextRefreshed...................");
/*    */     try
/*    */     {
/* 35 */       for (String pack : this.packagesToScan) {
/* 36 */         if (StringUtils.isNotEmpty(pack)) {
/* 37 */           Set<Class<?>> classSet = PackagesToScanUtil.getClasses(pack);
/* 38 */           for (Class miniDaoClass : classSet) {
/* 39 */             if (!miniDaoClass.isAnnotationPresent(MiniDao.class))
/*    */               continue;
/* 41 */             ProxyFactoryBean proxyFactoryBean = new ProxyFactoryBean();
/* 42 */             proxyFactoryBean.setBeanFactory(beanFactory);
/* 43 */             proxyFactoryBean.setInterfaces(new Class[] { miniDaoClass });
/* 44 */             proxyFactoryBean.setInterceptorNames(new String[] { "miniDaoHandler" });
/* 45 */             String beanName = MiniDaoUtil.getFirstSmall(miniDaoClass.getSimpleName());
/* 46 */             if (beanFactory.containsBean(beanName)) {
/*    */               continue;
/*    */             }
/* 49 */             logger.info("MiniDao Interface [/" + miniDaoClass.getName() + "/] onto Spring Bean '" + beanName + "'");
/* 50 */             beanFactory.registerSingleton(beanName, proxyFactoryBean.getObject());
/*    */           }
/*    */         }
/*    */       }
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/* 57 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ 
/*    */   public List<String> getPackagesToScan()
/*    */   {
/* 63 */     return this.packagesToScan;
/*    */   }
/*    */ 
/*    */   public void setPackagesToScan(List<String> packagesToScan) {
/* 67 */     this.packagesToScan = packagesToScan;
/*    */   }
/*    */ }

