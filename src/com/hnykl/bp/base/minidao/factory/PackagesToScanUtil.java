/*     */ package com.hnykl.bp.base.minidao.factory;
/*     */ 
/*     */ import java.io.File;
/*     */ import java.io.FileFilter;
/*     */ import java.io.IOException;
/*     */ import java.net.JarURLConnection;
/*     */ import java.net.URL;
/*     */ import java.net.URLDecoder;
/*     */ import java.util.Enumeration;
/*     */ import java.util.LinkedHashSet;
/*     */ import java.util.Set;
/*     */ import java.util.jar.JarEntry;
/*     */ import java.util.jar.JarFile;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ public class PackagesToScanUtil
/*     */ {
/*  28 */   private static final Logger logger = Logger.getLogger(PackagesToScanUtil.class);
/*     */   private static final String SUB_PACKAGE_SCREEN__SUFFIX = ".*";
/*     */ 
/*     */   public static Set<Class<?>> getClasses(String pack)
/*     */   {
/*  39 */     boolean recursive = false;
/*  40 */     if (pack.lastIndexOf(".*") != -1) {
/*  41 */       pack = pack.replace(".*", "");
/*  42 */       recursive = true;
/*     */     }
/*     */ 
/*  46 */     Set classes = new LinkedHashSet();
/*     */ 
/*  49 */     String packageName = pack;
/*  50 */     String packageDirName = packageName.replace('.', '/');
/*     */     try
/*     */     {
/*  54 */       Enumeration dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
/*     */ 
/*  56 */       while (dirs.hasMoreElements())
/*     */       {
/*  58 */         URL url = (URL)dirs.nextElement();
/*     */ 
/*  60 */         String protocol = url.getProtocol();
/*     */ 
/*  62 */         if ("file".equals(protocol)) {
/*  63 */           logger.debug("-------------- file类型的扫描 ----------------");
/*     */ 
/*  65 */           String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
/*     */ 
/*  67 */           findAndAddClassesInPackageByFile(packageName, filePath, 
/*  68 */             recursive, classes); 
					} else {
/*  69 */           if (!"jar".equals(protocol)) {
/*     */             continue;
/*     */           }
/*  72 */           logger
/*  73 */             .debug("------------------------ jar类型的扫描 ----------------------");
/*     */           try
/*     */           {
/*  77 */             JarFile jar = ((JarURLConnection)url.openConnection())
/*  78 */               .getJarFile();
/*     */ 
/*  80 */             Enumeration entries = jar.entries();
/*     */ 
/*  82 */             while (entries.hasMoreElements())
/*     */             {
/*  84 */               JarEntry entry = (JarEntry)entries.nextElement();
/*  85 */               String name = entry.getName();
/*     */ 
/*  87 */               if (name.charAt(0) == '/')
/*     */               {
/*  89 */                 name = name.substring(1);
/*     */               }
/*     */ 
/*  92 */               if (name.startsWith(packageDirName)) {
/*  93 */                 int idx = name.lastIndexOf('/');
/*     */ 
/*  95 */                 if (idx != -1)
/*     */                 {
/*  97 */                   packageName = name.substring(0, idx)
/*  98 */                     .replace('/', '.');
/*     */                 }
/*     */ 
/* 101 */                 if ((idx == -1) && (!recursive))
/*     */                   continue;
/* 103 */                 if ((!name.endsWith(".class")) || 
/* 104 */                   (entry.isDirectory()))
/*     */                   continue;
/* 106 */                 String className = name.substring(
/* 107 */                   packageName.length() + 1, name
/* 108 */                   .length() - 6);
/*     */                 try
/*     */                 {
/* 111 */                   classes.add(
/* 112 */                     Class.forName(packageName + '.' + 
/* 113 */                     className));
/*     */                 } catch (ClassNotFoundException e) {
/* 115 */                   logger
/* 116 */                     .error("添加用户自定义视图类错误 找不到此类的.class文件");
/* 117 */                   e.printStackTrace();
/*     */                 }
/*     */               }
/*     */             }
/*     */           }
/*     */           catch (IOException e)
/*     */           {
/* 124 */             logger.error("在扫描用户定义视图时从jar包获取文件出错");
/* 125 */             e.printStackTrace();
/*     */           }
/*     */         }
/*     */       }
/*     */     } catch (IOException e) {
/* 130 */       e.printStackTrace();
/*     */     }
/*     */ 
/* 133 */     return classes;
/*     */   }
/*     */ 
/*     */   private static void findAndAddClassesInPackageByFile(String packageName, String packagePath,final boolean recursive, Set<Class<?>> classes)
/*     */   {
/* 147 */     File dir = new File(packagePath);
/*     */ 
/* 149 */     if ((!dir.exists()) || (!dir.isDirectory()))
/*     */     {
/* 151 */       return;
/*     */     }
/*     */ 
/* 154 */     File[] dirfiles = dir.listFiles(new FileFilter()
/*     */     {
/*     */       public boolean accept(File file)
/*     */       {
/* 158 */         return (recursive && (file.isDirectory())) || 
/* 158 */           (file.getName().endsWith(".class"));
/*     */       }
/*     */     });
/* 162 */     for (File file : dirfiles)
/*     */     {
/* 164 */       if (file.isDirectory()) {
/* 165 */         findAndAddClassesInPackageByFile(packageName + "." + 
/* 166 */           file.getName(), file.getAbsolutePath(), recursive, 
/* 167 */           classes);
/*     */       }
/*     */       else {
/* 170 */         String className = file.getName().substring(0, 
/* 171 */           file.getName().length() - 6);
/*     */         try
/*     */         {
/* 177 */           String classUrl = packageName + '.' + className;
/*     */ 
/* 179 */           if (classUrl.startsWith(".")) {
/* 180 */             classUrl = classUrl.replaceFirst(".", "");
/*     */           }
/* 182 */           classes.add(Thread.currentThread().getContextClassLoader()
/* 183 */             .loadClass(classUrl));
/*     */         } catch (ClassNotFoundException e) {
/* 185 */           logger.error("添加用户自定义视图类错误 找不到此类的.class文件");
/* 186 */           e.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ }

