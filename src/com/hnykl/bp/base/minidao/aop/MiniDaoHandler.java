/*     */ package com.hnykl.bp.base.minidao.aop;
/*     */ 
/*     */ import java.lang.reflect.Method;
/*     */ import java.math.BigDecimal;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.regex.Matcher;
/*     */ import java.util.regex.Pattern;
/*     */ import ognl.Ognl;
/*     */ import ognl.OgnlException;
/*     */ import org.aopalliance.intercept.MethodInterceptor;
/*     */ import org.aopalliance.intercept.MethodInvocation;
/*     */ import org.apache.commons.lang.StringUtils;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.hibernate.engine.jdbc.internal.BasicFormatterImpl;
/*     */ import com.hnykl.bp.base.minidao.annotation.Arguments;
/*     */ import com.hnykl.bp.base.minidao.annotation.ResultType;
/*     */ import com.hnykl.bp.base.minidao.annotation.Sql;
/*     */ import com.hnykl.bp.base.minidao.hibernate.dao.IGenericBaseCommonDao;
/*     */ import com.hnykl.bp.base.minidao.spring.rowMapper.GenericRowMapper;
/*     */ import com.hnykl.bp.base.minidao.spring.rowMapper.MiniColumnMapRowMapper;
/*     */ import com.hnykl.bp.base.minidao.spring.rowMapper.MiniColumnOriginalMapRowMapper;
/*     */ import com.hnykl.bp.base.minidao.util.FreemarkerParseFactory;
/*     */ import com.hnykl.bp.base.minidao.util.MiniDaoUtil;
/*     */ import org.springframework.dao.EmptyResultDataAccessException;
/*     */ import org.springframework.jdbc.core.ColumnMapRowMapper;
/*     */ import org.springframework.jdbc.core.JdbcTemplate;
/*     */ import org.springframework.jdbc.core.RowMapper;
/*     */ import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
/*     */ import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
/*     */ 
/*     */ public class MiniDaoHandler
/*     */   implements MethodInterceptor
/*     */ {
/*  55 */   private static final Logger logger = Logger.getLogger(MiniDaoHandler.class);
/*     */   private JdbcTemplate jdbcTemplate;
/*     */   private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
/*     */   private IGenericBaseCommonDao miniDaoHiberCommonDao;
/*  60 */   private BasicFormatterImpl formatter = new BasicFormatterImpl();
/*     */ 
/*  62 */   private String UPPER_KEY = "upper";
/*  63 */   private String LOWER_KEY = "lower";
/*     */ 
/*  67 */   private String keyType = "origin";
/*  68 */   private boolean formatSql = false;
/*  69 */   private boolean showSql = false;
/*     */ 
/*     */   public Object invoke(MethodInvocation methodInvocation) throws Throwable {
/*  72 */     Method method = methodInvocation.getMethod();
/*  73 */     Object[] args = methodInvocation.getArguments();
/*     */ 
/*  75 */     Object returnObj = null;
/*     */ 
/*  77 */     String templateSql = null;
/*     */ 
/*  79 */     Map sqlParamsMap = new HashMap();
/*     */ 
/*  83 */     if (!MiniDaoUtil.isAbstract(method)) {
/*  84 */       return methodInvocation.proceed();
/*     */     }
/*     */ 
/*  87 */     Map rs = new HashMap();
/*  88 */     if (miniDaoHiber(rs, method, args)) {
/*  89 */       return rs.get("returnObj");
/*     */     }
/*     */ 
/*  92 */     templateSql = installDaoMetaData(method, sqlParamsMap, args);
/*     */ 
/*  95 */     String executeSql = parseSqlTemplate(method, templateSql, sqlParamsMap);
/*     */ 
/*  98 */     Map sqlMap = installPlaceholderSqlParam(executeSql, sqlParamsMap);
/*     */ 
/* 101 */     returnObj = getReturnMinidaoResult(this.jdbcTemplate, method, executeSql, sqlMap);
/*     */ 
/* 103 */     if (this.showSql) {
/* 104 */       logger.info("MiniDao-SQL:\n\n" + (this.formatSql ? this.formatter.format(executeSql) : executeSql) + "\n");
/*     */     }
/* 106 */     return returnObj;
/*     */   }
/*     */ 
/*     */   private boolean miniDaoHiber(Map rs, Method method, Object[] args)
/*     */   {
/* 124 */     if ("saveByHiber".equals(method.getName())) {
/* 125 */       this.miniDaoHiberCommonDao.save(args[0]);
/* 126 */       return true;
/*     */     }
/* 128 */     if ("getByIdHiber".equals(method.getName()))
/*     */     {
/* 130 */       Class clz = (Class)args[0];
/* 131 */       rs.put("returnObj", this.miniDaoHiberCommonDao.get(clz, args[1].toString()));
/* 132 */       return true;
/*     */     }
/* 134 */     if ("getByEntityHiber".equals(method.getName()))
/*     */     {
/* 136 */       rs.put("returnObj", this.miniDaoHiberCommonDao.get(args[0]));
/* 137 */       return true;
/*     */     }
/* 139 */     if ("updateByHiber".equals(method.getName())) {
/* 140 */       this.miniDaoHiberCommonDao.saveOrUpdate(args[0]);
/* 141 */       return true;
/*     */     }
/* 143 */     if ("deleteByHiber".equals(method.getName())) {
/* 144 */       this.miniDaoHiberCommonDao.delete(args[0]);
/* 145 */       return true;
/*     */     }
/* 147 */     if ("deleteByIdHiber".equals(method.getName())) {
/* 148 */       Class clz = (Class)args[0];
/* 149 */       this.miniDaoHiberCommonDao.deleteEntityById(clz, args[1].toString());
/* 150 */       return true;
/*     */     }
/* 152 */     if ("listByHiber".equals(method.getName())) {
/* 153 */       rs.put("returnObj", this.miniDaoHiberCommonDao.loadAll(args[0]));
/* 154 */       return true;
/*     */     }
/* 156 */     return false;
/*     */   }
/*     */ 
/*     */   private String parseSqlTemplate(Method method, String templateSql, Map<String, Object> sqlParamsMap)
/*     */   {
/* 169 */     String executeSql = null;
/*     */ 
/* 173 */     if (StringUtils.isNotEmpty(templateSql)) {
/* 174 */       executeSql = new FreemarkerParseFactory().parseTemplateContent(templateSql, sqlParamsMap);
/*     */     }
/*     */     else {
/* 177 */       String sqlTempletPath = "/" + method.getDeclaringClass().getName().replace(".", "/").replace("/dao/", "/sql/") + "_" + method.getName() + ".sql";
/* 178 */       logger.debug("MiniDao-SQL-Path:" + sqlTempletPath);
/* 179 */       executeSql = new FreemarkerParseFactory().parseTemplate(sqlTempletPath, sqlParamsMap);
/*     */     }
/* 181 */     return getSqlText(executeSql);
/*     */   }
/*     */ 
/*     */   private String getSqlText(String sql)
/*     */   {
/* 189 */     return sql.replaceAll("\\n", " ").replaceAll("\\t", " ")
/* 190 */       .replaceAll("\\s{1,}", " ").trim();
/*     */   }
/*     */ 
/*     */   private Map<String, Object> installPlaceholderSqlParam(String executeSql, Map sqlParamsMap)
/*     */     throws OgnlException
/*     */   {
/* 201 */     Map map = new HashMap();
/* 202 */     String regEx = ":[ tnx0Bfr]*[0-9a-z.A-Z]+";
/* 203 */     Pattern pat = Pattern.compile(regEx);
/* 204 */     Matcher m = pat.matcher(executeSql);
/* 205 */     while (m.find()) {
/* 206 */       logger.debug(" Match [" + m.group() + "] at positions " + m.start() + "-" + (m.end() - 1));
/* 207 */       String ognl_key = m.group().replace(":", "").trim();
/* 208 */       map.put(ognl_key, Ognl.getValue(ognl_key, sqlParamsMap));
/*     */     }
/* 210 */     return map;
/*     */   }
/*     */ 
/*     */   private Object getReturnMinidaoResult(JdbcTemplate jdbcTemplate, Method method, String executeSql, Map<String, Object> paramMap)
/*     */   {
/* 223 */     String methodName = method.getName();
/*     */ 
/* 225 */     if (checkActiveKey(methodName)) {
/* 226 */       if (paramMap != null) {
/* 227 */         return Integer.valueOf(this.namedParameterJdbcTemplate.update(executeSql, paramMap));
/*     */       }
/* 229 */       return Integer.valueOf(jdbcTemplate.update(executeSql));
/*     */     }
/* 231 */     if (checkBatchKey(methodName)) {
/* 232 */       return batchUpdate(jdbcTemplate, executeSql);
/*     */     }
/*     */ 
/* 235 */     Class returnType = method.getReturnType();
/* 236 */     if (returnType.isPrimitive()) {
/* 237 */       Number number = (Number)jdbcTemplate.queryForObject(executeSql, BigDecimal.class);
/* 238 */       if ("int".equals(returnType))
/* 239 */         return Integer.valueOf(number.intValue());
/* 240 */       if ("long".equals(returnType))
/* 241 */         return Long.valueOf(number.longValue());
/* 242 */       if ("double".equals(returnType))
/* 243 */         return Double.valueOf(number.doubleValue());
/*     */     } else {
/* 245 */       if (returnType.isAssignableFrom(List.class))
/*     */       {
/* 247 */         ResultType resultType = (ResultType)method.getAnnotation(ResultType.class);
/* 248 */         String[] values = (String[])null;
/* 249 */         if (resultType != null) {
/* 250 */           values = resultType.value();
/*     */         }
/* 252 */         if ((values == null) || (values.length == 0) || ("java.util.Map".equals(values[0]))) {
/* 253 */           if (paramMap != null) {
/* 254 */             return this.namedParameterJdbcTemplate.query(executeSql, paramMap, getColumnMapRowMapper());
/*     */           }
/* 256 */           return jdbcTemplate.query(executeSql, getColumnMapRowMapper());
/*     */         }
/*     */ 
/* 259 */         Class clazz = null;
/*     */         try {
/* 261 */           clazz = Class.forName(values[0]);
/*     */         } catch (Exception e) {
/* 263 */           e.printStackTrace();
/*     */         }
/* 265 */         if (paramMap != null) {
/* 266 */           return this.namedParameterJdbcTemplate.query(executeSql, paramMap, new GenericRowMapper(clazz));
/*     */         }
/* 268 */         return jdbcTemplate.query(executeSql, new GenericRowMapper(clazz));
/*     */       }
/*     */ 
/* 272 */       if (returnType.isAssignableFrom(Map.class))
/*     */       {
/* 274 */         if (paramMap != null) {
/* 275 */           return (Map)this.namedParameterJdbcTemplate.queryForObject(executeSql, paramMap, getColumnMapRowMapper());
/*     */         }
/* 277 */         return (Map)jdbcTemplate.queryForObject(executeSql, getColumnMapRowMapper());
/*     */       }
/* 279 */       if (returnType.isAssignableFrom(String.class))
/*     */         try
/*     */         {
/* 282 */           if (paramMap != null) {
/* 283 */             return this.namedParameterJdbcTemplate.queryForObject(executeSql, paramMap, String.class);
/*     */           }
/* 285 */           return jdbcTemplate.queryForObject(executeSql, String.class);
/*     */         }
/*     */         catch (EmptyResultDataAccessException e) {
/* 288 */           return null;
/*     */         }
/* 290 */       if (MiniDaoUtil.isWrapClass(returnType)) {
/*     */         try
/*     */         {
/* 293 */           if (paramMap != null) {
/* 294 */             return this.namedParameterJdbcTemplate.queryForObject(executeSql, paramMap, returnType);
/*     */           }
/* 296 */           return jdbcTemplate.queryForObject(executeSql, returnType);
/*     */         }
/*     */         catch (EmptyResultDataAccessException e) {
/* 299 */           return null;
/*     */         }
/*     */       }
/*     */ 
/* 303 */       RowMapper rm = ParameterizedBeanPropertyRowMapper.newInstance(returnType);
/*     */       try {
/* 305 */         if (paramMap != null) {
/* 306 */           return this.namedParameterJdbcTemplate.queryForObject(executeSql, paramMap, rm);
/*     */         }
/* 308 */         return jdbcTemplate.queryForObject(executeSql, rm);
/*     */       }
/*     */       catch (EmptyResultDataAccessException e) {
/* 311 */         return null;
/*     */       }
/*     */     }
/*     */ 
/* 315 */     return null;
/*     */   }
/*     */ 
/*     */   private int[] batchUpdate(JdbcTemplate jdbcTemplate, String executeSql)
/*     */   {
/* 325 */     String[] sqls = executeSql.split(";");
/* 326 */     if (sqls.length < 100) {
/* 327 */       return jdbcTemplate.batchUpdate(sqls);
/*     */     }
/* 329 */     int[] result = new int[sqls.length];
/* 330 */     List sqlList = new ArrayList();
/* 331 */     for (int i = 0; i < sqls.length; i++) {
/* 332 */       sqlList.add(sqls[i]);
/* 333 */       if (i % 100 == 0) {
/* 334 */         addResulArray(result, i + 1, jdbcTemplate.batchUpdate((String[])sqlList.toArray(new String[0])));
/* 335 */         sqlList.clear();
/*     */       }
/*     */     }
/* 338 */     addResulArray(result, sqls.length, jdbcTemplate.batchUpdate((String[])sqlList.toArray(new String[0])));
/* 339 */     return result;
/*     */   }
/*     */ 
/*     */   private void addResulArray(int[] result, int index, int[] arr)
/*     */   {
/* 349 */     int length = arr.length;
/* 350 */     for (int i = 0; i < length; i++)
/* 351 */       result[(index - length + i)] = arr[i];
/*     */   }
/*     */ 
/*     */   public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate()
/*     */   {
/* 358 */     return this.namedParameterJdbcTemplate;
/*     */   }
/*     */ 
/*     */   public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate)
/*     */   {
/* 365 */     this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
/*     */   }
/*     */ 
/*     */   private String installDaoMetaData(Method method, Map<String, Object> sqlParamsMap, Object[] args)
/*     */     throws Exception
/*     */   {
/* 380 */     String templateSql = null;
/*     */ 
/* 382 */     boolean arguments_flag = method.isAnnotationPresent(Arguments.class);
/* 383 */     if (arguments_flag)
/*     */     {
/* 385 */       Arguments arguments = (Arguments)method.getAnnotation(Arguments.class);
/* 386 */       logger.debug("@Arguments------------------------------------------" + Arrays.toString(arguments.value()));
/* 387 */       if (arguments.value().length > args.length)
/*     */       {
/* 389 */         throw new Exception("[注释标签]参数数目，不能大于[方法参数]参数数目");
/*     */       }
/*     */ 
/* 392 */       int args_num = 0;
/* 393 */       for (String v : arguments.value()) {
/* 394 */         sqlParamsMap.put(v, args[args_num]);
/* 395 */         args_num++;
/*     */       }
/*     */     }
/*     */     else {
/* 399 */       if (args.length > 1)
/* 400 */         throw new Exception("方法参数数目>=2，方法必须使用注释标签@Arguments");
/* 401 */       if (args.length == 1)
/*     */       {
/* 403 */         sqlParamsMap.put("dto", args[0]);
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 409 */     if (method.isAnnotationPresent(Sql.class)) {
/* 410 */       Sql sql = (Sql)method.getAnnotation(Sql.class);
/*     */ 
/* 412 */       if (StringUtils.isNotEmpty(sql.value())) {
/* 413 */         templateSql = sql.value();
/*     */       }
/* 415 */       logger.debug("@Sql------------------------------------------" + sql.value());
/*     */     }
/* 417 */     return templateSql;
/*     */   }
/*     */ 
/*     */   private static boolean checkActiveKey(String methodName)
/*     */   {
/* 428 */     String[] keys = "insert,add,create,update,modify,store,delete,remove".split(",");
/* 429 */     for (String s : keys) {
/* 430 */       if (methodName.startsWith(s))
/* 431 */         return true;
/*     */     }
/* 433 */     return false;
/*     */   }
/*     */ 
/*     */   private static boolean checkBatchKey(String methodName)
/*     */   {
/* 443 */     String[] keys = "batch".split(",");
/* 444 */     for (String s : keys) {
/* 445 */       if (methodName.startsWith(s))
/* 446 */         return true;
/*     */     }
/* 448 */     return false;
/*     */   }
/*     */ 
/*     */   private RowMapper<Map<String, Object>> getColumnMapRowMapper()
/*     */   {
/* 454 */     if (getKeyType().equalsIgnoreCase(this.LOWER_KEY))
/* 455 */       return new MiniColumnMapRowMapper();
/* 456 */     if (getKeyType().equalsIgnoreCase(this.UPPER_KEY)) {
/* 457 */       return new ColumnMapRowMapper();
/*     */     }
/* 459 */     return new MiniColumnOriginalMapRowMapper();
/*     */   }
/*     */ 
/*     */   public JdbcTemplate getJdbcTemplate()
/*     */   {
/* 464 */     return this.jdbcTemplate;
/*     */   }
/*     */ 
/*     */   public IGenericBaseCommonDao getMiniDaoHiberCommonDao() {
/* 468 */     return this.miniDaoHiberCommonDao;
/*     */   }
/*     */ 
/*     */   public void setMiniDaoHiberCommonDao(IGenericBaseCommonDao miniDaoHiberCommonDao) {
/* 472 */     this.miniDaoHiberCommonDao = miniDaoHiberCommonDao;
/*     */   }
/*     */ 
/*     */   public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
/* 476 */     this.jdbcTemplate = jdbcTemplate;
/*     */   }
/*     */ 
/*     */   public boolean isFormatSql() {
/* 480 */     return this.formatSql;
/*     */   }
/*     */ 
/*     */   public void setFormatSql(boolean formatSql) {
/* 484 */     this.formatSql = formatSql;
/*     */   }
/*     */ 
/*     */   public String getKeyType()
/*     */   {
/* 490 */     return this.keyType;
/*     */   }
/*     */ 
/*     */   public void setKeyType(String keyType)
/*     */   {
/* 496 */     this.keyType = keyType;
/*     */   }
/*     */ 
/*     */   public void setShowSql(boolean showSql) {
/* 500 */     this.showSql = showSql;
/*     */   }
/*     */ }

