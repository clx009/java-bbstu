/*    */ package com.hnykl.bp.base.minidao.util;
/*    */ 
/*    */ import freemarker.cache.TemplateLoader;
/*    */ import java.io.IOException;
/*    */ import java.io.Reader;
/*    */ import java.io.StringReader;
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class StringTemplateLoader
/*    */   implements TemplateLoader
/*    */ {
/*    */   private static final String DEFAULT_TEMPLATE_KEY = "_default_template_key";
/* 19 */   private Map templates = new HashMap();
/*    */ 
/*    */   public StringTemplateLoader(String defaultTemplate) {
/* 22 */     if ((defaultTemplate != null) && (!defaultTemplate.equals("")))
/* 23 */       this.templates.put("_default_template_key", defaultTemplate);
/*    */   }
/*    */ 
/*    */   public void AddTemplate(String name, String template)
/*    */   {
/* 28 */     if ((name == null) || (template == null) || (name.equals("")) || 
/* 29 */       (template.equals(""))) {
/* 30 */       return;
/*    */     }
/* 32 */     if (!this.templates.containsKey(name))
/* 33 */       this.templates.put(name, template);
/*    */   }
/*    */ 
/*    */   public void closeTemplateSource(Object templateSource)
/*    */     throws IOException
/*    */   {
/*    */   }
/*    */ 
/*    */   public Object findTemplateSource(String name) throws IOException
/*    */   {
/* 43 */     if ((name == null) || (name.equals(""))) {
/* 44 */       name = "_default_template_key";
/*    */     }
/* 46 */     return this.templates.get(name);
/*    */   }
/*    */ 
/*    */   public long getLastModified(Object templateSource) {
/* 50 */     return 0L;
/*    */   }
/*    */ 
/*    */   public Reader getReader(Object templateSource, String encoding) throws IOException
/*    */   {
/* 55 */     return new StringReader((String)templateSource);
/*    */   }
/*    */ }

