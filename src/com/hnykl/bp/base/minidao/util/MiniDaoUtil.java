/*     */ package com.hnykl.bp.base.minidao.util;
/*     */ 
/*     */ import java.io.BufferedReader;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.InputStreamReader;
/*     */ import java.lang.reflect.Field;
/*     */ import java.lang.reflect.InvocationTargetException;
/*     */ import java.lang.reflect.Method;
/*     */ import java.lang.reflect.Modifier;
/*     */ import java.util.Iterator;
/*     */ import java.util.Map;
/*     */ import java.util.Set;
/*     */ import org.apache.commons.beanutils.BeanUtils;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ public class MiniDaoUtil
/*     */ {
/*  26 */   private static final Logger logger = Logger.getLogger(MiniDaoUtil.class);
/*     */ 
/*     */   public static void main(String[] args) throws Exception {
/*  29 */     logger.debug(Boolean.valueOf(isWrapClass(Long.class)));
/*  30 */     logger.debug(Boolean.valueOf(isWrapClass(Integer.class)));
/*  31 */     logger.debug(Boolean.valueOf(isWrapClass(String.class)));
/*     */   }
/*     */ 
/*     */   public static boolean isWrapClass(Class clz)
/*     */   {
/*     */     try
/*     */     {
/*  42 */       return ((Class)clz.getField("TYPE").get(null)).isPrimitive(); } catch (Exception e) {
/*     */     }
/*  44 */     return false;
/*     */   }
/*     */ 
/*     */   public static boolean isAbstract(Method method)
/*     */   {
/*  55 */     int mod = method.getModifiers();
/*  56 */     return Modifier.isAbstract(mod);
/*     */   }
/*     */ 
/*     */   public static String getMethodSqlLogicJar(String sqlurl)
/*     */   {
/*  65 */     StringBuffer sb = new StringBuffer();
/*     */ 
/*  67 */     InputStream is = MiniDaoUtil.class.getResourceAsStream(sqlurl);
/*  68 */     BufferedReader br = new BufferedReader(new InputStreamReader(is));
/*  69 */     String s = "";
/*     */     try {
/*  71 */       while ((s = br.readLine()) != null)
/*  72 */         sb.append(s + " ");
/*     */     } catch (IOException e) {
/*  74 */       e.printStackTrace();
/*     */     }
/*  76 */     return sb.toString();
/*     */   }
/*     */ 
/*     */   public static String getFirstSmall(String name)
/*     */   {
/*  86 */     name = name.trim();
/*  87 */     if (name.length() >= 2) {
/*  88 */       return name.substring(0, 1).toLowerCase() + name.substring(1);
/*     */     }
/*  90 */     return name.toLowerCase();
/*     */   }
/*     */ 
/*     */   public static void populate(Object bean, Map properties)
/*     */     throws IllegalAccessException, InvocationTargetException
/*     */   {
/* 105 */     if ((bean == null) || (properties == null)) {
/* 106 */       return;
/*     */     }
/*     */ 
/* 109 */     Iterator names = properties.keySet().iterator();
/* 110 */     while (names.hasNext())
/*     */     {
/* 113 */       String name = (String)names.next();
/* 114 */       if (name == null) {
/*     */         continue;
/*     */       }
/* 117 */       Object value = properties.get(name);
/* 118 */       String camelName = CamelCaseUtils.toCamelCase(name);
/*     */ 
/* 121 */       BeanUtils.setProperty(bean, camelName, value);
/*     */     }
/*     */   }
/*     */ }

