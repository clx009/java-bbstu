/*    */ package com.hnykl.bp.base.minidao.util;
/*    */ 
/*    */ import java.io.PrintStream;
/*    */ 
/*    */ public class CamelCaseUtils
/*    */ {
/*    */   private static final char SEPARATOR = '_';
/*    */ 
/*    */   public static String toUnderlineName(String s)
/*    */   {
/*  8 */     if (s == null) {
/*  9 */       return null;
/*    */     }
/*    */ 
/* 12 */     StringBuilder sb = new StringBuilder();
/* 13 */     boolean upperCase = false;
/* 14 */     for (int i = 0; i < s.length(); i++) {
/* 15 */       char c = s.charAt(i);
/*    */ 
/* 17 */       boolean nextUpperCase = true;
/*    */ 
/* 19 */       if (i < s.length() - 1) {
/* 20 */         nextUpperCase = Character.isUpperCase(s.charAt(i + 1));
/*    */       }
/*    */ 
/* 23 */       if ((i >= 0) && (Character.isUpperCase(c))) {
/* 24 */         if (((!upperCase) || (!nextUpperCase)) && 
/* 25 */           (i > 0)) sb.append('_');
/*    */ 
/* 27 */         upperCase = true;
/*    */       } else {
/* 29 */         upperCase = false;
/*    */       }
/*    */ 
/* 32 */       sb.append(Character.toLowerCase(c));
/*    */     }
/*    */ 
/* 35 */     return sb.toString();
/*    */   }
/*    */ 
/*    */   public static String toCamelCase(String s) {
/* 39 */     if (s == null) {
/* 40 */       return null;
/*    */     }
/*    */ 
/* 43 */     s = s.toLowerCase();
/*    */ 
/* 45 */     StringBuilder sb = new StringBuilder(s.length());
/* 46 */     boolean upperCase = false;
/* 47 */     for (int i = 0; i < s.length(); i++) {
/* 48 */       char c = s.charAt(i);
/*    */ 
/* 50 */       if (c == '_') {
/* 51 */         upperCase = true;
/* 52 */       } else if (upperCase) {
/* 53 */         sb.append(Character.toUpperCase(c));
/* 54 */         upperCase = false;
/*    */       } else {
/* 56 */         sb.append(c);
/*    */       }
/*    */     }
/*    */ 
/* 60 */     return sb.toString();
/*    */   }
/*    */ 
/*    */   public static String toCapitalizeCamelCase(String s) {
/* 64 */     if (s == null) {
/* 65 */       return null;
/*    */     }
/* 67 */     s = toCamelCase(s);
/* 68 */     return s.substring(0, 1).toUpperCase() + s.substring(1);
/*    */   }
/*    */ 
/*    */   public static void main(String[] args) {
/* 72 */     System.out.println(toUnderlineName("ISOCertifiedStaff"));
/* 73 */     System.out.println(toUnderlineName("CertifiedStaff"));
/* 74 */     System.out.println(toUnderlineName("UserID"));
/* 75 */     System.out.println(toCamelCase("iso_certified_staff"));
/* 76 */     System.out.println(toCamelCase("certified_staff"));
/* 77 */     System.out.println(toCamelCase("user_id"));
/*    */   }
/*    */ }

