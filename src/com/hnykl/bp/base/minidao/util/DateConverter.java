/*    */ package com.hnykl.bp.base.minidao.util;
/*    */ 
/*    */ import java.text.ParseException;
/*    */ import org.apache.commons.beanutils.ConversionException;
/*    */ import org.apache.commons.beanutils.Converter;
/*    */ import org.apache.commons.lang.time.DateUtils;
/*    */ 
/*    */ public class DateConverter
/*    */   implements Converter
/*    */ {
/* 14 */   String[] parsePatterns = { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss.S" };
/*    */ 
/*    */   public Object convert(Class type, Object value)
/*    */   {
/* 18 */     if (value == null)
/* 19 */       return null;
/* 20 */     if ((value instanceof String)) {
/* 21 */       String tmp = (String)value;
/* 22 */       if (tmp.trim().length() == 0)
/* 23 */         return null;
/*    */       try
/*    */       {
/* 26 */         return DateUtils.parseDate(tmp, this.parsePatterns);
/*    */       } catch (ParseException e) {
/* 28 */         e.printStackTrace();
/*    */       }
/*    */     }
/*    */     else {
/* 32 */       throw new ConversionException("not String");
/*    */     }
/* 34 */     return value;
/*    */   }
/*    */ }

