/*    */ package com.hnykl.bp.base.minidao.util;
/*    */ 
/*    */ import org.apache.commons.beanutils.Converter;
/*    */ 
/*    */ public class IntegerConverter
/*    */   implements Converter
/*    */ {
/*    */   public Object convert(Class type, Object value)
/*    */   {
/* 12 */     if (value == null)
/* 13 */       return null;
/* 14 */     if ((value instanceof String)) {
/* 15 */       String tmp = (String)value;
/* 16 */       if (tmp.trim().length() == 0) {
/* 17 */         return null;
/*    */       }
/* 19 */       return new Integer(tmp);
/*    */     }
/*    */ 
/* 22 */     return null;
/*    */   }
/*    */ }

