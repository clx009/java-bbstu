/*    */ package com.hnykl.bp.base.minidao.util;
/*    */ 
/*    */ import java.math.BigDecimal;
/*    */ import org.apache.commons.beanutils.Converter;
/*    */ 
/*    */ public class BigDecimalConverter
/*    */   implements Converter
/*    */ {
/*    */   public Object convert(Class type, Object value)
/*    */   {
/* 14 */     if (value == null)
/* 15 */       return null;
/* 16 */     if ((value instanceof String)) {
/* 17 */       String tmp = (String)value;
/* 18 */       if (tmp.trim().length() == 0) {
/* 19 */         return null;
/*    */       }
/* 21 */       return new BigDecimal(tmp);
/*    */     }
/*    */ 
/* 24 */     return null;
/*    */   }
/*    */ }

