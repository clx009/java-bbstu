/*     */ package com.hnykl.bp.base.minidao.util;
/*     */ 
/*     */ import freemarker.template.Configuration;
/*     */ import freemarker.template.Template;
/*     */ import java.io.FileWriter;
/*     */ import java.io.StringWriter;
/*     */ import java.util.Map;
/*     */ 
/*     */ public class FreemarkerParseFactory
/*     */ {
/*  18 */   private static final Configuration _tplConfig = new Configuration();
/*     */ 
/*     */   public FreemarkerParseFactory() {
/*  21 */     _tplConfig.setClassForTemplateLoading(getClass(), "/");
/*  22 */     _tplConfig.setNumberFormat("0.#####################");
/*     */   }
/*     */ 
/*     */   public String parseTemplate(String tplName, String encoding, Map<String, Object> paras)
/*     */   {
/*     */     try
/*     */     {
/*  34 */       StringWriter swriter = new StringWriter();
/*  35 */       Template mytpl = _tplConfig.getTemplate(tplName, encoding);
/*  36 */       mytpl.process(paras, swriter);
/*  37 */       return swriter.toString();
/*     */     }
/*     */     catch (Exception e) {
/*  40 */       e.printStackTrace();
/*  41 */     }
				return "";
/*     */   }
/*     */ 
/*     */   public void parseTemplate(String tplName, String encoding, Map<String, Object> paras, FileWriter swriter)
/*     */   {
/*     */     try
/*     */     {
/*  55 */       Template mytpl = _tplConfig.getTemplate(tplName);
/*  56 */       mytpl.process(paras, swriter);
/*     */     } catch (Exception e) {
/*  58 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */ 
/*     */   public String parseTemplate(String tplName, Map<String, Object> paras)
/*     */   {
/*  70 */     return parseTemplate(tplName, "utf-8", paras);
/*     */   }
/*     */ 
/*     */   public String parseTemplateContent(String tplContent, Map<String, Object> paras, String encoding)
/*     */   {
/*  83 */     Configuration cfg = new Configuration();
/*  84 */     StringWriter writer = new StringWriter();
/*  85 */     cfg.setTemplateLoader(new StringTemplateLoader(tplContent));
/*  86 */     encoding = encoding == null ? "UTF-8" : encoding;
/*  87 */     cfg.setDefaultEncoding(encoding);
/*     */     try
/*     */     {
/*  91 */       Template template = cfg.getTemplate("");
/*  92 */       template.process(paras, writer);
/*     */     } catch (Exception e) {
/*  94 */       e.printStackTrace();
/*     */     }
/*  96 */     return writer.toString();
/*     */   }
/*     */ 
/*     */   public String parseTemplateContent(String tplContent, Map<String, Object> paras)
/*     */   {
/* 108 */     Configuration cfg = new Configuration();
/* 109 */     StringWriter writer = new StringWriter();
/* 110 */     cfg.setTemplateLoader(new StringTemplateLoader(tplContent));
/* 111 */     cfg.setDefaultEncoding("UTF-8");
/*     */     try
/*     */     {
/* 115 */       Template template = cfg.getTemplate("");
/* 116 */       template.process(paras, writer);
/*     */     } catch (Exception e) {
/* 118 */       e.printStackTrace();
/*     */     }
/* 120 */     return writer.toString();
/*     */   }
/*     */ }

