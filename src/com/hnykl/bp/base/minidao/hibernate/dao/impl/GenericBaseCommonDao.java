/*     */ package com.hnykl.bp.base.minidao.hibernate.dao.impl;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.hibernate.Criteria;
/*     */ import org.hibernate.Session;
/*     */ import org.hibernate.SessionFactory;
/*     */ import org.hibernate.criterion.Criterion;
/*     */ import org.hibernate.criterion.Example;
/*     */ import org.hibernate.criterion.Order;
/*     */ import org.hibernate.criterion.Restrictions;
/*     */ import com.hnykl.bp.base.minidao.hibernate.dao.IGenericBaseCommonDao;
/*     */ import org.springframework.stereotype.Component;
/*     */ import org.springframework.transaction.annotation.Transactional;
/*     */ import org.springframework.util.Assert;
/*     */ 
/*     */ @Component
/*     */ @Transactional
/*     */ public class GenericBaseCommonDao<T, PK extends Serializable>
/*     */   implements IGenericBaseCommonDao
/*     */ {
/*  36 */   private static final Logger logger = Logger.getLogger(GenericBaseCommonDao.class);
/*     */   private SessionFactory sessionFactory;
/*     */ 
/*     */   public SessionFactory getSessionFactory()
/*     */   {
/*  43 */     return this.sessionFactory;
/*     */   }
/*     */ 
/*     */   public void setSessionFactory(SessionFactory sessionFactory) {
/*  47 */     this.sessionFactory = sessionFactory;
/*     */   }
/*     */ 
/*     */   public Session getSession()
/*     */   {
/*     */     try
/*     */     {
/*  55 */       return this.sessionFactory.getCurrentSession(); } catch (Exception e) {
/*     */     }
/*  57 */     return this.sessionFactory.openSession();
/*     */   }
/*     */ 
/*     */   public <T> void save(T entity)
/*     */   {
/*     */     try
/*     */     {
/*  68 */       getSession().save(entity);
/*  69 */       getSession().flush();
/*  70 */       if (logger.isDebugEnabled())
/*  71 */         logger.debug("保存实体成功," + entity.getClass().getName());
/*     */     }
/*     */     catch (RuntimeException e) {
/*  74 */       logger.error("保存实体异常", e);
/*  75 */       throw e;
/*     */     }
/*     */   }
/*     */ 
/*     */   public <T> void saveOrUpdate(T entity)
/*     */   {
/*     */     try
/*     */     {
/*  90 */       getSession().saveOrUpdate(entity);
/*  91 */       getSession().flush();
/*  92 */       if (logger.isDebugEnabled())
/*  93 */         logger.debug("添加或更新成功," + entity.getClass().getName());
/*     */     }
/*     */     catch (RuntimeException e) {
/*  96 */       logger.error("添加或更新异常", e);
/*  97 */       throw e;
/*     */     }
/*     */   }
/*     */ 
/*     */   public <T> void delete(T entity)
/*     */   {
/*     */     try
/*     */     {
/* 106 */       getSession().delete(entity);
/* 107 */       getSession().flush();
/* 108 */       if (logger.isDebugEnabled())
/* 109 */         logger.debug("删除成功," + entity.getClass().getName());
/*     */     }
/*     */     catch (RuntimeException e) {
/* 112 */       logger.error("删除异常", e);
/* 113 */       throw e;
/*     */     }
/*     */   }
/*     */ 
/*     */   public <T> T findUniqueByProperty(Class<T> entityClass, String propertyName, Object value)
/*     */   {
/* 126 */     Assert.hasText(propertyName);
/* 127 */     return (T)createCriteria(entityClass, new Criterion[] { Restrictions.eq(propertyName, value) }).uniqueResult();
/*     */   }
/*     */ 
/*     */   private <T> Criteria createCriteria(Class<T> entityClass, boolean isAsc, Criterion[] criterions)
/*     */   {
/* 141 */     Criteria criteria = createCriteria(entityClass, criterions);
/* 142 */     if (isAsc)
/* 143 */       criteria.addOrder(Order.asc("asc"));
/*     */     else {
/* 145 */       criteria.addOrder(Order.desc("desc"));
/*     */     }
/* 147 */     return criteria;
/*     */   }
/*     */ 
/*     */   private <T> Criteria createCriteria(Class<T> entityClass, Criterion[] criterions)
/*     */   {
/* 159 */     Criteria criteria = getSession().createCriteria(entityClass);
/* 160 */     for (Criterion c : criterions) {
/* 161 */       criteria.add(c);
/*     */     }
/* 163 */     return criteria;
/*     */   }
/*     */ 
/*     */   public <T> List<T> findByProperty(Class<T> entityClass, String propertyName, Object value)
/*     */   {
/* 169 */     Assert.hasText(propertyName);
/* 170 */     return createCriteria(entityClass, new Criterion[] { Restrictions.eq(propertyName, value) }).list();
/*     */   }
/*     */ 
/*     */   public <T> T get(Class<T> entityClass, Serializable id)
/*     */   {
/* 178 */     return (T)getSession().get(entityClass, id);
/*     */   }
/*     */ 
/*     */   public <T> T get(T entitie)
/*     */   {
/* 183 */     Criteria executableCriteria = getSession().createCriteria(entitie.getClass());
/* 184 */     executableCriteria.add(Example.create(entitie));
/* 185 */     if (executableCriteria.list().size() == 0) {
/* 186 */       return null;
/*     */     }
/* 188 */     return (T)executableCriteria.list().get(0);
/*     */   }
/*     */ 
/*     */   public <T> List<T> loadAll(T entitie)
/*     */   {
/* 193 */     Criteria executableCriteria = getSession().createCriteria(entitie.getClass());
/* 194 */     executableCriteria.add(Example.create(entitie));
/* 195 */     return executableCriteria.list();
/*     */   }
/*     */ 
/*     */   public <T> void deleteEntityById(Class entityName, Serializable id)
/*     */   {
/* 205 */     delete(get(entityName, id));
/* 206 */     getSession().flush();
/*     */   }
/*     */ }

