/*    */ package com.hnykl.bp.base.minidao.hibernate;
/*    */ 
/*    */ import javax.persistence.GeneratedValue;
/*    */ import javax.persistence.Id;
/*    */ import javax.persistence.MappedSuperclass;
/*    */ import org.hibernate.annotations.GenericGenerator;
/*    */ 
/*    */ @MappedSuperclass
/*    */ public abstract class IdEntity
/*    */ {
/*    */   private String id;
/*    */ 
/*    */   @Id
/*    */   @GeneratedValue(generator="hibernate-uuid")
/*    */   @GenericGenerator(name="hibernate-uuid", strategy="uuid")
/*    */   public String getId()
/*    */   {
/* 16 */     return this.id;
/*    */   }
/*    */ 
/*    */   public void setId(String id) {
/* 20 */     this.id = id;
/*    */   }
/*    */ }

