package com.hnykl.bp.base.exception;
/**
 * 定义项目用到的异常异常常量字符串
 * @author Administrator
 *
 */
public class IllegalArgumentExceptionDefine {
    /**
     * 参数为null
     */
    public static final String ARGUMENT_IS_NULL = "IAE_ARGUMENT_IS_NULL";

    /**
     * 参数为空
     */
    public static final String ARGUMENT_IS_EMPTY = "IAE_ARGUMENT_IS_EMPTY";

    /**
     * 参数为'\0'(针对char类型的参数)
     */
    public static final String CHAR_TYPE_ARGUMENT_IS_ZERO = "IAE_CHAR_TYPE_ARGUMENT_IS_ZERO";

    /**
     * 参数是不为指定接口的实现
     */
    public static final String ARGUMENT_IS_NOT_INSTANCE = "IAE_ARGUMENT_IS_NOT_INSTANCE";

    /**
     * 参数不为指定类或其子类
     */
    public static final String ARGUMENT_IS_NOT_SUB_CLASS = "IAE_ARGUMENT_IS_NOT_SUB_CLASS";

    /**
     * 参数不为指定类或其子接口
     */
    public static final String ARGUMENT_IS_NOT_SUB_INTERFACE = "IAE_ARGUMENT_IS_NOT_SUB_INTERFACE";

    /**
     * 参数数组的元素个数小于某个值
     */
    public static final String ARRAY_TYPE_ARGUMENT_LENGTH_IS_LOWER = "IAE_ARRAY_TYPE_ARGUMENT_LENGTH_IS_LOWER";

    /**
     * 参数不相等
     */
    public static final String ARGUMENT_DO_NOT_EQUAL = "IAE_ARGUMENT_DO_NOT_EQUAL";

    /**
     * 参数小于某个值
     */
    public static final String ARGUMENT_IS_LOWER = "IAE_ARGUMENT_IS_LOWER";

    /**
     * 参数大于某个值
     */
    public static final String ARGUMENT_IS_UPPER = "IAE_ARGUMENT_IS_UPPER";

    /**
     * 参数非法(当碰到的“参数非法”不适合上述要求 并且 新增新的“非法类型”比较困难时，使用该id)
     */
    public static final String ARGUMENT_IS_ILLEGAL = "IAE_ARGUMENT_IS_ILLEGAL";
}
