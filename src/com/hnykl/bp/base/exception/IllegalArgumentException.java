package com.hnykl.bp.base.exception;
public class IllegalArgumentException extends CctirRuntimeException {

    /**
     * (序列化时为了保持版本的兼容性，即在版本升级时反序列化仍保持对象的唯一性)
     */
    private static final long serialVersionUID = 5497611344588924028L;

    /**
     * 构造无信息的异常。随后可以利用Throwable.initCause填充异常信息。
     */
    public IllegalArgumentException() {
    }

    /**
     * 利用提供的异常信息构造异常
     * @param msg 异常的信息
     */
    public IllegalArgumentException(String msg) {
        super(msg);
    }

    /**
     * 根据异常id,扩展异常信息构造无效参数异常
     * @param id 异常id
     * @param extmsg 扩展异常信息，该信息将会纪录到系统日志中
     */
    public IllegalArgumentException(String id, String extmsg) {
        super(extmsg);
    }

}