package com.hnykl.bp.base.exception;

import java.util.HashMap;

public interface ITMSException {
	public String getErrorCode();
	
	public HashMap getParameters();
	
	public Exception getErrorRoot();
	
	public String getErrorName();
	
	public String getErrorCause();
	
	public String getErrorSolution();
	
	public String getMessage();	
}
