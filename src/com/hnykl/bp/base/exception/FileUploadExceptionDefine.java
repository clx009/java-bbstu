package com.hnykl.bp.base.exception;

public class FileUploadExceptionDefine {
	/**
	 * 文件上传读写错误
	 */
	public static final String FILEUPLOAD_IO_EXCEPTION="FU_IO_EXCEPTION";
	/**
	 * 文件上传找不到文件
	 */
	public static final String FILEUPLOAD_FILE_EXCEPTION="FU_FILE_EXCEPTION";
}
