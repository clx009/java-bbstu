package com.hnykl.bp.base.exception;

import org.apache.commons.lang.exception.NestableRuntimeException;
import org.apache.log4j.Logger;

/**
 * 公共运行时异常类，公司所有运行时异常类都必须继承它。
 * 实现了嵌套异常处理、Log4j日志记录
 * @author powerquanc
 *  2013-6-6
 */
public class CctirRuntimeException extends NestableRuntimeException implements LogableException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2617526923391124023L;

    /**
     * 记录异常是否已经记录日志,每个异常在被重新封装前只记一次日志
     */
    protected boolean isLoged = false;

    /**
     * 构造无信息的异常。随后可以利用Throwable.initCause填充异常信息。
     */
    public CctirRuntimeException() {
    }

    /**
     * 利用提供的异常信息构造异常
     * @param msg 异常的信息
     */
    public CctirRuntimeException(String msg) {
        super(msg);
    }

    /**
     * 利用已有异常的信息构造异常
     * @param e 已有异常
     */
    public CctirRuntimeException(Throwable e) {
        super(e);
        if (e instanceof LogableException) {
            setIsLoged(((LogableException) e).getIsLoged());
        }
    }

    /**
     * 利用已有异常构建新异常,并加入自己的异常信息
     * @param msg 异常信息
     * @param e 已有异常
     */
    public CctirRuntimeException(String msg, Throwable e) {
        super(msg, e);
        if (e instanceof LogableException) {
            setIsLoged(((LogableException) e).getIsLoged());
        }
    }


    /**
     * 取得异常是否已经记录的标记
     * @return 异常是否已经记录
     */
    public boolean getIsLoged() {
        return isLoged;
    }

    /**
     * 设置异常是否已经记录的标记
     * @param loged 日志记录标记
     */
    public void setIsLoged(boolean loged) {
        this.isLoged = loged;
    }

    /**
     * 输出一个异常到日志中
     * @param logger log4j的Logger
     */
    public void log(Logger logger) {
        //异常只记录一次
        if (!this.isLoged) {
            //在debug模式下打印出异常的堆栈
            if (logger.isDebugEnabled()) {
                logger.debug(this.getMessage(), this);
            }
            //警告只是指出基本异常信息
            if (this.getMessage() != null)
            	logger.warn(this.getMessage());
            //设置异常已经记录的标志
            this.setIsLoged(true);
        }
    }

   
    public void debug(Logger logger) {
        if (!this.isLoged) {
            if (logger.isDebugEnabled()) {
                logger.debug(this.getMessage(), this);
            }
            this.setIsLoged(true);
        }
    }

  
    public void error(Logger logger) {
        if (!this.isLoged) {
            if (logger.isDebugEnabled()) {
                logger.error(this.getMessage(), this);
            }
            this.setIsLoged(true);
        }
    }

    
    public void warn(Logger logger) {
        if (!this.isLoged) {
            if (logger.isDebugEnabled()) {
                logger.warn(this.getMessage(), this);
            }
            this.setIsLoged(true);
        }
    }

}