package com.hnykl.bp.base.exception;
public class FileUploadException extends CctirRuntimeException {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3213389201272319019L;

    /**
     * 构造无信息的异常。随后可以利用Throwable.initCause填充异常信息。
     */
    public FileUploadException() {
    }

    /**
     * 利用提供的异常信息构造异常
     *
     * @param s 异常的信息
     */
    public FileUploadException(String s) {
        super(s);
    }

    /**
     * 利用已有异常的信息构造异常
     *
     * @param e 已有异常
     */
    public FileUploadException(Exception e) {
        super(e);
    }

    /**
     * 使用已有异常和错误信息构造异常
     *
     * @param msg 异常信息
     * @param e   异常
     */
    public FileUploadException(String msg, Exception e) {
        super(msg, e);
    }
}