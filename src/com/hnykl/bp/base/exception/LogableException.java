package com.hnykl.bp.base.exception;

import org.apache.log4j.Logger;

/**
 * 可记录的异常类
 * @author powerquanc
 *  2013-6-6
 */
public interface LogableException {

    boolean getIsLoged();

    void setIsLoged(boolean loged);

    void log(Logger logger);

    void debug(Logger logger);

    void warn(Logger logger);

    void error(Logger logger);

}
